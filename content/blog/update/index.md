---
title: update
date: "2023-12-24T22:40:33.169Z"
description: update
---

I don't know how many people actually read this blog, so any you who do AI and Dragons is moving to wordpress. Find the new site [here](https://patrickmcdonald122.wordpress.com/). Also due to SRD concerns the Märchenweltgrenze chronicles has been reworked to include new original races, monsters, magic items and shortly new character options.