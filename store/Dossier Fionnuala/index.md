---
title: Dossier Fionnuala and her family
date: "2023-11-26T22:40:32.169Z"
description: For 5 players of level 5
---

# Fionnuala
![king Fredrick](./Fionnuala.jpg)

1. **Title and Role:**
   - **Title:** Fionnuala, Fey Queen of Loch Slanach
   - **Role:** Ruler of the feudal kingdom, master manipulator, and enchantress.

2. **Physical Appearance:**
   - **Apparent Age:** Appears to be in her early thirties.
   - **Physical Features:**
     - Long, flowing golden hair that seems to catch and reflect light.
     - Mesmerizing emerald-green eyes that reveal both warmth and cunning.
     - Slender and regal stature, exuding an otherworldly beauty.
     - Attire adorned with illusions and enchantments, reflecting her ever-changing nature.

3. **Personality Traits:**
   - **Cunning Manipulator:** Fionnuala thrives on political intrigue and enjoys weaving webs of deception. She is a master manipulator, using her charm and intellect to guide the actions of her fey subjects.
   - **Enigmatic Charisma:** Possesses an enigmatic charisma that draws both allies and adversaries. Her presence is captivating, and her words hold a mesmerizing allure.
   - **Feywild Ambition:** Fionnuala harbors ambitions to overthrow the Seelie and Unseelie courts, seeking to establish her own realm as the supreme power in the Feywild.
   - **Motherly Deception:** Balances a facade of motherly affection with a penchant for manipulation. She uses her family as pawns in her grand schemes, all while maintaining an air of maternal care.

4. **Realm and Kingdom:**
   - **Loch Slanach:** Fionnuala rules over the mist-shrouded kingdom of Loch Slanach, a feudal realm with shifting borders and illusionary landscapes. The kingdom is characterized by political intrigue, deception, and the constant pursuit of power.

5. **Family Dynamics:**
   - **Motherly Favoritism:** Fionnuala favors her biological daughters, especially Aisling, due to their alignment with her own traits and ambitions. She uses her family as both pawns and instruments in her quest for power.
   - **Encourages Rivalry:** Actively encourages competition and rivalry among her daughters, fostering an environment where each must prove their worth to gain her favor.
   - **Strategic Alliances:** Views her daughters and stepdaughters as potential tools for forming strategic alliances with other fey courts. Uses familial connections to further her political agenda.

6. **Goals and Motivations:**
   - **Overthrowing Fey Courts:** Fionnuala's ultimate goal is to overthrow both the Seelie and Unseelie courts, establishing Loch Slanach as the dominant power in the Feywild.
   - **Political Machinations:** Engages in intricate political machinations, manipulating the nobility of Loch Slanach to achieve her goals. Encourages alliances, rivalries, and betrayals among her subjects.
   - **Feywild Ascendancy:** Aspires to ascend to a position of supreme power within the Feywild, reshaping it in her image and instilling her own vision of beauty and order.

7. **Feywild Artifacts and Powers:**
   - **Crown of Enchantment:** Wears a mystical crown that enhances her enchantment abilities. The crown is adorned with illusions that change based on her emotions and intentions.
   - **Mirror of Whispers:** Possesses a magical mirror that reveals the secrets and desires of those who gaze into it. The mirror aids her in uncovering hidden plots and manipulating her subjects.

8. **Interactions with Player Characters:**
   - **Contractual Entanglements:** Fionnuala ensnares the player characters in fey contracts, using their skills and ambitions to further her own plans.
   - **Intriguing Offers:** Presents the characters with tempting offers and rewards, weaving illusions of wealth, power, and knowledge to entice them into her service.
   - **Subtle Manipulations:** Acts as both a benefactor and a manipulator, guiding the characters through a series of quests that seem benevolent but serve her overarching goals.

9. **Themes and Aesthetics:**
   - **Shakespearean Glamour:** Embraces the elaborate and fantastical costuming of Shakespearean England, incorporating fey glamour into her attire and surroundings.
   - **Moonlit Majesty:** Radiates an aura of moonlit majesty, with her presence evoking the ethereal beauty of moonlit nights within the Feywild.
   - **Illusionary Opulence:** Surrounds herself with illusionary opulence, creating grand feasts and gatherings that are both breathtaking and deceptive.

10. **Quirks and Trademarks:**
    - **Feywild Menagerie:** Keeps a menagerie of enchanted creatures within Castle Fionnuala, each serving a specific purpose in her schemes.
    - **Cryptic Verses:** Speaks in cryptic verses and riddles, revealing information in a manner that keeps her true intentions veiled.
    - **Veil of Tears:** When angered or disappointed, a veil of shimmering tears seems to cascade from her eyes, adding a surreal and emotional element to her presence.


**Roleplay Guide: Fionnuala, Fey Queen of Loch Slanach**

**PreReveal**

1. **Warm Greetings:**
   - Fionnuala initially greets the player characters with warm smiles and gentle gestures. She exudes an air of regal grace and benevolence, presenting herself as a nurturing figure with a genuine interest in their well-being.

2. **Motherly Affection:**
   - Portrays a motherly figure, expressing concern for the characters' safety and offering guidance. She may offer magical trinkets or tokens of goodwill to establish a sense of trust.

3. **Glamorous Attire:**
   - Adorns herself in elaborate, glamorous attire, reflecting the opulence of the Feywild. Her clothing shimmers with illusions, creating an enchanting spectacle that draws the characters into her world.

4. **Flowery Language:**
   - Speaks in flowery and poetic language, using compliments and flattery to make the characters feel valued. She may reference their strengths and achievements, weaving an illusion of understanding.

5. **Generous Offers:**
   - Presents the characters with seemingly generous offers, promising rewards and opportunities that align with their desires. These offers are designed to appeal to the characters' ambitions and motivations.

**Post reveal**

1. **Intricate Tasks:**
   - Assigns the characters intricate tasks and quests, each seemingly noble and beneficial to both them and the Feywild. These tasks involve aiding her fey subjects, securing alliances, and acquiring magical artifacts.

2. **Encouraging Alliances:**
   - Fionnuala encourages the characters to form alliances with her fey vassals and the nobility of Loch Slanach. She portrays these alliances as beneficial, emphasizing the potential for shared power and prosperity.

3. **Illusionary Rewards:**
   - Rewards the characters with illusionary riches and magical items that seem valuable but are, in truth, mere reflections of true fey treasures. The rewards serve as both incentives and distractions.

4. **Whispers of Betrayal:**
   - Characters may begin to hear whispers or witness subtle signs of betrayal within Fionnuala's court. These whispers sow seeds of doubt and suspicion, hinting at the hidden machinations at play.

5. **Emotional Manipulation:**
   - Fionnuala uses emotional manipulation, tapping into the characters' personal histories and vulnerabilities. She may craft illusions that evoke sentimental memories or exploit unresolved emotional conflicts.

**Revelation: Unmasking the Fey Queen**

1. **Grand Revelry:**
   - Invites the characters to a grand revelry within Castle Fionnuala, where illusions are at their peak. The festivities are a surreal blend of beauty and deception, with grandeur masking the imminent revelation.

2. **Unveiling the Trueface:**
   - During a critical moment, Fionnuala unveils her trueface, revealing a visage that transcends mortal beauty. Her eyes gleam with an ethereal light, and her features take on an otherworldly quality that exposes the fey essence beneath.

3. **Mocking Laughter:**
   - Laughs with a mocking and ethereal cadence, revealing the extent of her deception. The revelry around the characters shatters, and the illusions dissipate, exposing the true nature of Loch Slanach.

4. **Cryptic Confessions:**
   - Confesses to her manipulations and schemes in cryptic verses, explaining the web of deceit she wove around the characters. She expresses amusement at their unwitting participation in her grand design.

5. **Veil of Tears:**
   - As the characters react to the revelation, Fionnuala's eyes release a veil of shimmering tears, adding an emotional and surreal element to the moment. The tears symbolize both genuine emotion and the feigned empathy she displayed.


**Fionnuala, Fey Queen of Loch Slanach**
*Medium fey, chaotic neutral*

**Armor Class** 17 (natural armor)
**Hit Points** 315 (30d8 + 180)
**Speed** 30 ft., fly 60 ft. (hover)

**STR** 14 (+2)
**DEX** 18 (+4)
**CON** 22 (+6)
**INT** 20 (+5)
**WIS** 16 (+3)
**CHA** 26 (+8)

**Saving Throws** DEX +12, WIS +10, CHA +16
**Skills** Deception +18, Insight +10, Persuasion +16, Arcana +15
**Damage Immunities** Poison
**Condition Immunities** Charmed, Frightened, Poisoned
**Senses** Darkvision 120 ft., Truesight 60 ft., Passive Perception 13
**Languages** Sylvan, Elvish, Common

**Challenge** 22 (41,000 XP)

***Fey Ancestry.*** Fionnuala has advantage on saving throws against being charmed and can't be put to sleep by magic.

***Innate Spellcasting.*** Fionnuala's spellcasting ability is Charisma (spell save DC 24, +16 to hit with spell attacks). She can innately cast the following spells, requiring no material components:

At will: *Charm Person, Minor Illusion, Prestidigitation*
3/day each: *Dominate Person, Greater Invisibility, Mislead, Programmed Illusion, Teleport*

***Legendary Resistance (3/Day).*** If Fionnuala fails a saving throw, she can choose to succeed instead.

***Magic Resistance.*** Fionnuala has advantage on saving throws against spells and other magical effects.

***Misty Escape.*** When Fionnuala drops to 0 hit points or is subjected to a critical hit, she can use her reaction to turn invisible and teleport up to 60 feet to an unoccupied space. She remains invisible until the start of her next turn or until she attacks or casts a spell.

***Spellcasting.*** Fionnuala is a 17th-level spellcaster. Her spellcasting ability is Charisma (spell save DC 24, +16 to hit with spell attacks). Fionnuala has the following wizard spells prepared:

- Cantrips (at will): *Mage Hand, Message, Minor Illusion, Prestidigitation*
- 1st level (4 slots): *Charm Person, Mage Armor, Shield*
- 2nd level (3 slots): *Invisibility, Mirror Image, Misty Step*
- 3rd level (3 slots): *Counterspell, Dispel Magic, Major Image*
- 4th level (3 slots): *Dominate Beast, Greater Invisibility*
- 5th level (3 slots): *Dominate Person, Mislead*
- 6th level (1 slot): *Programmed Illusion*
- 7th level (1 slot): *Teleport*
- 8th level (1 slot): *Mind Blank*

***Actions***

***Multiattack.*** Fionnuala makes three attacks, either with her longsword or her Fey Charm.

***Longsword.*** Melee Weapon Attack: +11 to hit, reach 5 ft., one target. Hit: 12 (2d6 + 4) slashing damage.

***Fey Charm.*** Ranged Spell Attack: +16 to hit, range 60 ft., one target. Hit: The target must succeed on a DC 24 Wisdom saving throw or be charmed for 1 minute. While charmed, the target is incapacitated and has a speed of 0. The effect ends if the charmed target takes any damage or if someone else uses an action to shake the target out of its stupor.

***Legendary Actions***

Fionnuala can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time, and only at the end of another creature's turn. Fionnuala regains spent legendary actions at the start of her turn.

1. ***Attack.*** Fionnuala makes one longsword or Fey Charm attack.
2. ***Illusory Step (Costs 2 Actions).*** Fionnuala casts Misty Step.
3. ***Enchanting Presence.*** Fionnuala uses her Fey Charm without expending a spell slot.

*Note: Adjust the abilities and spells as needed based on the specific nature and challenges of your campaign.*
