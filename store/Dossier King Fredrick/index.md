---
title: Dossier King Fredrick
date: "2023-11-20T22:40:32.169Z"
description: A character guide to Fredrick The Beastman King
---

## Fredrick of Bergherz - The Beastman King
![king Fredrick](./Fredrick.jpg)
### Appearance:
Fredrick is a towering beastman with felinish facial features, a powerful build, and thick brown fur. His eyes, once filled with the warmth of royalty, now carry the weight of centuries of struggle. Despite being technically only 20 years old, he bears the physical marks of immortality and the emotional scars of a tragic past.

### Background:

#### Prince Fredrick of Bergherz: 
Five centuries ago, Prince Fredrick stood poised to inherit the throne of the prosperous kingdom of Bergherz. His life took an unexpected turn when the court sorceress, Gisela, unleashed a cataclysmic event. The kingdom, along with several neighboring nations, was laid to waste as Gisela, driven to madness by a broken blood oath, tore the veil between the material world and the feywild.

#### Blood Oath and Service:
The blood oath between Fredrick's father and Gisela was intended to secure Gisela's service to the king. The steep price Gisela placed on her services was the union of their children in marriage. Tragically, both Fredrick and Gisela's daughter were only children themselves, unaware of the fate woven into their shared destiny.

#### Love and Betrayal:
As Fredrick and Gisela's daughter secretly fell in love, their union was both genuine and tragic. Unbeknownst to the royal court, their relationship was a beacon of love in the midst of political machinations. The shattered oath plunged Gisela into paranoia and madness. With the aid of Fionnuala, the Archfey, she unleashed wild magic that transformed the land, turning Fredrick into an immortal beastman and cursing him to be perceived as a raging monster by all humanoids.

#### Quests for the Fey Folk:
Fredrick sought refuge in the Hexenwald, a transformed land now inhabited by fey folk. To gain their charity and acceptance, he embarked on quests for the various fey settlements. These quests showcased his strength, resilience, and commitment to the well-being of the Hexenwald. The fey folk, impressed by his deeds, began to see him as a hero.

#### Guiding the Fey Small Folk:
With his growing reputation, Fredrick eventually led a group of fey smallfolk, including brownies and goblins, to the ruins of SLiberberg. Together, they transformed the remnants of the once-great capital into a thriving town. The fey settlements flourished under Fredrick's guardianship, and he became a symbol of resilience and hope in the Hexenwald.

#### Gisela's Transformation and Vengeance:
After tearing the veil between realities, Gisela ascended to the status of an Archfey. It was at Fionnuala's advice that she hired an army of goblins to conquer SLiberberg and the southern Hexenwald. Filled with resentment and fueled by newfound power, Gisela sought to annihilate everything Fredrick built.

#### Liberation by the Player Characters:
In a fateful turn of events, a group of courageous adventurers, the player characters, intervened and liberated Fredrick from his torment. The bonds forged in battle and the shared victory over adversity formed a deep connection between Fredrick and the adventurers. With SLiberberg free once more, Fredrick now faces the inevitability of a confrontation with the transformed Gisela, his heart heavy with the memories of a lost love and the weight of centuries of struggle.

#### Personality:
Fredrick is a brooding and melancholic figure, haunted by the memories of his lost lover and friends. In private, he often stares at a portrait of his long-lost love for hours. When angered or frustrated, he growls like a beast, followed by silent brooding. However, in the presence of his friends or a romantic partner, a subtle transformation occurs. The warmth of their companionship seems to dispel the shadows of melancholy that typically shroud him. Fredrick opens up, revealing a softer side that craves the solace and joy found in genuine connections.

#### Motivations and Goals:
Fredrick's primary motivation is to rebuild and protect SLiberberg. Despite the imminent confrontation with Gisela, his unwavering commitment to his kingdom drives him forward. He seeks closure for the tragedies of his past and, in the short term, knows that facing Gisela is inevitable.

#### Skills and Abilities:
As a Fighter wielding a greatsword, Fredrick is a master of physical combat. His immortal nature and experiences with wild magic have granted him insights into both temporal and magical aspects. A skilled strategist, he combines versatility and tactical acumen in battle.

#### Connections:
Fredrick is regarded as a hero among the good-aligned fey folk in the Märchenweltgrenze, having gained their gratitude through completing quests for them. He formed close ties with the player characters after they liberated him, creating a bond rooted in shared experiences and victories.

#### Habits:
Fredrick's bouts of melancholy involve staring at a portrait of his long-lost love for hours. When angry or frustrated, he growls like a beast, followed by periods of silent brooding. These habits provide glimpses into the emotional turmoil he battles internally.

#### Current Situation:
SLiberberg is in the process of rebuilding, and Fredrick actively oversees the restoration efforts. The impending confrontation with Gisela looms, and tensions rise within him as he balances the weight of the past with the necessity of securing SLiberberg's future.


---

### Roleplaying Guide for Fredrick - Beastman King

#### General Demeanor:
- **Brooding and Melancholic:** Fredrick carries the weight of centuries on his shoulders. His demeanor is often brooding and melancholic, haunted by the memories of a lost love and the tragedies of his past.

#### Private Moments:
- **Staring at the Portrait:** In private, Fredrick spends moments of solitude staring at a portrait of his long-lost love. This is a private ritual that reflects the depth of his emotions and the enduring pain he carries.

#### Emotional Outbursts:
- **Growls and Brooding:** When angered or frustrated, Fredrick tends to growl like a beast, followed by periods of silent brooding. These emotional outbursts offer a glimpse into the inner turmoil he battles.

#### Transformation with Friends/Romantic Partner:
- **Warmth and Openness:** In the presence of friends or a romantic partner, Fredrick undergoes a subtle transformation. The warmth of companionship dispels the shadows of melancholy, and he becomes more open, revealing a softer side.

#### Motivations and Goals:
- **Rebuilding SLiberberg:** Fredrick's primary motivation is to rebuild and protect SLiberberg. The impending confrontation with Gisela weighs heavily on him, but his commitment to his kingdom is unwavering.

#### Interaction with the Party:
- **Loyalty and Friendship:** Fredrick is fiercely loyal to the player characters, especially after they liberate him from Gisela's torment. He values the bonds forged in battle and adversity, viewing the party as true friends.

#### In the Face of Adversity:
- **Courage and Determination:** Despite his melancholic nature, Fredrick faces adversity with courage and determination. His experiences have shaped him into a resilient and capable leader.

#### Transformation into an Archfey:
- **Growing Power:** As Fredrick transforms into an Archfey, his powers and connection to the Märchenweltgrenze deepen. The players witness his ascendance, marking a pivotal moment in his journey.

#### Moments of Reflection:
- **Bouts of Melancholy:** Fredrick experiences bouts of deep melancholy, especially when alone. These moments provide opportunities for character development and introspection.

#### Guidance for DM:
- **NPC Interaction:** Fredrick engages with the party in a nuanced manner, evolving based on the relationships formed. Pay attention to his interactions with each character, particularly friends or potential romantic partners.
  
- **Campaign Integration:** Weave Fredrick's personal journey into the broader narrative, providing opportunities for the party to aid him in confronting Gisela and overcoming the challenges that lie ahead.

- **Emotional Arc:** Explore Fredrick's emotional arc, allowing him to gradually find solace and closure, especially as the party progresses in their adventures.

---
## Stats

### Fredrick - Beastman Fighter (Level 7)

**Armor Class:** 16 (Chain Mail)

**Hit Points:** 75 (7d10 + 35)

**Speed:** 30 ft.

#### Abilities:
- **Strength:** 18 (+4)
- **Dexterity:** 14 (+2)
- **Constitution:** 20 (+5)
- **Intelligence:** 12 (+1)
- **Wisdom:** 14 (+2)
- **Charisma:** 16 (+3)

#### Saving Throws:
- **Strength:** +7
- **Constitution:** +8

#### Skills:
- **Acrobatics:** +4
- **Athletics:** +7
- **Insight:** +4
- **Perception:** +4
- **Persuasion:** +5

#### Senses:
- Darkvision 60 ft., Passive Perception 14

#### Languages:
- Common, Sylvan

#### Features:
- **Fey Ancestry:** Advantage on saving throws against being charmed, and magic can't put Fredrick to sleep.
- **Feral Resilience:** Fredrick's hit point maximum increases by 1, and he gains an additional hit point whenever he levels up.
- **Second Wind:** Once per short rest, Fredrick can use a bonus action to regain hit points equal to 1d10 + Fighter level.
- **Action Surge:** Once per short rest, Fredrick can take an additional action on his turn.

#### Attacks:
- **Multiattack:** Fredrick makes two attacks with his greatsword.
- **Greatsword:** Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 2d6 + 4 slashing damage.

#### Special Abilities:
- **Beastman Transformation:** As a bonus action, Fredrick can embrace his beastman nature, gaining advantage on Strength-based checks and saving throws for 1 minute. Once used, he can't use this feature again until he finishes a short or long rest.
- **Inspiring Presence:** As a bonus action, Fredrick can choose up to three friendly creatures (including himself) within 30 feet. They gain temporary hit points equal to 1d6 + Fredrick's Charisma modifier.

#### Reactions:
- **Parry:** Fredrick adds 2 to his AC against one melee attack that would hit him. To do so, he must see the attacker and be wielding a melee weapon.

#### Equipment:
- Greatsword, Chain Mail, Shield, Explorer's Pack

#### Roleplaying Notes:
- Fredrick's melancholic demeanor hides a heart of determination and loyalty.
- In the presence of friends or a romantic partner, he opens up, becoming a source of inspiration and comfort.
- Fredrick's primary goal is to rebuild and protect SLiberberg, and he faces the impending confrontation with Gisela with both courage and concern for his companions.


At higher levels make the following changes

### Fredrick - Beastman Fighter (Level 11)

**Armor Class:** 18 (Plate Mail, Shield)

**Hit Points:** 120 (11d10 + 55)

#### Additional Features:

- **Extra Attack (2):** Fredrick can attack three times whenever he takes the Attack action on his turn.
- **Indomitable (1/Day):** If Fredrick fails a saving throw, he can reroll it and must use the new roll.

#### Improved Greatsword:

- **Greatsword:** Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 2d6 + 5 slashing damage.

### Fredrick - Beastman Fighter (Level 16, Archfey Transformation)

**Armor Class:** 20 (Plate Mail, Shield)

**Hit Points:** 180 (16d10 + 80)

#### Additional Features:

- **Extra Attack (3):** Fredrick can attack four times whenever he takes the Attack action on his turn.
- **Indomitable (2/Day):** Fredrick gains an additional use of Indomitable.
- **Fey Presence:** As an action, Fredrick can unleash his fey presence. Each creature of his choice within 10 feet of him must succeed on a Wisdom saving throw or be charmed or frightened until the end of his next turn (DC 17).

#### Empowered Greatsword:

- **Greatsword:** Melee Weapon Attack: +11 to hit, reach 5 ft., one target. Hit: 2d6 + 7 slashing damage.

#### Archfey Abilities:

- **Misty Step (Recharge 5-6):** As a bonus action, Fredrick can magically teleport up to 30 feet to an unoccupied space he can see.
- **Fey Ancestry (Enhanced):** Advantage on saving throws against being charmed, and magic can't put Fredrick to sleep. Additionally, he has advantage on saving throws against spells and other magical effects.

#### Archfey Presence:

- **Blessing of the Archfey:** As a bonus action, Fredrick grants himself and up to three friendly creatures within 30 feet advantage on attack rolls and saving throws for 1 minute. Once used, he can't use this feature again until he finishes a short or long rest.

### Fredrick - Beastman Fighter (Level 20, Archfey)

**Armor Class:** 21 (Plate Mail, Shield)

**Hit Points:** 225 (20d10 + 100)

#### Additional Features:

- **Extra Attack (4):** Fredrick can attack five times whenever he takes the Attack action on his turn.
- **Indomitable (3/Day):** Fredrick gains an additional use of Indomitable.
- **Fey Champion:** Fredrick's attacks score a critical hit on a roll of 19 or 20.

#### Master of the Märchenweltgrenze:

- Fredrick's connection to the Märchenweltgrenze deepens, granting him advantage on all saving throws against spells and other magical effects.

#### Ascended Greatsword:

- **Greatsword:** Melee Weapon Attack: +12 to hit, reach 5 ft., one target. Hit: 2d6 + 8 slashing damage.

#### Archfey Ascendance:

- **Regeneration:** At the start of his turn, Fredrick regains 10 hit points if he has at least 1 hit point.
