---
title: Sliberberg Gazetteer 
date: "2023-11-19T22:40:32.169Z"
description: An intro to Sliberberg
---

Nestled within the mystical Märchenweltgrenze, Sliberberg is a city that straddles the boundary between the material plane and the Feywild. The name itself echoes the silvery currents of the Silberfluse River that flows through its heart. Sliberberg is renowned for being a haven where beings from both realms coexist in a delicate balance, creating a cityscape that blends natural enchantment with mortal pragmatism.



## Geography

Sliberberg is situated atop a pair of hills that overlook the point where the Silberfluse River empties into the Feysee. The city's strategic position allows for a stunning panorama of the surrounding Märchenweltgrenze. The terrain is marked by ancient trees, moss-covered stones, and magical flora. The climate is influenced by the merging planes, resulting in ever-changing weather, from ethereal mists to vibrant rainbows.

## History


#### **Origins and Destruction:**
Sliberberg, once the proud capital of the kingdom of Bergherz, stood as a symbol of unity between the material world and the Feywild during the world's early middle ages. However, the kingdom met its tragic end when the court sorceress, Gisela, in a fit of vengeful madness, tore the veil between the planes. The king's broken blood oath triggered Gisela's rage, resulting in the destruction of Bergherz and the transformation of the land by wild magic. Sliberberg lay abandoned for about 350 years.

#### **The Immortal Beast and the Fey Brownies:**
The last prince of Bergherz, Fredrick, transformed into an immortal beastman, occasionally dwelled in the family castle amidst the ruins. Approximately 200 years ago, a very large group of fey goblins and various other fey settled in the city having followed Fredrick back from his adventuring days. The city had grown into a modest port city in the two centuries since.

#### **The Archfey's Wrath and Hobgoblin Conquest:**
Recently,  the ruthless Hobgoblin Mercenary General Vasrock, sought to conquer Sliberberg and the rest of the Märchenweltgrenze. Vasrock, in a savage battle, captured the beast and conquered the city. Slaving parties were dispatched to neighboring settlements and beyond the Märchenweltgrenze, furthering Gisela's influence.

#### **Rebellion and Liberation:**
A half a year after the conquest, a group of adventurers from outside the Märchenweltgrenze stumbled upon the city. With the help of a local witch they formed a resistance group and the King Fredrick, they successfully liberated Sliberberg. The city, once again free, stands as a testament to the resilience of its people and the precarious balance between the material plane and the Feywild.


## Government
At the helm of the city stands the Lord Mayor, a position of authority appointed directly by the reigning monarch. The Lord Mayor serves as the chief executive, entrusted with the responsibility of overseeing the administration, enforcing laws, and representing the interests of Sliberberg's diverse inhabitants. This appointment ensures a direct link between the city and the ruling monarchy, fostering stability and unity. Underneath the Lord Mayor's leadership, the city council plays a pivotal role in shaping the destiny of Sliberberg. Comprising representatives from various segments of society, including merchants, guild leaders, and influential citizens, the council convenes to deliberate on matters ranging from trade regulations to cultural initiatives. The council's composition reflects the city's commitment to inclusivity, acknowledging the importance of diverse perspectives in the decision-making process.


## Gazetteer
 ![map](./sliberbergmap.jpg)
### Old City (Alte Stadt):
Perched majestically atop a large hill, the  Old City of Sliberberg commands a panoramic view of the surrounding Feysee> THe old city has been long the home the rich and the nobly born. The residents of this ward like to show off their wealth through architecture. Eladrin style mansions with tall spires, delicate arches, and intricate filigree adorn the exteriors of these noble residences, palaces of pure crystal, and towering but completely impractical keeps straight out fairytale dominate the skyline with Castle Sliberberg towering all the others. Between the impressive residences are gardens filled with exotic flora from the feywild. Along the broad avenues of this ward are flowering trees that the form a canopy overhead. Thanks to fey magic they never stop blooming.

### Gazetteer
**City Hall** Nestled in a charming plaza within the Old City, the Alpenheim Rathaus stands as a symbol of governance and community. Designed in the distinctive German Alpine style characterized by steep-pitched roofs, decorative wooden fretwork, and vibrant, flower-filled window boxes the town hall is a testament to the enduring spirit of the city. Intricate fretwork adorns the building, adding a touch of elegance to its façade. The current Lady mayor is Elowen . As the leader of the resistance before Frederick's rescue, Elowen Whisperwind is a charismatic and visionary leader. Known for her unwavering commitment to the city's well-being, she transitioned seamlessly into the role of Lord Mayor. Elowen is dedicated to fostering unity, rebuilding the Old City, and strengthening the ties between the material plane and the Feywild.
**CIty Guard Compound** This strategically positioned compound is a bastion of security, consisting of a walled courtyard housing barracks, an administration building with an armory, and an imposing jail complex with a central tower. The compound is a testament to the city's commitment to maintaining law and order. The compound is encased in sturdy stone walls, providing a secure perimeter. Within the compound is a two story brick barracks, a brick administration building with armory and an L shaped jail with two wings joined by a three story tall tower. 
### The market ward
### Market Ward / Lower City (Marktviertel / Unterstadt):

**Description:**
Nestled between the picturesque lake shore and the elevated Old City, the Market Ward, also known as the Lower City or Marktviertel in German, is a vibrant hub of commerce and cultural exchange. Established 200 years ago during the city's resettlement, this district is a testament to the harmonious coexistence of fey and recently arrived humanoids.The heart of the ward is a bustling open marketplace, a lively tableau where traders and artisans showcase their wares. Colorful canopies stretch overhead, and the air is filled with the sounds of haggling and laughter.Flanking the central marketplace are blocks of shops, from enchanting curio shops to bustling taverns.Beyond the market blocks sprawl large residential neighborhoods. Narrow cobblestone streets wind through the area, lined with row houses and apartment buildings in a wide variety of architectural styles that fuse fey enchantment with practicality, buildings of luminous crystal, houses inside giant hollowed out gourds, and giant hollowed out trees with shops on the first floor and apartments on the upper floors just to name a few. Even the more mundane buildings styles make concessions to natural beauty with many featuring garden boxes and intricate lattice-work for vines.
#### Gazettteer
#####  The Grand fey Market
 In the heart of Sliberberg, The Grand fey Market is a mesmerizing blend of nature and commerce. Giant trees with intertwining branches form a majestic canopy that shelters the bustling bazaar below.  A second-story boardwalk spans across the intertwined branches, forming elevated streets that wind through the marketplace. The boardwalk is lined with shops, cafes, and stalls, offering a different perspective of the bustling activity below. Enchanting fairy lamps hang from the branches and boardwalk, casting a soft, magical glow over the entire marketplace. The warm lights create a cozy and inviting ambiance during evening hours, turning the marketplace into a fairy-lit wonderland
##### The Hillside Amphitheater
At the confluence of the Market Ward and the ascent toward the Upper City hill, a grand spectacle awaits—The Hillside Amphitheater. This open-air theaterseamlessly melds nature with artistry. Wooden bench seating and box seats provide a rustic yet comfortable setting for spectators. Above, a vast ceiling of woven branches shelters the amphitheater, casting a dappled play of light and shadow over the audience. Suspended from this enchanting canopy is a catwalk adorned with fairy light spotlights and operated by nimble gnome and goblin stagehands. The theater opens at 9 and runs productions well into the night. Typically the morning opens with Variety Show featuring new talent, one act play, small enable concerts and solo performances in the afternoon and large scale theater plays and concerts in the evening
###### Panthor Silverhoof
Owned and operated by Panthor Silverhoof, a charismatic satyr with a twinkle in his eye and a wild mane of curls. Once a star actor renowned for his unparalleled performances, tragedy struck during a fateful summer court performance for Titania herself. While acting out the tragic death of the protagonist in the tragic musical Eclipsed Reverie. The scene was meant to involve a collapsing bridge or platform. As Panthor, playing the lead role, delivered a poignant monologue in the character's final moments, the bridge was supposed to slowly lower, creating a dramatic visual effect. However, a combination of a faulty winch system and a miscommunication between stagehands resulted in a sudden, uncontrolled collapse of the bridge. Despite breaking both his legs in the accident Panthor continued the scene completing the monologue. Titania was so moved by the dedication of Panthor and his troupe that she bestowed upon them a king's ransom. Despite magic healing he was left permanently crippled and never returned to acting. He used his share to open the theater. That being said  he takes a hands-on approach in the day-to-day operations of the theater. From overseeing rehearsals to managing ticket sales, he ensures that every aspect is a reflection of his vision for the arts. While lacking in playwriting talent, Panthor has found his niche as the producer of every production at the theater. His keen eye for talent, attention to detail, and deep understanding of what captivates an audience have turned the Hillside Amphitheater into a beacon of theatrical success. He will likely be one the players that will try to schmooze the players upon their accension to power.

#### Goblin Market 
 Nestled at the city walls, the Goblin Market is a whimsical and otherworldly marketplace that deals in magical and arcane curiosities. This bustling market, run by goblinoids and fey creatures, is a kaleidoscope of colors, sounds, and inexplicable wonders. Merchants here are known for their willingness to trade not only in coin but also in memories, secrets, and dreams.
#### Silvermoon District:

Nestled within a five-block section of the Market Ward, the Silvermoon District, or Silbermond Viertel in German, is a prominent neighborhood renowned for its magical ambiance and enchanting allure. At the heart of this district stands the Silver Moon Cafe, a focal point that symbolizes the neighborhood's dedication to serving the needs of magic users. The area is a mismatch of architectural styles from the somewhat cliche middle eastern style of Farid's Enchanting Caravan Emporium to the rustic half timber fairytale charm of the Silvermoon cafe itself. Most of the shops offer above-store lodgings for the shop owners. Additionally, two boarding houses cater to those seeking temporary residence in this magical haven. The central feature of the district, the Silver Moon Cafe, is a magical establishment known for its ethereal atmosphere and exquisite offerings. The cafe serves as a gathering place for magic users and enthusiasts, fostering an environment of camaraderie and knowledge exchange.  To accommodate transient magic users and scholars, two boarding houses have been established in the district. These establishments provide a temporary home for those seeking to explore the magical offerings of the Silvermoon District.
Certainly! Let's add some details to the businesses in the Silvermoon District:
Buisness listing
1. **Boarding Houses:**
   - *Gleaming Starlight Haven:* This boarding house, run by the Eladrin couple Eirwyn and Finnea, offers enchanting accommodations for transient magic users. The interior is adorned with starlight motifs, and each room is named after a celestial body.

   - *Sonnenglanz Respite House:* Managed by the hospitable human couple, Heinrich and Greta Sonnenglanz, this boarding house provides a cozy haven for scholars and adventurers seeking a temporary residence. The rooms are filled with warm, natural light and decorated with magical tapestries.

2. **Large Inn:**
   - *Moonlit Haven Inn:* A spacious inn known for its mystical ambiance, the Moonlit Haven Inn is owned by the Eladrin innkeeper, Lirael. The common areas are adorned with lunar-themed decorations, and the inn hosts magical performances in its enchanting courtyard.

3. **Small Library:**
   - *Buchzauber Library:* Managed by the knowledgeable gnome librarian, Alaric Buchzauber, this small but well-curated library caters to magic users seeking rare texts and ancient lore. The shelves are filled with books on a wide range of magical subjects.

4. **Shops Catering to Magic Users:**
   - *Celtic Crystal Cauldron:* Run by the spirited Eladrin sorceress, Aoife Crystalheart, this shop specializes in crystals, gemstones, and mystical cauldrons. Aoife provides personalized consultations for customers seeking magical enhancements.

   - *Runenmeister Forge:* Owned by the skilled human artificer, Gunther Runenmeister, this forge crafts magical weapons, arcane-infused armor, and enchanted trinkets. Gunther is known for his meticulous craftsmanship and his ability to imbue items with magical properties.

   - *Sidhe Seer Emporium:* Managed by the insightful gnome diviner, Niamh Gleamgazer, this shop offers divination services, scrying mirrors, and enchanted eyewear. Niamh's shop is a popular destination for those seeking guidance from the fey.

5. **Farid's The Enchanting Caravan Emporium:**
   - *Farid's Verzauberter Karawanenmarkt:* A renowned magic item emporium owned by the charismatic and mysterious Farid. The shop is a treasure trove of enchanted items from across the realms, and Farid himself is known for his tales of exotic lands and magical discoveries.

### Dockside District (Hafenviertel):
The Dockside District, known as Hafenviertel in German, is a bustling extension of the Market Ward that stretches out into the lake on sturdy pilings and stone wharves. Built upon the ruins of the original Lower City, this maritime hub is a maze of narrow boardwalk streets, teeming with activity, trade, and the salty scent of the lake breeze. The docks extend gracefully into the lake, creating a bustling waterfront where ships from distant lands and fey vessels find a common berth. Sailors and dockworkers weave between boats, their silhouettes dancing against the sparkling waters.  Dominating the waterfront are sturdy stone wharves, remnants of the original Lower City. Warehouses and boat shops rise above the waterline, standing as weathered sentinels of the district's maritime past.

### North Ward (Nordviertel):

The North Ward, or Nordviertel in German, stands as the city's northernmost district, a testament to the city's resilience and adaptability. Initially conceived as a slum during the goblin occupation, intended to house laborers fueling Vasrock's army, it has transformed into a thriving residential quarter following the Kingdom of Mountain Heart's founding.  The architecture of the North Ward is characterized by a mix of hastily constructed structures from the goblin occupation era and newer buildings reflecting the diverse origins of its inhabitants. The district has a dynamic and evolving appearance, showcasing the adaptability of its residents. Following the Kingdom of Mountain Heart's founding, the North Ward has seen a surge in population as citizens from the kingdom sought refuge and opportunity within the city. The district has become a melting pot of cultures and stories, reflecting the city's commitment to unity and diversity.

### West Ward (Westviertel):

The West Ward, or Westviertel in German, stands as a testament to the recent upheavals and the city's shifting dynamics. Originally conceived as an industrial hub by the goblin army, its construction was interrupted by the player characters' uprising. Now, dominated by water-powered mills and the homes of mechanics and engineers.
Unlike the rest of the city archetecture is largely dominated by brickwork, large brick mills and workshop line the foot of the hill and the riverfront. Smaller workshops of craftsmen, tinkers and mages are found throughout he ward. Most of the residents are gnomes and as such the architecture prominently features brass and bronze fixtures and gear base decorations. The streets are brickwork with iron post holding up the fairy light streetlights.
At the northern edge of the ward, a prominent hill is crowned by the imposing structures of a wizard college. Founded during the city's recent upheavals, the college attracts aspiring mages and scholars, adding an intellectual and mystical dimension to the industrial landscape.

### Eastward / Outward (Ostviertel / Außen):

Eastward, known as Ostviertel in German or Outward in common parlance, is a longstanding upper-middle-class and lower-upper-class neighborhood situated beyond the old city walls. Named for its position outside the original city confines, this ward is a serene enclave filled with gardens, parks, and well-manicured estates. Predominantly inhabited by fey, with Eladrin and gnomes being dominant, Eastward is distinguished by its affluent and sophisticated atmosphere. At the edge of the primary park resides a large colony of pixies, sworn in fealty to Fredrick. These mischievous and magical beings contribute to the ward's ethereal ambiance, weaving spells and enchantments that add a touch of whimsy to Eastward.

# Encounter tables

### Old City (Alte Stadt):

1. **Ghostly Apparition:** A spectral figure from the city's past appears, seeking assistance in resolving a lingering matter. The players must uncover the ghost's story and help put its soul to rest.

2. **Magical Anomaly:** The wild magic residue in the Old City has created a temporary portal to the Feywild. Players may encounter curious fey creatures crossing into the material plane, leading to both amusing and challenging situations.

3. **Ancient Guardian:** A magical construct, once tasked with protecting the Old City, has been reactivated. The players must navigate its riddles and challenges to pass safely through the district.

4. **Cursed Artifact:** A long-lost artifact, tied to the city's history, is unearthed. Its malevolent influence spreads, causing strange occurrences and drawing the attention of dark forces.

5. **Time Distortion:** The veil between past and present weakens, causing momentary glimpses into different eras of the city's history. Players may witness scenes from the past or encounter echoes of historical figures.

### Market Ward / Lower City (Marktviertel / Unterstadt):

1. **Thieves' Guild Encounter:** A group of thieves attempts to pickpocket the players in the crowded market square. The encounter could lead to a negotiation, pursuit through the maze-like streets, or a chance to uncover the local Thieves' Guild.

2. **Magical Mishap:** A merchant's magical creation goes awry, causing chaos in the market. Players must assist in containing the situation before the magical effects spread.

3. **Fey Merchant:** A fey merchant, offering exotic and enchanted goods, sets up a temporary stall in the market. Bargaining with this otherworldly trader may lead to unusual and potent magical items.

4. **Rival Enchanters:** Two rival enchanters, each with a booth in the market, engage in a public magical duel. The players may choose to intervene, take sides, or negotiate a truce.

5. **Mysterious Message:** The players receive a cryptic message from an anonymous sender, hinting at a hidden magical artifact for sale in the market. Pursuing the message leads to a quest for a rare and powerful item.

### Dockside District (Hafenviertel):

1. **Smuggling Operation:** The players witness a smuggling operation on the docks, involving illegal magical goods. They must decide whether to report the activity, intervene, or negotiate with the smugglers for a share of the loot.

2. **Sea Monster Sighting:** Rumors spread of a sea monster lurking near the docks. The players may choose to investigate, encountering a real or illusory creature, or navigate the resulting panic among the dockworkers.

3. **Cursed Cargo:** A shipment of magical artifacts arrives with an unintended curse. The players must identify the cursed items and find a way to neutralize their effects before the curse spreads.

4. **Docks Sabotage:** A rival city or faction sends agents to sabotage the docks, disrupting trade and causing chaos. The players must uncover the perpetrators and prevent further damage.

5. **Fey Sea Trader:** A ship from the Feywild arrives at the docks, manned by fey traders seeking unique items from the material plane. Players can engage in magical bartering and may encounter curious fey creatures on board.

### North Ward (Nordviertel):

1. **Goblin Laborers Protest:** Former goblin laborers, once forced to work in the construction of the North Ward, organize a protest. Players can choose to support their cause, negotiate with city officials, or quell the unrest.

2. **Ancient Ruins Discovery:** During ongoing construction, ancient ruins are uncovered beneath the North Ward. Exploring the ruins reveals forgotten artifacts and magical secrets, but the players must contend with unexpected guardians.

3. **Political Rally:** A political rally takes place in the North Ward, drawing attention to recent changes in city leadership. The players may choose to participate, protect the event, or investigate potential disruptions.

4. **Arcane Experiment Gone Awry:** A local wizard's experiment results in a magical mishap, creating a temporary magical anomaly. The players must navigate the affected area, encountering unusual magical effects and creatures.

5. **Cultural Festival:** The North Ward hosts a cultural festival celebrating the diversity of the city's residents. Players can participate in festivities, engage in cultural exchanges, or uncover hidden tensions among different factions.

### West Ward (Westviertel):

1. **Mechanical Malfunction:** One of the water-powered mills malfunctions, causing chaos in the West Ward. Players must investigate the cause and decide whether to repair the machinery, negotiate with disgruntled workers, or exploit the situation for personal gain.

2. **Wizard College Duel:** Two ambitious wizard apprentices engage in a public magical duel near the wizard college. The players may choose to intervene, spectate, or even participate in the duel.

3. **Invention Demonstration:** An eccentric inventor showcases a new magical invention in the West Ward. The demonstration, however, spirals out of control, leading to unexpected consequences. Players

 can decide to assist, critique, or capitalize on the chaos.

4. **Goblin Saboteurs:** Agents of Vasrock, seeking revenge for the city's uprising, attempt to sabotage the construction projects in the West Ward. The players must thwart their plans and uncover any lingering connections to the goblin forces.

5. **Elemental Infestation:** A magical experiment involving elemental energies goes awry, leading to an infestation of elemental creatures in the West Ward. Players must contain the situation before the district is overwhelmed.

### Eastward / Outward (Ostviertel / Außen):

1. **Feywild Portal:** A temporary portal to the Feywild opens in Eastward, bringing forth fey creatures seeking refuge. The players must decide how to handle the situation, balancing hospitality with potential risks.

2. **Cultural Exchange:** A delegation from the Feywild arrives in Eastward for a cultural exchange. Players can engage in diplomatic efforts, protect the delegation, or navigate misunderstandings that arise during the exchange.

3. **Eladrin Nobility Visit:** High-ranking Eladrin nobles visit Eastward, seeking to strengthen diplomatic ties with the material plane. Players may be enlisted to provide security, assist in negotiations, or uncover potential political intrigue.

4. **Pixie Mischief:** The pixie colony at the edge of the primary park becomes mischievous, causing playful chaos in Eastward. Players can choose to interact with the pixies, assist in resolving any disruptions, or capitalize on the whimsical atmosphere.

5. **Cursed Fey Artifact:** A fey artifact, believed to be cursed, is discovered in Eastward. Players must navigate fey politics and decide whether to neutralize the curse, exploit the artifact's power, or return it to the Feywild.

