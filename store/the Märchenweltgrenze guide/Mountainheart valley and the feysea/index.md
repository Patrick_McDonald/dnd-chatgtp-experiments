---
title: The feysea 
date: "2023-12-16T22:40:33.169Z"
description: Part of the the Märchenweltgrenze guide series
---



**Bergherz**
Nestled on the north shore of the Great Fey Sea, at the very epicenter of the Märchenweltgrenze, lies the small yet enchanting kingdom of Bergherz. Evolving from humble beginnings into a city-state over the course of 200 years, Bergherz is a realm where Feywild magic intertwines with the material world. Ruled by the immortal and perpetually melancholic Beastman King Fredrick, this kingdom stands as a beacon of potential, waiting for the right circumstances to unfurl its wings and grow into something much grander. Despite its modest size, Bergherz is a kingdom that has defied expectations and continued to thrive in the heart of the Feywild influence. The enchanting energies that permeate the region have contributed to the kingdom's unique character, creating an atmosphere where both the mundane and magical coexist in a delicate balance. The city-state, born from the remnants of three valleys, has become a testament to the enduring spirit of the Märchenweltgrenze.

The capital of Bergherz, Sliberberg, sprawls along the northern shores of the Great Fey Sea. The city is a harmonious blend of Feywild-inspired architecture and practical urban planning. Tree-like structures rise from the ground, their branches forming intricate walkways and bridges. Enchanted gardens flourish throughout the city, creating pockets of natural beauty that coexist with bustling markets and lively taverns. We cover the city of sliberberg in the Sliberberg Gazetteer.


**The Weeping Spring:**
To the north of Bergherz, where the convergence of the Feywild and the material world is most pronounced, lies the hauntingly beautiful site known as the Weeping Spring. A colossal rift in the sky, connecting the realms, opens to the bottom of an ocean in the Feywild. This mysterious portal, born from the tumultuous events that shaped the Märchenweltgrenze, unleashed a massive flood upon the land when first opened. Today, the Weeping Spring continues to produce a steady stream of saltwater, carving its mark on the three mountains that form the Crown.

From the bottom of the Feywild ocean, a continuous stream of saltwater flows through the Weeping Spring and into the material world. The stream, though small, holds a potent connection to the Feywild, carrying with it the essence of both realms. The waters have a unique magical quality, and those who approach the spring often report feeling a subtle but profound shift in their perception of reality.

Over the centuries, the Weeping Spring's saltwater stream has sculpted the three mountains that form the Crown. The constant erosion has given the mountains a distinctive appearance, with worn grooves and intricate patterns etched into their surfaces. The Crown, once proud and unyielding, now bears the scars of the ongoing interplay between the Feywild and the material world.


**Title: Fey Cross Island**
In the heart of the Great Fey Sea lies the enchanting Fey Cross Island, a small haven with a cove that defies the boundaries between the material world and the Feywild. This magical island serves as a crossroad, connecting sailors from the Märchenweltgrenze to a counterpart island in an ocean within the Feywild. With a unique entry ritual involving sea shanties and backwards walks, Fey Cross Island has become a hub for sailors seeking both adventure and respite.

The cove that graces Fey Cross Island is no ordinary harbor; it's a fey crossroad large enough for ships to pass through. This mystical passage links the island to a counterpart in the Feywild, creating a seamless connection between realms. To unlock the fey crossroad, sailors and their crew must partake in a unique ritual. A sea shanty, resonating with the rhythms of the sea and the Feywild, must be sung while at least six crew members walk backward around the ship's mast. The harmonious blend of song and movement activates the magic of the crossroad, allowing the ship to traverse between realms.

Fey Cross Island and its Feywild counterpart host small towns that cater to the sailors passing through the fey crossroad. The Märchenweltgrenze side boasts a lively community with structures that incorporate both practical design and Feywild aesthetics. Meanwhile, the Feywild island mirrors its counterpart, creating a harmonious reflection between the two realms. Both towns on Fey Cross Island are infused with an enchanted atmosphere, reflecting the Feywild's influence. Fey creatures, drawn by the sea shanties and the magic of the crossroad, can be found in the markets and taverns. Exotic Feywild flora and fauna decorate the landscape, creating a vibrant and otherworldly ambiance for sailors seeking both adventure and relaxation. The base of Fey Cross Island on the Märchenweltgrenze side is home to a community of merfolk who claim that the island's foundation is marked by permanent rifts to the Feywild. The merfolk, in tune with the magical energies of the Great Fey Sea, offer guidance to sailors and share tales of the Feywild wonders. The waters surrounding the island are said to hold secrets and mysteries that only those with a keen connection to the Feywild can uncover.


**Port Eudoxia**

At the very southern end of the Great Fey Sea, perched on the precipice of the Märchenweltgrenze, thrives the vibrant port town of Eudoxia. Founded by settlers from the illustrious Empire of Byzantia, this bustling trade hub serves as a pivotal outpost on the verge of the Feywild influence. Built upon a flat table of rock, Port Eudoxia serves as a poignant reminder of the majestic mountain that once adorned the landscape before being obliterated during the formation of the Märchenweltgrenze. The absence of the mountain creates a distinctive terrain, providing a solid foundation for the town and offering unobstructed views of the vast Feywild expanse that stretches beyond the Fey Sea.
Port Eudoxia stands out as one of the primary settlements where material plane races have established thriving communities. Humans, elves, dwarves, and an array of other races from the material world coexist harmoniously with the fey creatures drawn to the nearby Feywild. This cultural tapestry has woven a spirit of cooperation and mutual exchange among the town's diverse inhabitants.
At the heart of Port Eudoxia's purpose is its role as a trade outpost to the Feywild. Merchants from both realms converge in bustling markets, engaging in the exchange of goods, stories, and magical artifacts. The town's strategic position at the edge of the Märchenweltgrenze makes it an indispensable stop for those seeking passage between worlds.Also Port Eudoxia proudly marks the terminus of the only other known road into the Märchenweltgrenze. This winding thoroughfare traverses mountains and valleys, serving as a lifeline for those venturing into the Feywild realms. The road stands as a tangible link between the material plane and the Feywild, beckoning travelers and traders alike to embark on extraordinary journeys.

**Seastadt:**

On the shores of the Great Fey Sea, Seastadt emerges as a beacon of harmony between two realms—land and sea. This small coastal town, nestled between the Feywild and the material world, thrives through a unique symbiotic relationship with a tribe of merfolk. Seastadt not only sustains itself with large-scale fish and shellfish farms assisted by the merfolk but also serves as a vital port for several inland settlements seeking the bounties of the Fey Sea.

Seastadt's prosperity is deeply intertwined with the bounty of the Fey Sea. The town boasts large-scale fish and shellfish farms, carefully tended by both human and merfolk hands. The enchanting energies from the Feywild infuse the waters, resulting in an abundance of exotic and magical aquatic life. The farms not only sustain the town but also contribute to the broader economy of the Märchenweltgrenze. The townsfolk and merfolk have forged a symbiotic alliance, working together to maintain vast fish and shellfish farms that provide sustenance for both communities.

