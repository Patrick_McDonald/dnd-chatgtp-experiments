---
title: Into The Märchenweltgrenze Part 1 the south western reaches
date: "2023-10-15T22:40:32.169Z"
description: The introductory adventure to the Märchenweltgrenze chronicles campaign
---
An introductory adventure for the Märchenweltgrenze chronicles for 5 players level 1. The heroes will have to explore the Märchenweltgrenze to find clues to the location of the goblin army lair.

# Into the Märchenweltgrenze
##Background:

In the expansive realm of the Holy Empire of Valoria, where the echoes of valor and honor resonate through the land, lies a shadowed border on its eastern frontier – the mysterious Märchenweltgrenze. This vast region stands as a precarious juncture where the ethereal realm of the Feywild bleeds into the material world, creating an enigmatic tapestry where the rules of both realms entwine in an unpredictable dance.

Over the past six months, an ominous threat has emerged from the heart of Märchenweltgrenze, casting a looming shadow upon the tranquil lands of the Holy Empire. Goblins, insidious and cunning, have begun to spill forth from the depths of the thicketrealm, wreaking havoc upon settlements and farms that once thrived in peace. These malevolent invaders are not mere marauders; they are a relentless force, leaving desolation in their wake.

The goblin incursion takes a disturbing turn, as reports tell of more than just the crude theft of food stores and valuables. Villagers speak of loved ones vanishing without a trace, carried away by unseen hands, lost to the twisted whims of the fey. Yet, the enigma deepens, for the goblins exhibit an inexplicable penchant for pilfering construction materials. Tools, cut timber, and nails vanish into the recesses of Märchenweltgrenze, leaving behind confused and beleaguered communities.

Adding to the gravity of the situation, the goblin horde is not alone. Fey creatures, ethereal denizens of the enchanted forests, dance alongside them, amplifying the menace that spills forth. Hobgoblins, disciplined and organized, have forged an unholy alliance with their goblin brethren, forming a coalition that strikes fear into the hearts of even the bravest souls.

In response to this escalating crisis, the Holy Empire of Valoria has sounded the clarion call for a heroic expedition into the heart of Märchenweltgrenze. The empire, recognizing the need for stout-hearted mercenaries and intrepid adventurers, extends an offer of 50 gold coins upfront, with the promise of an additional 150 gold coins upon the triumphant conclusion of the perilous mission.

As the drums of war echo through the empire, brave souls from all walks of life converge, answering the summons to confront this otherworldly menace. The expedition seeks not only to quell the immediate threat but to unveil the dark forces at play within Märchenweltgrenze. Will these valiant few stand as the bulwark against encroaching darkness, or will they become lost echoes in the ever-shifting realm where the Feywild meets the material world? The fate of the Holy Empire of Valoria hangs in the balance, poised on the edge of Märchenweltgrenze, awaiting heroes to inscribe their legacy upon its shadowed canvas.


## Adventure Begins

As the players arrive seperately in Steinwald, a quaint town nestled at the entrance to the mountain pass leading to Märchenweltgrenze, they find themselves amid an atmosphere of both tension and hope. The air is thick with the scent of evergreen pines, and the cobbled streets echo with a blend of hushed conversations and the clattering of hooves. The town, with its timber-framed buildings adorned with vibrant flower boxes, is a picturesque gateway to the mysterious realm beyond. Recent goblin attacks on hamlets beyond Steinwald have left an indelible mark on the town. Villagers, resilient yet weary, have come together, anxiously preparing for the caravan's departure. Buildings bear scars of recent conflicts, hastily repaired as the townsfolk brace for an impending expedition. In the town square, the caravan's departure point, the players witness a gathering of merchants and adventurers, all drawn by the promise of reward and the Emperor's call to arms. THe caravan to Eichenheim will leave at 3 pm so the the players have some time to explore the town. They will encounter the following sites of intrest.
1. **Ironforge Provisions:**
   *A sturdy timber and stone shop with a weathered sign swinging in the breeze, Ironforge Provisions offers a variety of goods for both mundane needs and adventuring essentials. The grizzled dwarf proprietor, Gorm Ironforge, stands ready to haggle and share tales of the mountains.*

2. **Wandering Wyrm Emporium:**
   *Known for its eccentric owner, an elderly gnome named Fizzlewhisk, this magical curiosity shop is packed to the brim with enchanted trinkets, arcane oddities, and peculiar items sourced from Märchenweltgrenze. The air crackles with an otherworldly energy as visitors peruse shelves lined with wonders.*

3. **St. Eldritch's Sanctuary:**
   *A serene stone chapel adorned with intricate carvings and surrounded by a small garden, St. Eldritch's Sanctuary offers solace to both locals and passing travelers. The resident cleric, Sister Miriam, provides blessings, healing services, and information on the dangers that lie beyond the town.*
When they finish exploring the town read the following

*You stand in the courtyard of the formidable "Sable Griffin Inn," its weathered stones whispering tales of countless adventurers who have passed through its gates. The air crackles with a blend of tension and excitement as the caravan, a mosaic of wooden wagons laden with supplies, is being meticulously organized. Merchants barter with caravan organizers, their voices carrying over the clamor of wagons being loaded. The scent of fresh hay and polished leather hangs in the air, a poignant reminder of the impending journey. As you observe the scene, a seasoned caravan master clad in a weathered cloak approaches, eyes sharp and calculating. "Welcome, brave souls," he calls, his voice cutting through the hum of activity. "To Eichenheim we go, through the mountain pass and into the heart of Märchenweltgrenze. Gather your resolve, for this journey is not for the faint of heart." The inn's courtyard, now a stage for the commencement of an epic quest, pulses with the promise of adventure, each creak of a wagon wheel echoing the heartbeat of the untamed realms that await beyond the looming mountains.*
Gruffen Ironstride, a grizzled figure, greets the players as they  approach, his eyes assessing the plauyers readiness for the journey ahead. "Welcome to the caravan," he says, his voice carrying the weight of countless trails conquered. He gives each player a firm handshake. He presents himself as all around jovial guy and once he done greeting the players he take out a map. Read the following aloud to the players
*"We'll be heading east into the mountains for the first two days, a journey that'll test your mettle. Once we breach the border of Märchenweltgrenze, the path becomes treacherous, even though it's just a single road. Brace yourselves; this land is as unpredictable as it is enchanting. The final destination is Eichenheim, but it'll be a six-day trek. Keep your wits about you; danger lurks in every shadow, and the road, though unchanging, holds its own perils."*

## Caravan Journey
Gruffen Ironstride will point the players to the final wagon in the caravan. Once the players climb inside read the following
*With a resounding bellow from Gruffen Ironstride, the caravan lurches into motion, the creaking wheels of the laden wagons marking the beginning of your journey. Seated among the cargo, you feel each bump and jostle as the convoy navigates the uneven terrain, the rhythmic clatter of hooves and the distant murmur of the mountain wind becoming the soundtrack to your venture into the unknown. As the wagons sway and jolt, you can't help but brace yourself, knowing that the true challenges of Märchenweltgrenze await just beyond the looming mountain pass.*
Then use this time for the players to introduce themselves to each other

### Goblin Ambush

The once serene journey takes a chaotic turn as the goblin raiders, driven by their insatiable greed and cunning, launch a surprise attack on the caravan. Your party, now the first line of defense, must protect the precious cargo at all costs. The caravan guards, seasoned but outnumbered, rally to your side as the clash begins.

#### Enemies:
- **12 Goblin Raiders (MM 166):** AC 15, HP 7 (2d6), Speed 30 ft.
  - *Armed with rusty scimitars and shortbows, these goblins exhibit a feral cunning as they swarm from the shadows.*

#### Allies:
- **4 Caravan Guards (Guard Stat Block - MM 347):** AC 16, HP 11 (2d8 + 2), Speed 30 ft.
- ** 1 Vetrean (Vetrean Stat Block - MM 347): AC 18, HP 52 (8d8 + 16), Speed 30 ft.
gth and experience among the caravan guards.
#### Terrain:
- **Wooded Area:** The combat takes place on a narrow mountain road flanked by dense forests. Players can use the trees for cover or attempt stealth maneuvers.

#### Victory Conditions:
- **Protect the Caravan:** Ensure that at least half of the wagons and the majority of caravan guards survive the encounter.
- **Defeat the Goblins:** Drive off or defeat the goblin raiders to secure the safety of the caravan.


### the vista
The caravan makes camp for the night. Then read the following

*As the next day dawns, the caravan, having weathered the tumultuous goblin ambush, advances cautiously towards the border of the Märchenweltgrenze. The once jubilant atmosphere has given way to an edgy tension, and the caravan guards exchange wary glances as they traverse the narrowing mountain pass. As you approach the border, the air becomes charged with an otherworldly energy, and the very atmosphere seems to shimmer with enchantment.*

*Crossing the border is an experience that sends a shiver down your spine. The landscape transforms as if a veil has been lifted, revealing an ethereal beauty that borders on the surreal. The trees are taller, their branches adorned with luminescent flora, and the air is alive with the gentle hum of arcane energy. The road ahead, while well-defined, appears to wind through a realm untouched by the rules of the material world. It becomes evident that Märchenweltgrenze is both enchanting and perilous, a place where the mundane and the magical coalesce in an intricate dance.*


## The Weary Pixie's Plea

*As you travel down the road, a sudden flurry of movement catches your eye. A small figure, no taller than a handspan, darts through the air in a desperate attempt to escape an unseen pursuer. With a sudden and comical crash, the pixie collides headlong into the face of one of your party members, her luminous wings scattering glittering motes in the air.Startled and disoriented, she quickly recovers, hovering in place with weariness etched across her delicate features. The once vibrant glow of her wings now flickers dimly.*

The pixie is shock babbling about goblins and suffering from two levels of exhaustion. If the players give the pixie food and water she calms down enough to be lucid. Read the following.
 "*I'm Tindra," she says, fluttering weakly. "I was fleeing the goblins who attacked my village. They are rounding up pixies and sprites for some reason"*
 If the players reveal they are on their way to Eichenheim village she perks up and asks to join them. Mistress Ironraisin is a powerful witch, she should be safe there.

## The Goblin Hunting Expedition
The rest of the day is uneventful until evening.  Ask the players to describe how they are passing the time in the caravan. When each player described how they spent the day read the followind
 *As the sun begins its descent, casting long shadows upon the winding path through Märchenweltgrenze, the air tingles with the delicate hum of magical energies. The day before you are to reach Eichenheim, a sense of foreboding envelops the party. The tranquility of the feywild is shattered as a group of pixies, panic-stricken, dart across the road before you. Without warning, the underbrush rustles, revealing the ominous figures of goblins, their nets poised for capture, led by a towering bugbear carrying cages filled with imprisoned pixies.*

### Enemies:
- **5 Goblin Raiders (MM 166):** AC 15, HP 7 (2d6), Speed 30 ft.
  - *Equipped with nets and crude weapons, these goblins are determined to capture the fleeing pixies.*
- **1 Bugbear Leader (MM 33):** AC 16, HP 27 (5d8 + 5), Speed 30 ft.
  - *The towering bugbear, adorned with trophies of past victories, leads the hunting expedition with a cruel glint in its eye.*

### Allies:
- **2 Caravan Guards (Guard Stat Block - MM 347):** AC 16, HP 11 (2d8 + 2), Speed 30 ft.
  - *The caravan guards, alerted by the commotion, join the fray, wielding swords to defend against the goblin incursion.*
- **Tindra** If she joined the players she wTindraill help the players free her friends. She uses the pixie stat block
### Terrain:
- **Forested Area:** The encounter takes place in a section of Märchenweltgrenze where the road is flanked by dense woods. Players can utilize the trees for cover or strategic positioning.

### Tactics:
- The goblins focus on capturing pixies, using their nets to restrain them and quickly load them into cages carried by the bugbear.
- The bugbear, employing its brutish strength, targets the players and caravan guards to create an opening for the goblins to complete their capture.
- The pixies, though terrified, may attempt to break free from the cages or use magical abilities to aid the players.
if the player manages to free the pixies they show gratitude for their rescue. They offer to help the caravan to set up their campsite for the evening.

## Eichenheim Under Siege

The next day the caravan reaches the town of Eichenheim
*The once quaint village of Eichenheim, nestled within the heart of Märchenweltgrenze, now reveals itself as a battleground. As you approach, the air thickens with tension, and the rhythmic thud of war drums reverberates through the air. Eichenheim, once a haven of peace, is now under siege by a formidable force of goblins and hobgoblins. From the chaos emerges a small detachment, led by a cunning hobgoblin, hell-bent on confronting you and the caravan.*



The fortified walls of Eichenheim loom ahead, shrouded in the haze of battle. A cacophony of clashing weapons, war cries, and the distant screams of villagers paint a grim picture. The village, now a battleground, is besieged by a massive force of goblins and hobgoblins. As you approach, a detachment breaks off from the chaos, led by a stern-looking hobgoblin with battle-hardened eyes.

### Enemies:
- **4 Goblin Raiders (MM 166):** AC 15, HP 7 (2d6), Speed 30 ft.
  - *These goblins, armed with crude weapons and fueled by ferocity, engage the players with reckless abandon.*
- **2 Hobgoblin Soldiers (MM 186):** AC 18, HP 11 (2d8 + 2), Speed 30 ft.
  - *Disciplined and strategic, the hobgoblins serve as the backbone of the detachment, coordinating attacks with military precision.*

### Allies:
- **Villagers (Commoner Stat Block - MM 345):** AC 10, HP 4 (1d8), Speed 30 ft.
  - *The desperate villagers, armed with improvised weapons, join the fray in defense of their home.*

### Terrain:
- **Siege Setting:** The encounter takes place in the outskirts of Eichenheim, with scattered barricades and damaged structures. Players can use cover strategically or engage in guerrilla tactics.

### Tactics:
- The goblins employ hit-and-run tactics, attempting to overwhelm the players with their sheer numbers.
- The hobgoblins coordinate attacks, focusing on the most threatening targets and exploiting any weaknesses in the party's defenses.
- Villagers fight alongside the players, providing support and acting as a buffer against the relentless goblin onslaught.

The goblins flee after the hobgoblins are slain. Read the following after the battle has ceased
*As the echoes of clashing steel fade and the goblins beat a hasty retreat, a brief respite settles over the besieged village of Eichenheim. Amidst the lingering tension, a figure on horseback emerges from the chaos, riding towards you with purpose. Sir Alaric Stormblade, a knight of noble bearing and regal armor, introduces himself as the leader of the expedition into Märchenweltgrenze. He commends your prowess in battle, extending a formal welcome to join the ranks of the expedition. With a gesture, Sir Alaric directs you towards the camp just outside the protective walls of the village.*


# Meeting Agatha Ironraisin
When the players have taken their spot in the camp read the following aloud.
*As you begin to settle into the makeshift camp just outside the protective walls of Eichenheim, a figure ambles towards you. In a pair of oversized overalls and a slightly stained white shirt, the bugbear's appearance is far from menacing. He pauses in front of your group, a friendly but vacant expression on his face.*

*"*G'mornin', folks! I'm Stein," *he says with a toothy grin. His voice is surprisingly genial for a bugbear. *"*Agatha Ironraisin, she's the witch and all, she sent me over. Said to tell ya she's got a spot of tea ready over at her place. Real nice-like. Wants to chat with ya, she does.*"

*Stein scratches his head, looking a bit perplexed as he adds,* "Tea's good. Real good. Agatha, she's nice. You should come. Yep."

*With a nod that seems a touch too enthusiastic, Stein gestures vaguely towards the direction of Agatha's house and adds,* "This way for tea. C'mon now, don't dawdle. Tea time, ya know."
Agatha house is easy to find it is carved into the massive tree in the center of the village. As they approach read the followind.

**Agatha Ironraisin's Enchanted Abode:**
*Nestled at the heart of the village of Eichenheim, Agatha Ironraisin's home is an extraordinary sanctuary carved into the ancient boughs of a colossal oak tree that looms majestically over the village center.As you approach, the natural curvature of the oak's trunk reveals an entrance adorned with ivy and small, glowing orbs that flicker like fireflies. Awakened robins, Agatha's familiars and loyal companions, flit about the exterior, their melodious chirps adding to the ambient enchantment. The interior of Agatha's abode is a cozy and warm haven, with wooden walls adorned with mystical sigils and shelves displaying curious artifacts. The air within is infused with the comforting aroma of brewing potions,baking cookies and herbs, creating an inviting atmosphere.*

When the players knock on the door it swings open on its own. Inside the players can hear a sweet melodic voice belonging to an older woman, she tells them to make themselves comfortable in the parlor while she gets the tea ready. If the players dawdle the voice then asks if they are going to stand around outside her door like fools, or are they going to come in. THe inside of Agatha's house is furnished in the style of the archetypical grandmothers house. The parlor is directly to the left of the door the players entered through.
Give the players time to explore Agatha's parlor. She has a large collection of various nicknacks and pictures. Describe in detail anything that gets the players fancy. Most of the portriats are of an older elf woman in the classic witches attire and various happy younger people, some of them witches and some of them not. After the players have explored for a while read the following

![Agatha Ironraisin](./dreamup_creation_by_botninja_dgk2ccu-pre.jpg)

*As you settle into the parlor, the door creaks open, revealing Agatha Ironraisin with a tea cart laden with a delightful assortment of cookies. The kindly, plump woman beams at you with warmth in her eyes, and her gentle aura fills the room with a sense of comfort.*

*"Ah, dear ones, so glad you could join an old witch for tea,"* she says, her voice a melodic blend of kindness and wisdom. *Her eyes twinkle as she distributes saucers and delicate teacups, each piece emanating an air of antiquity and magic. The aroma of freshly brewed tea intermingles with the sweet scent of the cookies, creating an irresistible atmosphere of hospitality.*

*Agatha takes a seat, her demeanor inviting and sincere. With a flourish, she pours tea into each cup, her gaze shifting from one player to another as she says,* "Now, take a moment. Sip your tea, enjoy a cookie or two. We've much to discuss, and I'm sure you've questions of your own. No rush, my dears. Let the magic of the moment unfold like the petals of a blossoming flower."

When the players have taken their seat Agatha then starts to ask the players questions. They will mostly be questions about their past and why they come to the region. If they ask question to her she will give noncommital, cryptic answers or respond with a question. A player who makes an insight check dc15 will get the feel that she is trying to get a feel for them as heroes. After about 7 minutes caasual chitchat read the following.

*As you sit in Agatha's charming parlor, the air thick with the aroma of tea and cookies, she leans forward with a thoughtful expression. A glimmer of something more regal briefly touches her eyes as she speaks.*

*"My dears,"* Agatha begins, her voice taking on a serious tone, *"I know where the goblins have set their roots, their citadel in the Silverberg. But, I worry for you. You're not quite ready for such a confrontation. Instead, I'd suggest visiting some of the other settlements scattered across the Great Valley."*

*Agatha retrieves a weathered parchment, her fingers tracing lines that mark the locations of neighboring hamlets and towns.* *"There are places where the wind whispers of troubles, and my heart aches for the lack of news. Visit these settlements, unravel their stories. Learn and grow, for the path ahead is fraught with challenges."*

*She hands you the map, caution in her eyes.* *"Remember, the wilds outside our haven have a way of shifting and folding, like pages in an old book. I can give you only the rough locations. Trust in the journey, my dears, and may the Summer Court guide you through these unfolding tales."*
Along with the map is a few handwritten letters. One allows them to get two mules from the trading post, the second is a letter of introduction and the thrid is a personal letter to the players with details of their lives that they did not reveal to the agatha and promise that their future will have a great reward.

### THe hexcrawl
![the great valley](thegreatvalley.jpg)
**Hex Crawl Rules in Märchenweltgrenze:**

In the enchanted expanse of Märchenweltgrenze, where the boundary between the material world and the feywild blurs, the very fabric of reality is subject to whimsical fluctuations. Navigating this mystical terrain demands not only courage but an attunement to the capricious nature of the fey.

1. **Survival Checks:**
   - When venturing through a hex, each player must make a Survival check. The DC is determined by the Dungeon Master based on the following table.

   Certainly! Below is a table of Survival Checks for navigating the various biomes in Märchenweltgrenze:

**Survival Check Table: Märchenweltgrenze Biomes**

| Biome              | DC Range | Description                                                            |
|--------------------|----------|------------------------------------------------------------------------|
| Verdant Woodlands  | 8-12     | Dense foliage, hidden trails, and occasional illusions.                |
| Great Swamp        | 10-15    | Murky waters, twisting paths, and deceptive reflections.               |
| Fungal Forest      | 12-16    | Enchanted mushrooms, glowing spores, and fairy circles.                |
| Silverspire Mountains | 14-18  | Treacherous slopes, unpredictable weather, and fey creatures.          |

**Survival Check Resolution:**

- **Success (DC or higher):** The party navigates the biome without incident, making progress toward their destination.
  
- **Failure (Below DC):** The party becomes loss (see bellow)
2. **Lost Table:**
   - When the players get loss role on the following table

   - **Consequences of Getting Lost Table (d6):**

    1. **Random Encounter:**
      - The party stumbles upon a random encounter appropriate to the biome they are in. Roll on the appropriate encounter table.

    2. **Adjacent Hex Exploration:**
      - The party ends up in an adjacent hex instead of the intended one. This may lead to discovering new locations or facing different challenges.

    3. **Time Distortion:**
      - A temporal disturbance causes the party to lose a significant amount of time. Consider rolling a d4 to determine the number of hours lost.

    4. **Enchanted Path:**
      - The party finds a mysterious, enchanted path that leads them through a scenic detour. While visually appealing, it adds extra time to their journey.

    5. **Fey Guidance:**
      - The party encounters a helpful fey creature that guides them back on track. However, this encounter may have additional consequences.

    6. **Hidden Wonders:**
      - The party stumbles upon a hidden and magical location in the hex they entered, presenting an opportunity for discovery or a potential challenge.


3. **Magical Delays:**
   - A significant failure could attract the attention of the feywild's enchantments, causing temporal distortions or brief dimensional shifts.
   - The party might experience time dilation, leading to unforeseen delays in reaching their destination.

4. **Navigational Aids:**
   - Certain magical items or fey-touched guides may provide advantage on Survival checks or offer protection against the whims of the feywild.
   - Wise use of fey-influenced landmarks or guidance from knowledgeable locals can also aid the party in navigating the hexes.

5. **Environmental Changes:**
   - Märchenweltgrenze is known for its ever-changing landscape. The Dungeon Master can introduce alterations to the environment, such as sudden thickets, ephemeral bridges, or ephemeral meadows, adding an element of unpredictability to the journey.

6. **Random Encounters**
- At the start of each day of travel or each hex entered, roll a d20. then role a d6 to deterime what kind of encounter it is. See the appendix for the encounter tables
- **Roll Result:**
   - **Roll of 16-20:** Random Encounter Occurs
   - **Roll of 1-15:** No Random Encounter


# Unique encounters of the hexcrawl
## Grumblegut's Vengeance

As the players venture through the Verdant Woodlands, they stumble upon a clearing near the river. There, they find an unusual sight—an ogre sharpening a massive axe with an air of determination. Grumblegut, significantly smarter than the average ogre with an intellect of 9, looks up as the party approaches, muttering ominously about getting those "damned goblins."

*Description:*

Grumblegut, sporting a thick English factory worker accent, regards the players with a mix of frustration and determination. *"Oi there! Them sneaky goblins took all me tools. Ain't gonna stand fer it. Need some clever folk to 'elp Grumblegut find 'em. Will ya lend a hand, eh?"*

*Encounter:*

1. **Investigation Check (DC 12):** Players can inspect the surroundings to find subtle goblin tracks leading away from Grumblegut's makeshift home. Success reveals the general direction the goblins went.

2. **Survival Check (DC 14):** By studying the terrain, players can track the goblins more effectively. Success grants a clearer understanding of the goblins' movements, potentially avoiding pitfalls.

3. **Persuasion Check (DC 10):** Players can try to calm Grumblegut, reassuring him that they'll handle the goblin situation. Success boosts Grumblegut's morale, providing advantage on subsequent skill checks.

*Easy Combat Encounter:*

Following the trail, the players locate the goblin hideout—a makeshift camp near the riverbank. The goblins, caught off guard, are initially unaware of the approaching party.

- *Goblin Ambush:* 3 goblins, armed with stolen tools-turned-improvised weapons, are startled as the players close in. They fight fiercely to protect their ill-gotten gains.

*Possible Outcomes:*

1. **Swift Victory:** The players defeat the goblins without much trouble, reclaiming Grumblegut's tools. Grumblegut, grateful for their assistance, offers a unique item or imparts knowledge about the feywild-infused woods.

2. **Goblin Negotiation:** The players, displaying prowess, convince the goblins to return the stolen tools without resorting to combat. Grumblegut is surprised but impressed by the party's diplomatic skills.

3. **Goblin Reinforcements:** Mid-combat, a group of goblin reinforcements arrives, making the encounter more challenging. Grumblegut, appreciating the party's bravery, lends his strength to ensure victory.

*Rewards:*

Upon success, Grumblegut rewards the players with a handcrafted wooden charm that provides a minor bonus to nature-related checks or a map leading to a hidden fey enclave. Grumblegut becomes a friendly contact, offering his unique perspective on the Verdant Woodlands and potential future aid.



**Unique Encounter: The Weary Unicorn's Flight**

As the players stroll along the serene shores of the Great Lake, their peaceful journey is abruptly disrupted by a chaotic scene unfolding before them. A majestic unicorn, her coat stained with blood, gallops desperately along the waterline, pursued by a horde of cackling goblins. The unicorn appears wounded and exhausted, her once-gleaming horn now dull and cracked.

*Description:*

The players witness the unicorn's plight from a distance, and it's clear that she won't be able to outrun her relentless pursuers for much longer. The goblins, armed with crude weapons and wicked grins, show no mercy in their pursuit.

*"Help me, kind beings! The woods have become treacherous, and my strength wanes. Aid me, and I shall share with you the secrets of my sanctuary,"* the unicorn pleads telepathically to the players' minds.

*Encounter:*

1. **Intervention Decision:** Players can choose to intervene and assist the unicorn or opt to observe from afar. The goblins are a considerable threat, and aiding the unicorn may bring future benefits.

2. **Chase Scene Skill Challenge:** If players choose to intervene, initiate a skill challenge to outpace and outmaneuver the goblins in the chase. Skills such as Athletics, Acrobatics, or Animal Handling can be employed to navigate the rugged terrain.

*Combat Encounter:*

- *Goblin Pursuers:* 6 goblins relentlessly chase the unicorn. Their determination makes them a formidable adversary.

Furthermore Starlight is wounded and almost ready to collapse from exhaustion make the following changes to her stats

### Adjusted Unicorn Starlight (Exhausted)

- **Hit Points (HP):** Reduce Starlight's maximum hit points by half to reflect her weakened state from days of relentless pursuit.

- **Speed:** Reduce Starlight's speed by 10 feet. The exhaustion has taken a toll on her swiftness.

- **Ability Scores:** Reduce Starlight's Strength, Dexterity, and Constitution scores by 2 each. The extended chase has left her physically and mentally drained.

- **Horn Attack:** Starlight's horn attack, while still magical, is less potent due to her exhaustion. Reduce the damage by 1d6.

- **Innate Spellcasting:** If Starlight has innate spellcasting abilities, consider reducing the number of spells available or the spell levels by one. The continuous use of magical abilities has strained her.

- **Magical Resistance:** If Starlight has magical resistance, reduce it to advantage on saving throws against being charmed or frightened. The exhaustion 

*Possible Outcomes:*

1. **Successful Intervention:** If the players successfully intervene and defeat the goblin pursuers, the grateful unicorn, whose name is Starlight, offers her sincere thanks. She shares the location of her sanctuary and offers to aid the players in future combats around the lake with a chance of summoning her ethereal presence.

2. **Partial Success:** The players manage to fend off some of the goblins, allowing the unicorn to escape. Starlight, weakened but grateful, shares her sanctuary's location as a token of appreciation.

3. **Failure:** The players are unable to prevent the goblins from catching up to the unicorn. Starlight is captured, and the goblins vanish into the woods. The players may have to track down the goblins and rescue the unicorn at a later time.

*Rewards:*

Upon success, Starlight shares the location of her sanctuary, a serene grove hidden deep within the Verdant Woodlands. She expresses her gratitude and promises the players her ethereal aid during future combats around the lake, providing a small chance of her intervening when the players face challenges in the region. The sanctuary may also contain valuable resources or feywild-related lore.
If the players take the compassionate initiative to heal Starlight during or after the encounter, her gratitude deepens, and she bestows an additional reward in the form of a favor. This favor can be called upon by the players at any time. Here are the details:

## Orphaned Wyrmling in the Silver Dragon's Lair

As the players explore the interior of the lair, a somber sight greets them—rampaged treasure, signs of a fierce struggle, and the lifeless form of a young silver dragon. However, their attention is drawn to a subtle whimpering sound emanating from a concealed crevasse. Upon closer inspection, the source becomes evident: a small, weeks-old silver dragon wyrmling.

*Description:*

The wyrmling, curled up in the protective embrace of the crevasse, blinks its large, innocent eyes at the approaching party. Freshly hatched eggshell fragments surround the nest, revealing the tragic fate of its mother. The wyrmling, though visibly frightened, manages a small, cautious tail wag upon sensing the players' presence. As the players discover the orphaned silver dragon wyrmling in the ransacked lair, they decide to bestow upon her the name "Talia." Through compassionate efforts and gentle gestures, the players manage to befriend the frightened wyrmling. As trust blossoms, Talia reveals a shiny trinket she managed to conceal from the goblins—a seemingly ordinary bauble that she believes holds no significance.

*Description:*

Talia, now with a name, responds to the players' kindness with grateful tail wags and a hint of curiosity. In broken Draconic, she conveys her gratitude and points to a small, shiny trinket nestled among the wreckage of her mother's lair.

*"Shiny... kept safe... goblins not find..."* Talia stammers, presenting the magical item to the players.

*Expanded Outcomes:*

1. **Befriending Talia:**
   - *Gaining Trust:* Through acts of kindness, the players gain Talia's trust, making her more receptive to their companionship.

   - *Communication Improvement:* As trust grows, Talia's ability to communicate in Draconic becomes clearer, facilitating interactions.

   - *Talia's Gift:* If the players successfully befriend Talia, she gifts them with a small, shiny item that she managed to keep hidden—a **Ring of Feather Falling**. Unaware of its magical properties, Talia believes it to be an ordinary bauble.

2. **Adopting Talia:**
   - *Magical Collar Arrival:* If the players decide to adopt Talia, a few hours later, Agatha's awakened songbirds arrive, carrying a magical collar. When placed on Talia, it magically adjusts her size to that of a kitten, making her more manageable as a companion.

   - *Bonding with Agatha's Familiars:* The awakened songbirds, serving as messengers for Agatha, convey her blessings and express delight in the players' decision to care for Talia. The magical collar is a thoughtful gift from the elderly witch.

   - *Potential Assistance from Agatha:* The players earn Agatha's favor, and she may be more inclined to provide assistance or information in the future.

*Challenges and Quests:*

1. **Identifying the Magic Item:**
   - *Arcane Knowledge Quest:* The players might need to embark on a quest to identify the true nature of the magical item Talia gave them. This could involve seeking out a knowledgeable mage or magical expert.

   - *Potential Side Quest:* Unraveling the mysteries of the magical item could lead to a side quest that unveils more about the dragon's lair, its connection to the feywild, or the origin of such magical artifacts.

2. **Caring for Talia:**
   - *Feeding and Nurturing:* Caring for a wyrmling involves finding suitable food and providing a safe environment. The players may need to explore the Great Valley to secure nourishment for Talia.

   - *Building Bonds:* Strengthening the bond with Talia requires ongoing care and attention. Talia may develop unique abilities or become a valuable companion as the players continue to nurture her.

*Rewards:*

Befriending Talia grants the players a **Ring of Feather Falling**, a magical item with potential utility. Adopting Talia, along with the magical collar, ensures a loyal and manageable companion. Agatha's favor may prove beneficial in future encounters or quests within the Märchenweltgrenze. The expanded encounter provides a mix of emotional depth, magical discovery, and potential ongoing quests.


**Encounter: The Enigmatic Merchant - Oni in Disguise**

As the players journey through the Great Valley, they unexpectedly come across a mesmerizing sight—a man of middle-eastern descent seated on an ornate carpet, surrounded by an array of magical items. The merchant, appearing friendly and approachable, welcomes the players with a warm smile.

*Description:*

The merchant, who introduces himself as Farid, is seated on a large, intricately designed flying carpet that hovers a few feet above the ground. His exotic wares, laid out on a vibrant cloth beside him, gleam with magical allure. Farid's appearance suggests a benign demeanor, and he gestures gracefully for the players to peruse his magical inventory.

*"Greetings, travelers! I am Farid, purveyor of wonders from distant lands. Behold, the treasures that may pique your interests!"*

*Magic Items for Sale (Uncommon):*

1. **Cloak of Elvenkind:** 300 gp
2. **Gloves of Thievery:** 300 gp
3. **Headband of Intellect:** 350 gp
4. **Pearl of Power:** 400 gp
5. **Ring of Jumping:** 250 gp
6. **Ring of Mind Shielding:** 350 gp
7. **Slippers of Spider Climbing:** 350 gp
8. **Wand of Magic Missiles (7 charges):** 400 gp
9. **Wand of Web (5 charges):** 300 gp
10. **Winged Boots:** 400 gp
11. **Cloak of Protection:** 350 gp
12. **Broom of Flying:** 450 gp

*Pricing Note:* All prices are at a slight discount, reflecting Farid's willingness to make enchanting deals.

*Interaction:*

1. **Buying Items:**
   - Players can purchase any of the listed magic items at the provided prices. Successful transactions result in Farid bidding them farewell and gracefully soaring into the sky on his flying carpet.
   - If players inquire about a specific item not listed, Farid expresses regret but mentions he only carries a limited selection.

2. **Selling Items:**
   - Farid offers to buy any magic items the players no longer desire. The buying prices are reasonable, allowing players to exchange unwanted items for gold.
   - The transaction adds to the intrigue, as Farid seems genuinely interested in the unique qualities of each magical artifact.

3. **Oni's True Nature:**
   - Perceptive players or those with a high passive Insight might notice subtle hints or irregularities in Farid's behavior, revealing his true nature as an oni in disguise.
   - Discerning the truth about Farid could lead to additional roleplaying opportunities or alter the players' approach to the encounter.

*Farewell:*
   
After a successful transaction or interaction, Farid bids the players a cryptic farewell, gracefully mounting his flying carpet. With a nod and a wave, he ascends into the sky, disappearing among the clouds. The encounter leaves the players with both magical treasures and a lingering sense of mystery surrounding the enigmatic merchant.

**Encounter: The Awakened Assembly - A Wolf's Predicament**

As the players traverse the Great Valley, they stumble upon an unusual gathering of chatty animals surrounding a trapped wolf. The wolf, suspended from a goblin deer snare, dangles helplessly, while an assembly of awakened birds, squirrels, and rabbits exchanges banter and somewhat judgmental comments.

*Description:*

The scene unfolds with a wolf entangled in the snare, his leg suspended several feet above the ground. Awakened birds, squirrels, and rabbits, displaying a surprisingly diverse range of personalities, form a loose circle around the trapped predator. Their discussions about the wolf's character and his apparent comeuppance are sprinkled with snark and disapproval.

*"Well, well, look who's gotten himself into a bit of a pickle. Karma's a funny thing, isn't it?"* chimes a particularly outspoken squirrel.

*Interaction:*

1. **Wolf's Plea:**
   - The trapped wolf, struggling and somewhat humbled, pleads with the players to free him. He insists that he has changed his ways and that this predicament is a result of a misunderstanding.

2. **Awakened Assembly:**
   - The other awakened animals exhibit various reactions. Some show indifference, continuing their gossip about the wolf's past behavior, while others express hostility toward the players for considering aiding the wolf.

3. **Player's Decision:**
   - Players can choose to release the wolf, using their skills or tools to free him from the snare. This action may influence the attitudes of both the wolf and the awakened assembly.

*Possible Outcomes:*

1. **Freeing the Wolf:**
   - If the players decide to release the wolf, he expresses gratitude and promises to amend his ways. The awakened assembly reacts with a mix of disdain and curiosity, as the wolf slinks away, perhaps with newfound humility.

2. **Leaving the Wolf:**
   - Players may choose to leave the wolf trapped, allowing the awakened assembly's judgment to unfold. This decision might lead to repercussions or alter the dynamics of future encounters with awakened creatures.

3. **Negotiation:**
   - Diplomatically engaging with the awakened assembly might provide an opportunity to convince them that the wolf has genuinely changed. This approach could lead to a more harmonious resolution.

*Challenges and Roleplaying:*

1. **Convincing the Assembly:**
   - Persuasion checks or charismatic roleplaying may sway the awakened assembly's opinion about the wolf. Players can attempt to showcase the wolf's remorse and desire for redemption.

2. **Dealing with Hostility:**
   - If the players choose to free the wolf, some awakened creatures may become openly hostile. Players may need to navigate potential conflicts or find diplomatic solutions.

3. **Unraveling the Wolf's Story:**
   - Investigating the wolf's claims of change could lead to uncovering the reasons behind his past behavior. This revelation may affect the players' judgment and interactions with awakened creatures.

*Rewards:*
If the players choose to release the wolf and navigate the dynamics of the awakened assembly, they earn a unique reward. This reward manifests as an increased likelihood of encountering awakened animals in the Great Valley, fostering potential alliances and sources of information. Here's the updated reward:

*Revised Reward:*

1. **Awakened Allies:**
   - After freeing the wolf and demonstrating their compassion, the players find that a magical aura surrounds them, attracting the attention of awakened creatures in the Great Valley.

2. **Favorable Encounters:**
   - Anytime the players encounter a small animal in the Great Valley, there's a 50/50 chance that it is an awakened creature. These creatures are more inclined to approach the players peacefully and may offer information or assistance.

3. **Communication and Aid:**
   - Players can approach awakened animals and, through gentle communication or gestures, seek information about the area or request their aid in various ways.

*Examples of Interactions:*

   - A rabbit may hop over and guide the players to a hidden trail.
   - A squirrel might chitter excitedly, warning the players of nearby dangers.
   - Birds could fly overhead, creating a temporary distraction to assist the players in avoiding a potential threat.

*Roleplaying Note:*

   - The players' actions in the awakened wolf encounter have established a positive reputation among the awakened creatures of the Great Valley. This reputation allows for more harmonious interactions, opening avenues for alliances and support from these magical denizens.

*Challenges:*

   - Players may need to discern when to approach awakened creatures, ensuring that their intentions are perceived as friendly rather than threatening.
   - Communication with awakened animals may require creative roleplaying or skill checks, reflecting the players' ability to understand and convey their intentions.

*Potential Story Hooks:*

   - Awakened animals might share rumors or insights about the activities of goblins and other creatures in the region, providing valuable information for the players' quest.
   - The players' reputation with awakened creatures could attract the attention of a more powerful fey entity, leading to additional quests or alliances.

**Encounter: The Feline Monarchy - The Coronation of the Cat King**

The players, meandering through the Great Valley, chance upon a peculiar sight—a regal funeral procession of awakened cats. The feline dignitaries, somber in their procession, approach the players with a curious message. They task the players with delivering news of a royal demise to a person with an odd name. Unbeknownst to the players, this leads to an unexpected venture into the whimsical world of feline monarchy.

*Description:*

The players find themselves in the midst of a funeral procession, where cats of various breeds and sizes march solemnly. Dressed in tiny regal attire, the leading cat approaches, its voice strangely articulate.

*"Hail, travelers. We bear unfortunate tidings of a royal passing. Seek out a person with an odd name and relay this message: 'The illustrious Sir Whiskerworth mourns the loss of Princess Fluffington.' May your journey be swift and respectful."*

*Interaction:*

1. **Locating Oddly Named Individuals:**
   - The players must embark on a quest to find a person with an odd name (Sir Whiskerworth) and inform them of the royal demise of another character with an equally odd name (Princess Fluffington).

2. **The Cat Monarchy Unveiled:**
   - Upon reaching the designated settlement, the players discover that Sir Whiskerworth is, in fact, a house cat. The feline claims that Princess Fluffington, another cat, has met her regal end, and as the surviving cat, Sir Whiskerworth now ascends to the throne as the King of the Cats.

3. **Return to the Funeral Procession:**
   - If the players choose to return to the site of the funeral procession, they find Sir Whiskerworth, now adorned in royal regalia, seated atop a makeshift throne. The cat monarch acknowledges the players and commends them for delivering the news.

*Rewards:*

   - **Charm for Deed:** Sir Whiskerworth, now the self-proclaimed Cat King, presents the players with a small, enchanted charm as a token of gratitude. The charm is akin to those found on DMG page 228 and may hold magical properties, providing a small bonus or advantage in future feline-related encounters.

*Roleplaying Opportunities:*

   - **The Royal Cat's Demeanor:** The Cat King, Sir Whiskerworth, might exhibit peculiar feline behaviors and expressions. Engaging in dialogue with the monarch can lead to amusing exchanges and insights into the whimsical world of the cat monarchy.

   - **Further Quests:** Sir Whiskerworth may enlist the players for additional tasks or adventures within the Great Valley, presenting an opportunity for comical or fantastical endeavors.



## Defending Oakheart's Sacred Grove
As the players journey through the Great Valley, they stumble upon a magnificent grove, bathed in the ethereal glow of fey magic. The ancient treant druid, Oakheart, stands resolute in defense of his sacred sanctuary against a relentless onslaught of goblin invaders.

*Introduction:*

The air crackles with tension as the players approach the grove. The ground shakes beneath the thunderous footsteps of Oakheart, who valiantly battles against a large force of goblins attempting to desecrate his sacred domain.

*"Brave wanderers, heed my plea!"* Oakheart's voice rumbles like distant thunder. *"The malevolent horde seeks to defile this hallowed grove. Will you lend your strength to protect the sanctity of nature?"*

*Encounter: The Battle for the Grove:*

1. **Goblin Onslaught:**
   - A horde of around 10 goblins, led by a hobgoblin sergent, swarms the grove. The players can choose to join Oakheart in the defense or observe from a distance.

2. **Joining the Fight:**
   - If the players choose to engage, the battle unfolds amid the towering trees and shimmering magic of the grove. Oakheart, with his immense strength and nature-based powers, fights alongside the players to repel the goblin invaders.

3. **Outcome:**
   - The battle's outcome depends on the players' actions and strategies. Victory ensures the sanctity of the grove, while defeat brings the risk of corruption and desolation.

*Reward: Oakheart's Gratitude:*

After the battle, Oakheart, his branches still bearing the scars of combat, expresses his gratitude to the players for their assistance in preserving the sacred grove. In appreciation, he presents them with a magical item salvaged from the fey energies of the grove.

*Magic Item Reward:*

- **Cloak of Elvenkind (DMG, Page 158):**
   - Oakheart gifts the players a wondrous Cloak of Elvenkind, enchanted with the essence of the feywild. This magical garment grants its wearer enhanced stealth and agility, aligning with the grace of the elven folk.

*Roleplaying Opportunities:*

- **Oakheart's Wisdom:**
   - Oakheart shares insights into the delicate balance of nature and the encroaching threats faced by the feywild-infused region. He may provide guidance or lore relevant to the players' quest.

- **Connection to the Grove:**
   - The players, having aided in the grove's defense, may establish a bond with Oakheart and gain a potential ally in their future endeavors within the Märchenweltgrenze.

*Challenges:*

- **Strategic Decision-Making:**
   - Players must choose whether to join the fight, strategize their approach, and decide which areas of the grove to defend against the goblin onslaught.

- **Hobgoblin Commander:**
   - The hobgoblin commander poses a formidable challenge. Players may need to devise tactics to overcome his leadership and martial prowess.

This final encounter, steeped in the magic of the feywild, offers the players a chance to safeguard the essence of nature and gain a valuable magical reward as a token of Oakheart's appreciation.
# The settlements of the great valley

## Eichenheim

Nestled within the enchanting embrace of Märchenweltgrenze, the village of Eichenheim stands as a testament to the delicate balance between the mundane and the magical. With its modest size and pastoral charm, Eichenheim exudes an atmosphere of tranquility, even as the feywild intrudes into the material realm.

### **Key Locations:**

1. **Trading Post and Inn: The Gilded Griffin**
   - A hub of activity, The Gilded Griffin serves as both a trading post and an inn. Its sturdy timber walls and warm hearths provide respite for weary travelers. The air is thick with the scent of home-cooked meals, and the innkeeper, a portly man named Otto Goldentongue, welcomes visitors with tales of the feywild.

2. **Chapel of St. Elara**
   - A quaint chapel dedicated to St. Elara, the patron saint of protection. Villagers gather here for solace and reflection. The chapel's modest stained glass windows cast kaleidoscopic hues across the wooden pews, creating a serene space for contemplation.

3. **Blacksmith's Forge: Ironheart Smithy**
   - Run by the skilled blacksmith Greta Ironheart, this forge produces sturdy tools and weapons essential for both farming and defense. The rhythmic clang of hammer on anvil is a constant in Eichenheim, a testament to the village's self-sufficiency.

4. **Agatha Ironraisin's House**
   - At the heart of the village stands the home of Agatha Ironraisin, the beloved town elder and a kindly witch. Her cottage, adorned with whimsical charms and herbs drying in the eaves, exudes an air of enchantment. Villagers seek her guidance, and her garden yields magical herbs that aid both healing and protection.

### **Livelihood and Community:**

Eichenheim thrives on simplicity and self-sufficiency. The villagers tend to orchards, cultivating apples and other magical fruits that thrive in the feywild-tinged soil. Small vegetable gardens supplement their diet, and skilled hunters venture into the nearby woods, providing fresh game for communal feasts.

### **Village Dynamics:**

- **Close-Knit Community:** The villagers of Eichenheim are bound by a shared respect for the delicate balance between the material world and the feywild. Festivals celebrating both natural and magical harvests strengthen communal bonds.

- **Guidance of Agatha Ironraisin:** As the town elder and resident witch, Agatha is a source of wisdom and guidance. Her magical prowess is employed not just for protection but also for the prosperity of the village.

- **Feywild Influence:** The village's proximity to the feywild imparts a subtle enchantment to daily life. Villagers embrace the quirks and occasional magical occurrences with a mixture of reverence and familiarity.

Eichenheim, with its unassuming beauty and harmonious coexistence with the feywild, serves as a haven of peace in the midst of Märchenweltgrenze's unpredictable landscape.

## Stonehelm Hold
Stonehelm Hold is nestled within the rugged embrace of the Silverspine Mountains, a sturdy bastion against the surrounding peaks. The hold is approximately two days' journey northeast from Eichenheim, concealed amidst towering cliffs and treacherous mountain terrain. The path leading to Stonehelm Hold winds through narrow passes, offering occasional glimpses of the distant valley below.

*Read Aloud Text:*
*As you approach the entrance to Stonehelm Hold, you find yourself standing at the base of the Silverspine Mountains, their majestic peaks reaching towards the sky. Before you lies a towering cliff face, its surface scarred with the marks of dwarven craftsmanship. Large boulders, meticulously arranged, form a protective barrier in front of a stout gate leading into the heart of the mountain. The stillness is broken only by the distant echoes of the mountains.*

**Encounter: Stonehelm Hold's Sealed Gate**

The players, standing before the sealed gate of Stonehelm Hold, notice the formidable defenses in place. To communicate with the hidden dwarven guards, they must shout to be heard amidst the mountain silence.

*Description:*
- The players' voices echo against the cliffs as they call out to Stonehelm Hold. After a few tense moments, a small opening in the cliff face above reveals hidden guards, scrutinizing the strangers below.
- The dwarven guards, wary but attentive, listen to the players' explanations. They can be convinced of the players' peaceful intentions through skillful persuasion or by presenting a token of goodwill.

*Interaction:*
- The players can attempt to convince the dwarven the worguards of their intent to help by providing Agatha's letter or sharing information about the goblin threat.
- The dwarven guards may lower a basket hoist from the concealed opening, allowing the players to ascend and enter Stonehelm Hold.
- Once inside, the players can engage in diplomacy with Thrain Ironshield and the dwarven community, learning about the goblin movements and potentially offering assistance.

*Possible Outcomes:*
1. **Successful Diplomacy:** The players gain the dwarves' trust, learn valuable information about the goblin threat, and may be asked for assistance in reinforcing Stonehelm Hold's defenses.
   
2. **Convincing Display:** The players present Agatha's letter or otherwise convince the dwarven guards of their goodwill, gaining access to Stonehelm Hold without conflict.
If the players have successfuly engaged the sentries there is a few minutes pause in the conversation. Then a section of the wall swings out to reveal a basket hoist. The basket is lowered down to the players. Read the following

*As you ascend the hidden basket hoist, the cool mountain air envelops you, and the rhythmic clinking of chains accompanies your journey upward. The cliff face opens into a concealed entrance, revealing the sturdy interior of Stonehelm Hold. Dim torchlight flickers against the stone walls, guiding you through narrow tunnels until you reach the heart of the dwarven stronghold. Thrain Ironshield, the leader of Stonehelm Hold, awaits you in a chamber adorned with intricate dwarven carvings. The air is thick with the scent of stone and the distant hum of dwarven industry.*

**Encounter: Meeting Thrain Ironshield**

The players find themselves in the presence of Thrain Ironshield, the stalwart leader of Stonehelm Hold. Thrain, seated upon a stone throne adorned with symbols of dwarven craftsmanship, scrutinizes the newcomers with a mix of caution and curiosity.

*Description:*
- Thrain Ironshield, a grizzled dwarf with a braided beard and eyes sharp as the blades forged in his hold, rises to greet the players. His demeanor, though wary, hints at a resilient determination.
- The chamber is a testament to dwarven craftsmanship, with intricate carvings depicting the hold's history and the strength of Silverspine Mountains.
- Thrain listens intently as the players share their purpose and assures them that Stonehelm Hold's sealed defenses are a precaution against the goblin threat.

*Interaction:*
- The players can engage in conversation with Thrain, learning about the goblin movements in the Silverspine Mountains and the dwarves' decision to seal the hold for protection.
- Thrain may request the players' assistance in reinforcing Stonehelm Hold's defenses or gathering information about the goblin forces in the region.
- Successful diplomacy may lead to the The dwarven leader presenting the players with a Dwarven Thrower.

## Serene Summit monestary

*As you approach the Serene Summit Monastery, the once tranquil air is rent with the cacophony of clashing steel and the disciplined movements of martial artists. The sound of goblin war cries pierces the mountain silence, and the desperate pleas of the monks echo through the cliffside. The serenity of the monastery is shattered by the urgency of battle. The martial artists, their movements swift and purposeful, face a relentless onslaught of goblins led by a Hobgoblin warlord. The situation demands your immediate intervention as the monks struggle to hold their ground against overwhelming odds.*

**Combat Encounter: Battle at Serene Summit Monastery**

The players find themselves on the outskirts of a fierce battle, where the Serene Summit Monastery's martial artists defend their sacred home from a goblin onslaught. The monks, disciplined and skilled, fight with a harmonious blend of martial arts styles against the ferocious goblins.

Certainly! Here's a breakdown of the friendly NPC and monster numbers for the combat encounter at the Serene Summit Monastery, designed for 4 level 2 players:

**Friendly NPCs (Martial Artists):**
- **Monks:** 4 skilled martial artists (each with the stats of a Commoner) fighting against the goblins.

**Monsters (Goblin Assault Force):**
- **Goblin Raiders:** 8 goblins (each with the stats of a Goblin).
- **Hobgoblin Warlord:** 1 formidable leader (with the stats of a Hobgoblin Captain or equivalent).
If the players succeed in fighting off the hobgoblins the remaing monks lead the players inside the village to the to the monestary itself. There they find the master monk sitting in the lotus position. Read the following aloud

"Your martial prowess has proven invaluable in our time of need. The Serene Summit Monastery is in your debt. Please, convey to Agatha Ironraisin that though bloodied, we stand, and her call for aid was not in vain."

*The master then motions for the players to accompany him to the edge of the cliff, where the magnificent vista of the Great Valley unfolds. With a sense of purpose, he explains his plan to dissuade further goblin incursions.*
 The monastery's master gifts the players with a pair of spell scrolls: Fly and Feather Fall. 
## Silvershade Glade

**Village Name:** Silvershade Glade

*Read Aloud Text:*
*As you step into the clearing of Silvershade Glade, a once-magical haven within the heart of the Fungal Forest, the vibrant hum of pixie wings and sprites' laughter that once filled the air is replaced by an eerie stillness. The houses, carved with intricate enchantments from colossal mushrooms, now stand in desolation. The remnants of a recent goblinoid attack are evident—toppled structures, broken enchantments, and the haunting absence of the fey residents. Silvershade Glade, once a sanctuary, now bears the scars of a sinister incursion.*

**Encounter: Desolation in the Fungal Forest**

The players, now in Silvershade Glade, bear witness to the aftermath of a goblinoid attack. The intricate houses crafted from colossal mushrooms, which once held the enchanting laughter of pixies and sprites, now stand silent and abandoned.

*Description:*
- Silvershade Glade, nestled amidst towering fungi, was a testament to fey craftsmanship. The houses, intricately carved from colossal mushrooms, now show signs of a recent struggle, with toppled structures and shattered enchantments.
- Goblinoid tracks, both large and small, scar the once-pristine ground, revealing the path of the assailants. The air, once filled with the shimmering glow of pixie dust, now hangs heavy with an unsettling stillness.
- The village, once teeming with fey energy, now echoes with the absence of the magical inhabitants. Broken enchantments and scattered belongings serve as poignant reminders of the goblinoid forces that disrupted the tranquil harmony.

*Interaction:*
- The players can investigate the village, following the goblinoid tracks and searching for any signs of the fey inhabitants. Successful Perception checks may reveal hidden clues about the attackers and the fate of the pixies and sprites.
- Exploration may uncover remnants of fey magic, broken enchantments, or clues leading to the direction the goblinoid forces took after the assault.
- Following the goblinoid tracks becomes a pivotal decision, potentially unraveling the mystery behind the attack and determining the fate of the fey inhabitants.
The player with the highest passive perception notices a few columns of smoke rising above the trees. Read teh followingf
*Above the towering trees and colossal mushrooms, wisps of smoke snake into the sky, subtle columns that pierce the eerie stillness of the Fungal Forest. A mysterious signal beckons, perhaps offering a clue or heralding the presence of unseen forces amidst the fungal landscape.*


## Goblin Pixie Hunters
Read the following when the players reach the source of the smoke
*As you cautiously approach the source of the smoke in the Fungal Forest, a chilling tableau comes into view. The goblin camp sprawls amidst the colossal mushrooms, and an unsettling sight unfolds before you. Mules laden with cages, each imprisoning distressed pixies and sprites, are tethered to the goblins' makeshift tents. The air is thick with the ominous presence of the Goblin Pixie Hunters, a formidable force ready to unleash havoc upon the fey inhabitants.*

*Description:*
- The goblin camp, marked by crude tents and a central fire, exudes an air of menace. 
- The mules, laden with cages strapped to their backs, seem restless and agitated. The captive pixies and sprites within emit faint glimmers of fey magic, their desperation palpable.

*Faction Dynamics:*
- **Goblin Pixie Hunters:** 8 goblins (each with the stats of a Goblin), including a Hobgoblin leader (Hobgoblin Captain or equivalent).
- **Fey Captives:** 6 pixies and 4 sprites, each held within separate cages. If freed they will help the players if they can.

*Interaction:*
- The players have the option to attempt a stealthy approach to gain a tactical advantage or engage in direct confrontation to free the fey captives.
- Successful stealth checks may allow the players to position themselves strategically, potentially enabling surprise rounds or advantageous positioning in the upcoming battle.
- The pixies and sprites, though currently imprisoned, express gratitude and a willingness to aid the players if freed.
When the battle is over read the following

*As the last goblin falls, defeated by your determined efforts, the pixies and sprites within their cages emit twinkling expressions of gratitude. With the cage doors opened, the liberated fey creatures flutter into the air, their tiny voices harmonizing in a chorus of thanks. One brave pixie steps forward, her wings glimmering with ethereal radiance.*

"Brave ones, you've saved us from the clutches of those vile goblins. Our home, Eichenheim, faces a dire fate. Most have fled or been captured. Follow us; we'll lead you back to our village."
The pixies tell the players where to find a hidden grove where they find an Uncommon Wand of Entangle.
With that, the pixies and sprites speed off in the direction of Eichenheim, their diminutive forms weaving through the towering mushrooms of the Fungal Forest. Their gratitude lingers in the air, and the urgency of the situation is reinforced by the realization that the captured fey were only a fraction of the town's inhabitants.

## Boghaven
**Location Description:**
Deep within the southern swamp, Boghaven is situated in the murky embrace of the swampy expanse, a three-day journey south from Eichenheim. The village, perched on stilts above the waterlogged terrain, is surrounded by twisted mangroves and obscured by hanging vines.  However, a shadow has fallen over Boghaven, as the goblins have seized control with a highly efficient resource-gathering operation.

*Italized Second Person Description:*
*As you embark on the three-day journey south from Eichenheim, the air becomes heavy with moisture, and the landscape transforms into a labyrinth of stagnant waters and dense foliage. The village of Boghaven emerges on the horizon, suspended above the swamp on sturdy stilts. Yet, an unsettling sight awaits – the goblins have orchestrated a highly efficient operation, transforming the once quaint settlement into a bustling hub of industry. A sophisticated lumber mill, hastily constructed but surprisingly precise, hums with activity on the outskirts of the village. Captured villagers work diligently, processing resilient cypress logs destined for Sliberberg's construction efforts. The acrid scent of sawdust mingles with the swamp air, marking the village's metamorphosis into a resource-gathering center for the goblin army.*

### Sawmill Skirmish

*As you approach the lumber mill, the rhythmic sounds of saws and hammers fill the air. The bustling activity within the mill becomes apparent, with goblin laborers and guards going about their tasks. Among the workers, you notice several captured villagers reluctantly aiding in the goblins' operations. The challenge is to navigate the sawmill, defeat the goblin guards, and liberate the prisoners without alerting the garrison in the town.*

* **Goblin Guards (4):** These goblin warriors, armed with scimitars and shortbows, patrol the sawmill. They are vigilant, keen on protecting their operation.
* **Goblin Laborers (2):** Goblins performing various tasks in the mill, armed with makeshift weapons. While not as combat-ready as the guards, they can still pose a threat in numbers.
* **Commoner Workers (3):** Captured villagers forced to aid the goblins. They are unarmed and frightened, but their presence adds a layer of complexity to the encounter.
#### Environmental Hazards:
 The sawmill is filled with machinery and obstacles. Players can leverage the environment to gain tactical advantages or use it to create distractions.

  1. **Moving Parts (Blade Mechanism):**
    - *Description:* A large saw blade, powered by a waterwheel, moves rhythmically across a track, slicing through logs.
    - *Effect:* Players can attempt to use the moving parts to their advantage. A successful Dexterity (Acrobatics) check allows a character to navigate through the moving parts, gaining advantage on attack rolls until the start of their next turn. Failure results in 1d6 slashing damage.

  2. **Catwalks and Platforms:**
    - *Description:* Elevated catwalks and platforms offer vantage points but are narrow and precarious.
    - *Effect:* Players on catwalks gain a +2 bonus to ranged attack rolls. However, if a character falls, they take 1d6 bludgeoning damage and are prone. Climbing onto or down from a catwalk requires a successful Dexterity (Acrobatics) check.

  3. **Steam Vent Diversion:**
    - *Description:* Steam vents release hot bursts of steam periodically, creating a distraction.
    - *Effect:* Players can activate a steam vent as an action, creating a cloud of steam that grants them advantage on Dexterity (Stealth) checks until the end of their next turn. However, the noise may alert nearby goblins, and a failed Intelligence (Investigation) check by a goblin results in it discovering the source of the distraction.

  4. **Gear Mechanism (Control Panel):**
    - *Description:* A control panel near the main gears can be manipulated to affect machinery.
    - *Effect:* A successful Intelligence (Investigation) check allows a character to temporarily disable or alter the function of specific machinery. For example, disabling the waterwheel can stop the saw blade temporarily, providing a safe passage. Failure may cause unintended consequences, such as attracting the attention of nearby goblins.

  5. **Conveyor Belt Obstacle:**
    - *Description:* A conveyor belt transports cut logs, creating a moving obstacle.
    - *Effect:* Characters can attempt to use the conveyor belt as cover or to maneuver around enemies. A successful Dexterity (Acrobatics) check grants advantage on Stealth checks. Failure may result in tripping, requiring a Dexterity saving throw to avoid falling prone.

## Docks Ambush

*As you approach the riverbank where rafts laden with processed cypress wood are docked, a group of goblin guards stationed on the rafts and along the shore launches an ambush. The encounter involves a mix of ranged and melee combat, with the added challenge of avoiding falling into the water during the skirmish.*
#### NPCS
* **Goblin Archers (2):** Positioned on the rafts, these goblin archers fire arrows at the players from a distance.
* **Goblin Dock Guards (3):** Armed with scimitars, these goblin guards patrol the docks, engaging in melee combat.
* **Commoner Workers (2):** Captured villagers forced to aid the goblins. They are unarmed and frightened but may become collateral damage if the combat escalates.


#### Rafts and Dock Environment:
 The players must navigate the wooden rafts and the dock structure. Falling into the water can be hazardous, requiring a successful Dexterity saving throw to avoid taking damage.


### Patrol Confrontation

*As the players engage in the docks ambush, the sounds of battle attract the attention of a small patrol returning from the village. The patrol, comprised of the remaining two goblin guards and a hobgoblin sergeant, moves swiftly along the series of boardwalks outside the sawmill. The encounter intensifies as the players must now face this additional threat.*
#### Monsters
* **Hobgoblin Sergeant:** A disciplined and strategic leader, the hobgoblin sergeant wields a wicked longsword and commands the patrol. He prioritizes eliminating threats and maintaining order among the goblins.
* **Goblin Guards (2):** The two goblin guards, armed with scimitars, move with agility and strike opportunistically. They rely on numbers and coordination.
When all the goblins are defeated read the following

*The grateful villagers gather around you, expressing heartfelt thanks for ridding their home of the goblin threat. They share stories of the hardships they endured and the relief that now washes over them. As they bid you farewell, they implore you to convey their fond regards to Mistress Ironraisin, the kind and wise elder of Eichenheim, who undoubtedly awaits news of your valiant deeds.*
The local alchemist present the players with a basket containing 2 potions of healing, and three potions of reistance.
## Driftshore

Driftshore is situated on the northern shore of the lake, its collection of humble buildings nestled between the water's edge and the dense tree line of the verdant woodlands. Positioned to facilitate the overland transfer of goods from keelboats, the village acts as a crucial supply depot for the goblin army's operations in Sliberberg. The lake's calm waters stretch out to the horizon, reflecting the surrounding greenery and the occasional silhouette of a cargo-laden boat.*Driftshore lies northeast of Eichenheim, a journey that involves traversing the rugged landscape surrounding the lake. Its strategic location allows for efficient transportation of goods, marking it as a key link in the logistical chain feeding the goblin occupation.

*As you approach Driftshore, you notice the signs of recent activity—the movement of workers unloading cargo, the stacking of supplies, and the occasional goblin overseer barking orders. The atmosphere is tense, the village caught in the grip of the goblin army's logistical operations.*


## Driftshore Docks Assault

*As the players approach the docks of Driftshore, they witness the busy scene of commoner laborers unloading keelboats under the watchful eyes of goblin overseers. The challenge is clear: liberate the village by confronting the goblin guards without alerting the entire garrison. The bell, ominously placed in the middle of the docks, serves as a potential tool or danger.*


* **Goblin Guards (3):** Armed with scimitars and vigilant eyes, these goblin guards patrol the docks, ensuring the laborers stay focused on their tasks. They are alert to any signs of trouble.
* **Commoner Laborers (7):** The captured villagers, forced to aid the goblins, are busy unloading cargo. While unarmed, they may become collateral damage if the situation escalates.
* **Bell Alarm Mechanism:** A bell situated prominently on the docks serves as a reinforcement alarm. If the bell is rung, it signals for the arrival of a squad of additional goblin guards. This squad consists of three goblin reinforcements armed with scimitars and keen to protect their territory. The bell will ring under the following circumstances
   - **Sound Disturbance:** If a loud noise, such as a battle cry, explosion, or magical effect, is generated within 30 feet of the bell.
   - **Intentional Activation:** A character can attempt to ring the bell intentionally by making a Dexterity (Sleight of Hand) check against a DC determined by the DM based on the circumstances. Failure results in the alarm being raised.
Once the bell is rung, the reinforcements arrive in 1d4 rounds
* **Dock Environment:** The encounter takes place on the expansive docks of Driftshore, with cargo strewn about and boats tied to the pier. Players can use the environment strategically, but they must also be cautious not to cause unnecessary harm to the commoner laborers.
When the goblins are all slain read the following
*As the final goblin falls, the dock falls into an uneasy silence. Amidst the scattered cargo and signs of the recent skirmish, a common laborer steps forward, eyes filled with a newfound determination. Swiftly grabbing a discarded goblin spear, the laborer raises it high and shouts, "Down with the goblins! Common boys, let's drive out these invaders!" The rallying cry echoes across the docks, and the formerly oppressed villagers, fueled by a newfound courage, join the cause to reclaim Driftshore.*

##  Driftshore Town Square Revolt

*The town square of Driftshore erupts in chaos as the townspeople, fueled by newfound courage, rise against the occupying goblins. Most of the goblins are in full retreat, but a determined group of hobgoblins stands their ground, ready to quell the rebellion. The players find themselves leading the charge alongside a sizable group of commoners and guards.*

* **Hobgoblin Commanders (2):** Armed with strategic minds and military discipline, these hobgoblin commanders direct the goblin forces with precision. They wield longswords and shields, leading the resistance against the uprising.
* **Goblin Guards (4):** These goblin guards, armed with scimitars, form a defensive line alongside the hobgoblins, determined to maintain control over the town square.
* **Townspeople Militia (Commoners and Guards):** A large group of commoners, armed with makeshift weapons, and a handful of town guards join the players in their revolt. While their individual combat prowess may be limited, their sheer numbers can tip the balance.
When the battle is over read the following
*As the last goblin falls under the collective might of the townsfolk and your group, a triumphant cheer erupts from the mob. Joyous villagers surround you, thumping you on the back and vigorously shaking your hands in gratitude. Amidst the jubilant crowd, a determined villager pushes through, introducing himself with genuine warmth. "I'm Lucas," he says, "on behalf of Driftshore, I can't thank you enough. You've given us hope and a chance to take our town back. We won't forget this, and our debt to you is immeasurable." The sincere gratitude in his eyes reflects the relief and newfound freedom that has swept through Driftshore.*
 The liberated townsfolk pool their resources and offer a financial reward of 300 gold coins.

# The goblin asssassins attack

*As you journey back through the Great Valley, the landscape shifts to a dark grove where the shadows seem to dance with malicious intent. Suddenly, a pair of figures emerges, moving with a sinister grace. Clad in shadowy armor, the hobgoblin assassins, known as Iron Shadows, strike from the darkness, their deadly blades gleaming with malevolence. You find yourselves ambushed, facing a lethal duo of assassins intent on ending your quest.*

**Hobgoblin Iron Shadows Encounter:**
The dark grove is alive with the rustling of leaves and the distant calls of unseen creatures. As you tread cautiously through the shadowy terrain, the air becomes charged with an ominous energy. Without warning, two figures clad in dark, silken armor materialize before you. These hobgoblin assassins move with an unsettling fluidity, their eyes gleaming with a ruthless intent.

**Enemy Forces:**
- **Hobgoblin Iron Shadows (2):** CR 2 assassins, these deadly hobgoblins are masters of stealth and assassination, equipped with poisoned blades and the ability to vanish into the shadows.
- **Dark Grove Terrain:** The grove provides ample shadows and cover, allowing the Iron Shadows to maneuver with exceptional agility.

# Return to the village
*As you return to the village from tReward:he last settlement on your list, Stein greets you at the entrance with a broad grin. "Agatha's been expectin' you," he says in his slightly dimwitted but amiable manner. Following him, you find yourself at the door of Agatha Ironraisin's cozy house.*

*Inside, the air is filled with the warm aroma of a home-cooked feast. Agatha, with a twinkle in her eye and a thick Scottish accent, welcomes you to her table. "Sit, sit! You've earned a good meal, and I've cooked it all meself," she beams. As you enjoy the hearty fare, Agatha commends your valor and noble deeds.*

*"Ye've got the hearts of true heroes," she declares, her eyes assessing you with a mix of wisdom and approval. "And now, it's time ye knew where those wretched goblins are makin' their lair." Leaning in, she reveals, "It's Sliberberg, the city in the next valley. They've taken the place and made a right mess of it. The poor prince, Fredrick the Beast, a strong fey and a master with the sword, was taken captive."*

*Agatha's expression turns somber. "Be ready, for Sliberberg is no ordinary goblin stronghold, and Prince Fredrick went down fightin'. They won't make it easy for ye." As you absorb her words, the weight of the impending challenge settles in, and you realize the gravity of the task that lies ahead.*
As a reward for the players completing her quest she presents the players with an Agatha's Enchanted Cottage Figurine and basket filled with treats. if the players adopted Talia she presents the players with a dream basket and an a leash of the guardian's bond stating that the players will need them on their trip to sliberberg. Also in recognition of the players' altruistic deeds and assistance to various inhabitants of Märchenweltgrenze, Agatha, bestows upon them magical consumables as tokens of her gratitude. Each reward is tied to a specific good deed performed by the players.

1. **Starlight's Respite:**
   - *Reward:* Potion of Healing (Uncommon)
   - *Description:* This potion, crafted with the essence of healing herbs and enchanted waters, provides rejuvenation when consumed. It is a token of appreciation from Agatha for helping the exhausted pixie, Starlight.

2. **Grumblegut's Gratuity:**
   - *Reward:* Potion of Climbing (Common)
   - *Description:* This potion, a concoction favored by the resourceful ogre woodsman, grants temporary climbing abilities. Agatha offers it as a gesture of thanks for assisting Grumblegut with the retrieval of his stolen tools.

3. **Oakheart's Oakward Gift:**
   - *Reward:* Potion of Barkskin (Uncommon)
   - *Description:* Infused with the essence of the verdant woodlands, this potion enhances the drinker's resilience. Agatha presents it as a token of gratitude for aiding Oakheart in defending his sacred grove.

4. **Wolf's Release Remedy:**
   - *Reward:* Potion of Animal Friendship (Common)
   - *Description:* This potion, derived from herbs that convey a sense of trust, helps establish a bond with creatures of the animal kingdom. It's Agatha's way of expressing thanks for freeing the trapped wolf.

5. **Sir Whiskerworth's Charm:**
   - *Reward:* Scroll of Charm Person (Uncommon)
   - *Description:* This enchanted scroll contains the arcane words to cast the charm person spell. Agatha offers it as a reward for interacting with the awakened cats during the encounter with Sir Whiskerworth.


# Key NPCs
**Agatha Ironraisin**

*Backstory:*
Agatha Ironraisin, the enigmatic elven witch of Eichenheim, has dwelled in the region for time unmeasured. Born amidst the ancient whispers of the feywild, Agatha has witnessed the ebb and flow of mortal lives around her. Though her true age remains a mystery, the formation of Eichenheim itself bears witness to her enduring presence. Secretly, Agatha is a minor archfey and a cherished figure in the Summer Court, known for her benevolent interventions in the lives of mortals.

*Personality Trait:*
Agatha embodies the archetype of the kindly old lady, radiating warmth and benevolence. Her demeanor is gentle and caring, and she finds joy in helping those around her. Despite her ageless existence, Agatha maintains a curiosity for mortal affairs, always seeking ways to bring happiness and light to the lives of the villagers.

*Ideal:*
Agatha is guided by the ideal of nurturing and cultivating goodness. She believes in the inherent potential for kindness within every being and actively strives to bring out the best in others. Her interventions often come in the form of subtle nudges and magical assistance, all aimed at fostering love, joy, and harmony.

*Bond:*
Agatha's deep bond lies with the village of Eichenheim and its residents. Having seen the settlement grow and evolve around her dwelling, she considers the villagers an extension of her own family. The interconnectedness of their lives fuels Agatha's commitment to ensuring the well-being and prosperity of Eichenheim.

*Flaw:*
Despite her good intentions, Agatha's meddling nature can occasionally lead to unintended consequences. Her understanding of mortal emotions and motivations may not always align with the complex realities of mortal lives. This well-intentioned interference may, at times, result in unforeseen challenges for those touched by her magic.

**Commander Alaric Stonehammer**

*Backstory:*
Commander Alaric Stonehammer is a seasoned dwarf with a storied military career within the Holy Empire of Valoria. Born into the proud Stonehammer clan, Alaric's youth was shaped by tales of valor and a deep sense of duty. Over the years, he rose through the ranks, earning respect for his strategic mind and unwavering commitment to protecting the empire. Now, as the appointed commander of the expedition into Märchenweltgrenze, Alaric shoulders the responsibility of eradicating the goblin threat.

*Personality Trait:*
Alaric embodies the stalwart and disciplined nature commonly associated with dwarven warriors. His demeanor is resolute, and he leads with a sense of purpose. Despite the weight of his military responsibilities, Alaric displays a genuine concern for the well-being of those under his command.

*Ideal:*
The ideal that drives Alaric is that of honor and duty. He believes in the duty of the Holy Empire to protect its citizens, and he sees the expedition into Märchenweltgrenze as a crucial mission to secure the eastern borders. Alaric's decisions are guided by a commitment to the greater good, even when faced with difficult choices.

*Bond:*
Alaric's strongest bond is with the soldiers and adventurers under his command. He views them as an extension of his own family, and their collective success and safety weigh heavily on his mind. The camaraderie forged on the battlefield forms the foundation of Alaric's leadership.

*Flaw:*
Alaric's unwavering commitment to duty can sometimes lead to a certain level of stoicism. He may struggle with expressing or understanding more nuanced emotions, which can create challenges in situations requiring delicate diplomacy or empathy. His resolute demeanor, while an asset in battle, may pose difficulties in navigating the complexities of the feywild.

**Oakheart, Guardian of the Verdant Grove**

*Backstory:*
Oakheart, a towering treant with branches that stretch toward the sky, is a manifestation of the ancient magic that permeates the Verdant Grove. As a high fey, Oakheart has transcended the typical existence of treants, attaining a level of power and wisdom that places him on the cusp of archfey status. For centuries, he has stood as the guardian of the sacred grove, ensuring its safety from intruders and preserving the delicate balance of nature.

*Personality Trait:*
Oakheart embodies the serene and timeless wisdom of the ancient fey. His demeanor is calm and contemplative, yet beneath the surface lies a fierce determination to protect the sanctity of his grove. Despite the passage of centuries, Oakheart's connection to the natural world has kept him attuned to the ebb and flow of life around him.

*Ideal:*
The ideal that guides Oakheart is that of balance and harmony within nature. He believes in the interconnectedness of all living things and strives to maintain equilibrium within the Verdant Grove. Oakheart's actions are driven by a deep sense of responsibility to the delicate ecosystems under his care.

*Bond:*
Oakheart's profound bond is with the Verdant Grove itself. He sees the ancient trees, the flowing streams, and the vibrant flora as extensions of his own essence. The preservation of this sacred space is both a duty and a source of solace for Oakheart, who draws strength from the symbiotic relationship he shares with the grove.

*Flaw:*
Oakheart's unwavering commitment to the natural order may lead to a certain level of inflexibility. He may struggle to understand or empathize with beings whose actions disrupt the delicate balance he seeks to maintain. This rigidity can pose challenges when dealing with creatures from other realms or those whose intentions may not align with the fey's ancient wisdom.


**Talia, the Silver Dragon Wyrmling**

*Roleplaying Guide:*

**Appearance:**
Talia is a small, silvery-scaled dragon wyrmling with wide, innocent eyes and tiny wings. Her movements are clumsy and playful, and her silvery scales glimmer in the light. Despite being a dragon, she exudes an air of vulnerability, resembling a very young child.

**Speech:**
Talia's ability to communicate is limited, and her speech is a mixture of draconic words and toddler-like babble. She may say things like "Mama," "Play," or "Hungry," in a charmingly broken manner. Her attempts to convey complex ideas are endearing but often result in adorable confusion.

**Personality:**
Talia is curious and full of boundless energy. She is easily fascinated by the world around her and delights in simple pleasures. Her emotions are transparent, and she can go from giggling with joy to pouting in a matter of moments. Despite her dragon nature, she lacks any malice and approaches everything with a sense of innocence.

**Bonding:**
If the players choose to adopt Talia, she quickly forms a strong bond with them. She may nuzzle against them for comfort, follow them with wide-eyed curiosity, and attempt to mimic their actions. Talia shows a clear preference for dragonborn players, perhaps drawn to the familiarity of their draconic heritage.

**Needs and Wants:**
Talia's basic needs, like food and rest, are evident. She appreciates simple toys and enjoys playtime. She is comforted by the presence of her adopted "family" and may become distressed if separated from them. Talia's emotional well-being is closely tied to the care and attention she receives.

**Special Abilities:**
While still a wyrmling, Talia exhibits some magical abilities inherent to her silver dragon heritage. She may unintentionally create small frosty effects when expressing excitement or emit a calming aura when her "family" is distressed. These abilities are beyond her control and emerge instinctively.

**Mother Figure:**
If one of the female players becomes Talia's "mother," she may seek comfort and protection from her chosen guardian. Talia will instinctively listen to her adopted mother, and the bond formed is both magical and emotional. The dragon wyrmling brings joy, responsibility, and the occasional chaotic moment to the party.

Navigating the challenges and joys of caring for Talia will be a unique and heartwarming aspect of the players' journey through Märchenweltgrenze. The dragon wyrmling is a constant reminder of the delicate balance between the fantastical and the endearing in the magical realms they explore.


# Hexcrawl Encounter Tables Appendix:

## Master encounter table.
| Roll  | Encounter Type                   |
|-------|----------------------------------|
| 1-4   | Goblin Army Encounter            |
| 5-8   | Fey Encounter                    |
| 9-12  | Unusual Phenomenon               |
| 13-20 | Expedition Adventure Encounter   |


## Goblin Army Encounter

| Roll  | Encounter                                           |
|-------|-----------------------------------------------------|
| 1-3   | Goblin Ambush (3 Goblins)                          |
| 4-5   | Goblin Patrol (1 Hobgoblin, 4 Goblins)             |
| 6-8   | Bugbear Raiding Party (1 Bugbear, 3 Goblins)       |
| 9-10  | Darkling Skirmish (2 Darklings, 2 Goblins)         |
| 11-12 | Hobgoblin Elite Squad (2 Hobgoblins, 2 Goblins)    |
| 13-15 | Goblin Hunting Party (1 Quickling, 3 Goblins)      |
| 16-18 | Mixed Goblin Forces (1 Hobgoblin, 1 Bugbear, 2 Goblins) |
| 19-20 | Goblin War Party (2 Hobgoblins, 1 Bugbear, 4 Goblins) |
Certainly! Here are separate markdown tables for fey encounters in various environments:

### Verdant Woodlands Fey Encounters

| Roll  | Encounter                                          |
|-------|----------------------------------------------------|
| 1-3   | Mischievous Sprites (3 Sprites)                    |
| 4-5   | Sylvan Deer Herd (1 Quickling, 2 Deer)             |
| 6-8   | Nymph's Melody (1 Nymph)                           |
| 9-10  | Satyr Revelry (2 Satyrs, 1 Dryad)                  |
| 11-12 | Treant Guardian (1 Treant)                         |
| 13-15 | Feywild Portal (1 Eladrin, 2 Pixies)               |
| 16-18 | Enchanted Glade (2 Brownies, 1 Sprite)             |
| 19-20 | Wild Hunt Pursuit (1 Wild Hunt Rider, 2 Hounds)    |

### Great Swamp Fey Encounters

| Roll  | Encounter                                       |
|-------|-------------------------------------------------|
| 1-3   | Croaking Bullywugs (3 Bullywugs)                |
| 4-5   | Swamp Wisps (2 Will-o'-Wisps)                   |
| 6-8   | Swamp Hags' Cove (1 Green Hag, 2 Twig Blights)  |
| 9-10  | Grippli Ambush (3 Gripplis, 1 Giant Frog)       |
| 11-12 | Bog Nixies' Dance (2 Nixies, 1 Mud Mephit)       |
| 13-15 | Swamp Serpent Encounter (1 Giant Constrictor Snake) |
| 16-18 | Fey Marsh Spirits (2 Water Weirds)              |
| 19-20 | Murmuring Fungus Circle (1 Myconid, 2 Shriekers) |


### Lakeshore Fey Encounters

| Roll  | Encounter                                    |
|-------|----------------------------------------------|
| 1-3   | Lake Sprite Gathering (3 Sprites)           |
| 4-5   | Swan Maiden's Grace (1 Swanmay, 2 Swans)    |
| 6-8   | Triton Diplomacy (1 Triton, 2 Dolphins)     |
| 9-10  | Merfolk Bounty (1 Merrow, 2 Merfolk)        |
| 11-12 | Lakeside Dryad's Lament (1 Dryad, 2 Squirrels) |
| 13-15 | Giant Toad Ambush (1 Giant Toad)             |
| 16-18 | Lake Nymph's Gift (1 Nymph, 2 Water Nymphs) |
| 19-20 | Elemental Lake Guardian (1 Water Elemental)  |


### Fungal Forest Fey Encounters

| Roll  | Encounter                                            |
|-------|------------------------------------------------------|
| 1-3   | Pixie Mischief (4 Pixies)                           |
| 4-5   | Myconid Circle (1 Myconid, 2 Needle Blights)        |
| 6-8   | Dryad's Haven (1 Dryad, 3 Twig Blights)              |
| 9-10  | Fungal Sprites' Waltz (2 Myconids, 1 Sprite)         |
| 11-12 | Elven Ranger Patrol (1 Elven Ranger, 2 Dire Wolves) |
| 13-15 | Fungus-Covered Treant (1 Treant)                    |
| 16-18 | Leprechaun's Hoard (1 Leprechaun)                   |
| 19-20 | Feywild Portal (1 Eladrin, 2 Pixies)                 |

### Mountain Fey Encounters

| Roll  | Encounter                                        |
|-------|--------------------------------------------------|
| 1-3   | Alpine Sprite Dance (3 Sprites)                  |
| 4-5   | Gnomish Tinkerer's Workshop (1 Gnomish Tinkerer) |
| 6-8   | Mountain Goat Herd (1 Satyr, 2 Mountain Goats)    |
| 9-10  | Rock Gnome Prospector (1 Rock Gnome)              |
| 11-12 | Griffon Soaring (1 Griffon)                       |
| 13-15 | Elemental Crystals (2 Earth Elementals)           |
| 16-18 | Crystal Cave Guardians (2 Galeb Duhrs)            |
| 19-20 | Storm Giant's Gaze (1 Storm Giant)                |
Certainly! Here are tables for Unusual Phenomena in various environments:

### Verdant Woodlands Unusual Phenomena

| Roll  | Phenomenon                                   |
|-------|----------------------------------------------|
| 1-3   | Whispers of the Trees (Speaking Trees)      |
| 4-5   | Dancing Lights in Clearing                  |
| 6-8   | Shimmering Faerie Ring (Portal to Feywild)  |
| 9-10  | Temporal Glitch (Time-Disoriented Creatures) |
| 11-12 | Echoing Footsteps of Phantom Hunt           |
| 13-15 | Overgrown Enchanted Garden                  |
| 16-18 | Whispering Winds of a Sudden Storm          |
| 19-20 | Polymorphed Critters (Common Animals with Unusual Traits) |


### Great Swamp Unusual Phenomena

| Roll  | Phenomenon                                   |
|-------|----------------------------------------------|
| 1-3   | Swamp Will-o'-Wisp Spectacle                |
| 4-5   | Haze of Invisible Shadows                   |
| 6-8   | Swamp Gas Bubble Orbs (Gas Spores)          |
| 9-10  | Toadstool Circle - Illusory Gateway         |
| 11-12 | Quicksand Mirage - Solid Ground Beneath      |
| 13-15 | Ghostly Swamp Lights Leading the Way        |
| 16-18 | Swamp Water Springing to Life               |
| 19-20 | Mysterious Fog Veil Over an Old Witch's Hut |

### Lakeshore Unusual Phenomena

| Roll  | Phenomenon                                   |
|-------|----------------------------------------------|
| 1-3   | Moonlit Reflections on the Water            |
| 4-5   | Celestial Constellation on Lake Surface     |
| 6-8   | Iridescent Waters - Feywild Portal          |
| 9-10  | Ethereal Mist Revealing Invisible Islands   |
| 11-12 | Crystal Geodes Emerging from Lake Bed        |
| 13-15 | Driftwood Sculptures Moving on Their Own    |
| 16-18 | Mirror of Erised - Reflections of Desires   |
| 19-20 | Naiad's Liquid Mirage - Illusion of Hidden Treasures |

### Fungal Forest Unusual Phenomena

| Roll  | Phenomenon                                   |
|-------|----------------------------------------------|
| 1-3   | Glowing Fungus Archway                      |
| 4-5   | Myconid Telepathic Exchange Zone            |
| 6-8   | Dancing Spore Fairies (1d4 Pixies)          |
| 9-10  | Petrified Myconid Circle - Living Statues   |
| 11-12 | Whispers of the Whispering Cap Mushrooms   |
| 13-15 | Ascending Fey Sigils in Floating Spore Clouds |
| 16-18 | Fungal Grove Blossoms Only Under Moonlight |
| 19-20 | Chrono-Displacement - Vision of the Past and Future |


### Mountain Unusual Phenomena

| Roll  | Phenomenon                                   |
|-------|----------------------------------------------|
| 1-3   | Singing Mountain Peaks                      |
| 4-5   | Rolling Stone Circle - Gravity Defiance     |
| 6-8   | Elemental Crystals Shimmering in Crevices   |
| 9-10  | Skyward Ascending Mountain Goats            |
| 11-12 | Crystalline Reflection of Surrounding Peaks |
| 13-15 | Whispering Winds Carrying Echoes of Ancients |
| 16-18 | Petrified Whispers of Ancient Runes         |
| 19-20 | Enchanted Ice Crystals Gleaming in Sunlight |
### Fellow adventuers

| Roll  | Expedition Name            | Disposition  | Members (Monster Manual Stat Blocks)                        |
|-------|-----------------------------|--------------|-----------------------------------------------------------|
| 1-3   | Silverstride's Surveyors    | Friendly     | Gruffen Ironstride (Knight), Elara Moonwhisper (Elf Scout), Rurik Thunderbeard (Dwarf Cleric)                     |
| 4-5   | Whispering Shadows Company  | Neutral      | Captain Silvaria Shadowblade (Drow Rogue), Lyra Swiftfoot (Halfling Ranger), Varis Nightshade (Elf Warlock)       |
| 6-8   | Starlight Vanguard          | Friendly     | Commander Selene Starfall (Aasimar Paladin), Orion Stardust (Gnome Sorcerer), Talia Sunflower (Human Druid)        |
| 9-10  | Obsidian Reclaimers         | Hostile      | Warlord Grogg Bloodfang (Bugbear), Thalia Flameheart (Fire Genasi Barbarian), Zara Frostblade (Half-Orc Fighter)   |
| 11-12 | Celestial Pathfinders       | Friendly     | High Priestess Althea Skywatcher (Aarakocra Cleric), Seraphina Moonshadow (Moon Elf Wizard), Orik Stoneheart (Earth Genasi Fighter) |
| 13-15 | Bloodmoon Mercenaries       | Neutral      | Captain Kael Bloodmoon (Half-Elf Rogue), Mara Shadowfox (Tabaxi Monk), Grim Ironhand (Dwarf Fighter)               |
| 16-18 | Ivory Expeditionaries       | Friendly     | Lady Aeliana Silverwind (Silver Dragonborn Paladin), Elandra Swiftgale (Wood Elf Rogue), Thrain Stormhammer (Dwarf Bard) |
| 19-20 | Ashen Outriders             | Hostile      | Inferno the Blazing (Fire Elemental Sorcerer), Sable Shadowheart (Dark Elf Warlock), Brakka Blackfury (Hobgoblin Barbarian) |


# Appendex Loot table.

| Roll  | Money (gp)  | Magic Item (Common)         | Magic Item (Uncommon)            |
|-------|-------------|-----------------------------|----------------------------------|
| 1-5   | 10          | Potion of Healing (1d4+1)   | Potion of Climbing               |
| 6-10  | 20          | Scroll of Cantrip (Random)  | Elixir of Health                 |
| 11-15 | 30          | 2d6x10 Copper Pieces       | Potion of Invisibility          |
| 16-20 | 50          | Scroll of 1st-Level Spell   | Oil of Slipperiness              |
| 21-25 | 75          | 2d6x10 Silver Pieces       | Bag of Tricks (Gray)             |
| 26-30 | 100         | Potion of Greater Healing  | Driftglobe                       |
| 31-35 | 150         | Scroll of 2nd-Level Spell   | Potion of Resistance (Type)     |
| 36-40 | 200         | 2d6x10 Gold Pieces         | Ammunition +1 (20)               |
| 41-45 | 250         | Potion of Fire Breath      | Potion of Flying                 |
| 46-50 | 300         | Scroll of 3rd-Level Spell   | Dust of Disappearance            |
| 51-55 | 400         | 2d6x10 Platinum Pieces    | Gem of Brightness                |
| 56-60 | 500         | Potion of Superior Healing | Wand of Magic Missiles           |
| 61-65 | 600         | Scroll of 4th-Level Spell   | Cloak of Elvenkind               |
| 66-70 | 750         | 2d6x10 Gold Pieces         | Bag of Holding                  |
| 71-75 | 1000        | Potion of Supreme Healing   | Pearl of Power                   |
| 76-80 | 1200        | Scroll of 5th-Level Spell   | Ring of Jumping                 |
| 81-85 | 1500        | 2d6x10 Platinum Pieces    | Eversmoking Bottle              |
| 86-90 | 2000        | Potion of Invulnerability  | Sending Stones                   |
| 91-95 | 2500        | Scroll of 6th-Level Spell   | Necklace of Adaptation           |
| 96-100| 5000        | 2d6x10 Gold Pieces         | Weapon +1 (of player's choice)  |
# Appendix magic items

**Agatha's Enchanted Cottage Figurine**

*Wondrous Item (Figurine), Rare*

This small, intricately carved figurine resembles a charming cottage, complete with a thatched roof, flower-filled windowsills, and a welcoming front door. When the command word is spoken, the figurine transforms into an enchanting two-story cottage, providing a secure and comfortable space for those within.

**Features:**

1. **Two-Story Design:** Unlike the typical Leomund's Tiny Hut, Agatha's cottage has two small floors connected by a staircase. This allows for separate living spaces or additional privacy for the occupants.

2. **Item Preservation:** Any items placed inside the cottage when it is deactivated remain inside when the cottage is reactivated. This feature allows the occupants to leave personal belongings, supplies, or even a cozy arrangement undisturbed between uses.

3. **Extended Activation:** The cottage remains activated until its command word is spoken again or until no living beings remain within its confines. This provides a more flexible duration for those using the cottage for extended periods.

4. **Disguise Enchantment:** While activated, the cottage figurine disguises itself using a subtle enchantment. Casual observers perceive it as a natural and permanent part of the environment, making it blend seamlessly with its surroundings. This magical camouflage enhances the figurine's utility for discreet and inconspicuous use.

 **Talia's Dream Basket:**
   - This seemingly ordinary basket with a lid is a magical haven for Talia, the Silver Dragon Wyrmling. When opened, the basket transforms into a cozy bed perfectly sized for Talia's reduced form. The enchantment within the bed ensures that it always feels just right, offering comfort and security for the young dragon. It is an ideal resting place for Talia during the group's travels.
**Leash of Guardian's Bond:**
   - This enchanted collar, an improved version of the one created by Agatha, serves multiple functions to enhance the bond between Talia and her adopted family:
      - **Invisible Leash:** A bracelet, when worn by a member of the party, forms an invisible, intangible, and unbreakable leash between the wearer and Talia. This feature allows the party to keep track of Talia's movements and ensures she remains close during their adventures.
      - **Size Alteration:** The collar can magically shrink Talia down to a more manageable size, making her less conspicuous and more adaptable to various environments.
      - **Disguise and Transformation:** Once per day, the collar can cast either the disguise self or polymorph spell on Talia. This provides additional versatility, allowing the party to temporarily alter Talia's appearance for situations that require discretion or when a different form might be advantageous.

