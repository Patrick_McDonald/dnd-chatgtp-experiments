---
title: Into The Märchenweltgrenze Part 3 Castle Sliberberg 
date: "2023-11-08T22:40:32.169Z"
description: For 5 players of level 5
---

# Assault on Castle Sliberberg

For 5 players level 5. The last of vasrock army is hold up inside the castle.
## Intro 
Read or paraphrase the following
*You find yourself in the makeshift headquarters of the newly renamed Sliberberg Army. The room is dimly lit, and the atmosphere is heavy with tension. At the head of a battered table sits the enigmatic figure known as "the Beast." You now understand that he was once the ruler of this city when its only inhabitants were fey. His body still bears the scars and signs of emaciation from his time in Vasrock's jail, but his eyes burn with a fierce determination to bring the fight to the goblin general.Around the table, other former resistance members have joined your cause. Their expressions range from fear and uncertainty to grim determination. Outside the makeshift headquarters, you can hear the distant sounds of hobgoblin archers patrolling the outer walls of Castle Sliberberg. The region around the castle has become a treacherous no man's land, and Vasrock's presence in the incomplete fortress looms like a dark cloud over the Märchenweltgrenze.*
# The secret passage
**Introduction to the Dungeon Crawl:**

*As the bustling sounds of the market ward surround you, Fredrick the beastman, his greatsword now serving as a makeshift walking stick, leads your group towards a seemingly nondescript commercial property. The weariness etched on his face tells a tale of past struggles and the weight of a kingdom lost. The building's exterior gives no hint of the hidden depths below.*

*Guiding you to the basement, Fredrick stops before an unassuming wall. With a solemn expression, he utters a command word, a key to unlock the arcane secrets hidden within the mundane facade. The air shimmers with a subtle energy, and before you, an archway outlined in glowing magical runes emerges.*

*Fredrick, his eyes reflecting determination, meets your gaze. His voice resonates with a mix of sorrow and resolve as he speaks,* "Get the bastard, Vasrock."

# Castle sliberberg
**Castle Sliberberg Dungeon:**
Constructed two decades after the resettlement of Sliberberg, Castle Sliberberg emerged as a testament to the enduring affection of the people for their perpetually melancholic king, Fredrick the Beastman. The grand structure was conceived as a means to uplift the spirits of a ruler whose sorrow ran as deep as the roots of the ancient trees that surrounded the castle. However, the castle's cheerful facade only served to accentuate Fredrick's internal struggles.

As one descends into the lower floors of Castle Sliberberg, the ambiance is bright and cheery. The walls are adorned with fey-inspired art, and the colors dance in harmony, attempting to counterbalance the shadows within Fredrick's heart. Yet, subtle hints at his inner turmoil emerge in the choice of art, revealing a struggle that transcends the surface merriment.However, the castle's true reflection of Fredrick's mental state unfolds as one ascends. The vibrant colors of the lower floors gradually mute into softer, melancholic tones, mirroring the depth of Fredrick's emotional journey. The art becomes more openly reflective of loss and longing, capturing the essence of a soul grappling with profound sorrow.

Interspersed throughout the castle are rooms personally requested by Fredrick, each a hopeful echo of his desire to find Sophia and live out their dream life. Yet, these rooms remain untouched, frozen in time, as the dream itself eluded Fredrick's grasp. The nurseries, in particular, stand as poignant reminders of a vision never realized, echoing the silence of a king who sought solace in the promise of family, yet found himself perpetually alone.

**Architect and Wizard: Professor Quizzlewidget Tumbletop**

Castle Sliberberg stands as a whimsical creation of the eccentric gnome architect and wizard, Professor Quizzlewidget Tumbletop. Known for his penchant for melding magical ingenuity with fantastical design, Professor Tumbletop embraced the challenge of crafting a structure that not only defied convention but also aimed to lift the spirits of a melancholic king.

The most enchanting aspect of Tumbletop's design lies in the near absence of traditional stairs within the castle. Instead, he wove a network of magical corridors that seamlessly traverse the vertical expanse of the castle. These corridors, infused with fey magic, allow inhabitants to move between floors effortlessly, creating an otherworldly experience within the walls of Sliberberg.
The atmosphere in Castle Sliberberg has shifted dramatically in the last six months since General Vasrock claimed it as his palace. The once vibrant halls now bear the stamp of the hobgoblin warlord's military rule. Banners displaying the sigils of Vasrock's army flutter in the courtyards, and the magical corridors, once filled with fey enchantment, echo with the disciplined footsteps of hobgoblin soldiers.
As the players' uprising gains momentum, Vasrock's paranoia intensifies. Every day, a tension-filled routine unfolds as one of his mages casts Divination, seeking answers to the same haunting question: "Am I going to die today?" The repeated ritual reflects Vasrock's fear and the looming threat he perceives from the approaching heroes.
In response to the imminent danger, Vasrock orders a lockdown of the palace. The once welcoming magic corridors are now guarded with increased vigilance, and entry points are sealed off. Hobgoblin soldiers patrol the corridors, their sharp eyes scanning for any sign of intrusion.
## Magic corridors


**Castle of Sliberberg: General Features**

1. **Ceiling Height:**
   - **First Floor:** The entrance hall and grand spaces of the first floor boast soaring ceilings, reaching a height of 20 feet. This allows for large, ornate chandeliers and grandiose fey-inspired decorations.
   - **Subsequent Floors:** The ceilings on the upper floors are more intimate, with an average height of 12 feet. This creates a cozier atmosphere but still maintains a touch of elegance.

2. **Corridor Width:**
   - The castle corridors are generally spacious, with a width of 10 feet on the first floor. This width accommodates the grandeur of the entrance and main hallways.
   - As you ascend, the corridors on the upper floors narrow slightly to 8 feet, maintaining functionality while fostering a sense of mystery and seclusion.

3. **Lighting:**
   - **Enchanted Lighting:** Throughout the castle, ethereal orbs of fey light gently illuminate the halls. The orbs change color based on the time of day, creating a dynamic and enchanting ambiance.
   - **Stained Glass Windows:** The stained glass windows depict scenes from fey mythology and bathe the corridors in colorful hues during daylight hours.

4. **Windows:**
   - **First Floor:** Large, arched windows on the first floor allow natural light to flood the castle during the day. These windows provide views of the surrounding courtyard and gardens.
   - **Upper Floors:** As you ascend, the windows become narrower, maintaining an aesthetic appeal while ensuring a level of defensibility. The scenes depicted on the stained glass become more subdued, reflecting the melancholy nature of King Fredrick.

5. **Flooring:**
   - **First Floor:** Polished marble flooring with intricate fey patterns greets visitors on the first floor. The reflective surface enhances the sense of grandeur.
   - **Upper Floors:** As you ascend, the flooring transitions to polished wood inlaid with delicate fey motifs, creating a warmer and more personal atmosphere.

6. **Architectural Details:**
   - **First Floor:** Ornate pillars and archways punctuate the grand spaces, adding to the fairy tale aesthetic. Fey-inspired sculptures and reliefs are strategically placed to accentuate the fey influence.
   - **Upper Floors:** The architectural details become more refined and less extravagant. Delicate fey carvings embellish door frames and windows, offering a subtle nod to the castle's fey origins.
**Magic Corridors of Castle Sliberberg:**
Within the enchanting confines of Castle Sliberberg, the magic corridors, a masterful creation of Professor Quizzlewidget Tumbletop, weave through the stone and echo with the whispers of fey magic. Appearing as delicate outlines of doors etched in flowing fey rune script when dormant, these corridors spring to life when activated, transforming into doorways that lead to different sections of the castle. The Corridors can be locked through the use of magic keys. To lock or or unlock a corridor place the key in the keyhole on the door's frame and turn once in any direction. All doors start locked when the players enter

1. **Servants' Quarter Hall to Kitchen:**
   - A corridor concealed within the servants' quarters offers quick and discreet access to the bustling heart of the castle's culinary operations.

2. **Library Balconies Network:**
   - Five interconnected magic corridors weave between the various balconies of the expansive library. These passages allow swift movement for those navigating the vast collection of books and magical tomes.

3. **Courtyard to Guest Wing Corridor:**
   - A passage connects the vibrant courtyard to the hushed corridors of the guest wing.

4. **Guest Wing Corridor to Royal Wing Foyer:**
   - An elegant doorway emerges within the guest wing corridor, linking directly to the foyer of the regal royal wing.

5. ** fourth floor foyer to Throne Room:**
   - A magical corridor traditionally providing a direct path from the serene quarters outside the royal bedchamber to the grandeur of the throne room.
6. **fourth floor foyer to Entrance Hall:**
   - A once-convenient link between the intimate halls outside the royal bedchamber and the bustling entrance hall.

7. **Royal Wing Foyer to fourth floor foyer:**
   - A discreet exit from the royal wing foyer to the exterior of the royal bedchamber, now sealed off at Vasrock's behest.

As tensions rise and Vasrock tightens his grip on the castle, certain corridors, specifically those leading to the royal bedchamber, have been forcefully disabled, restricting movement and complicating the intricate web of magical passages that once defined the castle's unique design.
# Castle sliberberg locations
![map](sliberbergcastlefirstfloor.jpg)
## 1 Entrance Hall 
**Description:**
*As you step through the teleportation corridor, you find yourself in a grand entrance hall that radiates an oddly cheery atmosphere. The room is adorned with light furnishings, vibrant banners hanging from the walls, and various paintings displaying scenes of hobgoblins engaging in seemingly mundane activities. The air is filled with a faint, enchanting melody, and a magical fountain in the center of the room glistens with colorful lights. As you take in the whimsical decor, you notice the hobgoblins depicted in the paintings shifting, their eyes narrowing as they come to life.*

**Combat Encounter:**
*The painted hobgoblins suddenly leap from their frames, weapons drawn, and surround the party. The enchanted atmosphere of the room seems to fuel their aggression.*

- **Enemies:** Hobgoblin Guards (4)
- **Initiative:** Roll initiative for the players and the hobgoblin guards.
- **Objective:** Defeat the hobgoblin guards to proceed further into the castle.
- **Special Features:**
  - The room is filled with decorative columns and furniture that provide partial cover for both the players and the hobgoblins.
  - Magical Illusions: The enchanted atmosphere in the room creates illusions that occasionally distort perception. Players and hobgoblins have disadvantage on Wisdom (Perception) checks.

## 2 Central Courtyard 
*Exiting the entrance hall, you step into the castle's central courtyard, a breathtaking garden bathed in the soft glow of magical light. The air is fragrant with the scent of otherworldly flowers, and the ground is adorned with lush, vibrant vegetation. A majestic statue of King Fredrick the Beastman stands proudly in the center of the courtyard, capturing his regal presence. The statue is surrounded by a mesmerizing array of fey creatures carved into the stone, creating an enchanting scene. However, as your gaze lingers on the statue, you notice an oddity. The expression on King Fredrick's face seems forced, as if he were concealing a different emotion when the sculptor captured his likeness. The central courtyard beckons, offering a moment of respite amid the fairytale chaos that awaits within the castle.*

Note: The player with the highest passive Wisdom perception check (or active check, if they specifically investigate the statue) notices that the statue's expression appears to betray a hidden emotion or feeling, suggesting a deeper layer to King Fredrick's story.

## 3 Throne Room 
*Stepping into the throne room, a stark contrast unfolds before you. The space is adorned with the same cheery colors as the entrance hall, creating an unsettling juxtaposition with the room's grave significance. The atmosphere is tense, and the air is charged with a mixture of enchantment and unease. The throne itself is an opulent seat of power, yet it too is engulfed in the whimsical hues that seem out of place in this solemn chamber. The windows lining the walls draw your attention with their stunning stained glass depictions. One grand window narrates the refounding of Sliberberg by King Fredrick the Beastman, surrounded by a diverse gathering of fey followers. The vibrant colors of the stained glass clash with the weighty atmosphere of the throne room, creating an eerie sense of discord that hangs in the air.*
**Lonely Pine Tree Painting Stash:**
*Among the vibrant and discordant paintings in the throne room, your attention is drawn to a landscape depicting a solitary pine tree standing proudly on a cliff overlooking the vast expanse of the sea. The colors are muted, evoking a sense of solitude and contemplation. As you inspect the painting more closely, you notice multiple tracks leading to and from the tree, suggesting that King Fredrick frequented this place.*

*With this insight, you reach out to the lonely pine tree in the painting, and as your hand passes through the canvas, a subtle ripple of magic signals the transition from the throne room to a hidden extradimensional space. You find yourself on the cliff, surrounded by the soothing sounds of the sea. The lonely pine stands resolute, and as you explore the area, you discover a hidden knot in the tree's trunk.*

Within this knot, carefully concealed, is a stash that seems to have been placed here regularly. Inside, you find several potions of healing, their magical glow hinting at their potency. A spell scroll of "Cause Fear" is neatly tucked among the potions. As you continue searching, you uncover a portrait of a young half-elf woman, approximately 19 years old. The details are striking, and her eyes seem to hold a story of their own. 
Also in the satchel are meticulously annotated maps,  The scribbled notes detail his increasing frustration, marked by crossed-out locations and desperate exclamations.e of the maps, particularly focused on the Feywild, is marred by Fredrick's anguished handwriting with  repeated declarations of "NO NO NO NO." It becomes evident that this secluded spot held significance for King Fredrick, a place of solace and reflection, guarded by the watchful gaze of the lonely pine.

## 4 Great Hall 
**Description:**
*As you step into the Great Hall, the atmosphere shifts from the grandeur of the throne room to a more utilitarian setting. Long feasting tables stretch across the hall, cluttered with empty plates and the remnants of recent meals. The vibrant banners that once adorned the walls have been replaced with makeshift sleeping quarters for the goblin army, creating a chaotic and cramped living space. During the day, the hall doubles as a mess hall for the hobgoblin soldiers, who now eye you with a mix of curiosity and hostility as they interrupt their meals to confront the intruders.*

**Combat Encounter:**
*The clamor of metal against metal and the guttural language of goblins fill the air as the hobgoblin soldiers, caught off guard during their mealtime, rise to confront the party.*

- **Enemies:** Hobgoblin Soldiers (6)
- **Initiative:** Roll initiative for the players and the hobgoblin soldiers.
- **Objective:** Overcome the hobgoblin soldiers to progress through the castle.
- **Special Features:**
  - **Covered Tables:** The long feasting tables provide cover for both the players and the hobgoblin soldiers.
  - **Cramped Quarters:** The crowded living conditions can make maneuvering challenging. Characters can use the environment to their advantage or risk provoking opportunity attacks in tight spaces.

## 5 Enchanted Kitchen 
**Description:**
*Leaving the chaos of the Great Hall behind, you enter the Enchanted Kitchen. The air is thick with the aroma of magical ingredients, and the room is filled with bubbling cauldrons, enchanted utensils, and mysterious ingredients that seem to stir themselves. However, the once bustling and harmonious kitchen is now tense and hushed. The kitchen staff, commoners under the hobgoblin rule, work in apprehensive silence, their eyes darting nervously as you enter. They are aware of the secrets that echo within the castle walls, secrets that could aid you in your quest. The clatter of pots and pans ceases momentarily as they regard you with a mix of fear and hope.*

**Interaction with the Kitchen Staff:**
*The commoners in the Enchanted Kitchen are on edge, and your approach will determine their level of cooperation. They know the locations of several magic teleportation corridors within the castle and might be willing to share this information if convinced that you pose no threat. However, the recent combat in the Great Hall has left them scared of retaliation by the hobgoblin soldiers.*

- **Commoner NPCs:**
  - Commoner (3): Level 0, AC 10, HP 4 (1d8 - 1), Speed 30 ft.
- **Interaction Options:**
  - **Diplomacy:** A successful Charisma (Persuasion) check may convince the commoners to share information about the teleportation corridors.
  - **Intimidation:** An intimidation attempt could coerce the commoners into compliance, though this might lead to negative consequences.
  - **Deception:** A character proficient in deception could attempt to gain the commoners' trust by posing as an ally or fellow servant.

## 6 Servants' Quarters 
**Description:**
*As you enter the Servants' Quarters, the air is heavy with a sense of emptiness. Rows of unoccupied beds, neatly made, stretch along the walls, revealing the absence of the castle's once-thriving staff. It seems that most of the inhabitants have either fled or are keeping to the shadows. The corridors echo with a haunting stillness. A thorough search reveals a solitary maidservant hunched over a mending task in one of the rooms, her eyes widening with a mix of surprise and trepidation as she notices your presence.*

**Interaction with the Maidservant:**
*The maidservant, seemingly one of the few remaining in the deserted quarters, glances up from her mending work. She appears to be cautiously sizing up the intruders, balancing fear with curiosity. With a gentle approach, you may learn more about the secrets that lie within the castle's walls.*

- **Maidservant NPC:**
  - Commoner (Maidservant): Level 0, AC 10, HP 4 (1d8 - 1), Speed 30 ft.
- **Interaction Options:**
  - **Persuasion:** A successful Charisma (Persuasion) check may encourage the maidservant to share information about the castle's hidden areas.
  - **Intelligence (Investigation):** An investigation check might reveal subtle clues about the locations of the hidden caches behind paintings or other secret areas.
  - **Deception:** A character proficient in deception could attempt to gain the maidservant's trust by posing as an ally or someone sympathetic to their plight.

## 7 Library 
**Description:**
*As you enter the Library, the scent of ancient books and the hushed whispers of knowledge surround you. Towering shelves filled with tomes, scrolls, and magical artifacts stretch two stories high, creating an awe-inspiring labyrinth of knowledge. The air is tinged with the subtle essence of arcane energies, and the soft glow of enchanted globes illuminates the vast space. Magical corridors intertwine throughout the room, leading to multiple balconies that overlook the expansive collection below. However, the serenity of the library is shattered as you witness a group of hobgoblin druids and their goblin assistants fervently at work, their intentions unclear.*

**Combat Encounter:**
*The peaceful ambiance of the library transforms into a battleground as the hobgoblin druids and goblin assistants, caught in the act of some arcane endeavor, turn their attention towards you.*

- **Enemies:**
  - Hobgoblin Druids (2): AC 13, HP 33 (6d8 + 6), Speed 30 ft.
  - Goblin Assistants (Commoners, acting as minions): AC 10, HP 4 (1d8 - 1), Speed 30 ft.
- **Initiative:** Roll initiative for the players, hobgoblin druids, and goblin assistants.
- **Objective:** Overcome the hobgoblin druids and goblin assistants to explore the library freely.
- **Special Features:**
  - **Magical Corridors:** The magical corridors provide elevated positions and shortcuts for both the players and the hobgoblins.
  - **Balconies:** The balconies offer vantage points for ranged attacks and opportunities for strategic positioning.
### Magic painting

**Painting Description:**
*Amidst the towering shelves in the library, your gaze falls upon a peculiar painting that seems to beckon with an otherworldly allure. The artwork depicts a lush and mystical forest bathed in the ethereal glow of twilight. The trees are ancient and towering, their branches intertwined in a dance with the magical energies that infuse the air. A small path meanders through the foliage, disappearing into the heart of the enchanted forest. As you study the painting, you notice a subtle shimmer, and the sounds of rustling leaves and distant whispers emanate from the canvas. It becomes evident that this is no ordinary scene; it is a gateway to another realm.*

## 8 Castle Barracks 
**Description:**
*As you cautiously make your way through the castle, the echoing sounds of combat and discovery gradually fade, replaced by a disconcerting silence. The castle barracks come into view, and upon entering, you find rows of empty bunks, neatly arranged and devoid of occupants. The air is still, and the room seems frozen in time, as if the very essence of life has momentarily abandoned this once-bustling space. The barracks, though empty, exude an air of anticipation, leaving you to wonder about the mysteries that may lie dormant within the walls.*

![map](sliberbergcastlesecondfloor.jpg)
## 9 Guest Wing
*As you explore the guest wing on the second floor, you find yourself amidst a symphony of vibrant hues. Each door along the corridor leads to a spacious guest room, and the colors that spill from within hint at the unique design awaiting discovery. The castle's fairytale magic comes to life as you approach the doors, but a challenge presents itself – most of them are securely locked, preventing easy entry. The enchanting colors beckon, promising a glimpse into a realm of fantasy within each guest room.*

**Mechanics of the Encounter:**
- **Locked Doors:** The doors to the guest rooms are magically sealed and require a Dexterity-based check (Lockpicking or Thieves' Tools proficiency), use of Fredrick Master key, or a successful Strength check to force open.
### treasure
Certainly! Here are a dozen small lootable items that the players may find among the belongings of the guests King Fredrick was entertaining in the guest rooms. Each item has an estimated value in gold:

1. **Silver Filigree Hairpin:**
   - A delicate hairpin adorned with intricate silver filigree. (Value: 25 gold)

2. **Embroidered Silk Handkerchief:**
   - A finely embroidered handkerchief made from luxurious silk. (Value: 15 gold)

3. **Gem-Encrusted Perfume Bottle:**
   - A small perfume bottle crafted from crystal and adorned with tiny gemstones. (Value: 40 gold)

4. **Ivory-Carved Music Box:**
   - A small music box with an intricately carved ivory exterior. (Value: 30 gold)

5. **Gilded Quill and Ink Set:**
   - A quill and ink set with gold-plated accents, perfect for elegant correspondence. (Value: 20 gold)

6. **Scented Beeswax Candles:**
   - A set of scented candles made from pure beeswax. (Value: 25 gold)

7. **Jeweled Wine Goblet:**
   - A wine goblet with a stem encrusted with small, sparkling gemstones. (Value: 35 gold)
### Corridor key
In the blue room hidden in a jewelery box is they key to the corridor to the 3 floor. Finding it requires a dc 15 arcane check or a dc 18 investigation check
### Red Room's Armory Magic Painting Cache:**
*As you enter the guest room known as the Red Room, your eyes are immediately drawn to a vivid painting hanging prominently on one wall. The artwork depicts an expansive armory, weapons gleaming and armor standing proudly. The colors within the painting seem to pulsate with magical energy, inviting you to explore its depths.*

**Magic Painting Cache Contents:**
1. **Quiver of +1 Arrows:**
   - A quiver containing a dozen finely crafted arrows, each enchanted with a +1 bonus to attack and damage rolls.

2. **Oil of Weapon Enhancement +1:**
   - A small vial of magical oil that, when applied to a weapon, grants it a +1 bonus to attack and damage rolls for one hour.

3. **Shield:**
   - A sturdy shield adorned with intricate patterns. When wielded, it provides a +2 bonus to Armor Class.

4. **Longsword:**
   - A well-balanced longsword with a keen edge, offering a +1 bonus to attack and damage rolls.

5. **Mastercraft Longbow:**
   - A finely crafted longbow, expertly designed for accuracy and power. It provides a +1 bonus to attack and damage rolls.

## 10 Royal Art Gallery 
*Entering the Royal Art Gallery, a symphony of colors and emotions unfolds around you. The walls are adorned with paintings that breathe life into fey subjects and cheerful landscapes. Yet, amidst the jubilation, your gaze is drawn to an especially large portrait that transcends the joyous scenes. It captures a poignant moment, featuring a pre-Beastman Prince Fredrick seated beside a half-elf girl named Sophia in a tranquil pastoral setting.*

**Mechanics of the Encounter:**
- **Magical Painting:**
  - The painting of the art gallery within the Royal Art Gallery is a magical portal leading to the actual royal art gallery and Fredrick's personal art studio.

- **The True Royal Art Gallery:**
  - The actual gallery predominantly features portraits of humans, including the half-elf girl Sophia.
  - The Gallery also includes a studio where Fredrick paints his portraits.
  - On the table in the studio is Fredrick spare master key to all rooms and corridors in the castle
  - Gloomy landscapes add an intriguing contrast to the otherwise cheerful fey-focused artwork.
  - The focal point of the gallery is the large portrait of a pre-Beastman Prince Fredrick with Sophia, capturing a moment frozen in time.
   - A half-completed masterpiece stands mounted on an easel—a scene that unfolds on a stormy hillside. The focal point of the painting is a gravestone or tomb, stark against the turbulent backdrop. The epithet upon the gravestone is fully rendered, and the words etched into the canvas read, "My beloved Sophia, gone too soon." The birth and death dates are 500 years ago.
   - Stacked against the wall are a number of painting featuring Fredrick and A Mysterious young Half elf Sophia the scenes include.

      1. The ethereal beauty of a radiant wedding day. Fredrick and Sophia stand at the altar at the city cathedral

      2. The second is a coronation is marked by a tapestry of colors, fey symbols, and the support of loyal fey subjects. The realm is at peace, and the love shared between them extends beyond their union to the entire kingdom.

      3. Fredrick gently cradles Sophia's pregnant belly, feeling the life growing within. 

      4.  Fredrick and Sophia stand side by side, witnessing the arrival of their firstborn. The room is aglow with the soft light of new beginnings.
      4.  A group portriat with Fredrick, Sophia and their three children. The features of the children have not been added yet.
![map](sliberbergcastlethirdfloor.jpg)
## 11 Grand Hallway Encounter:
*As you step into the Grand Hallway, a breathtaking display of magical paintings lines the walls, illustrating the raw power of nature in all its glory. Vibrant scenes of a forest fire, a swirling tornado, pounding surf, and a towering glacier captivate your attention. However, your focus is abruptly interrupted by the presence of a squad of hobgoblin guards, standing resolute in their duty.*

**Mechanics of the Encounter:**
- **Hobgoblin Shield Bash:**
  - The hobgoblins have a special Shield Bash ability. On a successful melee attack, they can choose to forgo damage to attempt to push the target. The target must make a Strength saving throw (DC 12) or be pushed 10 feet.

- **Magic Paintings:**
  - The paintings along the Grand Hallway contain the raging power of nature they depict. If a character is pushed into a painting, they must make a Dexterity saving throw (DC 15) to avoid being affected by the depicted natural force.
  - Failure results in taking 2d6 damage corresponding to the nature's power (fire for forest fire, bludgeoning for tornado, etc.).

- **Hobgoblin Tactics:**
  - The hobgoblins, aware of the paintings' power, will actively attempt to push players into them during combat. They prioritize this over dealing damage.

## 12 Royal Parlor
*As you enter the Royal Parlor, a wave of opulence surrounds you. The room is adorned with plush furnishings, elegant tapestries, and a grand picture window that offers a breathtaking view of the castle grounds. A subtle aroma of fey-inspired flowers permeates the air. However, your attention is drawn to a particularly enchanting painting hanging on one of the parlor walls.*

**Mechanics of the Encounter:**
- **Magic Painting - Library Connection:**
  - The painting  in the parlor reveals a mesmerizing tableau—a teenage half-elf dancing amidst the twilight fey woodland. Dressed in a flowing white gown and adorned with a delicate floral crown, she moves with an otherworldly grace. Should the players step through the frame into the fey woodland, the half-elf dancer, responds to their presence with a flirtatious giggle. The magic painting in the Royal Parlor serves as a portal to the library. When activated, it allows the party to traverse between the parlor and the library, providing a strategic shortcut.
- **The Destruction of Bergherz**
  - Dominating the parlor, a colossal magic landscape painting named "The Destruction of Mountain Heart" unfolds a heart-wrenching tableau. The depicted scene is hauntingly reminiscent of Sliberberg, seemingly consumed by a cataclysmic flood. The floodwaters rise so high that they breach the once-protective walls of the city. In the foreground, a young half-elf woman, unmistakably Sophia, is caught in the grasp of the torrent. Fredrick's hand, distinguished by its large, beastman form, reaches out from the edge of the canvas in a futile attempt to save her. Any player who dares to step into the enchanted painting experiences an unnerving encounter. As they enter the flood-ravaged realm, they are assaulted by a surge of psychic energy, taking 1d6 damage. Amidst the disorienting sounds of rushing waters, a haunting chorus emerges—the plaintive cries of a young woman calling out for Fredrick and the anguished voice of Fredrick desperately calling for Sophia.

- **Luxurious Atmosphere:**
  - The parlor's luxurious surroundings offer a comfortable setting for the party. Characters can take a short rest in the parlor, regaining hit points and spending hit dice to recover from previous encounters.
## 13 Queen's Sitting Room

*As you step into the Queen's Sitting Room on the third floor, an atmosphere of untouched elegance envelops the space. The room is impeccably clean, yet a subtle layer of dust on the furniture hints that it has never been truly used. The focal point of the room is the animated paintings adorning the walls, portraying a symphony of birds and flowers in perpetual motion.*
 Despite its pristine appearance, an air of melancholy pervades the room. It becomes evident that, although meticulously maintained, the Queen's Sitting Room has never witnessed the warmth of company or the laughter of its intended occupants.

## 14 Private Dining Room 

*As you approach the private dining room on the third floor of the royal residence, the faint aroma of sumptuous feasts lingers in the air. The door opens to reveal a well-appointed dining area adorned with rich tapestries and fine china. What catches your attention, however, is the evidence that this room has been recently used, as if a feast had taken place not long ago.*
 The dining table still bears the remnants of a lavish feast, though the food has long been consumed. Empty goblets, scattered crumbs, and faint traces of fey-inspired dishes suggest recent use.

## 15 Fredrick's Study 
*As you enter King Fredrick's private study, the room exudes an air of quiet contemplation. Shelves of books, maps, and scrolls line the walls, and a large desk commands the center of the room. Among the documents scattered on the desk, you find detailed notes chronicling King Fredrick's relentless search for his lost love, Sophia.*

**Magic Painting of Sophia:**
- **Animated Depiction:**
  - In the study there is a painting of a young woman seated in the castle garden on a bench swing built for three. She is dressed regally, as if embodying the role of a queen or princess, she exudes an aura of elegance amid the blooming flora. Should the players venture into the painting, they encounter a version of Sophia with limited sentience. This Sophia is but an echo, an extrapolation of Fredrick's memories projected into the present. Her responses are basic, centered around their relationship and shared moments.

**Fredrick diary**
**Fredrick's Diary - Echoes of Grief and Resolution:**

Within the confines of Fredrick's study, the players discover a weathered diary, its pages filled with the inked echoes of a king's quest for his lost love. The entries bear witness to the journey Fredrick embarked upon in search of Sophia, a tale that weaves through attempts to reunite and the eventual acceptance of a heart-wrenching truth.

**Key Diary Entries:**
1. **Before the Refounding of Sliberberg:**
   - *Dated just before the refounding of Sliberberg, the entry reveals Fredrick's elation at finally tracking down a seer capable of casting Divination. This spell becomes his beacon of hope, a means to pierce the shadows and locate Sophia.*

2. **The Day After the Divination:**
   - *Dated the day after the Divination, the tone of the entry abruptly shifts. The haunting repetition of the phrase "she is dead" echoes a dozen times, revealing the gut-wrenching revelation that Sophia's fate was not what Fredrick had desperately sought. The entry then descends into a despairing spiral, ending mid-word.*

3. **Six Months Ago:**
   - *Dated six months ago, the final entry holds a more somber tone. Fredrick's words speak of a slow emergence from the depths of despair. He acknowledges the weight of responsibility and the process of forgiving himself for Sophia's death. The entry hints at the notion of moving on, acknowledging the passage of time as a balm to wounds that may never fully heal.*

**Diary as a Narrative Device:**
The diary becomes a poignant narrative device, chronicling the tumultuous emotional journey of a king haunted by loss. Fredrick's pursuit of Divination represents the pinnacle of hope, a desperate grasp for a reunion. The subsequent entry, marked by repetition and despair, mirrors the shattering of that hope. Finally, the last entry hints at the tentative emergence of acceptance and the gradual process of healing.

## 16 Music Room

*Upon entering the Music Room, a hushed stillness hangs in the air, in stark contrast to the vibrant array of musical instruments that line the walls. The centerpiece is a harpsichord, custom-sized for King Fredrick, nestled in the corner. However, the majority of the instruments seem untouched, as if patiently waiting for the melodies that never graced this small, elegant chamber.*
Rows of instruments, including violins, flutes, and lutes, rest on stands and velvet-lined shelves. The gleam of polished wood and metal reflects the untouched nature of the instruments, as if they've been patiently waiting for skilled hands to bring them to life.

## 17 **Trophy Room Encounter:**

*As you cautiously enter the Trophy Room, the air becomes heavy with the scent of polished metal and the sight of mounted trophies adorning the walls. This smallish chamber, converted into a trophy room by General Vasrock, echoes with the tales of conquests and the spoils of war. However, at the center of the room, guarded by four vigilant hobgoblins, a pulsating magic crystal emanates an unsettling energy.*

**Mechanics of the Encounter:**

- **Mounted Trophies:**
  - The walls are adorned with mounted heads of various creatures, including fantastical beasts and fey creatures. The trophies serve as grim reminders of Vasrock's military prowess and conquests.

- **Hobgoblin Guardians:**
  - Four hobgoblin guards, adorned in armor and wielding weapons, stand watch around the pulsating magic crystal in the center of the room. They seem alert and ready for intruders.

- **Hobgoblin Tactics:**
  - The hobgoblins are disciplined fighters, employing tactics to protect the crystal. They fight strategically, using coordinated attacks and attempts to flank the party.
  The key to the magic corridor is hidden on the hilt of a sword. A player can find the the gem key with a dc 20 investigation check. Furthermore a complete search of the room will also work with no check
A **dagger of venom** is in one of the display cases as well as trophies worth 300gp to the right collector.



## 18 Private Chapel 
*As you enter the once serene Private Chapel, a stark transformation is evident. The air is heavy with the scent of incense, and the familiar fey-inspired stained glass windows have been replaced with symbols of war and conquest. At the altar, a hobgoblin cleric adorned in armor, bearing the markings of a war god, performs dark rites. The sacred space has been desecrated in the name of Vasrock's war deity.*

**Spell Scrolls in the Secret Compartment:**
1. **Cure Wounds:** A scroll containing the divine magic to heal wounds. The spell can be cast to restore vitality and mend injuries.

2. **Bless:** A scroll inscribed with the divine words of blessing. Casting this spell bestows advantages on attack rolls and saving throws to allies.

3. **Guiding Bolt:** A powerful scroll that invokes radiant energy. It can be cast to hurl a bolt of light at a target, providing advantage on the next attack against that target.

4. **Shield of Faith:** A scroll containing the sacred formula for protection. The spell can be cast to create an invisible barrier, increasing the target's AC.

5. **Detect Evil and Good:** A scroll with the divine magic to sense the presence of celestial, fiendish, undead, or aberration creatures in the vicinity.

![map](sliberbergcastlefourthfloor.jpg)
## 19 Fourth Floor Landing 
*As you ascend the grand staircase to the fourth floor, you arrive at the landing that serves as the threshold to the royal quarters. The air here is charged with anticipation, and a sense of heightened security is palpable. However, your arrival does not go unnoticed, as a formidable hobgoblin mage stands guard, flanked by an array of carefully arranged magic paintings on the floor.*

**Hobgoblin Mage Tactics:**
- **Spellcasting (Pushing Spells):**
  - The hobgoblin mage specializes in spells that manipulate force, pushing and displacing targets. Spells like Thunderwave, Gust, and Mage Hand are in their arsenal.
  - The mage strategically aims to use these spells to push characters towards the magic paintings, capitalizing on both arcane and environmental hazards.
  - the mage has following spells prepared
        1. **Mage Armor:** The mage prepares themselves for the encounter, casting Mage Armor to enhance their defenses.

        2. **Thunderwave (Level 2):** A powerful spell that creates a wave of thunderous force, pushing creatures away and potentially causing damage.

        3. **Mage Hand:** The mage uses Mage Hand creatively to manipulate objects and push characters, creating strategic advantages.

        4. **Gust of Wind:** Unleashing a strong gust of wind, the mage can push creatures away and control the battlefield.

        5. **Levitate:** The mage gains control over gravity, allowing them to lift and move themselves or other creatures, potentially pushing them into the magic paintings.

        6. **Ray of Frost:** A ranged spell that can slow down a target, providing the mage with additional control over the battlefield.


- **Magic Painting Traps:**
  - The magic paintings on the floor are enchanted to trigger upon contact, releasing forces that can cause harm. Each painting corresponds to an elemental force (fire, air, water, earth) and deals damage according to its theme.
  - Characters pushed into these paintings must make saving throws to avoid taking damage.

## 20 Royal Nurseries:
*Entering the corridor on the third floor, you find yourself facing a trio of nurseries,each adorned with delicate cribs, plush toys, and the promise of dreams that were never fulfilled. It's evident that these nurseries, meant for royal children, have never had the joy of hosting their intended occupants.*

**Royal Nurseries on the Fourth Floor:**

*As the party ascends to the fourth floor of the royal residence, they are met with the soft glow of moonlight filtering through the windows. Entering the space, they discover three nurseries, each adorned with delicate cribs, plush toys, and the promise of dreams that were never fulfilled. It's evident that these nurseries, meant for royal children, have never had the joy of hosting their intended occupants.*

Much of the castle was erected in an era when King Fredrick still clung to the hope of reuniting with Sophia, the love he lost in the devastating flood that claimed Bergherz. In those days, Sophia would often weave dreams of their perfect family, sharing with Fredrick the vision of a life they would build together. At the heart of this fantasy were three children: a son to carry the family name and two daughters to grace the castle halls with laughter and innocence.

The royal nurseries, standing testament to their shared dreams, were crafted in anticipation of the joyous reunion that never came to pass. Each crib, every lovingly adorned corner, and the soft hues that painted the walls told a tale of a future imagined but never realized. The castle itself, a monument of love and longing, echoed with the whispers of the family that never was, each room built with the anticipation of the day Sophia and Fredrick would once again embrace as a family.

- **Unfinished Paintings:**
  - Each nursery features an unfinished painting depicting the intended occupants—faces left blank, awaiting the portraits of the royal children who were never born. The canvases capture a poignant sense of unfulfilled expectations and unrealized dreams.

## 21 Royal Bathroom 
*The royal bathroom surprises you with its grandeur, featuring a large bath, ornate mirrors, and finely crafted cabinets. While exploring the room, your attention is drawn to a particular cabinet. Upon discovering the hidden compartment, a cache of magical items is revealed, providing a valuable resource for the challenges that lie ahead.*

**Cache Contents:**
1. **Pearl of Power:**
   - A lustrous pearl enchanted with magical energy. When held, it allows the attuned user to regain one expended spell slot of up to 3rd level per day.

2. **Wand of Magic Missiles:**
   - A slender wand that, when activated, can unleash a barrage of magical missiles. The wand contains 7 charges and regains 1d6 + 1 expended charges daily at dawn.

3. **Spell Scrolls (Evocation, Level 1 and 2):**
   - Several spell scrolls containing evocation spells of both 1st and 2nd levels. These scrolls can be used by characters with the appropriate spellcasting abilities to cast the spells.



## 22 Fredrick's Personal Armory

*As you enter Fredrick's Personal Armory, the sheer scale of the weaponry and armor on display is immediately striking. Massive suits of armor crafted for a beastman's formidable stature stand like silent sentinels, each piece adorned with intricate engravings depicting scenes of nature and fey creatures. Enormous blades, befitting a creature of immense strength, are neatly arranged on weapon racks. The air is heavy with the scent of oiled metal and aged leather. The armory is a testament to Fredrick's might, his connection to the fey, and the legacy of a ruler who once bore arms for both battle and the pursuit of lost love.*

## 23 Queen's powder room
*You step into the Queen's Powder Room, a chamber of opulence and regal allure. At its center stands a massive vanity, adorned with intricate carvings and crowned by a mirror that reflects the room's untouched elegance. Shelves line the walls, hosting busts adorned with jewelry and a collection of tiaras fit for a queen. The room is a testament to King Fredrick's unwavering commitment to provide the queen with a space of unmatched luxury. However, the telltale signs of disuse reveal a melancholic truth - the queen's powder room, despite its splendor, has never seen the application of powders or the donning of tiaras. The room remains an unexplored sanctuary, locked in time, where dreams of shared moments and royal preparations have been replaced by the solemn silence of an unfulfilled reality.*



## 24 Boss Encounter: The Royal Bedchamber:**

*hallway opens up to reveal three royal nurseries, each meticulously prepared yet forever empty. The first, adorned in shades of blue, whispers of adventures and knightly tales, waiting for the laughter of a young prince. The adjacent rooms, dressed in soft pinks and lavenders, exude a tender charm, their cribs untouched by the lullabies meant for princesses. It's a poignant tableau frozen in time, where the promise of a royal family remains unfulfilled, leaving the nurseries as silent witnesses to the untold stories that might have been*
**Magic Paintings:**
1. **Street Festival:**
   - Provides cover and distraction, making it difficult for ranged attacks to target Vasrock.

2. **Coastal Scenes (Pounding Surf):**
   - Creates barriers of pounding surf, offering both defense and hindrance to movement.

3. **Forge Painting:**
   - Transforms into a fiery forge, dealing fire damage to anyone entering or exiting.

4. **Thunderstorm Painting:**
   - Generates a zone of raging thunderstorm, causing random lightning strikes that can damage and disorient.

## 25 Royal Wardrobe:
*Adjacent to the Royal Bedchamber, the Royal Wardrobe awaits exploration. Stepping into the opulent space, rows of tailored garments hang in anticipation. Imposing beastman-sized regal costumes, adorned with fey-inspired motifs, stand as a testament to Fredrick's presence. However, the real poignancy lies in the untouched elegance of the queen's dresses. Cascading fabrics, delicate lace, and intricate embroidery grace dresses fit for a queen, yet they hang in silent repose, their beauty untouched and uncelebrated. The hushed atmosphere whispers of an unfulfilled promise, a vision of grandeur that never materialized in the life of the lonely beastman ruler.*


## Conclusion

Latter that day once it is clear to the defenders that Vasrock is dead Fredrick and the rest of the sliberberg army storm the outer walls.The goblins are quickly routed. Fredrick leads the charge with an almost supernatural grace with the broadsword. After the battle the force thing the Fredrick does is track down the player characters. He thanks the players.

The beast will take the player characters aside to speak with them privately. His voice, usually firm and commanding, now carries a hint of veiled fear or perhaps desperation. He implores the heroes to stay in Sliberberg and serve in his court. He emphasizes that he will not force them, nor imprison them against their will. His demeanor makes it clear he cannot accept a refusal.

For those who show hesitation or outright decline, the Beast offers titles of nobility and plots of land within the kingdom. To the ladies among the player characters who have established a unique bond with him, he extends an offer to become his consort.

An astute player, with a keen sense of insight, may discern that the Beast is harboring concerns he is hesitant to voice. An Insight check (DC 15) will pick up on his emotial turmoil. For 500 years he has not had any close personal relationships. Most of followers and subjects revere him almost divinely but from a distance. The player characters are the first people who got close to him in year. He also has start to have romantic feelings with at least one female player character

A persuasive character may seek to ease the Beast's apprehension. A successful Persuasion check (DC 20) prompts the Beast to open up, albeit cautiously. He revels his reasons for aking the players to stay.

When the heroes agree to stay and assist, the Beast expresses gratitude and immediately turns to them for help in recruiting vital allies. He seeks the aid of the centaurs, the elves, and the half-orcs, hoping to forge an alliance that will fortify the kingdom against the looming peril. The next chapter of their adventure unfolds as they navigate the delicate threads of diplomacy and unity, building a formidable force to safeguard Sliberberg and confront the shadows on the horizon.

# Appendix
## Vasrock

**Vasrock, Hobgoblin Warlock of the Blade**
*Medium humanoid (goblinoid), neutral evil*

**Armor Class** 15 (chain shirt)
**Hit Points** 78 (12d8 + 24)
**Speed** 30 ft.

**STR** 14 (+2)
**DEX** 16 (+3)
**CON** 14 (+2)
**INT** 12 (+1)
**WIS** 10 (+0)
**CHA** 18 (+4)

**Saving Throws** Str +4, Dex +5
**Skills** Athletics +4, Deception +7, Intimidation +7
**Senses** Darkvision 60 ft., Passive Perception 10
**Languages** Common, Goblin

**Spellcasting:**
- **Spellcasting Ability:** Charisma
- **Spell Save DC:** 15
- **Spell Attack Bonus:** +7

**Warlock Features:**
- **Pact Magic (5th Level):** Vasrock has four 2nd-level spell slots and two 3rd-level spell slots.
- **Eldritch Invocations:** Thirsting Blade, Agonizing Blast
- **Pact of the Blade:** Vasrock can use his action to create a pact weapon in his empty hand. The weapon is a finesse melee weapon, and he is proficient with it.

**Fey Presence:**
- As a bonus action, Vasrock can cause creatures of his choice within 10 feet to make a Wisdom saving throw (DC 15) or be charmed or frightened until the end of his next turn.

**Blade Pact Features:**
- Vasrock's pact weapon is a finesse melee weapon, and he adds his Charisma modifier to its attack and damage rolls.
- When Vasrock hits a creature with his pact weapon, he can expend a spell slot to deal additional damage (1d8 per level of the spell slot).

**Actions:**
- **Pact Blade Attack:** Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 1d8 + 3 slashing damage.
- **Eldritch Blast:** Ranged Spell Attack: +7 to hit, range 120 ft., one target. Hit: 1d10 + 4 force damage.

**Reactions:**
- **Counterstrike:** When Vasrock is hit by a melee attack, he can use his reaction to make a melee attack against the creature that hit him.

**Legendary Actions (3):**
- **Teleportation:** Vasrock can magically teleport up to 30 feet to an unoccupied space he can see.
- **Blade Flourish (Costs 2 Actions):** Vasrock makes an additional Pact Blade Attack.
- **Fey Step (Costs 2 Actions):** Vasrock can cast Misty Step without expending a spell slot.

**Equipment:**
- Chain Shirt
- Pact Blade (Finesse Melee Weapon)
- Component Pouch
- Arcane Focus
- Cloak of Elvenkind

**Traits:**
- **Master Fencer:** Vasrock gains a +2 bonus to attack rolls made with finesse weapons.
- **Tactical Commander:** Vasrock can use a bonus action to grant advantage on the next attack roll made by an ally within 30 feet who can see or hear him.

