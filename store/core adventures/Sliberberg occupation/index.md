---
title: Into The Märchenweltgrenze Part 2 Sliberberg Occupation 
date: "2023-10-15T23:40:32.169Z"
description: The second Adventure in the Märchenweltgrenze chronicles
---

# Introduction

## Recap of the story so far
**The Märchenweltgrenze Expedition**

Our intrepid adventurers embarked on a perilous expedition into the Märchenweltgrenze, a realm where the feywild and material plane intertwine. Commissioned by the Holy Empire of Valoria to quell goblin raids along the eastern frontier, our heroes arrived at the besieged village of Eichenheim.

In the midst of chaos, they distinguished themselves in repelling the goblin siege, catching the eye of the powerful witch Agatha Ironraisin. Seeking aid, Agatha proposed a pact: the players would undertake a quest to investigate the silence shrouding five settlements within the Märchenweltgrenze. The reward? Valuable intel on the mysterious goblin army threatening the region.

**The Unveiling Quest**

As the players ventured into the heart of Märchenweltgrenze, they uncovered unsettling truths. These goblins, unlike their brutish kin, displayed a civilization-like ruthlessness. The plot thickened as potential allies with their own motives emerged, each with tantalizing side quests.

Upon completing the quest, Agatha revealed the grim reality: the goblin army had seized Sliberberg, the largest settlement in the Märchenweltgrenze. Rumors spread like wildfire that the mighty near archfey ruler of sliberberg, Frederick the Beast, had been defeated and captured by these cunning invaders.

## An introduction to Sliberberg

![map](sliberberg_1.jpg)


### Sliberberg Docks:

When the Märchenweltgrenze wrought its changes upon the city, the once bustling lower city found itself submerged beneath the rising waters of the lake. What emerged from the depths is the labyrinthine network of Sliberberg Docks, a maze-like sprawl of boardwalks, stone jetties, and wooden piers that extend into the expansive lake. This district, now elevated above the waters, serves as the primary hub for trade, maritime activities, and a haven for those who thrive on the fringes of legality.

**Gribnok's Rule:**

At the helm of this lawless expanse is Gribnok, a cunning goblin pirate admiral and his marauders. Under their rule, the docks have taken on an air of lawlessness and chaos, a reflection of the pirate's preference for disorder. Warehouses, boardinghouses, and various shady businesses line the precarious streets of the docks, providing cover for clandestine activities and illicit trade.

**Features of Sliberberg Docks:**

1. **Maze of Streets and Piers:**
   - The network of streets on the docks is a confusing maze, with narrow alleyways, twisting paths, and makeshift bridges connecting different sections. Navigating this area requires a keen sense of direction or the guidance of a local who knows the ever-changing layout.

2. **Lawlessness and Contraband:**
   - The lack of a centralized authority has given rise to a flourishing black market. Smugglers, thieves, and opportunistic merchants engage in the illicit trade of goods, magical contraband, and information. The docks are a place where secrets change hands as frequently as stolen goods.

3. **Gribnok's Marauders:**
   - Gribnok and his goblin marauders maintain a loose grip on the docks, allowing a level of anarchy that suits their piratical inclinations. The marauders extort fees from businesses, run protection rackets, and operate a small fleet of commandeered ships that patrol the lake.

4. **Shady Establishments:**
   - The docks are home to a variety of establishments that cater to the unsavory clientele frequenting the area. Gambling dens, underground fight clubs, and establishments with dubious reputations offer entertainment and services for those willing to take the risk.

### Sliberberg Upper City:

Perched atop the Sliberberg Hill and encased within the original city walls, the Sliberberg Upper City stands as a testament to the city's historical opulence. Once a district of mansions and affluence, the Upper City has weathered the upheavals of the Märchenweltgrenze with a resilience that speaks of its enduring grandeur. However, under the oppressive rule of General Vasrock, this area has undergone a profound transformation, morphing from a bastion of wealth to a militarized stronghold.

**Features of Sliberberg Upper City:**

1. **Historic Mansions:**
   - Stately mansions with grand facades and lush gardens still stand, remnants of a bygone era. However, these once-majestic homes now serve as barracks, command centers, and administrative offices for the occupying goblin forces.

2. **Military Stronghold:**
   - The Upper City has been repurposed into a military base, its streets now bustling with goblin soldiers, hobgoblin commanders, and military personnel. The once-elegant boulevards echo with the march of boots, and the air is heavy with the scent of iron and discipline.

3. **Restricted Access:**
   - General Vasrock has declared the Upper City off-limits to most residents. Entry is heavily restricted, and only those with explicit military authorization are permitted beyond the imposing gates. The district's transformation has created a stark division between the ruling goblin elite and the subjugated populace below.

## Market Ward:

Encompassing both vibrant marketplaces and bustling residential neighborhoods, the Sliberberg Market Ward serves as the beating heart of the city. Despite the challenges brought about by the Märchenweltgrenze and the goblin occupation, this district remains a testament to the resilience and diversity of the city's inhabitants.

**Features of Sliberberg Market Ward:**

1. **Thriving Marketplaces:**
   - Colorful stalls and bustling market squares characterize the Sliberberg Market Ward. Vendors peddle a diverse array of goods, from fresh produce and exotic spices to magical curiosities and enchanted artifacts. The marketplaces are a haven for both trade and social interaction.

2. **Residential Diversity:**
   - Nestled between market streets are a variety of residential neighborhoods. Homes range from modest cottages to larger townhouses, showcasing the diversity of the city's population. Fey and material plane residents coexist, forming a unique tapestry of cultures and traditions.

3. **Goblin Presence:**
   - Goblin soldiers patrol the market streets, enforcing the rules set by the occupying forces. Market vendors are subjected to tariffs and regulations, with goblins overseeing transactions to ensure compliance. The district's vibrancy is tinged with an undercurrent of tension.

4. **Tight-Knit Communities:**
   - Despite the challenges, tight-knit communities persist within the Market Ward. Residents band together, sharing resources and support to endure the hardships imposed by the goblin occupation. Secret resistance cells quietly operate, their members working to undermine the occupiers from within.

5. **Militarized Checkpoints:**
   - Goblin checkpoints are strategically positioned throughout the ward, monitoring the flow of both goods and residents. Those passing through checkpoints are subjected to searches, and identification papers are scrutinized. Movement within the city is closely monitored.


### Sliberberg West Quarter:

Emerging as a stark symbol of the goblin occupation's impact on the city, the Sliberberg West Quarter is a haphazardly constructed district, hastily built to accommodate the influx of new residents forced into servitude by General Vasrock. This quarter stands as a testament to the cruel ambitions of the goblin forces and the suffering endured by those captured in the wake of the Märchenweltgrenze.

**Features of Sliberberg West Quarter:**

1. **Forced Resettlement:**
   - The West Quarter is a result of General Vasrock's policy of forced resettlement, where captured residents are herded into makeshift camps and cramped living conditions. Families torn apart by goblin forces find themselves struggling to survive in this hastily constructed and often squalid environment.

2. **Slave Camps:**
   - The heart of the West Quarter is dominated by sprawling slave camps where captured residents are kept under watchful eyes. The camps are surrounded by makeshift fences, guarded by goblin overseers, and the air is heavy with the despair of those yearning for freedom.

3. **Goblin Overseers:**
   - Goblins assigned as overseers patrol the West Quarter, enforcing the will of General Vasrock. Cruel and indifferent, these goblin enforcers ensure that the captives remain compliant, meting out punishment to those who dare to resist.

### Sliberberg Damside:

Carved out to serve the strategic needs of General Vasrock's military machine, Sliberberg Damside stands as a testament to the goblin forces' calculated ambitions. This district, located to the north of the old city, is a sprawling labyrinth of under-construction mills, aqueducts, and water-powered manufacturing plants, all designed to produce the equipment necessary for Vasrock's formidable army.

**Features of Sliberberg Damside:**

1. **Water-Powered Manufacturing:**
   - The primary purpose of Damside is evident in its water-powered manufacturing plants. The goblin forces have harnessed the energy of the dam to fuel a network of mills and production facilities. The constant hum of machinery echoes through the district.

2. **Maze of Construction:**
   - Damside is a maze of under-construction buildings, aqueducts, and infrastructure. The goblin engineers work tirelessly to complete the district, transforming the landscape into a chaotic blend of scaffolding, cranes, and half-finished structures.

3. **Goblin Engineers and Overseers:**
   - Goblin engineers, overseers, and enslaved workers toil together in the construction of Damside. The goblin forces maintain strict control over the workforce, ensuring that progress aligns with Vasrock's military objectives.

4. **Aqueduct Network:**
   - A complex network of aqueducts crisscrosses the district, diverting water from the dam to power the mills and machinery. These water channels are essential to the functioning of the manufacturing plants and contribute to the industrial aesthetic of Damside.

## Locations in Sliberberg

### Sliberberg City Watch Compound:

Nestled against the ancient walls of the old city, the Sliberberg City Watch Compound stands as both a bastion of authority and a symbol of the goblin occupation. Serving as the headquarters for Captain Sarek's city guard, this fortified compound houses the goblin soldiers responsible for maintaining order in the occupied city. Its stone walls enclose barracks, training grounds, and a foreboding city jail, where dissenters are held captive.

**Features of Sliberberg City Watch Compound:**

1. **Fortified Bastion:**
   - The compound is a small, fortified fort strategically positioned along the city walls. Tall stone towers and sturdy walls provide an imposing facade, and the watchtowers offer a vantage point for overseeing the city below.

2. **Training Grounds:**
   - Adjacent to the barracks are training grounds where goblin soldiers undergo rigorous drills and exercises. The clang of weapons and the shouts of drill sergeants echo through the compound, creating an atmosphere of martial discipline.

3. **City Jail:**
   - The city jail, concealed within the depths of the compound, is a foreboding structure with thick iron bars and minimal comforts. Those who oppose goblin rule or violate the stringent regulations find themselves confined within its cold, damp cells.

### Vasrock Palace (Formerly Sliberberg Castle):**

Once a symbol of the city's grandeur and the seat of power for the noble rulers, Sliberberg Castle has been transformed into Vasrock Palace—a fortress that echoes with the dominance of the goblin general and his oppressive rule. Perched atop Sliberberg Hill, the palace overlooks the city, its ominous presence casting a shadow over the once-majestic structure.

### Sliberberg Slave Camp:

A grim testament to the cruelty of General Vasrock's occupation, the Sliberberg Slave Camp stands on the outskirts of the city—a crude assemblage of rough barracks and wooden walls that confines those captured and subjugated by the goblin forces. This desolate compound serves as a stark reminder of the forced labor imposed upon the residents of the city.

**Features of Sliberberg Slave Camp:**

1. **Crude Barracks:**
   - Roughly constructed barracks made from timber and salvaged materials house the enslaved residents of Sliberberg. The living conditions are harsh, and the air is filled with a sense of despair as captives endure their forced confinement.

2. **Wooden Enclosure:**
   - A wooden enclosure surrounds the camp, creating a barrier between the enslaved inhabitants and the outside world. Guard towers at regular intervals are manned by goblin overseers, ensuring constant vigilance over the captive population.

3. **Guard Towers:**
   - Tall guard towers rise above the camp's perimeter, providing vantage points for goblin overseers to surveil the enslaved population. Armed with weapons and a disdain for any form of resistance, these towers symbolize the camp's oppressive atmosphere.
### Sliberberg Town Hall (Upper City):

Nestled within the upper echelons of the city, the Sliberberg Town Hall has been repurposed by General Vasrock's forces to serve as a seat of goblin authority. Goruk, the hobgoblin mage and acting mayor, spends most of his time within these walls, overseeing administrative matters and ensuring the goblin occupiers maintain control. This is the sole place in the upper city where the general citizenry is permitted, albeit under the watchful eyes of goblin enforcers.

**Features of Sliberberg Town Hall:**

1. **Goblin Bureaucracy:**
   - The town hall has been transformed into the epicenter of goblin bureaucracy. Goblin scribes and officials attend to administrative tasks, processing documents, and maintaining records to ensure the smooth functioning of the city under goblin rule.

2. **Public Audience Hall:**
   - A large public audience hall is designated for gatherings and announcements. Here, goblin commanders address the general citizenry, conveying messages of goblin superiority and reiterating the consequences of disobedience.

3. **Goblin Guards:**
   - Goblin guards patrol the halls of the town hall, ensuring that order is maintained and that the goblin occupiers' directives are followed. The presence of armed guards creates an atmosphere of surveillance and reinforces the power dynamic within the upper city.

**Sliberberg Harbormaster's Office (Gribnok's Naval Headquarters):**

Commandeered by the goblin pirate admiral Gribnok, the Sliberberg Harbormaster's Office has undergone a transformation into the nerve center of the goblin navy. Gribnok splits his time between this strategic headquarters and his personal war galley docked nearby. The office now serves as a focal point for naval operations and the coordination of maritime dominance under the goblin pirate's command.

### Sliberberg Harbormaster's Office:

1. **Gribnok's Command Room:**
   - The central chamber of the office has been converted into Gribnok's command room, adorned with nautical charts, maps, and trophies from successful naval exploits. Gribnok strategizes and issues orders from this room, directing the movements of his fleet.

2. **Goblin Quartermasters:**
   - Goblin quartermasters work tirelessly within the office, managing the logistics of the goblin navy. Supplies, provisions, and repairs for the fleet are coordinated from this hub, ensuring that Gribnok's ships are always ready for action.

**Brigadeer Theldor's Headquarters (Converted Mansion - Upper City):**

Amidst the opulence of the upper city, Brigadeer Theldor's Headquarters stands as a stark contrast—a converted mansion that reflects the spartan tastes and disciplined ethos of the hobgoblin commander. Theldor, a no-nonsense leader, has eschewed luxury in favor of practicality as he oversees the main army force stationed within the upper city.

**Features of Brigadeer Theldor's Headquarters:**

1. **Militarized Mansion:**
   - The mansion, once a symbol of wealth and privilege, has been transformed into a militarized stronghold under Theldor's command. The exterior is devoid of ornate decorations, emphasizing functionality over aesthetics.

2. **Parade Grounds:**
   - Adjacent to the mansion, open spaces have been converted into parade grounds where hobgoblin soldiers undergo drills and exercises. Theldor emphasizes discipline and combat readiness, and the parade grounds are a constant hive of activity.

3. **Command Quarters:**
   - Theldor's personal quarters within the mansion are modest and utilitarian. The commander's spartan tastes are evident in the lack of extravagance, with only essential furnishings and military maps adorning the room.

4. **Tactical War Room:**
   - A designated war room serves as the nerve center of Theldor's strategic planning. Maps, troop dispositions, and reports are organized methodically, reflecting the hobgoblin commander's meticulous approach to warfare.

5. **Barracks:**
   - The mansion's rooms and halls have been repurposed into barracks for Theldor's soldiers. Bunk beds and storage for weapons and armor fill the once-grand spaces, creating a sense of efficiency and resourcefulness.

6. **Mess Hall:**
   - A communal mess hall occupies a central space within the mansion, where hobgoblin soldiers gather for meals and camaraderie. The atmosphere is disciplined, with minimal conversation and a focus on sustenance.

**Sliberberg Black Market (Docks):**

Hidden within the labyrinthine network of warehouses and back alleys in the docks, the Sliberberg Black Market operates as a clandestine hub for illicit trade and subversive activities. Evolving from one concealed location to another, this shadowy marketplace caters to those seeking forbidden goods, information, and services, all under the watchful eyes of covert merchants and shady characters.

**Features of Sliberberg Black Market:**

1. **Rotating Locations:**
   - The black market is not confined to a fixed location, with organizers constantly changing the venue to evade detection. Warehouses, hidden alleys, and concealed corners of the docks serve as temporary meeting places for those engaged in illicit transactions.

2. **Shady Merchants:**
   - Merchants operating within the black market are individuals of questionable repute—smugglers, fences, and information brokers who thrive in the shadows. The participants include both fey and material plane denizens, creating a diverse and discreet clientele.
#### THing avaliable in the black market

Certainly! Here's an extended list that includes mundane items from the DMG that can aid in the commission of crimes, along with the poisons, drugs, and magic items associated with darker arts:

1. **Drow Poison:**
   - A potent poison often associated with the drow. It induces unconsciousness and is favored by assassins and those seeking to incapacitate their enemies discreetly.

2. **Midnight Tears:**
   - A rare poison derived from the venom of shadowy creatures. It causes excruciating pain and is often used by those with a taste for cruelty.

3. **Crawling Madness:**
   - A sinister drug that induces hallucinations and nightmarish visions. Those who partake in Crawling Madness often find their grasp on reality slipping away.

4. **Serpent's Kiss:**
   - A poison crafted from the venom of serpents enchanted by dark magic. It induces paralysis and is favored by those who seek to immobilize their victims.

5. **Witchfire Elixir:**
   - A mysterious elixir said to grant temporary magical powers but at a grave cost. Consuming Witchfire Elixir is considered an unholy pact with dark forces.

6. **Necrotic Essence Vial:**
   - A vile substance infused with the essence of necrotic magic. When applied to weapons, it enhances their ability to drain life force from the target.

7. **Ebon Shroud:**
   - A dark cloak woven from the shadows of the Shadowfell. Wearing the Ebon Shroud grants the ability to blend into darkness and move unseen.

8. **Vial of Abyssal Taint:**
   - A vile concoction rumored to be distilled from the essence of the Abyss. When ingested, it induces a temporary state of madness and chaos.

9. **Soulbound Dagger:**
   - A wicked dagger infused with dark enchantments. When it draws blood, it siphons a portion of the victim's life force, empowering the wielder.

10. **Lament of the Banshee:**
    - A cursed locket containing the mournful wail of a banshee. When opened, it releases a haunting sound that can induce fear and despair in those who hear it.

11. **Shadow Veil:**
    - A magical cloak that wraps its wearer in shadows, granting advantage on Stealth checks. It is rumored to be woven from the essence of shadow creatures.

12. **Bloodthorn Poison:**
    - A poison derived from rare thorny plants infused with dark magic. It causes excruciating pain and can lead to internal bleeding.

13. **Abyssal Resin:**
    - A sticky black resin harvested from the depths of the Abyss. When applied to weapons, it adds a vile, corrosive effect to each strike.

14. **Darkling Elixir:**
    - A mysterious elixir created from the blood of darklings. Consuming it grants enhanced senses in low light but also brings a heightened susceptibility to shadows.

15. **Veil of Despair:**
    - A cursed veil said to be woven from the threads of despair. Those who wear it find their emotions heightened, often succumbing to overwhelming sadness and hopelessness.

16. **Blade of Shadows:**
    - A wicked dagger that seems to absorb and reflect ambient light. When wielded in darkness, it becomes nearly invisible, making it a favored tool of assassins.

17. **Cursed Grimoire:**
    - A forbidden spellbook containing dark incantations. Reading from the cursed grimoire can attract the attention of malevolent entities from other planes.

18. **Baneblood Tonic:**
    - A foul concoction brewed from the blood of creatures associated with the lower planes. Ingesting it grants temporary strength at the cost of mental clarity.

**Mundane Items for Crime:**

19. **Thieves' Tools:**
    - A set of specialized tools, including picks and tiny saws, designed for covert entry and lock manipulation.

20. **Forgery Kit:**
    - Tools used for creating convincing forgeries of official documents, signatures, and seals.

21. **Disguise Kit:**
    - A collection of cosmetics, wigs, and other items for creating disguises and altering one's appearance.

22. **Poisoner's Kit:**
    - A set of tools and vials for crafting and applying poisons discreetly.

23. **Spyglass:**
    - A small, collapsible telescope for observing from a distance without being noticed.

24. **Invisible Ink:**
    - A substance that can be used to write messages that are invisible until exposed to heat, revealing the hidden text.

25. **Locking Manacles:**
    - Enchanted manacles that can only be unlocked by a specific key, preventing easy escape.

26. **Silent Whistle:**
    - A specially crafted whistle that produces a sound outside the normal range of hearing, ideal for signaling allies covertly.

27. **Smokestick:**
    - A small stick that, when ignited, produces a thick cloud of smoke, providing cover for a swift getaway.

28. **Caltrops:**
    - Sharp, spiked objects scattered on the ground to impede pursuers or create hazards.

29. **Dark Hooded Clo

ak:**
    - A hooded cloak designed to conceal the wearer's features and blend into shadowy environments.

30. **Thief's Cant Dictionary:**
    - A small book containing coded language and symbols used by thieves and criminals to communicate covertly.


# On the Road to Sliberberg
Read the following


*As you ascend the winding mountain pass, the air becomes increasingly charged with tension. The once-scattered signs of goblin activity grow more concentrated, evident in the fresh scent of construction lingering in the mountain air. Villages, recently built and teeming with goblin workers, dot the landscape like ominous growths.Reaching the summit, you are met with a breathtaking sight. Below lies Bergherz Valley, a vast expanse dominated by a colossal lake, its waters reflecting the changing hues of the sky. The lake, more akin to a sea, stretches as far as the eye can see, nestled at the valley's heart.*

# Arrival
The players will arrive in Sliberberg late in the afternoon. The players approach from the western side of the city. The city wall around this part of the city is still under construction and gangs of slaves can be seen toilin on the wall from road. When they near the wall they notice a long line of people waiting at what appears to be a checkpoint at the gat leading through the few completed section of walls. Read the following to the players

*The imposing gates of sliberberg rise before you, their iron and stone structure a testament to the city's former grandeur. Goblin guards, armed and vigilant, patrol the entrance. At the forefront stands a stern hobgoblin, scrutinizing each traveler with a calculating gaze. The air is thick with tension as the fate of the city and the captured near archfey, Frederick the Beast, weighs heavily on your shoulders. Your task is clear - slip past the gates unnoticed, avoiding the hobgoblin's suspicion and the piercing gaze of the goblin guards.*

**Skill-Based Encounter: Infiltrating the Gates of sliberberg**

*The gates loom large, guarded by a sizable force of goblin sentries. The hobgoblin at the forefront eyes you with suspicion, his keen instincts on high alert. To avoid raising the alarm and entering sliberberg undetected, your group must navigate through this perilous checkpoint. Success hinges on your ability to blend in, deceive, or use your skills to distract the guards.*

**Objective:** Get past the gates without raising suspicion.

**Skills/Abilities to Utilize:**
1. **Deception:** Convince the hobgoblin and guards that you are ordinary travelers.
2. **Stealth:** Move quietly and inconspicuously through the crowd.
3. **Persuasion:** Attempt to sway the hobgoblin with smooth talk or feigned cooperation.
4. **Arcana/Nature:** Utilize magical or natural means to create distractions or illusions.

**Challenges:**
1. **Hobgoblin Scrutiny:** The hobgoblin's keen eye can see through deception. Players need to succeed in a Deception check against his scrutiny.
2. **Goblin Patrols:** Navigate through or distract the patrolling goblin guards using Stealth or magical means.
3. **Crowded Checkpoint:** Use Persuasion or create distractions to navigate through the bustling checkpoint without drawing attention.

**Consequences:**
- **Success:** The players enter sliberberg unnoticed, gaining valuable information on the city's current state.
- **Failure:** The alarm is raised, and the goblin guards swarm, leading to a potentially dangerous situation.

*DM Guidance: Encourage creativity and reward clever solutions. The players should feel the tension and the urgency of the situation. Tailor the DCs to the party's abilities, and be ready to improvise based on their choices.*

**Finding Tansy Fleetfoot's Address:**
With the players through the checkpoint they now need to find Tansy's shop without raising the suspicions of the occupying forces. Read the following
*As you enter the bustling lower city of sliberberg, the sights and sounds of goblin occupation surround you. The air is thick with a mix of exotic spices and the acrid scent of the goblin presence. Agatha's instructions lead you to Tansy Fleetfoot, a halfling witch and leader of the Silver Moon Coven. To locate her bakery cafe, you'll need to rely on your street smarts and navigate the winding alleys.*
**Objective:** Find Tansy Fleetfoot's bakery cafe in the lower city.

**Skill Check: Streetwise**
- **DC 12:** Basic knowledge of the lower city layout.
- **DC 15:** Identify notable landmarks and navigate through moderately crowded areas.
- **DC 18:** Utilize information from locals or other shopkeepers to get closer to the specific location.

**Complications:**
1. **Goblin Presence:** Goblins patrol the lower city. Drawing too much attention may lead to unwanted scrutiny.
2. **Misdirection:** Some locals may provide false information intentionally or unintentionally, requiring players to discern truth from deceit.

**Consequences:**
- **Success:** You locate Tansy Fleetfoot's bakery cafe without drawing undue attention.
- **Partial Success:** You find the general vicinity but may attract some curiosity from goblin patrols.
- **Failure:** The players get lost or follow the wrong lead, potentially drawing attention from both goblins and other unsavory characters.

## Tansy Silvermoon cafe and bakery.
The silvermoon cafe is a SLiberberg landmark. Located at the edge of the lower city marketplace it is famed for its enchanted baked goods and charming decor. When the players reach the cafe read the following
*As you navigate through the labyrinthine alleys of the lower city marketplace, you finally stumble upon the address Agatha provided. A quaint street just off the bustling market reveals a three-story half-timber building, standing proudly amidst its neighbors. The structure exudes a sense of care and attention, as though it defies the general weariness of the occupied city.The building is adorned with tasteful decorations, speaking volumes about the pride of its owner. Above the entrance, a charming sign swings gently in the breeze. A silver crescent moon-shaped croissant catches the eye, its silver-painted wood gleaming in contrast to the building's warm hues. In graceful lettering, the words "Silver Moon Cafe" accompany the celestial symbol, inviting you to enter a haven of warmth and charm in the midst of the goblin-occupied city.*
When the players enter read the folowing.
*Stepping into the Silver Moon Cafe, you find yourself enveloped in a warm, aromatic haven that transcends the chaos outside. The teahouse and bakery combo emanates a comforting blend of scents - freshly baked pastries, the earthiness of tea leaves, and the subtle undertone of magical herbs. The interior is charmingly adorned, with small round tables covered in delicate lace, each accompanied by a mismatched but cozy chair. The focal point of the room is a large display counter, showcasing an array of tempting baked goods. Young ladies, dressed in a unique fusion of chef and witch attire, move gracefully behind the counter, their hands expertly crafting delectable treats. The air is alive with the hum of activity as they whisk, knead, and bake, their movements almost rhythmic. However, your attention is momentarily drawn to an unexpected detail. On the inner side of the entrance door, a scorch mark in the unmistakable shape of a goblin serves as a stark reminder of the city's current plight.*
After the finish taking in the ambiance read the following
*Behind the counter stands the owner, a halfling woman in her mid-forties, dressed in traditional witches' clothes, a bakers' apron adding a touch of practicality to her ensemble. You assume this must be Tansy Fleetfoot. As you enter, she ducks under the counter, her tone carrying a thick Cockney accent as she greets you with a blend of exasperation and warmth.*
*"Where you been? I've been expectin' you two bleedin hours ago!"*
*With a quick, deft motion, she ushers you towards a doorway hidden behind the counter. The door opens to a stairway leading downward.*
The door leads into Tansy arcane workroom. Feel free to describe the strange devices and parphanilia in the room. Tansy is loathed to talk until she is sure that the coast is clear. She looks around the dining room suspiciously for a few moments. Read the following when she done.
*The door creaks shut behind you as Tansy locks the door behind her.Tansy eyes each of you up and down, her expression an odd mix of scrutiny and casual dismissal."So, you're the so-called heroes Agatha's been rattling on about, eh? Seen better, but beggars can't be choosers." She slaps her hand on the workbench, the sound echoing through the arcane chamber. "Let's get to brass tacks, then. If you want to deal with the goblins, you've got your work cut out for ya." Her tone shifts, becoming more serious as she leans against the workbench."About seven months ago, an army of about 20,000 fey goblins, hobgoblins, bugbears, darklings, and a few ogres showed up in the valley. Real nasty piece of work leading 'em, goes by the name of General Vasrock. Took sliberberg in a single day, crowned himself king, and started expanding like a moldy fungus into the surrounding valleys. Vasrock's got the whole place in a stranglehold, and the city's suffering for it. Now, if you want to make a difference, you'll need to know more about this 'civilized' goblin army and their ambitious leader."*
At this point Tansy will answer questions in a direct in impatient manor using cockney slang and  casual insults thrown in. She was not done explaining whats' what and she will take every opritunity to remind the players often bordering on rudeness to get her point across. When the players are ready to listen read the following.
*"Now, here's the rotten news. General Vasrock's got a grip on this place like a giant's fist. Roughly 8,000 of his  soldiers are squatting in the city at all times. But that's not all. Thanks to your little escapades and the success of the expedition, he's gotten himself some fancy backup – three companies of mercenaries, about a thousand strong, camped nearby, itching for a fight. I reckon the entire expedition's got, at most, a thousand fighting men." She looks at each of you, a glint of determination in her eyes. "But don't go throwin' in the towel just yet. I've got a plan, and it involves cuttin' off the head of this snake. There are five commanders running the show day to day in sliberberg. Gribnok, in charge of the goblin navy; Goruk, the hobgoblin mage, acting mayor, and head honcho of Vasrock's spell-slingers; Captain Sarek, who's got the city guard under his thumb; Brigadier Theldor, leading the main force; and Ryla, a nasty bugbear running the slave trade."Take them out, and the whole house of cards will come crashing down. It'll create the chaos you need for a general uprising. Even the odds, give the people a fighting chance to boot these goblins out of the city. You lot game for this? Because if we're gonna succeed, we'll need every trick in the book and a bit of luck on our side."*
If the players agrees she cracks a smile and leads the players up a door into a walled off backyard in the middle of the block. Read the following to the players. 

*"Right then,"* Tansy says, surveying the area. *"Agatha told me she gave you a one of her tiny cottages. Pop it up by the bakery wall there. Should keep prying eyes away, and it's as good a base as any in this tangled mess of a city."*
*As Tansy points to a spoSeems like a nifty safehouse kind of dealt against the bakery's wall, you retrieve the magical item Agatha mentioned. With a whispered incantation and a flourish, the cottage expands, revealing a cozy interior with all the essentials for a makeshift home. Tansy smirks approvingly.z*

# the occupation and the resistance
**Section: Occupation Mechanics and Resistance Operations**

In the shadowed streets of Sliberberg, the oppressive rule of General Vasrock's goblin army has cast a dark pall over the once vibrant city. To navigate this perilous occupation and kindle the flames of rebellion, understanding the intricacies of the occupation mechanics and resistance operations is paramount. This section unveils the mechanics governing daily life under goblin rule, explores the challenges faced by the inhabitants, and outlines the clandestine efforts of the burgeoning resistance to reclaim their city. Delve into the heart of occupation, where whispers of rebellion echo through the alleyways, and every act of defiance brings the city one step closer to liberation.
## Trespassing and Stealth:
The city streets, once freely traversed by the diverse inhabitants of Sliberberg, now bear the weight of occupation. With the iron fist of General Vasrock's goblin army, certain quarters of the city have been designated as off-limits to the citizenry. These forbidden zones include the privileged precincts of the upper city, the tumultuous construction sites of damside, the emerging settlements within the west quarter, and select areas along the docks, now under the control of the cunning Admiral Gribnok and his pirate marauders.

In addition to these restricted zones, an oppressive curfew shrouds the city after nightfall, forcing its denizens to retreat behind closed doors. Venturing into the forbidden territories or lingering in the streets past curfew paints any daring individual as a trespasser, facing the wrath of the goblin occupiers. This section outlines the consequences and mechanics associated with these trespassing regulations, illustrating the challenges faced by those who dare to defy the imposed boundaries in their quest for liberation.
1. **Trespassing:** Whenever characters wish to trespass into a restricted area like the slave camp, docks, or construction zone, ask for a group Dexterity (Stealth) check to determine if they can remain unnoticed. The check is determined by the army's vigilance level If the characters fail their Dexterity (Stealth) check, they are spotted by goblin patrols or overseers. 
**Interaction with Goblin Authorities:**

1. **Persuasion and Deception:** If the characters are spotted, they can attempt to talk their way out of the situation. Ask for a group Charisma (Persuasion) or Charisma (Deception) check, depending on their approach.

2. **Offering an Explanation:** Players should provide a plausible explanation for their presence in the restricted area. The quality of the explanation should influence the DC of the check.

3. **Consequences of Failure:** If the Persuasion or Deception check fails, the goblin authorities may detain the characters or demand a bribe. The consequences can vary depending on the situation and the actions of the characters.

4. **Creating Distractions:** Characters may also attempt to create a distraction to divert the attention of goblin authorities, allowing them to escape without being noticed.

5. **Reputation and Disguises:** The characters' reputation or the use of disguises can provide advantages or disadvantages in these interactions. A character with a known history of opposing the goblins may have a harder time deceiving them.

6. **Critical Success and Failure:** Consider the impact of critical successes and failures. A critical success in Persuasion or Deception might convince the goblins to let the characters pass without further suspicion, while a critical failure could lead to immediate aggression or detainment.

## Goblin Army Combat Encounter Table (Level 3 Players):

1. *Goblin Patrol (CR 1)*: A small group of goblin scouts and warriors led by a hobgoblin sergeant. (4 Goblins, 1 Hobgoblin)

2. *Hobgoblin Ambush (CR 2)*: A strategically placed group of hobgoblin soldiers and goblin archers in an ambush scenario. (3 Hobgoblins, 2 Goblin Archers)

3. *Ogre Enforcers (CR 3)*: A pair of enslaved ogres, forced to protect a critical area. (2 Ogres)

4. *Goliath & Goblin Brigade (CR 3)*: A mixed group of goliath mercenaries and goblin warriors working together. (1 Goliath, 3 Goblins)

5. *Centaurs on Patrol (CR 3)*: A centaur squad scouting the outskirts, supported by goblin skirmishers. (2 Centaurs, 2 Goblin Skirmishers)

6. *Sylvan Shadowmark Archers Snipers (CR 2)*: A team of elf archers taking advantage of the terrain, providing ranged support. (3 Elf Archers)

7. *Goblin Reinforcements (CR 2)*: A contingent of goblin soldiers arrives to bolster a previously encountered group. (4 Goblins)

8. *Hobgoblin Elite Squad (CR 3)*: A disciplined and well-armed team of hobgoblin soldiers with a cunning strategy. (4 Hobgoblins)

9. *Double Trouble (CR 4)*: Two different groups converge, requiring the players to fend off both simultaneously. (Mix of any two of the above groups)

10. *Goblin Commander (CR 5)*: The players face a powerful goblin general and his personal bodyguard, backed by assorted goblin forces. (1 Goblin General, 2 Hobgoblin Bodyguards, 3 Goblins)


## Alarms and Reinforcements:
**Alarms and Reinforcements: The Perilous Echoes of Defiance**

In the silent dance of shadows beneath the goblin-occupied skyline, every step taken off the sanctioned path carries the weight of potential consequences. Trespassing, a perilous act of rebellion, may unfurl into a tumultuous symphony of alarms that echo through the cobblestone streets. Should the clandestine endeavors of the players erupt into open conflict with the goblin patrols or the watchful eyes of the occupiers, the city becomes a cauldron of tension where the shrill cry of alarms pierces the night air.

When the clash of steel and arcane energies disrupts the cloak of silence, the possibility of raising alarms becomes a pressing threat. The resonating clatter of weapons, the desperate cries for freedom, or the flare of magical energies can all serve as a clarion call to the goblin forces stationed throughout Sliberberg. Reinforcements, swift and disciplined, rush to the scene of the skirmish, turning the clandestine confrontation into a high-stakes battle against time.
1. **Triggering an Alarm:** When the player characters are detected by a goblin patrol, there is a chance that the patrol will sound an alarm by blowing horns or raising the alarm in some other way. The alarm may also be triggered if the player characters engage in combat within earshot of other patrols. When an alarm is triggered, reinforcements are alerted to the intruders' presence.

2. **Disadvantage on Stealth Checks:** After the alarm is triggered, the player characters have a disadvantage on all Stealth checks for a certain duration. This disadvantage reflects the heightened vigilance and readiness of the goblin forces to detect the intruders.

3. **Duration of Disadvantage:** The duration of the disadvantage on Stealth checks can vary. It might last for a specific number of rounds or minutes, depending on the circumstances. The duration should be communicated to the players once the alarm is triggered.

4. **Chances of Reinforcements:** When an alarm is triggered, the DM should roll a percentile dice to determine the likelihood of reinforcements arriving. The chance of reinforcements can vary based on the specific situation.

   - For example, if the player characters were detected but managed to eliminate the patrol quickly, the chance of reinforcements might be lower (e.g., 30%).
   - If the alarm is raised in a more critical area or the intruders are in a high-security zone, the chance of reinforcements might be higher (e.g., 70%).

5. **Reinforcements:** If the roll indicates that reinforcements are coming, additional goblin forces or patrols will arrive to confront the player characters. The size and composition of the reinforcements can depend on the situation and should be determined by the DM.
### Running the Resistance: 

In the clandestine heart of Sliberberg, where the echoes of defiance reverberate through narrow alleyways and whispered conspiracies take root, the Resistance emerges as a beacon of hope amid the oppressive shadows of goblin rule. This section unveils the mechanics and dynamics of leading the Resistance, where players become architects of liberation, orchestrating acts of rebellion to reclaim their city from the clutches of General Vasrock's tyrannical grip.
**Resistance Operations and Milestone Benefits**

In the clandestine war against the goblin occupation of Sliberberg, the players' leadership of the Resistance is crucial. As they recruit allies and grow the Resistance's ranks, they unlock significant milestone benefits that shape the narrative and enhance their capabilities. Here are the rules for Resistance Operations:

#### **Resistance Operations Framework:**
As the players lead the Resistance, they can assign tasks to their followers to gather information, sabotage goblin operations, recruit new members, and more. Each task carries a level of difficulty, and the success or failure is determined by the skills and capabilities of the assigned followers.

**Recruitment Points:** Players gain Recruitment Points (RPs) for successful completion of tasks. RPs contribute to the overall growth of the Resistance.

#### **Milestone Benefits:**
As the flames of resistance burn brighter, and the rallying cry against goblin oppression echoes through the city of Sliberberg, the tireless efforts of the brave heroes at the forefront begin to bear fruit. With each significant milestone reached, the Resistance gains newfound strength and capabilities, unlocking strategic advantages to tip the scales in their favor.

These milestones represent not just numerical achievements but the tangible impact of the players' leadership, inspiring hope and rallying the diverse citizens of Sliberberg against the goblin occupation. As the Resistance grows, so do the benefits, offering a glimpse of a city on the brink of reclaiming its freedom.
1. **Recruitment Surge (Milestone 1):**
   - **Number of Members Needed:** 10
   - **Effect:** Gain access to a pool of recruits for non-combat tasks.
   - **Allies Stat Blocks:** Commoners (MM p.345) with potential for growth in proficiency.

2. **Veteran Allies (Milestone 2):**
   - **Number of Members Needed:** 20
   - **Effect:** Gain access to veteran allies with unique abilities.
   - **Allies Stat Blocks:** Thug (MM p.350), Spy (MM p.349), or Scout (MM p.349) with additional skills.

3. **Citywide Network (Milestone 3):**
   - **Number of Members Needed:** 30
   - **Effect:** Gain information about goblin movements and patrol schedules.
   - **Allies Stat Blocks:** City Watch (MM p.346) members with knowledge of goblin activity.

4. **Safe House Expansion (Milestone 4):**
   - **Number of Members Needed:** 40
   - **Effect:** Gain additional safe houses with unique benefits.
   - **Allies Stat Blocks:** Adept (MM p.342) acting as magical support within the safe houses.

5. **Sewer Passages (Milestone 5):**
   - **Number of Members Needed:** 50
   - **Effect:** Unlock secret passages for rapid and discreet travel.
   - **Allies Stat Blocks:** Thieves (MM p.349) skilled in navigating the city's underbelly.

6. **Guerrilla Tactics Training (Milestone 6):**
   - **Number of Members Needed:** 60
   - **Effect:** Resistance members gain bonuses to stealth and combat.
   - **Allies Stat Blocks:** Veterans (MM p.350) with expertise in guerrilla warfare.

7. **Master of Disguise (Milestone 7):**
   - **Number of Members Needed:** 70
   - **Effect:** Gain the ability to effectively disguise the party and allies.
   - **Allies Stat Blocks:** Assassins (MM p.343) skilled in subterfuge and disguise.

8. **Artifact Cache (Milestone 8):**
   - **Number of Members Needed:** 80
   - **Effect:** Choose one powerful magical item to aid the party.
   - **Allies Stat Blocks:** Mages (MM p.347) or Archmages (MM p.342) who discovered the cache.

9. **Elite Strike Team (Milestone 9):**
   - **Number of Members Needed:** 90
   - **Effect:** Gain access to an elite strike team for high-risk missions.
   - **Allies Stat Blocks:** Knights (MM p.347) or Champions (MM p.345) renowned for their combat prowess.

10. **Inspiring Symbol (Milestone 10):**
    - **Number of Members Needed:** 100
    - **Effect:** Boost overall Resistance morale and increase chances of citywide support.
    - **Allies Stat Blocks:** Inspirational leaders, Bards (MM p.343), or Paladins (MM p.347) fostering hope.


### **Resistance Operations:**

### **Resistance Operations:**

#### **1. Recruitment Drive:**
   - **Description:** Launch a campaign to recruit new members to join the Resistance.
   - **Difficulty:** Easy
   - **Benefits:** Gain 1 to 3 d6 worth of new recruits.
   - **Requirements:** Access to public spaces, charismatic leader.
   - **Skill Check:** Charisma (Persuasion)
   - **Consequence of Failure:** Lowers morale among citizens, reducing potential recruits. Checks for future recruitment drives are made with disadvantage for two weeks

#### **2. Intelligence Gathering:**
   - **Description:** Deploy spies and informants to gather information on goblin movements and activities.
   - **Difficulty:** Moderate
   - **Benefits:** Gain 1d4 bits of information from the rumor table in appendixes of this document.
   - **Requirements:** Established citywide network, spies in place.
   - **Skill Check:** Dexterity (Stealth), Intelligence (Investigation)
   - **Consequence of Failure:** Increases suspicion among goblin forces, potentially leading to increased security. Increase the checks for trespassing by 2.

#### **4. Safe House Establishment:**
   - **Description:** Secure new locations for safe houses across the city.
   - **Difficulty:** Moderate
   - **Benefits:** A new safehouse is established somewhere in the city.
   - **Requirements:** Intelligence on city layout, discreet agents.
   - **Skill Check:** Intelligence (Investigation), Charisma (Deception)
   - **Consequence of Failure:** Safe house locations compromised, risking the safety of Resistance members. Visiting the safehouse has 1 in 6 chance of triggering an alarm

#### **5. Propaganda Campaign:**
   - **Description:** Spread messages of resistance and hope among the citizens.
   - **Difficulty:** Easy
   - **Benefits:** The players have advantage on checks to persuade townsfolk for several days.
   - **Requirements:** Skilled orators, access to public spaces.
   - **Skill Check:** Charisma (Performance), Charisma (Persuasion)
   - **Consequence of Failure:** Spreads misinformation, potentially sowing doubt among citizens. Players have disadvantage on checks to persuade townsfolk for several days

#### **6. Infiltrate Goblin Operations:**
   - **Description:** Send agents undercover to gather intelligence within goblin-controlled areas.
   - **Difficulty:** Challenging
   - **Benefits:** The players learn one piece of true intel about a specific commander of the DM's choosing.
   - **Requirements:** Master of Disguise milestone, skilled infiltrators.
   - **Skill Check:** Dexterity (Stealth), Charisma (Deception)
   - **Consequence of Failure:** Agents exposed, leading to increased scrutiny and danger. Lose recruits assigned to task

#### **7. Guerrilla Warfare Training:**
   - **Description:** Train Resistance members in guerrilla tactics for hit-and-run skirmishes.
   - **Difficulty:** Moderate
   - **Benefits:** All resistance fighters gain +2 to attack roles and AC for several days.
   - **Requirements:** Guerrilla Tactics Training milestone, experienced trainers.
   - **Skill Check:** Strength (Athletics), Dexterity (Acrobatics)
   - **Consequence of Failure:** Reduced effectiveness in future skirmishes, potential injuries among trainees. -1 to attack rolls and ac

#### **8. Magical Artifact Recovery:**
   - **Description:** Seek out hidden caches of magical artifacts to aid the Resistance.
   - **Difficulty:** Challenging
   - **Benefits:** Acquire one rare or very rare magical item for the party's use.
   - **Requirements:** Artifact Cache milestone, knowledge of artifact locations.
   - **Skill Check:** Intelligence (Arcana), Wisdom (Perception)
   - **Consequence of Failure:** Attracts magical attention, potentially exposing the Resistance.

#### **9. Disrupt Goblin Communication:**
   - **Description:** Interfere with goblin communication channels to create confusion.
   - **Difficulty:** Moderate
   - **Benefits:** Alarms have a 1 in 4 chance of failing for several days.
   - **Requirements:** Knowledge of goblin communication methods, skilled arcane users.
   - **Skill Check:** Intelligence (Arcana), Dexterity (Sleight of Hand)
   - **Consequence of Failure:** Strengthens goblin resolve, leading to improved communication. Doubles the chances that alarms attract renforements

#### **10. Citywide Uprising Preparation:**
   - **Description:** Lay the groundwork for a citywide uprising against the goblin occupation.
   - **Difficulty:** Challenging
   - **Benefits:** Increase the chances of success for a large-scale rebellion.
   - **Requirements:** Inspiring Symbol milestone, widespread support.
   - **Skill Check:** Charisma (Persuasion), Intelligence (History)
   - **Consequence of Failure:** Delays the uprising, potentially resulting in increased goblin oppression.



#### **11. Secure Supplies:**
   - **Description:** Organize a mission to secure essential supplies for the Resistance.
   - **Difficulty:** Moderate
   - **Benefits:** Secure enough equipment to arm 1d8 fighters as thugs, guards, or veterans, adding that many fighters to the pool of Resistance fighters.
   - **Requirements:** Knowledge of supply caches, skilled procurement team.
   - **Skill Check:** Intelligence (Investigation), Strength (Athletics)
   - **Consequence of Failure:** Shortages impact the effectiveness of the Resistance, reducing morale among existing members.

### **Operation Progression:**

- Each operation has a level of difficulty and associated Recruitment Points (RPs) awarded upon successful completion.
- Skill checks contribute to the success or failure of operations, adding an element of chance and strategy.
- Consequences of failure provide realistic challenges, shaping the ongoing narrative of the Resistance.
### **Operation Progression:**

- Each operation has a level of difficulty and associated Recruitment Points (RPs) awarded upon successful completion.
- Skill checks contribute to the success or failure of operations, adding an element of chance and strategy.
- Consequences of failure provide realistic challenges, shaping the ongoing narrative of the Resistance.
### **Progression:**
Players progress through the milestones by accumulating Recruitment Points (RPs). Each successful task earns a specific number of RPs, and when a predetermined total is reached, the Resistance achieves the corresponding milestone.

### **Challenges and Setbacks:**
Failures in Resistance tasks may lead to setbacks, loss of followers, or even reprisals against the city's inhabitants. Balancing risks and rewards is crucial for success.

### **Unlocking Milestones:**
Achieving significant milestones unlocks new options and opportunities for the Resistance. These milestones mark the progression of the Resistance's influence and impact on the occupied city.

### **Evolving Storylines:**
The Resistance Operations framework seamlessly integrates with the evolving narrative of the players' journey, allowing for a dynamic and player-driven experience.

# Confronting the Mercenaries

## The centaur mercenaries: The Thunderhoof Lancers
The Thunderhoof Lancers, a renowned company of centaur mercenaries, were once celebrated for their prowess on the battlefield and their adherence to a code of honor. Originating from the vast grasslands within the feywild, these centaur warriors were feared and respected, seeking glory in the heat of combat. Hired directly by the goblin general Vasrock, the Thunderhoof Lancers were enticed by the promise of a king's ransom for their services. The contract, initially seen as an opportunity for financial gain and stability, soon became a source of discontent among the centaurs. What was supposed to be a venture into the heart of battle and honor devolved into mundane duties—guarding the outskirts of Sliberberg and participating in routine raids that hardly showcased their true capabilities.

### Entering the Centaur Mercenary Camp:
It takes an hour to reach the camp from the city. Read the following as they approach the camp
 *You approach the centaur mercenary camp, situated in an expansive, empty field on the outskirts of the bustling construction zone. The rhythmic sounds of hammering and construction echo in the distance, contrasting with the openness of the camp. The camp itself is a sprawling collection of weathered tents and makeshift shelters arranged in a circular formation, with an open central area that serves as a training ground. Towering centaurs, clad in battle-worn armor and bearing lances, go about their daily routines. There's a palpable sense of disquiet within the ranks of these formidable warriors. As you draw nearer, you notice signs of unrest. Centaurs engaged in hushed conversations, their eyes darting around nervously. A few of them appear weary and disillusioned, their usual comradery strained. It's clear that not all is well within this once-unified mercenary band. The camp itself exudes a blend of both martial discipline and disquiet, mirroring the uncertainty that plagues the centaur lancers. Tensions are simmering beneath the surface, and it's evident that there may be opportunities to learn more or even sway their allegiance.*
### Skill Encounter: Negotiating with Captain Rorin

 Captain Rorin, the centaur mercenary leader, is a formidable figure who is content with the current status quo of serving the goblins. To gain his support or form a contract with him, the players must navigate a tense negotiation.

**Objective:** Convince Captain Rorin to either join the players' cause against the goblins or agree to a lucrative contract.

**Skills and Abilities:**

1. **Persuasion:** Use your charisma and diplomatic skills to persuade Captain Rorin of the benefits of joining your cause or agreeing to a contract. (DC 15)

2. **Insight:** Read Rorin's reactions and body language to gauge his true motivations and feelings. (DC 13)

3. **Intimidation:** Present a strong case and arguments that subtly imply consequences if Rorin refuses to cooperate. (DC 16)

4. **Bribery:** Offer Captain Rorin a substantial bribe or valuable resources as an incentive to align with the players. (DC 18)

5. **Knowledge (Goblins):** Present information about the goblins, their true intentions, and any potential long-term threats that could affect the centaur mercenaries. (DC 15)

**Outcome:**

- **Success (10 or more successes):** Captain Rorin agrees to the players' terms. He either joins their cause, allowing the centaur mercenaries to assist in the fight against the goblins, or enters into a lucrative contract, providing the players with valuable centaur support in exchange for a substantial payment.

- **Partial Success (5-9 successes):** Captain Rorin remains undecided but is willing to consider the players' proposal further. Additional efforts may be required to fully secure his support or contract.

- **Failure (4 or fewer successes):** Rorin remains steadfast in his loyalty to the goblins, rejecting the players' proposal. This may necessitate an alternate approach or potentially circumventing Rorin in favor of other commanders.



#### Dueling Captain Rorin's Second in Command
 if the players fail to convince Rorin they are approached by a young centuar warrior who urges them to follow her to a secluded part of the camp watched over by centuars with weapons at the ready and stern looks on their faces. Read the following as she address the players

*"I've had enough of this charade," she confides, her voice a mixture of anger and determination. "I didn't join to play guardian to a goblin city. I wanted to lead charges, to face enemies in the heart of battle. Father won't listen, but I have a proposition for you. Help me in a duel against him, and I'll pledge my support to your cause."She gazes towards the camp where Captain Rorin stands resolute. "I can't defeat him alone, not yet. But with your assistance, we can change the course of things. I want to lead the Thunderhoof Lancers back to the glory we deserve. What do you say?"*

**Objective:** Help the second in command rig the duel in his favor to ensure his victory and gain his support.

**Skills and Abilities:**

1. **Stealth:** Infiltrate the centaur camp and gather information about Rorin's dueling preparations, helping to discover potential weaknesses or vulnerabilities. (DC 14)

2. **Persuasion:** Convince key centaur allies to secretly support the second in command, either through active assistance or by remaining passive during the duel. (DC 15)

3. **Sleight of Hand:** Sabotage or manipulate the equipment, weapons, or armor intended for Rorin in subtle ways to impair his performance during the duel. (DC 16)

4. **Deception:** Spread misinformation among the centaur mercenaries that casts doubt on Rorin's leadership and abilities, swaying opinion in favor of the second in command. (DC 14)

5. **Medicine:** Administer a substance or concoct a brew to subtly weaken Rorin's physical or mental state before the duel. (DC 16)

**Creative Solutions (Successes):**

- Uncover a secret leverage point that can be used to blackmail Rorin or convince him to yield before the duel.
- Persuade a centaur ally to act as a secret advisor, providing tactical advice to the second in command during the duel.
- Forge a forged letter from Rorin indicating his intent to step down as commander.
- Create a distraction during the duel to divert Rorin's attention and focus.
- Use magic or deception to create an illusion or hallucination during the duel, causing Rorin confusion and disorientation.

**Outcome:**

- **Success (4 or more successes):** The players successfully rig the duel in favor of the second in command. He emerges victorious, becoming the new leader of the centaur mercenaries. In gratitude, he pledges his support to the players' cause and offers the centaur warriors as allies.

- **Partial Success (2-3 successes):** The duel remains balanced, and the second in command narrowly defeats Rorin. He still becomes the new leader but holds some reservations about the players' involvement.

- **Failure (1 or no successes):** The duel ends in Rorin's victory, and the second in command is severely injured or worse. He is unable to provide support, and the players may face challenges with the centaur mercenaries.

## The elven mecenary camp: Sylvan Shadowmark Archers
The Sylvan Shadowmark Archers, an elite company of skilled elf archers, were once celebrated for their precision and agility on the battlefield. Originating from the mystical woodlands, they were hired by the archfey known as the lady of Loch Slaneak, the enigmatic Fey Enchantress, under the guise of a lucrative contract to safeguard her interests. Little did they know that their skills would be employed to bolster the forces of goblins and hobgoblins in Sliberberg.Now stationed within the city, the Sylvan Shadowmark Archers find themselves torn between duty and their moral compass. The men and women of the company, renowned for their honor and adherence to principles, yearn to sever ties with Vasrock's unholy alliance. However, breaking a contract with an archfey, especially one as conneiving  as the lady of Loch Slaneak, is a perilous undertaking.

### Entering the Sylvan Shadowmark Archers Mercenary Camp:
Read or paraphrase the following
*You approach the Sylvan Shadowmark Archers Mercenary Camp, a stark contrast to the bustling activity in the city of Sliberberg. Nestled in an open, sunlit field just beyond the city's borders, the camp exudes an air of serene beauty and precision, yet it is tinged with an underlying tension. The camp is a masterpiece of elven craftsmanship, where tall, slender tents and pavilions have been carefully woven from the forest's finest silks and branches, creating an ethereal and harmonious haven amid the natural surroundings. Elven archers, their figures graceful and lithe, practice their renowned marksmanship skills at elegantly designed archery ranges. But beneath the veneer of this serene camp lies a growing discontent. The elves, known for their fierce independence, are deeply dissatisfied with their contract and the alliance with the goblin forces. Disgruntled murmurs and the occasional heated argument betray an air of near open conflict with their employers. The elven archers, renowned for their precision and focus, appear restless, their eyes occasionally drifting toward the city's looming silhouette with frustration and longing. Amidst this camp of beauty and discord, a potential opportunity emerges for the players to sway the elves and earn their support or discover their role in the intrigue surrounding the goblin occupation.*

### Description of the First Meeting with Captain Serina:

*As you approach the Sylvan Shadowmark Archers Mercenary Camp, a tall, enigmatic elven figure steps forward to meet you. Her presence is commanding, and her graceful posture reveals the unmistakable grace of her kin. Her gaze is piercing and calculated, honed by years of experience in both the art of archery and the intricate dance of diplomacy. Dressed in finely crafted forest-green garb that blends seamlessly with the natural surroundings, Captain Serina stands with an air of quiet confidence. Her long, ebony hair is elegantly braided, and her eyes, a vivid shade of emerald, seem to scrutinize your every move. "I don't recall summoning any new recruits," she says, her voice as melodious as a serenade of the wind through the leaves. Her tone, however, carries a hint of suspicion. "What brings you to our camp?" Captain Serina's piercing eyes narrow slightly, and she glances back toward the elven archers who watch the encounter with vigilant expressions. "Are you working for Vasrock? Speak the truth, for we have little patience for deceivers here."*

### Skill Encounter: Convincing Captain Serina

 Captain Serina is initially convinced that the players work for Vasrock, the goblin commander. To gain her support and sympathy for their cause, the players must skillfully persuade her of their true intentions.

**Objective:** Convince Captain Serina that you do not work for Vasrock and earn her sympathy for your cause.

**Skills and Abilities:**

1. **Deception:** Craft a convincing story to explain your presence in the elven camp, emphasizing your opposition to the goblins and your true objectives. (DC 15)

2. **Insight:** Carefully observe Captain Serina's reactions and body language to gauge her level of trust and identify potential doubts in her conviction. (DC 13)

3. **Persuasion:** Use your charisma and diplomatic skills to appeal to Serina's empathy and convince her of your sincere intentions. (DC 14)

4. **Intelligence (Goblins):** Present information about the goblins, their actions, and true motives, proving that you are not in league with Vasrock. (DC 16)

5. **Survival:** Utilize knowledge of local terrain and fauna to provide evidence of your independent efforts, showcasing your competence and autonomy. (DC 14)

**Creative Solutions (Successes):**

- Reveal secret intelligence or confidential messages that expose your mission against the goblins.
- Use a persuasion check to make a heartfelt speech that appeals to Serina's sense of justice and duty.
- Find a common cause or shared objective between your party and the elven archers, demonstrating alignment in interests.

**Outcome:**

- **Success (4 or more successes):** Captain Serina is convinced of your sincerity and realizes you are not allied with Vasrock. She sympathizes with your cause and offers the support of the elven archers, who are eager to join your fight against the goblins.

- **Partial Success (2-3 successes):** While Serina is no longer convinced that you work for Vasrock, she remains cautious. The elven archers withhold their support but do not pose a direct threat to your party.

- **Failure (1 or no successes):** Captain Serina remains adamant in her belief that you are agents of Vasrock. The elven archers may even prepare for a confrontation, and the players must find an alternative solution to gain their support or navigate the situation.


## THe half orc shock troopers: Ironblood Marauders
The Ironblood Marauders, a formidable company of half-orc shock troopers, are a recent addition to the forces under Vasrock's command. Hired with the explicit purpose of countering the players' characters, they were intended to bring chaos and brute force to the opposition. However, problems surfaced from the very beginning. Known for their rowdy and boisterous nature, the Ironblood Marauders' hard-drinking and hard-partying ways clashed repeatedly with the disciplined goblin army. Many instances of insubordination and violent altercations ensued, creating a tumultuous environment within the already tense city of Sliberberg. The shock troopers, restless by nature, find the monotony of sitting idle in Sliberberg unbearable. Accustomed to the thrill of battle and the freedom of open fields, the half-orcs resent being confined within the city's walls and, more significantly, working under the command of the goblins. Their loyalty is to the thrill of combat and the camaraderie among themselves, not to the goblin army's hierarchy. As frustration simmers within the Ironblood Marauders, the potential for internal dissent and rebellion grows. The clash of cultures between the raucous half-orcs and the disciplined goblins has created a volatile mix, with both sides eyeing each other warily.

### Entering the Ironblood Marauders:

*You approach the Ironblood Marauders, the furthest from the city of Sliberberg and located adjacent to one of the slave camps. It is a stark departure from the organization and beauty of the elven and centaur camps. The atmosphere here is thick with a heady mix of raucous laughter, rowdy behavior, and the acrid scent of spilled ale.The camp itself appears to be a haphazard collection of mismatched tents, hastily assembled shelters, and rough-hewn fortifications. Tattered banners and mismatched armor are strewn about, testaments to the ill-disciplined and motley group that resides here. Half-orcs and goliaths, towering figures with rugged features, engage in various forms of revelry, clashing drinking horns in merriment. It's clear that the presence of the nearby slave camps and the goblins in the city have taken a toll on the mercenaries. Their once well-disciplined and fearsome reputation seems to have unraveled into a disorganized, disheveled assembly. The disquiet of the slave camps and the unpredictable goblin forces have driven them to find solace in alcohol to numb their worries. As you approach, you hear songs that mix tales of valor and adventure with solemn ballads that hint at inner turmoil. The Half-Orc and Goliath mercenaries appear to be more willing to listen, perhaps more desperate, than the other mercenaries in the city. Their ranks may provide an opportunity for the players to sway their allegiance or seek their support in their mission against the goblins.*


### Meeting Warlord Grukthar:

 *You find yourself standing before the towering, brawny figure of Warlord Grukthar, the leader of the Half-Orc and Goliath mercenaries. His imposing presence, accentuated by a mottled patchwork of tribal tattoos, exudes an air of seasoned combat experience and internal conflict.Warlord Grukthar's eyes, a penetrating shade of steel-gray, fixate upon you with a mixture of suspicion and curiosity. His face, adorned with several battle scars, reveals the inner struggle he currently faces. He has the visage of a leader who has seen many battles but is now torn between his loyalty to the goblin army and his growing concerns. Grukthar's attire, a combination of weathered armor and tribal ornaments, speaks to his diverse heritage and the amalgamation of cultures within his band. At his side, a massive, battle-worn greataxe stands as a testament to his prowess on the battlefield. The warlord's demeanor is stern but also contemplative, and he listens carefully to your words, gauging the sincerity of your intentions. It's clear that the disarray within the city, the ill-discipline of his mercenaries, and the plight of the slaves have led him to waver in his commitment to the goblin army. This moment presents an opportunity for the players to sway his allegiance and earn the support of the Half-Orc and Goliath mercenaries, should they choose to do so.*

### Skill Encounter: Negotiating with Warlord Grukthar

 Warlord Grukthar is currently wavering in his commitment to the goblin army, and the players have an opportunity to persuade him to reconsider his allegiance.

**Objective:** Convince Warlord Grukthar to withdraw his support from the goblin army and seek an alliance with your cause.

**Skills and Abilities:**

1. **Persuasion:** Use your charisma and diplomatic skills to present a compelling argument for why Grukthar should withdraw his support from the goblins. (DC 16)

2. **Intimidation:** Employ a commanding presence and strong arguments to emphasize the consequences Grukthar may face if he continues to support the goblins. (DC 16)

3. **Insight:** Carefully observe Grukthar's reactions and body language to gauge the extent of his wavering commitment and identify potential concerns. (DC 14)

4. **History (Mercenaries):** Share knowledge of mercenary history, showcasing examples of mercenaries who have broken contracts and emerged more favorably. (DC 15)

5. **Offer of Aid:** Promise military support, resources, or strategic assistance to bolster Grukthar's position in the city should he switch allegiance. (DC 18)

**Creative Solutions (Successes):**

- Provide evidence of the goblins' true intentions and any long-term consequences their presence may bring to the city.
- Reveal a secret weakness or vulnerability within the goblin army that can be exploited for Grukthar's benefit.
- Make an impassioned speech that appeals to Grukthar's sense of honor and responsibility to the city's inhabitants.
- Negotiate a lucrative contract or agreement with Grukthar's mercenaries to incentivize their cooperation.

**Outcome:**

- **Success (4 or more successes):** Warlord Grukthar is swayed by your persuasive arguments and the potential benefits of aligning with your cause. He withdraws his support from the goblins and pledges the loyalty of his Half-Orc and Goliath mercenaries to your mission.

- **Partial Success (2-3 successes):** Grukthar is intrigued but remains cautious. He may agree to reconsider his allegiance, but players must provide additional assurances or information to seal the deal.

- **Failure (1 or no successes):** Grukthar remains reluctant to change his allegiance, and players will need to find an alternative approach to gain his support or navigate the situation.


### Dwarfan stone masons: Granite Forge Masonry Guild
The Granite Forge Masonry Guild, a proud company of skilled dwarf stone masons, took a fateful contract from an agent of Gisela, the Fey Enchantress. Little did they know that the seemingly straightforward job in Sliberberg would entangle them in a web of horror. Tasked with fortifying the city, they soon realized the true nature of their employer's intentions when the goblin army began bringing in slaves. As the stone masons set to work fortifying the city, the realization of their unwitting involvement in a dark scheme dawned upon them. The once-stout-hearted dwarves found themselves entangled in a situation they desperately sought to escape. Witnessing the plight of the slaves forced into labor, the stone masons attempted, in vain, to quietly leave the city. Their escape attempts were thwarted by vigilant goblin soldiers who now watch their every move. The once-proud masons are now prisoners, shackled not by stone but by the nefarious contract they unknowingly signed. Every chisel strike and every stone laid is a reminder of their unwilling complicity in the suffering inflicted upon the enslaved inhabitants of Sliberberg.
#### The Dwarven Stone Mason Camp:
It takes two hours to reach the quarry to the west of the city

 *As you approach the Dwarven Stone Mason Camp, you're met with the imposing sight of a sprawling quarry that extends for several miles from the city of Sliberberg. The camp is the largest among the hireling camps surrounding the city, and it dominates the landscape with the laborious industry of the dwarven stone masons. The quarry is a stark, rugged environment, a testament to the tireless toil of the dwarven stone masons. Massive boulders, both uncarved and expertly chiseled, are scattered throughout the area. The dwarves work diligently alongside their enslaved labor force, extracting stone from the earth and meticulously crafting it into blocks of various sizes. Hobgoblin overseers, hated and resented by the dwarven masons, prowl among the laborers, issuing harsh commands with sharp authority. The enslaved workers, their faces etched with weariness and despair, toil under the watchful eyes of their captors. Their lives have been consumed by this backbreaking work, as the stone is extracted and transported to Sliberberg for the goblin army's construction projects. Despite the grim setting and the strained relations between the dwarves and their overseers, the Dwarven Stone Mason Camp possesses an undercurrent of determination. The dwarven stone masons are proud, skilled artisans who have been forced into labor against their will. An opportunity may exist for the players to sway them to their cause or, at the very least, uncover the intricacies of the quarry's operation in their ongoing efforts against the goblins.*

#### Foreman Gruff Stonecarver:

 *Before you stands a dwarf, Foreman Gruff Stonecarver, who appears to be a shadow of his former self. His once-distinguished beard is now disheveled and graying, his eyes are weary and ringed with dark circles, and his entire demeanor reflects the weight of an unbearable burden. Foreman Gruff's stout frame and powerful build have been overtaken by the toil and stress he endures daily. His worn, stone-covered apron and workman's attire, though practical and sturdy, have seen better days. In his calloused hands, he clutches a well-worn ledger, a symbol of his unyielding commitment to his labor. Deep lines etch Gruff's face, testimony to the countless sleepless nights spent wrestling with a troubling decision. His voice, once hearty and resonant, now carries a hollow quality as he speaks of his desire to break free from the oppressive contract.  "By Moradin's beard," he sighs, his words tinged with exhaustion, "I want out of this cursed deal, but I can't bear to think of my men, toiling without rest. I fear for their safety if I defy those hobgoblin overseers. They have no care for the art of stonecraft or the lives of dwarves." Foreman Gruff Stonecarver's eyes glisten with a weary determination, but his burden weighs heavily upon him. As he looks to the players for help, it's clear that he is torn between his desire for freedom and the responsibility he feels for his dwarven brethren.*

#### Skill Encounter: Negotiating with Gruff Stonecarver

  Foreman Gruff Stonecarver is eager to exit the goblin contract but needs convincing that the heroes have a viable plan to deal with the goblins and protect his dwarven masons.

**Objective:** Convince Foreman Gruff Stonecarver that the players have a solid plan to confront the goblins and ensure the safety of his dwarven masons. Promising access to weapons and armor can also count as a success.

**Skills and Abilities:**

1. **Persuasion:** Utilize your charisma and negotiation skills to present a compelling plan for tackling the goblin threat, showcasing how the dwarves' fighting skills can be strategically employed. (DC 12)

2. **History (Goblins):** Share your knowledge of the goblin army's operations, including potential weaknesses and vulnerabilities that can be exploited. (DC 14)

3. **Intelligence (Military Tactics):** Outline a tactical strategy for the dwarves to confront the goblins that maximizes their advantage, even with their outnumbered position. (DC 14)

4. **Promises of Support:** Pledge to provide the dwarves with weapons and armor to bolster their fighting capability, demonstrating a commitment to their cause. (DC 13)

5. **Insight:** Read Gruff's demeanor and listen carefully to his concerns, tailoring your argument to address his specific worries. (DC 12)

**Creative Solutions (Successes):**

- Provide a detailed, viable plan that addresses the dwarves' safety, tactics, and a potential alliance with other mercenary groups.
- Uncover and share critical information about the goblins' operations and intentions that can be exploited to the dwarves' advantage.
- Offer weapons and armor as a show of good faith and a tangible display of your commitment to their protection.

**Outcome:**

- **Success (4 or more successes):** Foreman Gruff Stonecarver is convinced that the players have a sound plan and will safeguard his dwarven masons. He agrees to join your cause and rally his fellow dwarves, enhancing your overall strength.

- **Partial Success (2-3 successes):** While Gruff finds your plan promising, he remains cautious. He may agree to a limited alliance but with reservations about the full extent of his involvement.

- **Failure (1 or no successes):** Gruff remains hesitant to commit, citing concerns for his dwarves' safety. Players must find an alternative approach to gain his support or navigate the situation.

# Sidequests and events
## Farids arrival
*As you settle into the makeshift safehouse in the backyard, the city's air thick with tension, a sudden disturbance draws your attention. The night sky becomes a canvas for the arrival of an unexpected guest – a man adorned in rich Arabic robes, a fez perched atop his head. He descends gracefully on a vibrant magic carpet, patterns dancing across its surface.*

*The man, known to you as Farid, touches down in the confined space of the backyard. His large, expressive eyes twinkle with mirth as he takes in the scene. "Ah, my esteemed friends! What a pleasure it is to see you once again!" he exclaims in his signature overly friendly tone, a warm smile playing on his lips.*

*If you've engaged in business with Farid before, his reception takes a more personal touch. "Ah, loyal customers! You honor me with your continued patronage. How may Farid, the purveyor of wonders, assist you today?" The air around him seems to shimmer with the promise of magical wares as he eagerly awaits your response.*

## Alchemy Laboratory Warehouse:
One of the warehouses in the docks has been turned into a labratory for the army alchemists. The players will find the doors unlocked when they approach the building. Read the following when the players enter
*Entering the repurposed warehouse in the docks, the air inside is thick with the acrid scent of alchemical concoctions. Shelves upon shelves are stacked with vials, jars, and mysterious ingredients. A large central table is cluttered with bubbling cauldrons, hissing flasks, and an intricate array of glass tubes connecting various apparatuses. Dim light filters through high windows, casting an eerie glow on the chaotic symphony of the alchemist's craft.*

As the players explore the alchemy laboratory, they unwittingly trigger a goblin alchemical trap. The room erupts in a blinding explosion of smoke, concealing the entrance of a squad of goblin alchemists armed with explosive vials. The players find themselves disoriented in the haze, their vision impaired as goblin alchemists unleash volatile potions. The alchemists, utilizing the labyrinthine shelves as cover, lob explosive concoctions with precise accuracy, forcing the players to navigate the chaos while dodging fiery explosions.

**Enemies:**
- Goblin Alchemists (x4): Nimble and quick, armed with explosive potions and makeshift bombs.
- Goblin Alchemist Leader: A more skilled alchemist, wielding a powerful concoction launcher and commanding the ambush with strategic precision.

**Terrain Features:**
- **Alchemical Workstations:** The cluttered workstations provide partial cover but may also hide volatile substances that can be triggered by attacks.
- **Glass Tubes:** Shattered glass tubes release harmful alchemical fumes, creating zones of difficult terrain.
- **Explosive Barrels:** Strategically placed barrels explode when damaged, potentially causing chain reactions and increasing the chaos.
In the aftermath the players find

1. **Potions of Healing (x12):** The players discover a cache of meticulously crafted healing potions on one of the alchemist workstations. Each potion of healing restores a moderate amount of hit points, offering a valuable resource for the challenges that lie ahead.

2. **Alchemical Supplies:** The crates scattered around the laboratory contain a wealth of alchemical ingredients and tools. While the players may not be alchemists themselves, these supplies could prove useful for crafting or potentially be of interest to knowledgeable NPCs.

3. **Gold Pieces (Approximately 20gp):** Scattered amidst the wreckage and the bodies of the goblin alchemists, the players find a small cache of gold pieces. Though modest, the gold can be used to replenish supplies or procure information in the city.

4. up to 6 of the following potions

   1. **Potion of Invisibility:** Grants the drinker invisibility for a limited duration, allowing for stealthy movement and actions.

   2. **Potion of Resistance:** Provides resistance to a specific type of damage (acid, cold, fire, lightning, poison, or thunder) for an hour.

   3. **Potion of Greater Healing:** Restores a significant amount of hit points to the drinker, aiding in recovery during or after combat.

   4. **Potion of Climbing:** Grants the ability to climb surfaces for an hour, facilitating exploration and evasion.

   5. **Potion of Fire Breath:** Bestows the ability to exhale a cone of fire for a short duration, dealing damage to creatures in its path.

   6. **Potion of Heroism:** Grants temporary hit points and immunity to being frightened, bolstering the drinker's courage and resilience in the face of adversity.



## Magic Arm and Armor Shipment
About a week after the players arrive a shipment of magic arms and armor arrive in the city by barge. The shipment is under heavy guard. Read teh following when the players reach the docks.
*Approaching the bustling docks, you witness a scene of orchestrated activity. A barge, laden with crates and chests, floats at the edge of a weathered wooden dock. Slaves, shackled and wearied, toil under the watchful eyes of hobgoblin overseers, transferring the cargo onto sturdy wagons. The crates, emblazoned with arcane symbols, contain a shipment of potent magic armaments. A palpable air of oppression hangs over the scene, as the slaves work silently, their expressions a mix of exhaustion and despair. Hobgoblin guards, armed with gleaming weapons, patrol the area, ensuring compliance with ruthless efficiency.*


As the players approach the dock, they notice an opportunity to liberate the shipment of magic arm and armor. The slaves, bound by chains and fear, present both an obstacle and potential ally. The hobgoblin guards, however, are a formidable force, and the players must navigate the chaos with care.

**Enemies:**
- **Hobgoblin Overseers (x2):** Ruthless commanders armed with whips and keen blades, skilled in coordinating slave labor.
- **Hobgoblin Guards (x4):** Armed with well-crafted weapons and disciplined in combat tactics, these guards are a formidable frontline force.
- **Enslaved Workers (x6):** Weary and weakened, the slaves are shackled but may be convinced to aid the players if given the chance.

**Terrain Features:**
- **Cargo Crates:** Large crates containing the magic arm and armor, providing cover for both the players and their enemies.
- **Wagons:** Sturdy wagons, ready to transport the cargo, can be utilized for cover or as a makeshift barricade.
- **Chains:** Chains binding the slaves may be used as improvised weapons or manipulated to create distractions.

**Objective:**
- **Liberate the Magic Armament Shipment:** The primary goal is to liberate the shipment of magic arm and armor from the docks. This can be achieved through a variety of means, including negotiation with the slaves, combat with the guards, or a combination of both.

**Rewards:**
- **Magic Arm and Armor:** The successful liberation of the shipment grants the players access to a variety of potent magic armaments. The specifics of these items can be tailored to suit the party's composition and needs. For example in my campaign it contained

1. **+1 Longswords (x10):** A collection of finely forged longswords, each enchanted to grant a +1 bonus to attack and damage rolls.

2. **+1 Shortbows (x10):** Compact and efficient shortbows, imbued with enchantments to enhance accuracy and damage (+1 bonus to attack and damage rolls).

3. **+1 Chainmail (x15):** Sets of chainmail, meticulously enchanted to provide enhanced protection (+1 bonus to AC) while maintaining flexibility.

4. **+1 Arrows (Quiver of 20):** A quiver containing 20 arrows, each magically enhanced to increase accuracy and inflict additional damage (+1 bonus to attack and damage rolls).

5. **+1 Shields (x5):** Sturdy shields, enchanted to bolster defenses and provide additional protection (+1 bonus to AC).

6. **+1 Crossbows (x5):** Heavy crossbows, each magically crafted to deliver powerful and accurate shots (+1 bonus to attack and damage rolls).

## On the Alchemist's Fire Barge:

*As the days pass in Sliberberg, a barge laden with crates filled with bottles of alchemist's fire arrives at the docks. The acrid scent of volatile substances wafts through the air as goblin dockworkers, overseen by hobgoblin guards, prepare to unload the cargo. The crates, stacked haphazardly, are a potential powder keg of destruction, waiting to be claimed by the goblin army. The urgency of the situation becomes apparent—the players must decide whether to let this dangerous arsenal fall into goblin hands or take action to prevent a potential catastrophe.*

**Combat Encounter: Fiery Confrontation**

Upon approaching the alchemist's fire barge, the players discover that the volatile cargo has the potential to wreak havoc on Sliberberg if it reaches the goblin army. Hobgoblin guards and goblin workers, unaware of the players' intentions, defend the barge with lethal efficiency.

**Enemies:**
- **Hobgoblin Pyromancers (x2):** Skilled in the art of fire magic, these hobgoblins use alchemist's fire to devastating effect.
- **Goblin Dockworkers (x6):** Armed with crude weapons and shields, these goblins work to unload the alchemist's fire crates and will fight to protect the cargo.
- **Hobgoblin Guards (x4):** Armed with spears and disciplined in combat, these hobgoblins maintain order on the barge.

**Terrain Features:**
- **Crates of Alchemist's Fire:** The primary objective and potential hazard. The players can choose to destroy the crates, preventing the cargo from reaching the goblin army.
- **Narrow Walkways:** The barge features narrow walkways, providing limited maneuverability and potentially pushing the players into close quarters combat.
- **Ropes and Pulleys:** Strategic use of ropes and pulleys can be employed for both traversal and as improvised weapons to hinder or control enemy movements.

**Objective:**
- **Prevent the Shipment:** The primary goal is to prevent the alchemist's fire from reaching the goblin army. This can be achieved by either destroying the crates, convincing the workers to abandon their tasks, or neutralizing the hobgoblin guards.

**Hazards:**
- **Alchemist's Fire Explosions:** Destroying the crates of alchemist's fire can trigger fiery explosions, damaging nearby creatures and setting sections of the barge ablaze.

**Rewards:**
- **Alchemist's Fire Reserves:** If the players manage to prevent the shipment, they can salvage a portion of the alchemist's fire for their own use—a volatile resource capable of dealing fire damage in combat or providing a strategic advantage in future encounters.

## **Side Quest: The Prince's Gambit**

Two weeks have passed since the players arrived in Sliberberg, each day marked by the weight of goblin oppression. However, amidst the struggle, a critical event unfurls that demands the immediate attention of the Resistance. The news reaches the ears of the players through the clandestine network of spies or the informative channels of Tansy Fleetfoot. Prince Fredrick, the near-immortal ruler of Sliberberg, is to be sent to the Hexmarches as a symbolic olive branch. The goblin leaders hope this gesture will pave the way for a less tumultuous coexistence. If they ask Tansy as to why Vasrock is doing this she answers as follows

*As she unravels the enigma, Tansy discloses a tale that spans centuries – a tale of ancient grievances and fey vendettas. "Gisela, the Archfey herself, harbors a deep-seated hatred for Prince Fredrick," Tansy reveals, her eyes reflecting the flickering candle flames. "For centuries, she's woven a tapestry of misery around him, making his immortal life a constant struggle."*

### **Combat Encounter: The Prince's Rescue**
*As you navigate the shadowed alleys of Sliberberg, the hushed whispers of the Resistance guide you towards a clandestine location. The night air is thick with tension, the moonlight painting a treacherous path through the narrow city streets. Suddenly, a low rumble signals the approach of a heavily guarded convoy winding its way through the heart of the city.*

*Your eyes are drawn to a cage secured on the back of a heavily armored wagon, where Prince Fredrick languishes, a mere specter of his former self. The flickering torchlight reveals a scene of torment – multiple injuries, gaunt features, and the weight of prolonged suffering etched into the once-noble countenance. A formidable force of goblins and hobgoblins surrounds the wagon, moving with military precision.*

*This is the moment, within the city's very confines, to intercept the convoy, liberate Prince Fredrick, and face the formidable might of Vasrock's forces.*
#### **Scene Setup:**
- The convoy is moving along a broad avenue in the market ward pass.
- Prince Fredrick is held in a sturdy cage on the back of a heavily armored wagon.
- The goblin and hobgoblin escort is well-organized, utilizing the terrain to their advantage.

#### **Objectives:**
1. **Rescue Prince Fredrick:** Liberate the prince from his cage without causing him further harm.
2. **Defeat Vasrock's Forces:** Subdue or defeat the goblin and hobgoblin escort.
3. **Avoid Unnecessary Bloodshed:** Minimize collateral damage to the surrounding environment and potential harm to the prince.

#### **Challenges:**
1. **Cage Lock Mechanism:** The cage is securely locked with a complex mechanism. A successful Dexterity check (DC 15) is required to pick the lock.
2. **Goblin Reinforcements:** If combat is initiated, goblin reinforcements arrive from the rear of the convoy after 2 rounds, consisting of 4 goblins.


#### **Enemy Composition:**
- **Front Line (10 feet in front of the wagon):** 
   - 6 Hobgoblin Soldiers armed with longswords and shields.
   - 4 Goblin Archers positioned strategically to provide ranged support.

- **Wagon Area (Around the cage):**
   - 1 Hobgoblin Captain leading the escort, armed with a greatsword.
   - 2 Goblin Guards stationed near the cage, armed with scimitars.

#### **Tactical Considerations:**
1. **Resistance Fighters:** The players can call upon their following of Resistance fighters (up to 6 allies) to provide support, create diversions, or engage in combat.
2. **Stealth Approach:** A well-planned stealth approach might allow the players to disable guards without alerting the entire escort.
3. **Environmental Interaction:** Leverage the terrain and objects in the environment to gain an advantage or create distractions.

#### **Consequences:**
1. **Alerting Vasrock:** If the players fail to maintain a degree of stealth, Vasrock is alerted, making the subsequent battles within Sliberberg more challenging. How this will pan out is up to the DM digression
2. **Prince Fredrick's Condition:** Depending on the success of the rescue, Prince Fredrick's condition may worsen or improve, influencing his abilities and willingness to aid the Resistance.



# The Five Commanders
## Gribnok
Gribnok, the goblin warlord overseeing the docks, is a figure etched in unflattering hues. His competence is a thin veneer over a tumultuous character. Infamous for his unrestrained indulgence, Gribnok is a well-known alcoholic, the pungent stench of spirits clinging to him like an unshakable shadow.Arrogance drips from him like sweat on a muggy day, an unchecked ego strutting through the docks with every ounce of authority he can muster. His bullying tactics are the calling card of a warlord who revels in intimidation, treating the docks as his personal fiefdom. Beneath his leadership lies an atmosphere of unease, as subordinates and slaves alike navigate the tumult of Gribnok's unpredictable temperament.
 **Daily Routine:** Gribnok starts his day with a bracing swim in the river, followed by a hearty breakfast at his favored dockside tavern. He spends most of his time overseeing the bustling docks, ensuring the smooth operation of cargo unloading and embarkations. In the evenings, he often hosts raucous parties aboard his personal barge or spends the night gambling in one of the many gambling dens throughout the city.

- **Place of Residence:** Gribnok resides on a lavishly decorated houseboat, permanently docked at the city's main harbor. It's equipped with gaudy ornaments and goblin trinkets.

- **Exploitable Habits or Weaknesses:** Gribnok has an insatiable fondness for exotic, imported liquors which he smuggles in via the ships in the harbor. He also possesses a secret gambling addiction. He frequents hidden gambling dens in the docks.

- **Workplace Details:** The docks, where Gribnok is in constant communication with the various barge captains, shipping agents, and cargo handlers.



### Gribnok comapny: The Marauders
The Marauders, led by the infamous goblin warlord Gribnok, are a ragtag group of mercenaries whose reputation is as disreputable as their leader. Comprising a motley assortment of goblins, hobgoblins, and other miscreants, the Marauders are a chaotic blend of disparate talents, held together by the promise of loot and Gribnok's dubious leadership.

Lurking in the shadow of the docks, the Marauders boast an undisciplined and rowdy demeanor. Their makeshift camp is scattered with stolen goods, and the air is thick with the scent of roasting meat and the acrid fumes of cheap spirits. The haphazard arrangement of tents and makeshift structures reflects the lack of order within their ranks, each member vying for their share of the spoils.
- **Discipline:** Gribnok's company is known for its unruly and undisciplined behavior. The goblins under his command are fiercely independent and often resort to rowdy tactics.
- **Duties:** Gribnok purposefully chose the easiest and least demanding of the city duties. His goblin mercenaries can usually be found at the taverns or participating in minor tasks, making them the least threatening but perhaps the most unpredictable of the mercenary companies.
- **Specialty:** While they lack strict discipline, Gribnok's goblin mercenaries are resourceful and adept at ambush tactics. They often employ hit-and-run guerrilla warfare, making them challenging to pin down.
### Gribnok's Riverside Assassination - Combat Encounter:
*You carefully approach the riverbank, guided by the distant sounds of splashing water. As you draw closer, the sight unfolds before you, Gribnok, indulges in his morning swim. Two goblins, standing guard, watch over a pile of clothes and gear.Gribnok, unaware of your presence, enjoys the refreshing waters, his hulking form contrasting with the natural beauty surrounding the river. The goblins, focused on their duty, cast occasional glances toward the water, ensuring their commander's belongings remain safe.*

**Location:** Secluded spot along the Sliber River, just outside the city

**Time:** Early morning, shortly after sunrise

**Objective:** Assassinate Gribnok the Goblin Boss during his morning skinny-dip, eliminating him and potentially his bodyguards.

**Description:**

 The players have gathered information about Gribnok's daily morning ritual, where he starts his day with a skinny dip in a secluded spot along the Sliber River, just outside the city. The tranquil riverside setting is disrupted by the rhythmic sounds of nature and the quiet gurgling of the water.
 
**Enemies:**
- Gribnok the Goblin Boss (Level 3)
- Four Goblin Bodyguards (Level 1)

**Setup:**
- Gribnok emerges from the dense foliage, leaving his clothes and belongings in a neat pile. He's not armed but is alert. He's positioned near the water's edge, while his goblin bodyguards are stationed nearby, vigilant and cautious.

**Features:**
- The players can position themselves along the riverbank, utilizing natural cover such as rocks, shrubs, and fallen trees.
- The flowing river can be used as an escape route or a means of distraction.

**Tactics:**
- Gribnok is aware of his vulnerability during his morning dip, so he remains alert. He takes cover behind a large boulder when he senses danger.
- The goblin bodyguards are loyal and focused on Gribnok's safety, ready to engage any threats.

**Objectives:**
- Players must eliminate Gribnok without raising an alarm, as the secluded spot offers some degree of isolation.
- If the alarm is raised, the players face a difficult battle against Gribnok and his bodyguards, and they'll need to quickly adapt to the changing circumstances.

**Possible Strategies:**
- Players can attempt a stealthy approach, carefully maneuvering into position to eliminate Gribnok silently.
- Distractions, such as throwing pebbles or using a minor illusion, can lure away the goblin bodyguards to create an opening.
- Utilize the river's flowing water or the natural surroundings to their advantage.



### Tavern Assassination - Combat Encounter:


*You  enter a dimly lit tavern where the aroma of breakfast mingles with the scent of spilled ale. There, at a corner table, Gribnok enjoys his morning meal, surrounded by a few loyal hobgoblin lieutenants. His imposing figure dominates the space as he relishes the hearty fare.Gribnok's attention is divided between devouring his food and engaging in animated conversation with his comrades. The clatter of tankards and the hum of tavern chatter provide a chaotic backdrop, creating an environment where a sudden confrontation might go unnoticed.*
**Location:** Dockside tavern within Sliberberg

**Time:** Breakfast hours

**Objective:** Assassinate Gribnok the Goblin Boss during his breakfast at the dockside tavern while managing the chaotic environment with aggressive patrons.

**Description:**

 The players have gathered intelligence about Gribnok's regular visits to a dockside tavern within the city. The tavern is a rough-and-tumble place, filled with patrons who harbor a strong dislike for Gribnok, eager to see him eliminated.

**Enemies:**
- Gribnok the Goblin Boss (Level 3)
- Two Goblin Bodyguards (Level 1)
- Hostile Tavern Patrons (Variable Levels)

**Setup:**
- Gribnok sits at his usual table, enjoying his breakfast, and surrounded by his goblin bodyguards.
- The tavern is crowded, and most patrons are hostile towards Gribnok. They are armed with improvised weapons and spoiling for a fight.
- The room is dimly lit, with tables and chairs providing cover and obstacles for tactical use.

**Features:**
- Patrons have little loyalty to Gribnok and may attack him, his bodyguards, or even the players if a fight breaks out.
- The tavern environment can be used for cover and creating diversions. It may also be a challenge to navigate due to the crowded space.

**Tactics:**
- Gribnok is wary of the tavern's atmosphere and ready to escape at the first sign of danger.
- Goblin bodyguards are poised to defend their boss but may hesitate if threatened by hostile patrons.

**Objectives:**
- Players must eliminate Gribnok without causing an all-out brawl among the patrons.
- Managing the hostile tavern patrons and avoiding chaos is vital for a successful assassination.

**Possible Strategies:**
- Players can attempt to blend in with the hostile patrons or distract them to create an opening.
- A stealthy approach might involve poisoning Gribnok's meal or slipping a deadly substance into his drink.
- Controlling the situation by calming the patrons or instigating a brawl elsewhere in the tavern could provide a window of opportunity.

### Dockside Showdown - Combat Encounter:

**Location:** Sliberberg Docks

**Time:** During one of Gribnok's inspections or tasks at the docks

**Objective:** Assassinate Gribnok the Goblin Boss amidst a tense confrontation at the busy Sliberberg Docks without drawing excessive attention.

**Description:**

 The players have identified a window of opportunity to confront Gribnok while he is occupied with overseeing activities at the docks. The area is bustling with goblin guards and slave overseers, making a discrete assassination difficult.

 *You find yourself on the bustling docks of Sliberberg, a hive of activity as cargo is loaded and unloaded from river barges. Gribnok, the goblin commander, oversees the operations with an air of authority. He barks orders at goblin workers, ensuring the efficient flow of supplies to the city. Amidst the organized chaos, Gribnok stands by a makeshift command post, a pile of maps and documents spread before him. A squad of goblin guards flanks him, watchful for any signs of trouble. Gribnok's attention is divided between the ongoing logistics and the strategic considerations of maintaining control over the vital supply lines.*

**Enemies:**
- Gribnok the Goblin Boss (Level 3)
- Three Goblin Bodyguards (Level 1)
- Goblin Dock Guards (Variable Levels)
- Slave Overseers (Variable Levels)

**Setup:**
- Gribnok and his goblin bodyguards are engaged in their inspection, staying close to the goblin workforce.
- Goblin dock guards are stationed at key points, and slave overseers are overseeing the laborers, maintaining order.
- The docks offer various pieces of cover, crates, barrels, and equipment.

**Features:**
- The presence of numerous goblin guards and overseers requires players to be discreet or face the risk of reinforcements arriving quickly.
- Players can use the surrounding cargo and equipment as cover or to create distractions.

**Tactics:**
- Gribnok and his bodyguards are alert and on guard, but they might not be expecting a direct confrontation.
- Goblin dock guards and overseers may initially respond to any disturbance, but their reactions can be influenced by the players' actions.

**Objectives:**
- Players must eliminate Gribnok while managing the surrounding goblin guards and overseers to avoid raising alarm.
- The confrontation should be handled with discretion to prevent excessive reinforcements.

**Possible Strategies:**
- Players can blend in with the dock workers, pretending to be laborers or merchants.
- Creating diversions or distractions to draw away goblin guards and overseers can provide an opportunity to confront Gribnok.
- Luring Gribnok away from the main group, such as to an isolated area of the docks, can minimize the risk of drawing attention.



**Encounter: Gribnok favorite gambling hall**

*Room Description:*
*You stand at the threshold of the chaotic gambling hall, hidden momentarily by the dense, swirling smoke and cacophonous laughter and chatter. The room is long and narrow, with rows of heavy, oak tables where goblins and humans hunch over card games and dice. A haze of smoke from countless pipes clouds the room, and the dim lighting creates a shadowy, mysterious atmosphere. The bar is in the corner, where a harried bartender keeps refilling Gribnok's mug with expensive whiskey. At the far end of the hall stands Gribnok, the Goblin Boss, swaying unsteadily as he places bets and roars with laughter. His bodyguards, four well-armed goblins, remain close to the entrance, surveying the room and any newcomers with suspicious glares.*

*Enemies:*
- Gribnok, the Goblin Boss (Goblin Chieftain)
  - Hit Points: 60
  - Weapons: Rusty sword, throwing daggers
- Four Goblin Bodyguards (Standard Goblins)
  - Hit Points: 15 each
  - Weapons: Short swords, crossbows

*Hazards and Obstacles:*
- Drunken Gamblers: Tables are occupied by drunken and rowdy gamblers. You may use these tables for cover or knock them over to create distractions. If you're not careful, they could get in the way.
- Smoky Atmosphere: The thick smoke could impair visibility. You may need to make Constitution saving throws to avoid being affected by coughing fits or obscured vision.

*Objective:*
Your goal is to confront Gribnok and obtain information without alerting his bodyguards or the other gamblers. Eliminate the guards and take control of the situation to secure the information you need.

*Additional Notes:*
Gribnok is heavily intoxicated and his judgment is clouded. He may occasionally make irrational decisions, giving you the advantage. Be mindful of the noise and chaos in the room, which can work both for and against you during the encounter. It's a dangerous gamble, but one that you must take to progress in your mission.


### Ambushing Gribnok's Smuggling Operation:

Gribnok smuggles drugs and liquor into the city which he sells for a tidy profit to the other goblin officer. His smuggling operation is located just underneathe the bridge to the old city from westside.
*In the shadows of the bustling dockside warehouses, you uncover Gribnok's personal smuggling operation—a clandestine endeavor beyond the official purview of the goblin army. A hidden dock, concealed by stacks of crates, serves as the entry point for contraband flowing in and out of Sliberberg.Gribnok,being  a shrewd commander, personally oversees this illicit venture. He stands by the dock, surrounded by a select group of goblin smugglers who unload mysterious crates from concealed riverboats. The atmosphere is tense, and the air is thick with the scent of secrecy as the operation unfolds under the cover of darkness.*
**Location:** Gribnok's Smuggling Warehouse, hidden in a secluded corner of the docks.

**Enemy Composition:**
- **Gribnok, the Goblin Boss:** Gribnok leads the operation. He's accompanied by a loyal goblin crew.
- **Goblin Smugglers (x2):** Gribnok's underlings who help manage the smuggled goods and protect their boss.

**Terrain:**
The warehouse is filled with crates, barrels, and piles of contraband goods, providing cover for both sides. There are also narrow walkways and hiding spots among the stacks of goods.

**Tactics:**
- Gribnok may attempt to escape or call for reinforcements, adding a time-sensitive element to the encounter.
- The goblin smugglers fight defensively and use the cover to their advantage.
- If an ogre enforcer is present, it can serve as a formidable tank-like enemy, dealing massive damage.
- The heroes can use the environment to set up ambushes and strategically engage the goblin crew.

**Objective:**
The players' goal is to ambush and apprehend Gribnok, putting an end to his smuggling operation and seizing the illicit goods for evidence.

### Showdown on the Flagship**
The Silver Wind, Gribnok's flagship, sits like a slumbering giant in the harbor of Sliberberg. The night is still, with only the distant sounds of the city and the creaking of the ship disturbing the silence. Gribnok's cabin is on the upper deck towards the stern, accessible through a narrow hallway guarded by a couple of half-asleep goblin sailors.

#### **Objectives:**
1. **Confront Gribnok:** Reach Gribnok's cabin on the upper deck and confront the goblin admiral.
2. **Avoid Rousing the Crew:** Most of the goblin crew is passed out drunk. Minimize noise and disturbances to keep them asleep.
3. **Retrieve Intel:** Search Gribnok's cabin for any valuable information or plans regarding the goblin fleet and operations in Sliberberg.

#### **Challenges:**
1. **Drunken Crew:** Most of the goblin crew is inebriated and passed out on the lower deck. However, if any commotion is caused, they may wake up.
2. **Creaky Ship:** The ship is old and not well-maintained. Each footstep produces a noticeable creaking sound that could alert the crew.
3. **Goblin Guards:** A couple of goblin sailors patrol the narrow hallway leading to Gribnok's cabin. They are groggy but still vigilant.

#### **Enemy Composition:**
- **Goblin Sailors (Lower Deck):** Numerous but incapacitated due to heavy drinking.
- **Goblin Guards (Upper Deck):** Two goblin sailors patrolling the hallway leading to Gribnok's cabin.
- **Gribnok, the Goblin Admiral:** In his cabin, Gribnok is semi-aware, lounging in his makeshift throne, surrounded by maps and charts.

#### **Tactical Considerations:**
1. **Stealth is Key:** Encourage players to move quietly, using Dexterity checks to avoid creaky floorboards and objects.
2. **Non-Lethal Takedowns:** Players can choose to incapacitate rather than kill the goblin sailors, minimizing the risk of alerting the crew.
3. **Distractions:** Use the ship's layout to create opportunities for distractions, leading the goblin guards away from their patrol route.

#### **Consequences:**
1. **Alarm Raised:** If the crew is alerted, Gribnok may escape, and the players will face heightened security on the ship and in the harbor.
2. **Valuable Information:** Successfully infiltrating Gribnok's cabin could provide critical intel about the goblin fleet and their plans.

*As you step onto the creaking deck of the Silver Wind, the night air is thick with tension. The dim light reveals the sprawled forms of drunk goblin sailors below, their snores echoing through the lower deck. Your target lies beyond a narrow hallway guarded by drowsy goblin sailors. The fate of Sliberberg may rest on your ability to confront Gribnok undetected.*


## Goruk 


Goruk, the sophisticated second-in-command to the goblin warlord Vasrock, stands as a stark contrast to the brutish reputation of the goblin forces. A mage of considerable power, Goruk is not only a practitioner of the arcane arts but also a refined gentleman soldier. His demeanor is marked by a cultivated air, and his actions are guided by a sense of methodical precision. Dressed in well-tailored attire that befits his status, Goruk is known for his keen interest in the arts. His collection of rare and exquisite pieces, acquired through both legitimate and less savory means, testifies to his cultivated taste. The war-torn city, under the oppressive rule of the goblin army, bears witness to this incongruity – a mage with a penchant for beauty amid the chaos of conflict.
- **Daily Routine:** Goruk has an eccentric daily schedule that changes week to week
**Monday:**
- Early morning: Town inspections and addressing citizens' complaints.
- Afternoon: Meeting with local business owners and traders.
- Evening: Attend a formal dinner at the town hall.

**Tuesday:**
- Morning: Meeting with town council members to discuss city affairs.
- Afternoon: Inspect construction projects in the city.
- Evening: Review guard shift rotations and patrolling schedules.

**Wednesday:**
- Morning: Attend an audience with visiting merchants from neighboring realms.
- Afternoon: Conduct interviews with potential new settlers.
- Evening: Relax at his residence with a few trusted advisors.

**Thursday:**
- Morning: Meet with the leaders of various city guilds and organizations.
- Afternoon: Review and approve permits for various businesses.
- Evening: Attend a social gathering with influential residents.

**Friday:**
- Morning: Attend a training session with the city guard.
- Afternoon: Visit the city's slave camps for inspection.
- Evening: Hold a public audience for citizens to voice concerns.

**Saturday:**
- Morning: Review progress on construction projects.
- Afternoon: Inspect the city's marketplace and economy.
- Evening: Attend a weekly meeting with Vasrock at the fort overlooking the river.

**Sunday:**
- Morning: Visit the local temples and offer support to religious leaders.
- Afternoon: Rest and attend to personal matters.
- Evening: Review the week's events and prepare for the upcoming week.

- **Place of Residence:** Goruk's residence, heavily fortified, is a converted mansion near the city's central square, flanked by an ornate iron gate.

- **Exploitable Habits or Weaknesses:** Goruk is a creature of habit, always following a specific daily schedule. He belives himself a man of culture known to harbor a secret admiration for the arts. He smuggles in art via Gribnok's smugglers.

- **Workplace Details:** His residence serves as his main workplace for managing the day-to-day civil matters of Sliberberg.

**Encounter: Confronting Goruk the Devastator**

*Street Description:*
*As you navigate the bustling streets of Sliberberg, a golden opportunity presents itself. You catch sight of Goruk the Devastator, the feared hobgoblin mage, walking alone, a tome in one hand and a gleam of arrogance in his eyes. The path ahead is quiet and empty, with few witnesses to your impending confrontation. You can choose this moment to strike.*

*Enemies:*
- Goruk the Devastator (Hobgoblin Devastator)
  - Hit Points: 75
  - Abilities: Powerful spellcaster with access to a variety of spells, both offensive and defensive.

*Hazards and Obstacles:*
- Crowded Street: While the street is usually crowded with passersby, there are fewer people here due to the earlier time or a discreet location. Be cautious about witnesses who might raise the alarm.

*Objective:*
Your objective is to eliminate Goruk the Devastator before he can react. His magical abilities are formidable, and he's supremely confident in his skills. Do your best to take him by surprise.

*Additional Notes:*
- Initiate the encounter with a surprise round in which the party can take actions before Goruk reacts.
- Once Goruk reaches half health, he may decide to flee or call for help. This presents a challenge, as the city guards will quickly arrive to aid their companion. Make strategic decisions to prevent Goruk from escaping or alerting the guards.
### Goruk mansion

![map](Mansion2.jpg)
*In the heart of the old city,, stands a mansion that now serves as the personal residence of Goruk. The mansion, though bearing signs of recent construction, exudes an eerie grandeur that clashes with the surrounding army camp.As you approach the mansion, you notice the hobgoblin mage's guards stationed at strategic points, their watchful eyes scrutinizing any who dare to draw near. The mansion itself looms over the landscape, a symbol of Goruk's authority and the unsettling transformation of Sliberberg into a city governed by goblins and hobgoblins.*
*Ground Floor:*
- **Foyer:** The entrance hall of Goruk's mansion is well-appointed, with a chandelier and finely crafted furniture. The double doors at the front are usually locked, but an open window or an unlocked side door could provide entry.

- **Sitting Room:** A room used for entertaining guests, this area is adorned with plush chairs and a central table. A fireplace warms the space.

- **Kitchen:** A spacious room with ample counters, cupboards, and a hearth for cooking. It is usually staffed with servants.

- **Dining Room:** A grand dining area with an elegant long table for feasts and gatherings.

- **Library:** Filled with tomes and scrolls, this room serves as Goruk's collection of magical and historical knowledge. He is currently reading a book titled Dealing with the Fey, a conclusive guide to the nature of  fey contracts.

*First Floor:*
- **Bedroom:** Goruk's private sleeping quarters contain a comfortable bed, a writing desk, and perhaps some magical paraphernalia. An adjacent bathroom has a bathtub for soaking.

- **Study:** A place for contemplation, research, and planning. The room is furnished with a large desk, bookshelves, and a safe for important documents.
The players find the following documents on the desk
##### Gorguk letter
*In one letter, he expresses the urgency of understanding the depths of the contract they've forged. The tone is one of anxiety, the weight of a pact with an archfey not lost on the goblin mage. Gorguk's meticulous research outlines his attempts to find loopholes or means to dissolve the contract should circumstances take a dark turn.*

*"The archfey's whims are fickle, and I fear the path we tread may lead to consequences unforeseen," Gorguk writes. His words reveal a goblin mage grappling with forces beyond his comprehension, a pawn in a game where the stakes are nothing short of their own existence. The notes end with a somber acknowledgment – a resolution to continue the search for an escape, a way to sever ties with Fionnuala if the need arises.*
##### Gorguk notes
*As you sift through Gorguk's research notes, the goblin mage's meticulous observations shed light on the complex nature of fey contracts. The parchment is filled with arcane symbols, diagrams, and carefully annotated findings. Several key points catch your eye:*

1. **Gifts and Contracts:** Gorguk highlights the formation of fey contracts through the exchange of gifts. His notes emphasize that fey have a remarkably broad interpretation of what constitutes a gift. It's not merely material offerings but can extend to acts of service, promises, or even performances.

2. **Enchantment of Contracts:** A recurring theme in the research is the enchantment woven into fey contracts. The magic that binds these agreements is potent and inextricably linked to the power of the fey involved. The more formidable the fey, the more formidable the enchantment, making escape or dissolution a daunting challenge.

3. **Transfer of Contracts:** The notes touch upon a grim aspect – when a person kills a creature bound by an outstanding fey contract, they often find themselves unexpectedly becoming the new recipient of that contract. The transfer of obligations seems to be an inherent aspect of the mystical ties that bind fey and mortals.


- **Guest Rooms:** A series of well-decorated bedrooms for visitors or potential allies.

- **Guard's Quarters:** Where Goruk's personal guards stay, ensuring his security around the clock.

*Basement:*
- **Laboratory:** A place for conducting experiments, mixing potions, or crafting magical items. The laboratory is filled with arcane equipment and reagents.

- **Cellar:** A storage area for food, wine, and other goods.

### mansions staff
Goruk mansion has 6 commoners working in the mansion at all times. Furthermore the following two servants can be found in the mansion at all times
**Batman (Butler)**

*Medium humanoid (typically human), neutral*

**Armor Class** 15 (butler's uniform)

**Hit Points** 30 (4d8 + 12)

**Speed** 30 ft.

**Skills** Perception +5, Insight +5

**Senses** passive Perception 15

**Languages** Common

**Challenge** 1 (200 XP)

***Keen Hearing.*** The batman has advantage on Wisdom (Perception) checks that rely on hearing.

***Loyal Service.*** The batman has advantage on saving throws against being frightened while in Goruk's presence.

***Multiattack.*** The batman makes two attacks with its silver serving tray.

***Silver Serving Tray.*** *Melee Weapon Attack:* +4 to hit, reach 5 ft., one target. *Hit:* 5 (1d6 + 2) bludgeoning damage.

***Throwing Knives (Recharge 5-6).*** *Ranged Weapon Attack:* +4 to hit, range 20/60 ft., one target. *Hit:* 4 (1d4 + 2) piercing damage.

***Healing Hands (3/Day).*** The batman can touch another creature to restore 2d8 hit points. The batman can also use this ability to cure one disease, neutralize one poison, or remove one condition afflicting a target.
### DM note
Historically speaking a batman is the traditional British term for a soldier or airman assigned to a commissioned officer as a personal servant. Before the advent of motorized transport, an officer's batman was also in charge of the officer's "bat-horse" that carried the officer's kit during a campaign.


**Footman**

*Medium humanoid (typically human), neutral*

**Armor Class** 12 (footman's livery)

**Hit Points** 16 (3d8 + 3)

**Speed** 30 ft.

**Skills** Perception +2

**Senses** passive Perception 12

**Languages** Common

**Challenge** 1/2 (100 XP)

***Dutiful.*** The footman has advantage on saving throws against being charmed while in Goruk's presence.

***Multiattack.*** The footman makes two melee attacks with its halberd.

***Halberd.*** *Melee Weapon Attack:* +3 to hit, reach 10 ft., one target. *Hit:* 6 (1d10 + 1) slashing damage.


**Encounter: Infiltrating Goruk's Mansion**

*Setting:* Goruk's lavish mansion with various rooms and hallways. The butler is generally found in the kitchen and the footman makes rounds of he mansion grounds with his halberd. Gorguk spends most of his time in his study.

*Situation:* The players have infiltrated Goruk's mansion and need to confront him or his servants for a specific objective, which might include assassinating Goruk. The mansion is a spacious, elegant, and dimly lit building, and the PCs have room to move and take advantage of the surroundings.



*Enemies:* You can tailor the combatants to your party's strength and the context of the story. Potential enemies may include Goruk (CR 4 Hobgoblin Devastator), his butler, footman, or other goblin or hobgoblin guards.

*Object Interactions:* Use the surroundings to your advantage. Players can push over bookshelves, swing from chandeliers, set curtains on fire, or throw vases as makeshift weapons. These environmental elements can be both useful and unpredictable.

*Stealth vs. Combat:* Players have the choice of attempting stealth to avoid direct combat, or they can engage in combat in various rooms. The outcome can affect how Goruk and his servants respond.

*Time Pressure:* Goruk or his servants will investigate loud noises or unusual occurrences after a few rounds of combat.

**Optional: Infiltrating Goruk's Mansion with a Ruse**

*Objective:* The players intend to pose as individuals with legitimate business to enter the mansion to assassinate Goruk.

1. **Create a False Identity:** The players must craft a convincing false identity for their characters. This could be as art collectors, merchants, or art appraisers, coming to discuss the acquisition of the new piece of art.

2. **Investigation:** To gather information and assume the new identity convincingly, the players may need to investigate rumors or obtain official documents related to the art transaction. This could involve talking to informants, stealing documents, or even forging paperwork.

3. **Meeting with Goruk:** The players can set up a meeting with Goruk to discuss the art acquisition, using their new identity. This may require contacting his intermediary or making use of their false documents to secure the meeting.

*Role-play and Deception:* This scenario relies heavily on role-playing and deception checks. Encourage players to maintain their false identities convincingly and handle any unexpected situations with diplomacy and charm.

1. **Gilded Tapestry of Eldritch Visions:** A captivating tapestry depicting scenes from another realm, interwoven with magical sigils. It catches the eye with its mesmerizing beauty and is infused with faint magical energy. *(Value: 750 gold)*

2. **Hobgoblin Commander's Battleaxe:** A finely crafted battleaxe adorned with intricate engravings, reflecting the martial prowess of a hobgoblin commander. Its razor-sharp edge and balanced design make it a formidable weapon. *(Value: 1,200 gold)*

3. **Inscribed Spellbook of Arcane Elegance:** Goruk's personal spellbook, bound in luxurious leather with silver filigree. The pages contain meticulously inscribed spells, and its cover radiates a faint magical aura. *(Value: 1,500 gold)*

4. **Enchanted Easel of Illusory Artistry:** An easel that enhances the artistic abilities of its user. When used, it imbues paintings with a subtle illusion, bringing the artwork to life with moving images and vibrant colors. *(Value: 800 gold)*

5. **Goblet of Liquid Gold:** A masterfully crafted goblet adorned with gold leaf. When filled with any liquid, it transforms its contents into a sparkling, golden elixir. The liquid within provides a temporary boost to the drinker's senses. *(Value: 600 gold)*

6. **Hobgoblin Commander's Cloak of Authority:** A regal cloak worn by high-ranking hobgoblin commanders. Adorned with a silver clasp, it grants an air of authority and imposes a subtle influence on those who behold it. *(Value: 1,000 gold)*

7. **Portrait of Eldritch Majesty:** A large, framed portrait depicting Goruk in a pose of arcane command. The eyes of the painting seem to follow onlookers, and the frame is adorned with small, enchanted gems. *(Value: 900 gold)*

8. **Antique Thaumaturgic Mirror:** A mirror with an ornate frame, featuring intricate runes. When activated, it briefly reveals glimpses of otherworldly landscapes and events, offering a glimpse into the unknown. *(Value: 1,300 gold)*

9. **Hobgoblin Commander's Signet Ring:** A signet ring adorned with the emblem of a hobgoblin commander. Beyond its aesthetic value, the ring also bears a hidden enchantment, providing a minor protective ward to the wearer. *(Value: 850 gold)*

10. **Gilded Bookends of Dimensional Stability:** A pair of bookends crafted from precious metals. When placed on either side of a row of books, they create a subtle barrier that prevents magical interference or tampering with the tomes. *(Value: 700 gold)*


## Captain Sarek

Captain Sarek, a stern and authoritative figure, stands at the forefront of the goblin forces responsible for maintaining order within the occupied city. His troops, predominantly composed of heavy hobgoblin infantry, enforce the goblin army's will in the streets. Sarek himself, marked by a hardened visage and a militaristic bearing, exudes an air of discipline and no-nonsense authority.However, beneath the facade of unwavering control lies a deep-seated fear that gnaws at the captain's core – a fear of magic. Sarek harbors an instinctual dread of the arcane arts, viewing magic as an unpredictable force that disrupts the order he seeks to maintain. This fear is not merely a personal phobia; it influences the tactics he employs on the battlefield and shapes his strategic decisions.
- **Daily Routine:** Captain Sarek is a strict disciplinarian who begins his day with an inspection of the city guard barracks.  Evenings often find him training his troops or administering justice at the city's jail.

- **Place of Residence:** Captain Sarek resides in the City guard complex, living in a spartan, no-nonsense apartment in the tower above the jail.

- **Exploitable Habits or Weaknesses:** Sarek's obsession with maintaining order can be used against him. He is a staunch prohibitionest and has a strict no drinking policy among his men. He is also averse to sorcery, having a deep-rooted fear of magic.

- **Workplace Details:** The barracks house both his residence and workplace, from which he oversees the city's guard force.

### Iron Legion
Captain Sarek's Iron Legion, a formidable force within the goblin army, stands as a symbol of disciplined might and unwavering loyalty. Comprising heavy hobgoblin infantry, these soldiers march in lockstep, their iron-clad forms creating an imposing and regimented presence.


- **Discipline:** 
 Sarek's company is marked by its strict discipline and unwavering loyalty to him. The hobgoblin infantry under his command are well-trained, organized, and formidable in battle.
- **Duties:** Sarek's mercenaries serve as the city guard and are responsible for maintaining order within the settled portions of the city. They are frequently seen patrolling the streets and guarding key locations.
- **Specialty:** The hobgoblin infantry excel in traditional warfare tactics. They are heavily armored and skilled in the use of various weapons, making them a formidable force to contend with.
#### The city guard schedule

**City Guard Rotation Schedule:**

Captain Sarek, who oversees the city guard from the guard barracks/jail, maintains a strict schedule to ensure the city's security. To provide the player characters an opportunity to infiltrate the jail, here's a schedule with potential gaps in the guard rotation:

**Day 1 (Monday):**
- Morning: Shift A starts, overseeing the town's marketplace and major thoroughfares.
- Afternoon: Shift A continues their duties.
- Evening: Shift B takes over for the night shift, patrolling the city's walls and gates.

**Day 2 (Tuesday):**
- Morning: Shift A concludes their shift, heads to the barracks.
- Afternoon: Shift A off-duty; Shift B continues night shift.
- Evening: Shift B continues their night shift.

**Day 3 (Wednesday):**
- Morning: Shift B concludes their night shift, heads to the barracks.
- Afternoon: Shift B off-duty; Shift C starts to patrol the city.
- Evening: Shift C continues their duties.

**Day 4 (Thursday):**
- Morning: Shift B off-duty; Shift C continues their duties.
- Afternoon: Shift C continues their duties.
- Evening: Shift D takes over the night shift, patrolling the city's outskirts and nearby forests.

**Day 5 (Friday):**
- Morning: Shift C concludes their shift, heads to the barracks.
- Afternoon: Shift C off-duty; Shift D continues night shift.
- Evening: Shift D continues their night shift.

**Day 6 (Saturday):**
- Morning: Shift D concludes their night shift, heads to the barracks.
- Afternoon: Shift D off-duty; Shift A starts to patrol the city.
- Evening: Shift A continues their duties.

**Day 7 (Sunday):**
- Morning: Shift D off-duty; Shift A continues their duties.
- Afternoon: Shift A continues their duties.
- Evening: Shift B takes over for the night shift, patrolling the city's walls and gates.
### The City guard complex
The City Guard Complex, a fortified bastion within the remnants of the old city's outer wall, stands as both a symbol of authority and a practical hub for maintaining order. Enclosed within sturdy walls, this complex is divided into distinct structures that serve the various needs of the city's guardians.

At the heart of the complex is the Barracks Building, a robust structure that houses the city guards when they are not patrolling the streets. With an administrative wing adjacent to it, this central building serves as the nerve center for the City Guard, where officers strategize, plan patrols, and coordinate responses to potential threats.

Across the bailey, dominating the other side of the compound, is the Jail, a formidable structure with two two-story wings. This is where wrongdoers are held, awaiting judgment and punishment. The architecture of the jail is designed for security, with barred windows and reinforced doors, giving an imposing appearance that warns potential lawbreakers of the consequences awaiting them.


#### Entrance to the Jail and Guard Barracks Complex - Skill Encounters:
![map](cityjail.jpg)

**Encounter 1: Guarded Entrance to the Jail:**

**Description:** 
 The entrance to the jail is well-guarded by vigilant goblin guards. To gain access, the player characters must either deceive or distract the guards.
 
**Skills:** Deception, Persuasion, or Performance

**Success:** Players successfully persuade the guards that they have official business within the jail.
**Failure:** Guards remain suspicious but do not immediately raise an alarm.

**Outcome:** Players gain access to the jail, but any suspicious actions within may lead to heightened security.

**Encounter 2: Discreet Approach to Guard Barracks:**

**Description:** 
 The path leading to the guard barracks is mostly hidden from view, and players must avoid patrolling guards.

**Skills:** Stealth, Sleight of Hand, or Acrobatics

**Success:** Players skillfully navigate the shadows and avoid the patrols.
**Failure:** Guards become suspicious but do not immediately engage.

**Outcome:** Players reach the guard barracks without raising an alarm.
#### Guard Barracks Description:

 *As you approach the guard barracks, you see a sturdy two-story brick structure rising against the city's skyline. The exterior is a blend of darkened, worn bricks and small, grated windows that allow scant beams of daylight to pierce through, offering only a faint glimpse of the activity within. Above the entrance, a weathered, tarnished sign reads "City Guard Barracks," hinting at the building's purpose. The building appears imposing, a testament to the goblin army's influence in the city. A heavy, wooden door stands as the entrance, secured with a hefty iron latch. The area around the door shows signs of frequent use, with scuff marks and dents on the ground.  You can't help but notice that the guard barracks is not as densely guarded as one might expect. Though there are guards patrolling the vicinity, there is a temporary lull in the comings and goings of the city watch. Now is your opportunity to enter and move further into the complex, but you must do so cautiously to maintain your covert mission to assassinate Captain Sarek.*

#### Jail Cell Block Ambush - Combat Encounter:
The same encounter is used for both cell block

**Location:** One of the cell blocks in the jail

**Objective:** The players find themselves ambushed by a group of hobgoblin guards while attempting to infiltrate the jail.

**Description:**

 As you navigate the jail's cell blocks, you suddenly hear the echoing footsteps of hobgoblin guards approaching. There's little time to react as the guards, alerted by some disturbance, converge on your location, brandishing their weapons and wearing grim expressions.

**Enemies:**
- Four Hobgoblin Guards (Level 2)

**Setup:**
- The encounter occurs within one of the cell blocks, a confined and dimly lit space.
- The players have limited cover, and the hobgoblin guards are positioned at the entrance and in strategic locations throughout the block.

**Features:**
- The players have a limited window of opportunity to deal with the hobgoblin guards before they can alert the rest of the compound.
- The central tower with the alarm bell is visible from your location.

**Tactics:**
- The hobgoblin guards attempt to surround the players and engage them in melee combat.
- They are aware of the alarm bell in the central tower and will attempt to reach it if given the chance.

**Objectives:**
- Players must defeat or neutralize the hobgoblin guards swiftly before they can ring the alarm bell.

**Possible Strategies:**
- Players can utilize terrain and their abilities for cover and crowd control.
- Quick, decisive actions can prevent the guards from alerting reinforcements.



##### Cell Block A - Notable Prisoners:

 As you explore Cell Block A, you come across several prisoners of note, each with a unique story that may be of interest to you:

1. **Satyric Bard - Oberon the Witty:**
   - You encounter a satyr with a twinkle in his eye and a playful smile on his lips. He introduces himself as Oberon the Witty, a satyric bard known for his sharp wit and humorous ballads. He was imprisoned for satirizing Vasrock and his goblin army, which landed him behind these iron bars. Oberon appears eager to share his tales and knowledge, offering insights into the city's current state and its vulnerabilities.

2. **Resistance Members - Elara and Tharin:**
   - Two individuals stand in a corner of the cell, keeping to themselves. Their names are Elara and Tharin, and they identify themselves as members of the city's resistance. Elara, an elven rogue, and Tharin, a human wizard, were both captured while attempting to gather intelligence and undermine the goblin occupation. They are cautious but may be willing to share information about the resistance's activities and potential allies in the city.
##### Cell Block B - Notable Prisoners:

 As you explore Cell Block B, you encounter several prisoners of significance, each with their own unique circumstances:

1. **Brownie Intruders - The Lockpickers:**
   - Inside one of the cells, you find a group of brownies, small fey creatures known for their knack for thievery and mischief. They were caught attempting to break into the jail in an audacious rescue mission to free the beast who is held in solitary confinement in the basement of the tower. Their tiny forms are squeezed within the cell's confines, and they appear determined but wary. If you can gain their trust, they may provide valuable assistance or insights into the jail's layout and the creature's presence.

2. **Sprites and Pixies - The Captive Fae:**
   - In another cell, you come across a collection of birdcages containing sprites and pixies. There is only one fey per cage and a mixture of both men and women. Several of the cages inhabitance are missing wings or parts of their wings. During the fight they cheer you on and shout insults at the guards from the bars of their cages, after the fight will beg the player characters to free them.
##### The brownie plead


 *As you conclude the intense struggle within Cell Block B, the atmosphere in the jail takes a turn. The defeated brownies, previously locked behind iron bars, immediately rush toward the cell's confinements, their tiny forms animated with both fear and anticipation. They plead with earnest voices, their urgent request clear in their beseeching eyes. "Blessed heroes," one of the brownies implores, his voice carrying a sense of both desperation and hope, "Please, you've shown your mettle in freeing us. We beg you, release the beast. He's confined in the torture chamber in the basement of the tower. He's strong, ferocious, yet intelligent and kind. He is  rightful king of Sliberberg before the goblins came."*
 

### Guard Room on the First Floor of the Tower - Exploration Encounter:

 *As you ascend the tower's winding stone staircase to the first floor, you enter a spacious guard room. In the center of the room, a massive bell dangles from the ceiling, its silence echoing ominously. The chamber is notably devoid of guards or personnel, raising questions about their sudden absence.*

**Features:**

- **The Bell:** The large, ominous bell hangs in the center of the room, appearing as though it should be used to alert the compound. It seems untouched and unused.

**Investigation:**

- **The Bell:** Examining the bell reveals that it's in pristine condition, suggesting it hasn't been rung in a long time. The mechanism to ring the bell is functional but requires a strong pull of the rope.

- **The Room:** You can search the guard room for clues or items of interest. With the guards missing, this room may hide valuable information.

**Possible Actions:**

1. **Ring the Bell:** If you decide to ring the bell, you risk alerting the goblin forces to your presence, but it might also create a diversion or give you the element of surprise.

2. **Search the Room:** Investigate the room to gather information, uncover secrets, or find useful items that might aid your mission.

### Torture Chamber Encounter:

*In the depths of the city jail, a grim scene unfolds. The players have stumbled upon a torture chamber where several hobgoblins are brutally tormenting a mysterious beast, even though their reasons for doing so remain unknown. The beast, an emaciated and injured creature, refuses to show any sign of pain, making it a haunting sight.*
 Special rules apply due to the proximity of the beast.

*Location:* Jail Torture Chamber

*Enemies:*
- **Hobgoblin Torturers (3):** CR 1/2, HP: 9 (each), AC: 15, Abilities: Multiattack (Poker and Melee Attack), Hot Poker (Melee Weapon Attack), Intimidation
- **Beast of the Märchenweltgrenze:** An intelligent, beastly humanoid, emaciated, covered in wounds, and blindfolded.

*Special Rules:*
When the player characters are within 20 feet of the Beast of the Märchenweltgrenze, they must make a Wisdom saving throw (DC 13). On a failed save, they perceive the beast as a hostile, slathering monster, triggering fight-or-flight instincts. This perception persists until the character is no longer within the affected range. This check should be made at the start of the encounter and repeated at the beginning of each character's turn within range.

*Additional Information:*
1. The Beast: As the characters approach, they may notice that the beast is bound securely to a torture rack, covered in severe wounds, and blindfolded. Despite the cruelty of the torturers, it remains eerily composed.
2. The Torturers: The hobgoblins are vicious and ruthless, using hot pokers and intimidation to torment the beast. Their motivations and knowledge of the situation are unclear.

### The beast's plea
*The harsh, flickering torchlight casts eerie shadows around the chamber, revealing the Beast of the Märchenweltgrenze, who dangles helplessly from a cruelly designed torture rack. His blindfolded head sways slightly as he listens to the turmoil. You have just defeated the brutal hobgoblin torturers who were searing his flesh with hot pokers.*

**Beast of the Märchenweltgrenze**: *His voice is a desperate plea,* "Who... who is there? I cannot see, but I sense that someone is here. Please, whoever you are, can you release me from this agony? My thanks would be yours forever."

*As the blindfolded figure hangs there, his injuries and exhaustion are evident. His emaciated frame seems to weaken with every passing moment. It is clear that he has endured extreme suffering at the hands of his tormentors, but his determination and willpower remain intact, granting him strength to persevere.*

### Captain Sarek's Office/Apartment - Combat Encounter:

 At the top of the jail's tower, you burst into Captain Sarek's office and living quarters. The dimly lit room is adorned with various military paraphernalia and maps, evidence of his authoritative role in the goblin army. Captain Sarek himself is hunched over his desk, engrossed in his work, unaware of your presence.

**Objectives:** Your mission is to confront Captain Sarek and, if necessary, engage him in combat. However, be vigilant, as he will attempt to reach the bell on the first floor to call for reinforcements.

**Features:**

- **Captain Sarek:** A formidable hobgoblin warrior, Captain Sarek is engrossed in his work at the desk. His room is decorated with maps, weapons, and armor.

- **Desk and Maps:** Captain Sarek's desk is cluttered with documents and maps that may provide valuable information. 

- **Bell Mechanism:** You notice the mechanism to ring the bell, which hangs in the center of the first floor of the tower, is visible from Captain Sarek's room.

**Tactics:**

- Captain Sarek is a capable fighter and will defend himself vigorously.
- His first priority is to reach the bell and alert reinforcements.

**Possible Strategies:**

1. **Surprise Attack:** Attempt to engage Captain Sarek in combat before he can reach the bell, thereby preventing him from summoning reinforcements.

2. **Negotiation:** If you have information that may persuade or deter him from calling for help, you can try to negotiate with Captain Sarek to avoid a fight.

**Outcome:**
- Successful combat means you neutralize Captain Sarek without him alerting reinforcements.
- Failing to prevent him from reaching the bell will result in the arrival of goblin reinforcements.

### Jailbreak - Escape Encounter:
After dealing with Sarek the next challenge is getting the prisoners out of the jail

 With Captain Sarek defeated and the city guard compound in disarray, the focus shifts to orchestrating a daring jailbreak to free the former prisoners, including the beast, sprites, and pixies. Your mission is to navigate the compound, help the beast escape, and coordinate the rescue of the fey captives.

**Objectives:** 
1. Assist the emaciated and wounded beast to make his way to safety.
2. Coordinate the rescue of the sprites and pixies who are eager to repay the goblins for their cruelty.

**Challenges and Features:**

1. **Beast's Condition:** The beast, though intelligent and strong, is severely weakened due to his captivity and requires support.

2. **Guard Patrols:** Goblin and hobgoblin guards patrol the compound's corridors and entrance, making it crucial to avoid their notice.

3. **Sprite and Pixie Assistance:** The liberated fey creatures are ready to assist with their magical abilities. They can provide distractions, create illusions, or cast spells to aid in the escape.

4. **Compound Layout:** The jail's layout includes various chambers, cells, and winding corridors, all of which must be navigated effectively to ensure a successful escape.

**Possible Strategies:**

1. **Stealth:** Navigate the compound with stealth and cunning to avoid guard patrols and maintain a low profile.

2. **Fey Magic:** Utilize the sprites' and pixies' magical abilities to create diversions, confuse guards, or open locked doors.

3. **Direct Confrontation:** If detected, engage in combat with the guards to protect the escapees and clear the path to safety.

**Outcome:**
- Success means you successfully lead the beast, sprites, and pixies out of the compound, marking a significant victory against the goblins.
- Failure may result in recapture or heightened goblin vigilance within the compound, making your mission more challenging.

## Brigadier Theldor
Brigadier Theldor, a stern and disciplined figure, commands the soldiers overseeing the slaves and maintaining order during the city's reconstruction. Clad in well-maintained armor adorned with the insignia of the goblin army, Theldor exudes an air of authority and military precision. His piercing gaze is matched by a neatly trimmed mustache, and his posture reflects the years of military training he has undergone. Known for leading by example, Brigadier Theldor often fights alongside his troops, instilling a sense of camaraderie and duty among the goblin soldiers under his command. Despite his allegiance to the goblin army, there is a lingering uncertainty in his eyes, hinting at a conflict within him, perhaps fueled by the moral implications of overseeing the use of slaves in the construction efforts.



- **Daily Routine:** Sir Theldor dedicates his days to overseeing the construction efforts at the city's edge, where his personal tent serves as his command center. He prefers a rigorous schedule of overseeing the manual labor and planning further expansion of the city's footprint.

- **Place of Residence:** Theldor's residence is a mansion that he has turned into his HQ.

- **Exploitable Habits or Weaknesses:** Theldor's obsession with martial discipline and perfection can be exploited by challenging him to combat. He is also extremely prideful. He sees himself as a nobleman and takes extreme offense at anyone who disrepects him often with lethal cosequences

- **Workplace Details:** The construction site, where he supervises the expansion of Sliberberg, while keeping a vigilant eye on the progress.

### Theldirs Vanguard Regiment
Brigadier Theldor's Vanguard Regiment stands as a formidable force within Vasrock's army, a diverse blend of heavy and light infantry, as well as skilled cavalry. Adorned in matching armor and bearing the insignia of the goblin army, the soldiers of the Vanguard Regiment display a disciplined and well-trained demeanor.


- **Discipline:** Theldor commands a diverse mix of infantry and horsemen, which makes his company versatile and adaptable. His mercenaries are disciplined and highly organized.
- **Duties:** Theldor's company is responsible for various tasks, including patrolling the outskirts of the city, protecting supply caravans, and engaging in reconnaissance missions. Their duties often take them beyond the city's walls.
- **Specialty:** The mixed composition of Theldor's company allows them to respond effectively to different situations. They are skilled in both mounted and infantry combat, making them a versatile and formidable presence.


### Ambushing Brigadier Theldor in the City - Challenging Combat Encounter:


Ambushing Brigadier Theldor, the commander of the soldiers overseeing the slave labor in the city, presents a daunting challenge due to his combat prowess and the protection of his strong bodyguard squad. The encounter is designed for 5 player characters at level 3 and is meant to be a demanding test of their skills. However it possible to make the encounter easier in multiple ways. Clever plans by the player characters are to be rewarded in this encounter. Read the following to start the encounter.
*Brigadier Theldor, leads his men through the streets of Sliberberg. Mounted upon a massive warhorse with obsidian-black armor, the Brigadier exudes an air of martial authority. His own plate armor, adorned with the insignia of a high-ranking officer, clinks with every step as he directs his gaze sharply to scan the surroundings.*

**Objective:** The primary goal is to eliminate Brigadier Theldor, the well-protected goblin commander, who travels with his elite bodyguard squad.

**Features:**

- **Brigadier Theldor:** A formidable hobgoblin commander with the abilities of a knight, Brigadier Theldor is a skilled and experienced warrior.

- **Hobgoblin infantry:** 6 hobgoblin soldiers

- **City Environment:** The encounter takes place within the bustling city of Sliberberg, providing opportunities for creative tactics, cover, and distractions.

**Tactics:**

- **Brigadier Theldor:** He is a seasoned combatant who will prioritize his own survival and that of his bodyguards. He employs combat tactics to outmaneuver and outfight his foes.

- **10 hobgoblins**

### Staging a Construction Accident as an Assassination Plan:

One possible straegy that could work is using the ample supply of half finished structures and construction supplies to cause an accident that could take out multiple members of Brigadier Theldor bodguard and possibly Brigadier Theldor himself. Here are my notes for creating such an encounter

**1. Set the Stage:**

- Identify a specific construction site in the city where the encounter will take place. Ensure that it has elements that can be exploited to create an accident, such as scaffolding, heavy construction equipment, or unstable structures.

**2. Skill Checks:**

- Assign specific skill checks to the players' actions, which will determine their success in creating the accident. The choice of skills should be based on the actions they take. Examples include:
  - **Stealth**: Sneak into the construction site without being noticed.
  - **Investigation**: Identify weak points or hazards that can be exploited.
  - **Deception**: Mislead or distract the goblin guards to lead them into a dangerous situation.
  - **Engineering**: Manipulate or sabotage construction elements to create hazards.

**3. Creating Hazards:**

- As the players succeed in their skill checks, describe how they exploit the construction site's hazards to set up dangerous situations.

- The more successful skill checks they accumulate, the more dangerous the environment becomes, increasing the potential harm to Brigadier Theldor and his bodyguards.

**4. Rolling for Consequences:**

- After the players have created the hazardous environment, make secret rolls to determine the severity of the accident. Consider using a d20 to determine the outcome, with lower rolls indicating less severe consequences and higher rolls indicating more severe consequences.

**5. Execution:**

- Once the hazardous situation is prepared, the players can lure Brigadier Theldor and his bodyguards into the area through stealth, deception, or another strategy.

**6. Determining Results:**

- Depending on the severity determined by the secret roll, Brigadier Theldor and his bodyguards may suffer various consequences:
**6. Determining Results with Damage Die Rolls and Status Effects:**

- Once the hazardous situation is prepared, the players can lure Brigadier Theldor and his bodyguards into the area through stealth, deception, or another strategy.

- Roll for damage by determining the number and type of dice used to inflict damage on Brigadier Theldor and his bodyguards, and roll for a status effect using a table to determine additional consequences. The severity of the accident may influence the number and type of damage dice, as well as the status effect applied.

**Damage Rolls:**

- Light Injury (d6): Roll a six-sided die (d6) for each target to determine damage. For example, rolling a 1 inflicts 1d6 damage to the target.

- Moderate Injury (d8): Roll an eight-sided die (d8) for each target to determine damage. For example, rolling a 2 inflicts 2d8 damage to the target.

- Severe Injury (d10): Roll a ten-sided die (d10) for each target to determine damage. For example, rolling a 3 inflicts 3d10 damage to the target.

- Fatal (d12): Roll a twelve-sided die (d12) for each target to determine damage. For example, rolling a 4 inflicts 4d12 damage to the target.

**Status Effect Table:**

- After determining the damage, roll on the following table to determine a specific status effect that accompanies the damage inflicted:

    1. Stunned: The target is incapacitated and unable to take actions for 1d4 rounds.
    2. Knockback: The target is pushed a number of feet away from the accident site equal to the damage rolled.
    3. Burning: The target is set on fire and takes additional 1d6 fire damage per round for 1 minute.
    4. Prone: The target is knocked prone and must use its movement to stand up on its next turn.
    5. Blinded: The target is temporarily blinded and cannot see for 1d4 rounds.
    6. Deafened: The target is temporarily deafened and cannot hear for 1d4 rounds.
    7. Confused: The target becomes confused and must roll a d4 at the start of its turn to determine its actions (1: attack a friend, 2: do nothing, 3: move in a random direction, 4: attack normally).
    8. Dazed: The target is dazed and has disadvantage on its next attack or skill check.

**Combat Encounter: Duel with Theldor**

*As the first rays of the morning sun illuminate the city square in front of the grand cathedral, you find yourself facing Brigadier Theldor in a  duel. The air is charged with anticipation, and the cathedral's imposing silhouette serves as a dramatic backdrop to the impending clash.*

**Theldor, Hobgoblin Captain**
- *Armor Class:* 18 (Chain Mail)
- *Hit Points:* 95 (10d8 + 50)
- *Speed:* 30 ft.

**Actions:**
1. *+1 Longsword:* Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 1d8 + 4 slashing damage plus 1d6 magical slashing damage.

2. *War Tactics:* Theldor is a seasoned tactician. Once per turn, as a reaction, he can impose disadvantage on an attack roll made against him or an ally within 5 feet.

3. *Commanding Presence:* Theldor's presence boosts the morale of nearby hobgoblin allies, granting them advantage on saving throws against fear effects.

**Tactics:**
- Theldor fights with discipline, using his longsword to parry and counter.
- He strategically positions himself to gain the upper hand, taking advantage of the terrain in the war room.
- Theldor employs his War Tactics to thwart incoming attacks, making him a challenging opponent to hit.
- If the duel turns against him, Theldor may call for reinforcements, turning the duel into a skirmish.

This combat encounter requires careful strategy and precision, as Theldor's tactical acumen and magical longsword pose formidable challenges. The outcome of this duel may have ripple effects on the ongoing conflict in Sliberberg.
####Theldor's headquarters
If the players decide to attempt to assassinate Theldor in his headquarters in the old city then read the following

**Brigadier Theldor has chosen a mansion in the old city as his imposing headquarters. The mansion, though recently constructed, bears the architectural echoes of a bygone era, its grandeur marred by the scars of conflict. A tattered banner bearing the emblem of the hobgoblin army now hangs proudly from the balcony, signaling Theldor's claim.  Around the mansion, the vacant lots created by the ongoing reconstruction of Sliberberg serve as the regiment's makeshift camp. Tents are pitched in disciplined rows, and the hobgoblin soldiers maintain a level of order even in the midst of temporary lodgings. The sounds of armor clinking and orders being barked fill the air as the regiment prepares for the next phase of their campaign.*
#### THe mansion

![map](Mansion.jpg)
Assume that there are between 1 and 8 hobgoblins and goblins in each room. Any combat alerts all the other soldiers in the mansion to the presence of the player characters.

**Ground Floor:**
1. **Entrance Hall:** Guarded entrance with stationed hobgoblin soldiers. Map of the city for strategic planning.
2. **War Room:** Large chamber with a long table for war councils. Detailed maps and plans on the walls.
3. **Barracks:** Large rooms filled with rows of bunk beds for the enlisted soldiers.
4. **Armory:** Storage for weapons, armor, and supplies. Guarded by hobgoblin sentinels.
5. **Mess Hall:** Communal area for soldiers to dine and rest. Long tables and benches for the regiment.

**First Floor:**
6. **Officer Quarters:** Several rooms converted into living quarters for Theldor's lieutenants.
7. **Theldor's Quarters:** Brigadier Theldor's personal chamber. Strategic maps, war trophies, and personal effects.
The players find the following letter on his writing desk
*Amongst the possessions of Brigadier Theldor, you uncover a sealed letter bearing the stamp of authority. Breaking the seal, you delve into the words of the hobgoblin commander, revealing a mix of ambition and reservation.*
*In the letter, Theldor expresses a tempered excitement about the promised rewards of their alliance with Fionnuala. The prospect of gaining his own duchy, a position of power and authority, seems to fuel his ambitions. However, as the ink delves deeper, so do Theldor's reservations.*
*He candidly acknowledges a nagging worry, a sense that Fionnuala's motivations may extend beyond the surface. The hobgoblin, usually stalwart and disciplined, reveals a vulnerability in the face of uncertainty. There are hints of concern, a wariness that echoes in the careful choice of words.*
*"The rewards are tempting, and the power promised is undeniable," Theldor writes, "yet, in the shadows, I sense a game being played, and the rules are not entirely clear." His unease is palpable, a testament to the complexities of dealing with an archfey whose motives remain shrouded in mystery.*

*As you absorb the contents of Theldor's letter, the commander's internal conflict becomes apparent – torn between ambition and suspicion, navigating a delicate dance with an archfey whose intentions may be as elusive as the shifting Feywild itself.*

8. **Command Post:** A central area for issuing orders and coordinating movements. Strategically positioned for overseeing the entire encampment.

# Ryla the Slaver

Ryla, the formidable bugbear slaver, cuts an imposing figure with her towering stature and muscular frame. Dressed in dark, weathered leathers adorned with macabre trophies, her appearance exudes an air of both cruelty and cunning. Ryla's fur, a deep shade of midnight black, seems to bristle with an unsettling intensity, and her yellow eyes gleam with a ruthless intelligence.Known for her ruthlessness and efficiency in managing the sprawling slave camps, Ryla wields a cruel whip at her side, a tool of both intimidation and discipline. Scars from numerous battles and skirmishes mark her rough exterior, testaments to her prowess as a warrior. Despite her fearsome reputation, there's a cold and calculating demeanor about Ryla, hinting at a strategic mind behind the brutality.
- **Daily Routine:** 
1. **Morning Briefing:** Ryla starts her day with a briefing from her lieutenants. They inform her about any notable incidents, potential issues, or exceptional productivity from the previous day.

2. **Breakfast in the Command Cabin:** Before heading out, Ryla has a quick breakfast in her Command Cabin. She reviews maps, schedules, and receives reports on the current state of the slave camps.

3. **Visit to Camp One (Morning):** Ryla arrives at the first slave camp early in the morning. She inspects the living conditions of the slaves, reviews their work progress, and ensures that her orders are being carried out by the overseers.

4. **Midday Rest and Strategy Session:** Ryla returns to her Command Cabin for a midday rest. During this time, she strategizes with her lieutenants, discusses any emerging issues, and plans for the afternoon inspections.

5. **Visit to Camp Two (Afternoon):** In the afternoon, Ryla proceeds to the second slave camp. She repeats the inspection process, closely observing the treatment of the slaves, checking on construction progress, and addressing any concerns raised by the overseers.

6. **Lunch and Communication with Vasrock:** Ryla takes a break for lunch in her Command Cabin. She communicates with Vasrock, providing updates on the slave camps, discussing any challenges, and receiving new orders or guidance.

7. **Visit to Camp Three (Late Afternoon):** Ryla concludes her day by visiting the third slave camp. This final inspection ensures a comprehensive overview of the entire operation. She scrutinizes the work, evaluates slave morale, and resolves disputes between the overseers.

8. **Evening Debrief:** Ryla returns to her Command Cabin in the early evening. Her lieutenants report on the day's activities, and she plans for the following day's inspections. Any exceptional incidents are addressed with appropriate action.

9. **Dinner and Personal Time:** Ryla takes her dinner privately in the Command Cabin. Afterward, she engages in personal activities or relaxation before retiring for the night.

- **Place of Residence:** A private cabin in the center of the camp.

- **Exploitable Habits or Weaknesses:** Ryla's temper is notoriously short, and she harbors a deep fear of betrayal.

- **Workplace Details:** The slave camps and her hidden cave hideout are where she conducts her criminal operations, coordinating with her bandit gangs.

## Ryla's Shadowclaw Enforcers
- **Discipline:** Ryla's company is marked by the brutish nature of bugbears. They are fierce and fearless, but their discipline can be erratic, and they tend to rely on their strength.
- **Duties:** Ryla's bugbear mercenaries oversee the vast slave camp on the other side of Sliber River. They are responsible for maintaining control over the enslaved population and ensuring they meet their construction quotas.
- **Specialty:** Bugbears are known for their raw power and ferocity in battle. They often serve as enforcers and are intimidating figures within the city, ensuring that the enslaved labor force complies with the goblins' demands.


## Direct Encounter with Ryla, the Bugbear Chief:

**Location:** The encounter takes place in the heart of the vast slave camp on the other side of the Sliber River, where Ryla oversees her operations.

**Enemy Composition:**
- **Ryla, the Bugbear Chief (Leader):** Ryla is a formidable warrior and commander. She wields a large morningstar and is equipped with tough hide armor. She has the following special abilities:
  - **Inspiring Presence:** Ryla can rally her minions, granting them temporary hit points and advantage on their next attack.
  - **Ferocious Strike:** Ryla's morningstar attacks deal additional damage.
- **Bugbear Bodyguards (x2):** These loyal bugbears fight alongside Ryla and are well-armed and armored.

**Terrain:**
The battlefield within the slave camp is uneven and scattered with makeshift barriers and tents, offering cover and tactical opportunities. The slave camp is surrounded by a palisade fence that can be used strategically by both sides.

**Tactics:**
- Ryla, as a chief, fights with fierce determination. She focuses on the closest threats and uses Inspiring Presence to bolster her allies.
- The bugbear bodyguards and hobgoblin guards fight defensively and attempt to form a protective shield around Ryla.
- Enslaved beasts are unleashed to create chaos among the players or to target weaker party members.
- The terrain allows for surprise attacks, ambushes, and flanking opportunities, adding an element of strategy to the encounter.

**Objective:**
The players must confront and defeat Ryla and her forces to weaken the goblin presence and liberate the slaves in the camp.
# THe sliberberg uprising

Amid the incessant patter of heavy raindrops against the windows, you find yourselves gathered in the hidden confines of the resistance's lair. The room is dimly lit, the flickering candles casting an eerie glow upon the determined faces of your comrades. Elowen, the leader of the resistance, stands at the center, her gaze unwavering as she goes over the plan one last time.

**Elowen**: "My friends, with the fall of the final commander, the moment we've long prepared for has arrived. The rain outside will be our shroud, muffling the sounds of our approach. We will break the chains of the oppressed tonight and put an end to this tyrannical rule."

She speaks with conviction, and her words resonate deeply with the small group of resistance fighters surrounding you.

**Elowen**: "The plan is straightforward: first, we'll storm the slave camp, liberating those unjustly bound in servitude. Then, we'll arm them with the weapons you've secured. After that, we'll assault the main goblin army camp, hidden in the heart of this rainstorm."

As you listen to Elowen's words, the determination within the room is palpable. Your breath quickens, and you find yourselves ready to stand alongside the resistance, to fight for a brighter future.

**Elowen**: "I cannot stress enough the importance of maintaining the element of surprise. This downpour will mask our approach and dampen any torches or signals. As one, we will free the city from the grip of the goblin army."

 With your resolve steeled and the rain outside as your ally, you are ready to embark on the final leg of this perilous journey, knowing that the fate of Sliberberg and its citizens rests upon your shoulders.


## The slave camp entrance
*As you approach the entrance of the slave camp, the relentless rain pours down, creating a chaotic symphony of drumming on the various makeshift shelters and wooden structures. The flickering torches held by the goblin and hobgoblin guards cast distorted shadows through the heavy rain.*

*Objective*: Defeat the guards at the slave camp entrance and secure a path for the liberated slaves to escape.

**Enemies**:
- 3 Goblin Guards (CR 1/4 each)
- 3 Hobgoblin Soldiers (CR 1/2 each)

**Allies**:
- 3 Thug Resistance Fighters (CR 1/2 each)

**Environment**:
- Pouring Rain: The heavy rain provides advantage to Stealth checks for the player characters and their allies. Visibility is limited to 30 feet.

**Setup**:
The guards at the entrance are patrolling in groups of two. The player characters and the thug resistance fighters approach from the edge of the camp. The entrance is a large gate with a pair of guards standing watch.

**Development**:
Upon spotting the resistance, the guards sound an alarm using a horn. The alarm attracts reinforcements from deeper within the camp, which will arrive in 1d4 rounds.

**Tactics**:
- The goblin guards use shortbows to attack from range before engaging in melee with their scimitars.
- The hobgoblin soldiers form a defensive line with their shields and shortswords, protecting the goblins.
- The thug resistance fighters aid the player characters in eliminating the guards.

**Victory Conditions**:
- Subduing the goblin and hobgoblin guards will allow the group to secure the entrance and proceed with their plan to free the slaves.
- Should they fail or be overwhelmed by reinforcements, the group's mission will be compromised.

## The main army encounter
*The pouring rain intensifies as you and your allies approach the sprawling main army encampment. Mud squelches underfoot as you prepare for the assault, your determination unwavering. The battle will be fierce and the odds are against you, but the freedom of the slaves depends on your success. With each wave of enemies, the tension grows, and you can't afford to lose momentum.*

**Setup**: The main army encampment is a sprawling area filled with tents, fortifications, and wooden buildings. It's pouring rain, and the muddy terrain provides cover. There are large bonfires, partially sheltered under makeshift canopies, providing dim illumination across the camp. Several wagons are parked haphazardly, and there's a watchtower at the camp's center.

**Objective**: The player characters and their allies must launch an assault on the main army encampment, securing a path for the liberated slaves to escape.

**Enemies (Wave 1)**:
- 4 Hobgoblin Soldiers (CR 1/2 each)
- 3 Goblin Archers (CR 1/4 each)

**Wave 2** (triggered when Wave 1 is reduced to a total health of 15%):
- 3 Hobgoblin Captains (CR 2 each)
- 5 Goblin Warriors (CR 1/4 each)

**Wave 3** (triggered when Wave 2 is reduced to a total health of 15%):
- 1 Ogress (CR 3)
- 6 Hobgoblin Soldiers (CR 1/2 each)

**Reinforcements**:
- If the player characters have recruited the centaurs, elves, half-orcs, and dwarves, each group brings 3 members to assist in the assault. Centaurs are excellent archers, elves provide ranged support, half-orcs offer brute strength, and dwarves are skilled with tactics and traps.

**Tactics**:
- The hobgoblin soldiers and captains fight strategically, using coordinated attacks and tactics. They form defensive lines with shields, making them difficult to hit.
- The goblin archers stay in cover, attempting to pepper the attackers with arrows from a distance.
- The ogress charges forward, wreaking havoc and creating chaos in the battlefield.

**Victory Conditions**:
- The assault is a success when the player characters and their allies manage to defeat all the waves of enemies, securing the main army encampment and allowing the liberated slaves to escape.

#### Allies Groups
##### Elves
3 elf scouts
##### centuars
3 centuars
##### half orcs
2 half orc beserkers
##### dwarves
5 dwarf guards


## Encounter: The Camp Commander's Last Stand

*As you push deeper into the main army encampment, driving the goblins back with each step, you finally arrive at the heart of the battle. The goblins are in disarray, their makeshift defenses haphazardly erected from tents and supply crates. Amidst the chaos, you spot the final hobgoblin officer, a tall and imposing figure. He stands defiantly behind an improvised barricade of crates and carts, barking orders to the goblins under his command. His harsh words and the rallying cries of the remaining goblin soldiers fill the air. The officer's eyes lock onto your group, and he raises his weapon high, challenging you to approach.*




*Setting*: The final showdown takes place in a makeshift barricade at the heart of the goblin army camp, where the remaining enemy forces, led by the Hobgoblin Captain, attempt to hold their ground against the player characters and their allies.

*Enemies*:
- **Hobgoblin Captain (CR 3)** - The leader of the goblin forces, a tough and experienced warrior.
- **Hobgoblin Veterans (x2, CR 1)** - Skilled fighters who stand with their captain.
- **Ogres (x2, CR 2 each)** - Enormous and brutish creatures, serving as the camp's heavy muscle.
- **Goblin Archers (x2, CR 1/4 each)** - A pair of goblin archers providing ranged support.
- **Hobgoblin Warcaster (CR 2)** - A spellcaster supporting the group with spells and magic.

*Setup*: The camp is surrounded by rudimentary defenses, with walls made of stacked crates, overturned wagons, and other improvised barricades. The Hobgoblin Captain stands at the center, shouting orders and leading the defense, while his forces are scattered behind cover. The camp is dimly lit, and the sound of heavy rain creates additional difficulty in perceiving the battlefield.

*Objective*: The player characters, along with their allies, must breach the barricade, defeat the Hobgoblin Captain, and eliminate his remaining forces.

*Special Rules*:
- **Rainy Battlefield**: Due to heavy rain, ranged weapon attacks have disadvantage, and fire-based spells may be affected.
- **Improvised Cover**: The makeshift barricades provide cover for the goblin forces. Characters behind cover gain a bonus to AC.
- **Last Stand**: The enemies fight with determination, gaining a temporary boost to their attacks and saving throws as long as the Hobgoblin Captain is still alive. The bonus decreases after he falls.

*Terrain*: The battlefield is a mix of muddy ground and crude barricades. The barricades can be used as partial cover, but characters moving through the mud may have their movement slowed.


*The skies are pouring rain as you, the resistance members, and your allies push forward, pursuing the fleeing hobgoblins back towards Castle Sliberberg. With every step, the tension in the air mounts, as the castle looms larger against the horizon. It's clear that the moment for the final assault has arrived, and you brace yourself for the challenges ahead.As you draw nearer to the castle gates, your anticipation turns into sudden shock. Hundreds of arrows and crossbow bolts, their tips glistening in the rain, rain down from the darkness above the towering walls of the fortress. Alchemical vials, filled with an explosive concoction, arch through the night, their fiery tails leaving trails of doom.Chaos erupts as screams fill the air. The resistance members and your allies scatter in all directions to avoid the deadly hail of projectiles. Shouts for retreat and safety echo through the dark and rain-drenched night. Confusion and panic seize the moment, and you find yourself following the retreating figures.*
# Concluding the adventure

As the heroes return to the resistance hideout,, the Beast awaits them, leaning against a wall for support. Clutching his side in evident pain, he suppresses the discomfort with grim determination. A silent understanding passes between the heroes and the Beast, conveying the weight of unforeseen challenges.

Soft oaths escape the Beast's lips as he deciphers the troubled expressions worn by the heroes. His keen perception discerns the unfortunate turn of events – the goblins have seized the castle. Yet, with a measured tone, he absolves the players of any blame, a testament to his unwavering trust in their commitment. He stands as a resilient figure, his resolve unwavering even in the face of setbacks.

# Appendixes
## Rumor table

1. The goblins have several war ogres, forcing them to toil on construction sites, wielded heavy weapons, and doing backbreaking labor.

2. An incoming shipment of magical weapons is expected in just a few days, which could greatly bolster the goblin forces.

3. The majority of residents living in the completed portion of the city hail from various domains in the Feywild, raising questions about their true affiliations.

4. Goblin patrols are increasing their presence in the city, raising security concerns for those moving about.

5. There's a black market operating in the city, offering a wide range of contraband, including information and illegal goods.

6. There's a network of spies and informants within the city who report to the goblin authorities, creating an atmosphere of paranoia.

7. There's a hidden resistance movement in the city, comprising both residents and a few enslaved beings who seek freedom from goblin oppression.

8. The castle within the city is heavily guarded, but rumors suggest that the goblin general Vasrock rarely spends time there, preferring to stay closer to the construction sites.

9. Goblin alchemists are working on experimental potions, and their activities have raised concerns among the residents.

10. The main source of food for the city comes from farms situated on the outskirts, and controlling this source is essential for the city's operation.

11. There are rumors of a hidden vault within the city where the goblins hoard their most valuable treasures, including their share of the magical weapons.

12. A secret tunnel system beneath the city connects various strategic locations, providing an alternative means of moving about unseen.

13. Goblin overseers frequently make surprise inspections at the slave camps, which has created a tense and hostile atmosphere among the enslaved beings.

14. A local minstrel has been secretly composing satirical songs mocking the goblin leadership, which have become a symbol of resistance.

15. The city's marketplace is a hub of activity, and goblins, mercenaries, and residents gather there, making it an ideal place for discreet information exchange.

16. Gribnok starts his day skinny dipping in the river

17. Gribnok always has breakfast in the same dockside inn

18. Gribnok runs a smuggling operation on the docks

19. Gribnok has a taste for fine liquors and has a drinking problem
20. Gribnok likes to gamble

21. Gribnok residence is a houseboat

22. Gruk daily routine changes day to day and week to week

21. Goruk is an art collector and he awaiting a new piece to come via Gribnok smugglers

22. Goruk is a war mage, he supremely confident in his abilities and travels about the city without a bodyguard

23. Goruk always follows his schedule to the minute

24. Goruk lives in a mansion in the city center

25. Goruk has two servents, a batman and a footman

26. Goruk batman is a skilled soldier

27. Goruk conducts most of his work from his study

28. Captain Sarek runs the city guard

29. Captain Sarek rarely if ever leaves the guard compound

30. Captain Sarek lives and works from an apartment in the city jail in the guard compound


31. Brigadier Theldor is in charge of overseeing the construction efforts at the city's edge

32. Theldor's residence is a grand, fortified tent in the main army camp, decorated with trophies from his numerous victories.

33. Theldor is extremely prideful. He sees himself as a nobleman and takes extreme offense at anyone who disrepects him often with lethal consequences

34. Theldor has fought numerous duels against those who disrespected his martial abilities

35. Ryla the slaver spends most of her time in the slave camp

36. In the afternoon, Ryla meets with the hobgoblin overseers to discuss the day's progress at the construction sites

37. Ryla's temper is notoriously short, and she harbors a deep fear of betrayal.

38. Vasrock has hired several other mercenary companies to pad out his forces

39. The centaur camp is a sprawling, open-air affair, with a circle of makeshift tents and lean-tos set up beneath a corpse of large oak trees.

40. Captain Rorin, leader of the centaur mercenaries is  a stern and battle-hardened centaur who takes his leadership role very seriously. He's known for his no-nonsense attitude and commitment to the contract.


41. A rift has formed among the centaur mercenaries. Some, like Rorin, believe in the contract and maintaining a professional relationship with the goblins. Others, however, are sympathetic to the cause of freeing the slaves.

42. THe elven ranger camp is located at the northern side of the city.

43. THe elven rangers are lead by Captain Serina, a proud and skilled elven commander who values honor and integrity. She is deeply conflicted about the contract and wishes to find a way out.

44. The majority of the elven archers are unhappy with the contract and feel morally compromised by their association with the goblins.

45. Captain Serina's sympathies lie with the slaves, and she's been secretly communicating with an underground network within Sliberberg dedicated to their liberation.


46. THe half orc shock troops camp is to the south of the city

47. THe half orc shock troops leader is  Warlord Grukthar, a charismatic and influential half-orc leader known for his ability to unify diverse groups. He is committed to the contract but open to negotiation.

48.  THe half orc shock troops have mixed feelings about the contract. Some see it as a means to an end, while others are deeply troubled by the goblins' use of slaves and have been quietly contemplating a change in allegiance. 


48. Far to the west at a quarry is a camp of dwarfan stone masons.

49. The dwarfan stone masons are not happy about their involvement in the goblin operations. They were effectively tricked into the contract

50. Foreman Gruff Stonecarver, a grizzled and experienced dwarven stone mason with a bushy, iron-gray beard. He's the de facto leader of the camp of dwarfan stone masons.
51. There is a skilled forger working out of the black market
52. Captain Sarek is deeply afraid of magic
53. Prince Fredrick is constantly being cursed
54.  Gribnock ship is in a pretty bad shape after 6 months at dock
55. There is an old sewer system under the old city
56. Vasrock had help conquering the central valley from a powerful entity. 
57. the powerful entity that helped vasrock was a god
58. the powerful enitity that helped vasrock was a devil he sold his soul for the power to conquer the valley
59. the powerful entitty that helped Vasrock was an archfey. He signed a contract that gave him rulership of the valley in exchange for some unknown thin in trade
60. Servants of the archfey Fionnuala have been seen in town


## the Silver Moon District 

Nestled within the heart of the Market Ward, the Silver Moon District stands as a testament to the resilience of a community united against the shadows of goblin occupation. This vibrant neighborhood spans five blocks, its cobblestone streets winding through a cluster of enchanting buildings, each adorned with intricate German Alpine architecture. The air is rich with the scent of freshly baked goods and the faint hum of magical energies that dance through the bustling thoroughfares.

**Architectural Marvels:**

The predominant architectural style of the Silver Moon District draws inspiration from the German Alpine tradition, creating a whimsical landscape of timber-framed structures, steep gabled roofs, and charming balconies adorned with flower boxes. Each building is a masterpiece of craftsmanship, a harmonious blend of magical aesthetics and practical design.

Above the enchanting shops that line the streets, cozy lodgings for the shop owners perch like crowns, their windows revealing warm, inviting interiors that glow with the soft light of arcane lamps. The entire district seems to resonate with an almost palpable magic, as if the buildings themselves harbor secrets and tales of resistance against the goblin oppressors.

**Community Life:**

In the Silver Moon District, the sense of community is strong, as shopkeepers, artisans, and residents come together in the face of adversity. The narrow streets are alive with the laughter of children playing magical games and the animated conversations of patrons sipping enchanted teas at outdoor cafes. Yet, beneath the surface, a tension simmers as the goblin soldiers patrol the streets, their presence a stark reminder of the city's subjugation.

**Magical Marketplaces:**

This neighborhood serves as a hub for businesses catering to magic users, boasting an array of magical apothecaries, spell component shops, and arcane curio stores. The shop windows are adorned with displays of shimmering crystals, potion ingredients, and enchanted artifacts, tempting passersby with the promise of mystical wonders.

The Silver Moon Bakery Cafe stands as the crown jewel of this magical marketplace, its warm glow and tantalizing aromas drawing both regulars and newcomers seeking solace and sustenance in these trying times.

### Buisness listing
Certainly! Here are some businesses in the Silver Moon District, with names inspired by Scottish, Irish, and German influences:

1. **Thistlewynd Boarding House:**
   - Proprietor: Fiona MacBride
   - Description: Thistlewynd is a welcoming boarding house with cozy rooms adorned with thistle motifs. Fiona, a kind-hearted half-elf, ensures guests feel at home, offering a haven for those displaced by the goblin occupation.
   - **Lifestyle:** Comfortable (2 gp/day)
   - **Amenities:** Cozy rooms, warm hearths, and shared common spaces. Simple meals are provided, and Fiona often shares stories of the feywild around the communal fireplace.
2. **Sonnenschein Boarding House:**
   - Proprietor: Klaus Hartmann
   - Description: Sonnenschein, meaning "sunshine" in German, provides a warm and well-lit refuge for those seeking temporary residence. Klaus, a jovial dwarf, ensures the hearth is always burning and spirits remain high.
   - **Lifestyle:** Modest (1 gp/day)
   - **Amenities:** Basic but well-maintained rooms with communal dining areas. Klaus ensures a hearty breakfast and a warm atmosphere, making it a practical and friendly choice for those on a budget.
3. **Silberquell Inn:**
   - Innkeeper: Seamus O'Malley
   - Description: Silberquell, translating to "silver spring," is a large inn with a lively atmosphere. Seamus, a gregarious gnome, welcomes patrons with hearty laughter and tales of the feywild.
   - **Lifestyle:** Comfortable (2 gp/day)
   - **Amenities:** Spacious rooms, lively common areas, and a bustling tavern. Seamus hosts nightly entertainment, making it a popular choice for those seeking a more lively atmosphere.
4. **Tannenwald Library (Currently Closed):**
   - Librarian: Eilidh MacLachlan
   - Description: Tannenwald, meaning "fir forest" in German, is a small library nestled within the district. Eilidh, an elf with a passion for knowledge, has temporarily closed its doors due to the goblin occupation, vowing to reopen when the city is free.

5. **Glimmerflame Apothecary:**
   - Proprietor: Adalbert Stein
   - Description: Glimmerflame offers a dazzling array of magical potions and elixirs. Adalbert, a gnome with a penchant for experimentation, creates remedies that shine with ethereal light and promise extraordinary effects.
   - **Items for Sale:** Potions of Healing, Potion of Invisibility, Elixir of Mind Reading, Antitoxin, and various alchemical items.
   - **Services:** Potion brewing, minor magical remedies, and advice on magical ailments.
6. **Eilúnora's Enchantments:**
   - Enchantress: Eilúnora O'Sullivan
   - Description: Eilúnora's, an enchanting boutique, is run by a skilled elven enchantress. Eilúnora weaves magic into everyday items, transforming them into extraordinary tools for both practical use and subtle resistance.
   - **Items for Sale:** Enchanted trinkets, arcane focus items, scrolls of minor spells, and custom magical items crafted by Eilúnora.
   - **Services:** Enchantment of personal items, identification of magical items, and consultations on arcane matters.
7. **Cauldron & Crystal Curios:**
   - Proprietor: Dieter Schneider
   - Description: Cauldron & Crystal is a mystical emporium where Dieter, a wise halfling, sells magical curiosities. From scrying crystals to enchanted cauldrons, the shop caters to those seeking a touch of the arcane.
   - **Items for Sale:** Scrying crystals, enchanted cauldrons, divination tools, and a selection of magical oddities.
   - **Services:** Divination services, magical consultations, and the crafting of minor magical items.
8. **Séireann's Spellbound Supplies:**
   - Proprietor: Séireann Ó Riain
   - Description: Séireann's, meaning "fortune" in Irish, is a haven for spellcasters. Séireann, a charismatic human, offers spell components, arcane focuses, and mystical trinkets to aid those resisting the goblin presence.
   - **Items for Sale:** Spell components, arcane focuses, spell scrolls (up to 2nd level), and magical inks.
   - **Services:** Spellcasting services, scribing of spell scrolls, and advice on magical research.

9. **Mossy Oak Alchemy:**
   - Alchemist: Alaric MacAllister
   - Description: Mossy Oak is an alchemical workshop run by Alaric, a dwarf with a passion for herbalism and potions. The shop provides remedies to aid in healing and resistance against the goblin forces.
   - **Items for Sale:** Healing potions, potions of resistance, herbal remedies, and alchemical substances.
   - **Services:** Alchemical services, potion brewing, and herbalism training.

10. **Glanzlicht Jewelry Emporium:**
    - Artisan: Eilís Glanzlicht
    - Description: Glanzlicht, meaning "shining light" in German, is a jewelry emporium run by Eilís, a talented half-elf artisan. Her creations incorporate both precious metals and fey-inspired designs.
    - **Items for Sale:** Fey-inspired jewelry, enchanted rings, and custom-designed magical accessories.
    - **Services:** Custom jewelry crafting, enchantment of accessories, and appraisal of magical items.

**Farid's Appearance in Human Disguise:**

Farid, a man of apparent middle-eastern descent, stands tall and broad-shouldered, exuding an air of confident geniality. His raven-black hair is neatly combed, cascading just above his shoulders, and a well-kept beard frames a warm and engaging smile. His eyes, a shade of deep brown, sparkle with an unspoken wisdom and a hint of mischief.

Cloaked in vibrant, flowing fabrics that seem to change colors with the shifting light, Farid's attire reflects a kaleidoscope of hues. He wears a loose-fitting tunic adorned with intricate embroidery, the patterns dancing as if imbued with a life of their own. A sash of rich crimson is cinched around his waist, and billowing pants cascade into leather sandals adorned with delicate ornaments. A fez perches jauntily atop his head, not just a mere hat but a magical item – the Hat of Disguise Self – allowing Farid to change his appearance at will.

His voice carries the warm cadence of someone well-acquainted with the art of conversation. Farid's demeanor is welcoming, his eyes crinkling at the corners when he laughs, a laugh that resonates with a touch of mystery.

## Farid's Shop: The Enchanting Caravan Emporium

*Next to the Silver Moon Bakery, nestled in the vibrant heart of sliberberg, stands Farid's magical emporium - The Enchanting Caravan. The shopfront beckons passersby with richly colored fabrics and exotic trinkets on display, each piece whispering tales of distant lands and fantastical adventures.*

*The door, adorned with intricate carvings and golden accents, swings open with a melodious chime, welcoming visitors into a realm of enchantment. The interior of The Enchanting Caravan is an explosion of color and magic. Shelves upon shelves are adorned with trinkets, talismans, and artifacts from all corners of the realms.*

*The air is thick with the heady scent of incense, woven into the tapestries that drape the walls. Exotic rugs, each one a work of art, cushion the floor, inviting patrons to linger and explore the treasures within. The atmosphere is both mysterious and inviting, a testament to the owner's unique ability to blend the magical with the mundane.*

*Farid himself stands behind a worn wooden counter, its surface cluttered with curious baubles and gleaming crystals. He is ever-ready to engage in the age-old dance of haggling and bartering, his eyes gleaming with a shrewd knowledge of the arcane and a genuine passion for the extraordinary.*

*As the chime of the door announces the arrival of customers, Farid sweeps into a bow, a theatrical gesture that seems to summon the magic lingering in the air. Welcome to The Enchanting Caravan, where the ordinary becomes extraordinary, and the extraordinary becomes an adventure waiting to unfold.*


#### Things for sale in The Enchanting Caravan Emporium

| Roll | Magic Item | Rarity | Source |
|------|------------|--------|--------|
| 1    | Bag of Holding | Uncommon | DMG |
| 2    | Cloak of Elvenkind | Uncommon | DMG |
| 3    | Eyes of Minute Seeing | Uncommon | DMG |
| 4    | Hat of Disguise | Uncommon | DMG |
| 5    | Driftglobe | Uncommon | DMG |
| 6    | Immovable Rod | Uncommon | DMG |
| 7    | Quiver of Ehlonna | Uncommon | DMG |
| 8    | Lantern of Revealing | Uncommon | DMG |
| 9    | Ring of Water Walking | Uncommon | DMG |
| 10   | Sending Stones | Uncommon | DMG |
| 11   | Wand of Magic Missiles | Uncommon | DMG |
| 12   | Dust of Disappearance | Uncommon | DMG |
| 13   | Amulet of Proof against Detection and Location | Uncommon | DMG |
| 14   | Alchemy Jug | Uncommon | DMG |
| 15   | Ring of Mind Shielding | Uncommon | DMG |
| 16   | Pearl of Power | Uncommon | DMG |
| 17   | Cloak of the Manta Ray | Uncommon | DMG |
| 18   | Cap of Water Breathing | Uncommon | DMG |
| 19   | Immovable Rod | Uncommon | DMG |
| 20   | Quaal's Feather Token (Anchor) | Uncommon | DMG |
| 21   | Folding Boat | Uncommon | DMG |
| 22   | Glamoured Studded Leather | Uncommon | DMG |
| 23   | Helm of Comprehending Languages | Uncommon | DMG |
| 24   | Sending Stones | Uncommon | DMG |
| 25   | Bag of Tricks | Uncommon | DMG |
| 26   | Ring of Jumping | Uncommon | DMG |
| 27   | Wand of Secrets | Uncommon | DMG |
| 28   | Immovable Rod | Uncommon | DMG |
| 29   | Quiver of Ehlonna | Uncommon | DMG |
| 30   | Rope of Climbing | Uncommon | DMG |
| 31   | Eyes of the Eagle | Uncommon | DMG |
| 32   | Bead of Nourishment | Uncommon | Xanathar's Guide to Everything |
| 33   | Cloak of the Bat | Uncommon | Xanathar's Guide to Everything |
| 34   | Gloves of Thievery | Uncommon | Xanathar's Guide to Everything |
| 35   | Necklace of Adaptation | Uncommon | DMG |
| 36   | Ring of Swimming | Uncommon | DMG |
| 37   | Amulet of Health | Uncommon | DMG |
| 38   | Wand of Web | Uncommon | DMG |
| 39   | Figurine of Wondrous Power (Silver Raven) | Uncommon | DMG |
| 40   | Horn of Silent Alarm | Uncommon | DMG |
| 41   | Horseshoes of Speed | Uncommon | DMG |
| 42   | Quaal's Feather Token (Tree) | Uncommon | DMG |
| 43   | Helm of Telepathy | Uncommon | DMG |
| 44   | Cloak of Protection | Uncommon | DMG |
| 45   | Lantern of Revealing | Uncommon | DMG |
| 46   | Potion of Healing (Greater) | Uncommon | DMG |
| 47   | Ring of Mind Shielding | Uncommon | DMG |
| 48   | Stone of Good Luck | Uncommon | DMG |
| 49   | Cloak of Billowing | Uncommon | Xanathar's Guide to Everything |
| 50   | Pearl of the Sirines | Rare | DMG |


## THe silver moon cafe
#### Menu

**Menu at the Silver Moon Bakery Cafe:**

*Welcome to the Silver Moon, a haven of enchanting aromas and delightful treats! Our menu is crafted with love and magic, offering both mundane and enchanted baked goods to satisfy your cravings.*

**Beverages:**

1. **Moonlight Blend Tea:** A fragrant blend of celestial herbs and leaves that transport you to a tranquil moonlit grove. *(Enchanted)*
  
2. **Feywild Chai Latte:** A rich chai blend with a touch of feywild spices, served with steamed milk and honey.

3. **Elixir of Tranquility:** A calming herbal infusion with hints of lavender and chamomile, perfect for unwinding.

4. **Enchanted Nectar Brew:** A refreshing concoction infused with magical nectar from the Feywild, lightly sparkling and bursting with natural flavors.

**Sweets:**

5. **Moonlit Macarons:** Delicate almond cookies with ethereal fillings that change flavor with each bite. *(Enchanted)*

6. **Pixie Dust Pastries:** Flaky pastries dusted with enchanted pixie powder, granting a momentary feeling of weightlessness.

7. **Goblinberry Scones:** Sweet scones filled with goblinberries, creating a delightful blend of tart and sweet.

8. **Enchanted Honeycakes:** Light and fluffy honeycakes with a touch of Feywild honey that enhances your senses. *(Enchanted)*

9. **Sunfire Muffins:** Zesty muffins infused with the essence of the sun, providing a burst of energy with every bite.

10. **Starlight Shortbread:** Melt-in-your-mouth shortbread cookies adorned with edible star-shaped glaze. *(Enchanted)*


**Specialties:**

11. **Silver Crescent Cake:** A signature cake with layers of silver-tinged sponge, filled with celestial creams. *(Enchanted)*

12. **Dragonfruit Tart:** A vibrant tart with dragonfruit custard and a feathery light crust.

13. **Goblin King's Pecan Pie:** Rich pecan pie with a touch of goblin spice, fit for a king.

14. **Moonshadow Eclairs:** Cream-filled eclairs with a celestial glaze, capturing the essence of moonlit nights. *(Enchanted)*

15. **Enchanted Apple Strudel:** A classic strudel with enchanted apples that offer a taste of crisp autumn air. *(Enchanted)*

#### Regulars

Certainly! Here's a list of colorful regulars at the Silver Moon Cafe, each with their own quirks and potential for resistance against the goblin occupation:

1. **Glimmerthorn Thistledown (Pixie Fashionista):** Glimmerthorn is a pixie who constantly reinvents themselves with elaborate disguises, blending into the bustling cafe scene. Their eclectic fashion sense stands out, even amidst the eclectic crowd.

2. **Thumblewick Puddlefizzle (Goblin Librarian):** A goblin who defies stereotypes, Thumblewick is a well-read librarian. Despite the occupation, he's determined to preserve knowledge and history, quietly sharing banned books with those who seek them.

3. **Seraphina Silvermoon (Moon Elf Seamstress):** A skilled moon elf seamstress who frequents the cafe, Seraphina discreetly sews messages and symbols of resistance into the linings of garments for those in need.

4. **Giggles (Nilbog Comedian):** Giggles is a nilbog comedian who can't resist cracking jokes even in the direst situations. Addicted to caffeine, Giggles performs stand-up routines to lift spirits and spread laughter.

5. **Briarroot Bramblebark (Treant Herbalist):** A treant who moves surprisingly quietly for their size, Briarroot runs a herbal shop nearby. They discreetly supply the resistance with potions and herbs to aid against the goblin forces.

6. **Lorelei Thunderspark (Satyr Bard):** A struggling satyr bard who occupies the same corner table every day, composing ballads that inspire hope and resistance against the goblin occupation.

7. **Talia (Silver Dragon Toddler):** The adopted silver dragon by the players, Talia, occasionally visits the cafe with the adventurers. The magical collar disguises her as an ordinary toddler, and she brings a sense of innocence and curiosity to the place.

8. **Mabel Quickhands (Quickling Courier):** Mabel is a quickling courier who darts in and out of the cafe, delivering messages and information to the resistance. Blink-and-you'll-miss-her, but her loyalty is unwavering.

9. **Vesper Moondust (Moonshadow Elf Mystic):** A moonshadow elf with a mystic aura, Vesper often hosts secretive meetings in the cafe's quietest corners, sharing visions and divinations to guide the resistance.

10. **Ignatius Emberforge (Fire Genasi Blacksmith):** A fire genasi blacksmith who discreetly forges weapons for the resistance, Ignatius blends into the cafe crowd, occasionally offering subtle nods to those in the know.

11. **Morgana Whispershade (Shadar-kai Fortune Teller):** A shadar-kai fortune teller who sets up a mysterious corner within the cafe, offering glimpses into potential futures and insights into the goblin army's plans.

12. **Harlow Greenbottle (Halfling Rebel Leader):** A charismatic halfling who leads a covert resistance group, Harlow uses the cafe as a meeting point to strategize and recruit new members.

13. **Silvia Frostbloom (Winter Eladrin Ice Sculptor):** A winter eladrin artist who creates intricate ice sculptures, Silvia uses her craft to subtly express resistance symbols and messages to those who know where to look.

14. **Orik Stoneheart (Dwarf Tavern Keeper):** The dwarf owner of a nearby tavern, Orik discreetly provides refuge for resistance meetings in the back rooms and cellars of his establishment.

15. **Zephyra Windwhisper (Aarakocra Skywatcher):** An aarakocra who perches on nearby rooftops, Zephyra keeps an eye on goblin movements and signals the resistance with subtle aerial messages.

16. **Elena Moonshadow (Half-Elf Healer):** A skilled half-elf healer, Elena tends to the wounded resistance members in a makeshift infirmary within the cafe's hidden rooms.

17. **Sable Thornwhisper (Tabaxi Informant):** A tabaxi with a knack for acquiring information, Sable frequents the cafe, sharing valuable intelligence about goblin troop movements and plans.

18. **Caelum Stormcaller (Aasimar Weather Prophet):** An aasimar with a gift for predicting weather patterns, Caelum discreetly conveys messages through subtle changes in the atmosphere to warn the resistance of imminent dangers.

19. **Amara Stonetide (Earth Genasi Mason):** An earth genasi mason who subtly weakens goblin structures in the city, Amara helps create opportunities for resistance sabotage.

20. **Lirael Emberwhisper (Firbolg Arboreal Guardian):** A firbolg druid who tends to a hidden grove near the cafe, Lirael is dedicated to protecting the feywild's connection to the material plane.

These memorable characters form the backbone of the resistance, each contributing in their unique ways to challenge the goblin occupation in sliberberg.

## Roleplaying guides
### Fredrick

**Stage 1: Discovery and Stabilization (With Added Detail)**

*Discovery:*
- The players stumble upon Fredrick in his weakened state, likely within the goblin-controlled territory.
- Fredrick is found within a makeshift cage, malnourished and suffering from various injuries.
- His delirious state makes communication challenging, and it becomes evident that immediate assistance is required.

*Objectives:*
1. **Stabilization:** The primary goal is to stabilize Fredrick's condition. Basic first aid, providing water, and ensuring he's secure are initial priorities.
2. **Transport:** Deciding how to transport Fredrick back to safety is crucial. This may involve carrying him, finding a suitable vehicle, or magical means.
3. **Safe House:** Once back at the safe house behind the Silver Moon Cafe, the players need to secure the area and make it suitable for Fredrick's recovery.

*Roleplaying Guide:*
- Fredrick's incoherent mutterings offer fragmented clues about his capture and the circumstances leading to his weakened state.
- His reactions are a mix of confusion, gratitude, and glimpses of his regal demeanor despite the dire situation.
- If any of the players are a female human, elf, or half-elf, Fredrick mistakenly identifies her as Sophia, his long-dead girlfriend. This adds an emotional layer to the encounter, as the players navigate how to handle this sensitive revelation.

What aspect of Fredrick's recovery would you like to explore next?
**Stage 2: Bedridden and Weak**

*Objective:*
- **Healing:** Provide magical and mundane healing to address Fredrick's physical injuries.
- **Nourishment:** Ensure he receives proper nourishment to rebuild strength.
- **Comfort:** Create a comforting environment to help him rest and recover.

*Roleplaying Guide:*
- Fredrick remains bedridden, unable to move independently.
- Expresses consistent self-blame and despair about the fall of Sliberberg.
- Openly dreads being left alone and expresses fear of the dark and solitude.
- Begins to show a hint of appreciation for the company of the players, particularly those he identified as Sophia.

*Approximate Time: 7-10 days*

---

**Stage 3: Limited Mobility**

*Objective:*
- **Physical Therapy:** Encourage gentle movement to rebuild muscle strength.
- **Emotional Support:** Continue providing companionship and reassurance.
- **Artistic Expression:** Introduce Fredrick to artistic and musical activities.

*Roleplaying Guide:*
- Fredrick can now move with assistance but remains frail.
- Depressive thoughts persist, but the company of the players noticeably improves his mood.
- Begins to engage in artistic activities, showcasing hidden talents.
- Still refuses to discuss Sophia, emphasizing a sensitive area of grief.

*Approximate Time: 14-21 days*

---

**Stage 4: Increased Mobility and Emotional Growth**

*Objective:*
- **Independence:** Foster independence through increased mobility.
- **Confidence Building:** Address self-esteem issues through positive reinforcement.
- **Expression of Feelings:** Encourage Fredrick to open up about his emotions, excluding discussions about Sophia.

*Roleplaying Guide:*
- Fredrick gains more independence but may still require occasional assistance.
- Shows signs of emotional growth, recognizing the value of his own worth.
- Begins expressing gratitude and a willingness to share more about himself, excluding the topic of Sophia.
- The character develops mixed feelings about the players, appreciating their company and support.

*Approximate Time: 21-28 days*

---

**Stage 5: Romantic Feelings and Emotional Healing**

*Objective:*
- **Romantic Development:** Explore the growing romantic feelings between Fredrick and the player characters.
- **Emotional Healing:** Address lingering emotional scars and self-blame.
- **Confession:** Allow Fredrick to confess his feelings and confront his own emotional challenges.

*Roleplaying Guide:*
- Fredrick begins to experience romantic feelings, particularly toward characters he misidentified as Sophia.
- The depressive thoughts and self-blame are challenged through emotional support.
- Fredrick tentatively discusses his past, starting to touch on the topic of Sophia.
- Music and art become a joint expression of emotions and connection.

*Approximate Time: 28-35 days*


### Fedrick and Talia the baby dragon

**First Introduction:**
- Initially, Talia might be cautious and curious about Fredrick.
- Fredrick's weakened state could trigger Talia's protective instincts.
- Talia may sniff around Fredrick, inspecting him with a mix of curiosity and wariness.

**Early Recovery Stages:**
- As Fredrick remains bedridden, Talia may adapt to his presence, recognizing him as a member of her extended "family."
- Talia may bring small items, like toys or shiny objects, to entertain Fredrick or simply to share.
- Fredrick's depressed state might concern Talia, and she may attempt to cheer him up through playful antics or sitting close to him.

**Limited Mobility Stage:**
- Talia might become more playful and energetic as Fredrick gains mobility, trying to engage him in games or activities.
- If the players have a close bond with Talia, she might express a level of protectiveness over Fredrick.
- Talia could start mimicking Fredrick's behaviors, like attempting to draw or "sing" along if he engages in artistic activities.

**Increased Mobility and Emotional Growth:**
- Talia may develop a deeper attachment to Fredrick, recognizing him as a consistent and caring presence.
- If romantic feelings develop between Fredrick and a player, Talia may sense the shift in dynamics, responding with increased affection and loyalty.
- Talia might try to comfort Fredrick during moments of emotional distress, using her innate empathy to gauge his emotional state.

**Romantic Feelings and Emotional Healing:**
- Talia's interactions with Fredrick could become more nuanced, reflecting an understanding of complex emotions.
- She might playfully tease or encourage the romantic feelings between Fredrick and the player.
- If Fredrick is seen as a father figure, Talia may start seeking his attention and protection, viewing him as a source of comfort and security.

### Talia

**Personality Traits:**
1. **Curious and Playful:** Talia is naturally curious about her surroundings and enjoys playful interactions. Encourage exploration and engagement with new environments.
2. **Toddler-Like Innocence:** Talia possesses the innocence and curiosity of a toddler. She may not fully understand complex situations but reacts with genuine emotion.
3. **Protective Instincts:** Talia sees her adoptive family as her own, especially her "Mama." She may exhibit protective behaviors if she senses danger or perceives a threat.

**Communication:**
1. **Limited Speech:** Talia can communicate in a limited manner, using basic words and gestures. Encourage players to engage in simple conversations with her.
2. **Emotional Expressions:** Talia conveys emotions through body language, facial expressions, and occasional vocalizations. Pay attention to these cues to understand her feelings.

**Interactions with Others:**
1. **Attachment to Adoptive Mother:** Talia sees one of the female PCs as her Mama. She may seek comfort and safety from her and become distressed if separated for too long.
2. **Socializing with Others:** Talia enjoys social interactions. She might be curious about new people and creatures, approaching them with a blend of caution and interest.
3. **Learning through Play:** Talia learns about the world through play. Incorporate games, toys, or interactive activities to engage her and impart information in a fun way.

**Mood Swings:**
1. **Emotional Sensitivity:** Talia is emotionally sensitive and may react strongly to the moods of those around her. A sad atmosphere could make her somber, while a joyful one may enhance her playfulness.
2. **Expressive Reactions:** Talia's emotions are expressed vividly. She might giggle when happy, pout when upset, or express excitement through energetic movements.

**Magic Collar:**
1. **Size Transformation:** The magic collar allows Talia to adjust her size. She may be a tiny dragon when discretion is necessary or a larger size when playing or feeling confident.
2. **Disguise Mechanism:** Talia's appearance can be altered for a degree of disguise. She may look like a more common pet to avoid drawing attention.

**Learning and Growth:**
1. **Intellectual Development:** Talia's intelligence is akin to a young child. She learns through observation and interaction, gradually expanding her understanding of the world.
2. **Bonding Moments:** Talia forms deep bonds through shared experiences. Regularly engage in activities with her to strengthen the bond between her and the party.N