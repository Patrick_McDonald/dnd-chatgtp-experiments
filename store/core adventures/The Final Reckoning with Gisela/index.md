---
title: The Final Reckoning with Gisela
date: "2023-12-20T22:40:32.169Z"
description: A adventure for player of level 10
---
## Adventure background


**Adventure Background:**

For months the Märchenweltgrenze has become the battleground for a proxy war waged between King Fredrick's court and the enigmatic Archfey Gisela, a sorceress whose ambitions transcend the boundaries of the Feywild. The player characters as high-ranking members of Fredrick court, has valiantly fought on the front lines of this hidden war. Initially, the conflict seemed rooted in the struggle for control over settlements within the Märchenweltgrenze, as both King Fredrick and Gisela sought to expand their influence. However, beneath the surface of diplomatic overtures and territorial disputes, however Gisela has resorted to darker methods.As the conflict reaches its zenith, the Fredrick and the court faces a dire revelation: Gisela is effectively immortal within her domain, the northern reaches of the Märchenweltgrenze. To confront her directly, the players must discover a means to neutralize her eternal existence. Fionnuala of the Summer Court, a entity one believed to be a friend, offers assistance for a price, Fionnuala seeks to enthrall the players and King Fredrick to use them in her own grand dark plan.


## The adventure begins
The players have been summoned by Fredrick to participate in a council of war
*As you gather in the dimly lit council chamber of Castle Silverberg, the air is heavy with tension. King Fredrick, his face etched with concern, stands at the head of the table, flanked by military commanders and the enigmatic leaders of the Silvermoon coven, Agatha Ironraisin and Tansy Fleetfoot. The rain outside matches the somber atmosphere within, pounding against the castle windows like a relentless drumbeat. Fredrick's voice cuts through the heavy silence as he addresses the room, "My friends, we are here because Gisela needs to be stop. Gisela's dark forces threaten the settlements throughout the Märchenweltgrenze. The time for diplomacy and gathering allies is past, and we must strike her down."*
The witches will point out that Gisela is effectively immortal while in her realm. She will simply rise from the dead somewhere safe in her realm if killed, the various other participants will suggest various strategies. The conversation will go nowhere and eventually Fredrick will suggest seeking the Council of Fionnuala the Archfey. The witches will try to advise against this corse of action because they know that Fionnuala motivations are sinister in nature, she gets her kicks granting wishes of people who wish to usurp thrones, destroy governments or scheme against another. Fredrick reveals that they have no choice in the matter.

## The march to Hexmarches
It is a 2 week march up the Sliberfluss river to Gisela domain of delight the Hexmarches. Along the way three random encounters from the following table occur. 
**Combat Encounter Table: Gisela's Forces**

Roll a d20 to determine the nature of the encounter. On a result of 1-11, the encounter is a combat encounter. On a result of 12-20, the encounter is a non-combat encounter.

1. **Mercenary Ambush:** A group of skilled mercenaries, hired by Gisela, emerges from the shadows, seeking to eliminate any threats to their dark mistress. (Monsters: Bandit Captain, Mercenaries)
  
2. **Wandering Shadows:** Dark fey, cloaked in shadow, ambush the party, seeking to sow chaos and despair on Gisela's behalf. (Monsters: Shadow, Night Hag)

3. **Undead Horde:** A horde of undead creatures, reanimated by Gisela's necromantic power, emerges from the Märchenweltgrenze. (Monsters: Wights, Ghouls)

4. **Cursed Guardians:** A pair of corrupted treants, twisted by Gisela's dark magic, stand as silent sentinels, attacking any intruders. (Monsters: Corrupted Treants)

5. **Dark Fey Ritual:** A group of dark fey is performing a ritual to strengthen the barrier between the material world and the Feywild. The players must interrupt the ritual. (Monsters: Drow Mages, Fey Cultists)

6. **Haunted Clearing:** The party stumbles upon a clearing haunted by vengeful spirits. They must navigate the ethereal disturbance while avoiding spectral attacks. (Monsters: Ghosts, Poltergeists)

7. **Illusory Passage:** Gisela's trickster fey have created an illusory pathway, leading the players into a trap. (Monsters: Green Hags, Fey Tricksters)

8. **Feathered Menace:** A flock of harpies, under Gisela's influence, descends upon the party, seeking to distract and disorient them. (Monsters: Harpies, Nightmares)

9. **Graveyard Encounter:** The players come across an ancient graveyard filled with undead minions, stirred by Gisela's dark presence. (Monsters: Skeletons, Zombies, Wraiths)

10. **Feywild Anomaly:** The boundary between the material world and the Feywild weakens, releasing unpredictable magical energies and summoning fey creatures. (Monsters: Blink Dogs, Faeries)

11. **Corrupted Fey Guardian:** A once-protective fey creature corrupted by Gisela's influence now attacks anyone who approaches its territory. (Monsters: Treant, Darklings)

12. **Wisdom of the Ancient Trees:** The players come across a grove of ancient, wise trees that offer cryptic advice or insights into Gisela's schemes.

13. **Feywild Portal:** A temporary portal to a peaceful corner of the Feywild opens, offering the players a brief respite and a glimpse of the Feywild's natural beauty.

14. **Blessing of the Silvermoon Coven:** The Silvermoon coven's witches appear and offer a magical blessing to aid the players in their quest.

15. **Enchanted Springs:** The party discovers mystical springs with healing properties. Drinking from these springs provides temporary hit points and removes minor ailments.

16. **Cursed Crossroads:** The players encounter a crossroads where a fey creature challenges them with riddles. Solving the riddles grants them luck, while failure brings minor misfortune.

17. **Feywild Wildlife Refuge:** A sanctuary for unique Feywild creatures offers the players a chance to observe and interact with these magical beings peacefully.

18. **Lunar Guidance:** The players encounter a celestial creature that provides guidance based on the phases of the moon, offering insights into Gisela's plans.

19. **Illusory Meadow:** The meadow seems ordinary but is actually an illusion concealing a hidden Feywild passage. Dispersing the illusion reveals the true path.

20. **Feywild Artisan:** An artisan fey offers to craft magical items or enhance the players' equipment in exchange for rare Feywild materials.

### Entering The Hexmarches

*As your party and the army traverse the ethereal border between the material realm and Gisela's domain, the Hexmarch, a profound shift occurs. The air thickens with an eerie gloom, and the landscape transforms into a haunting tapestry of shadows and twisted vegetation. The transition is marked by the unsettling sensation of passing through a misty veil that separates the known from the unknown.As the mist envelops you, the world on the other side reveals itself—a realm gripped by perpetual twilight, where the sky is a canvas of muted grays and purples. The land undulates with rolling hills and tangled thickets, where skeletal trees claw at the overcast sky. Wisps of mist cling to the ground, adding to the otherworldly atmosphere.*


The journey through the Hexmarch is fraught with the ominous sounds of unseen creatures and the occasional eerie whisper carried by the wind. The army marches with grim determination, their armor clinking softly, a stark contrast to the oppressive silence that blankets the land. After a days march the army reaches Finsterburg

*As you approach Finsterburg, the atmosphere shifts palpably. The gloom of the Hexmarch intensifies, casting long shadows over the town that huddles in the perpetual twilight. The cobblestone streets, worn and weathered, wind through clusters of somber stone buildings. The architecture, devoid of the whimsy found in the Feywild or the Märchenweltgrenze, stands as a testament to the resilience of those who call Finsterburg home.The buildings rise like silent sentinels, their facades etched with the passage of time and tainted by the lingering aura of Gisela's fortress. Lanterns adorned with protective symbols flicker outside doorways, attempting to push back the encroaching darkness with feeble but determined light.  As you make your way through Finsterburg, the air is heavy with an unspoken unity among the residents. They exchange cautious glances, acknowledging the shared burden of living so close to Gisela's castle. The distant tolling of the chapel bell, a haunting melody that seems to linger in the air, serves as a constant reminder of the ominous presence that looms over the town.*

**Battle of Finsterburg**

*As you approach Finsterburg, the air crackles with an oppressive tension. The townsfolk, who moments ago went about their daily lives, now rush toward the town's outskirts with fear etched across their faces. The distant sounds of clashing weapons and haunting wails punctuate the gloomy atmosphere.Emerging from the heart of Finsterburg, a horde of malevolent creatures advances. Redcaps, their bloodied hats giving them an ominous appearance, lead the charge. Darklings, quicklings, and the relentless rattling of skeletal bones in the form of skeletons and wights create a cacophony of impending doom. Among them, the Deathless Riders, undead knights clad in sinister armor, wield cursed blades with deadly precision.*

**Monsters:**
- Redcaps (1d4)
- Darklings (2d6)
- Quicklings (2d8)
- Skeletons (1d12)
- Wights (3)
- Deathless Riders (CR 5, Undead Knights)

**Allies:**
- Elven Scouts (2d6)
- Centaurs (1d8)
- Knights (1d6)
- Guards (2d8)

**Special Ally:**
- King Frederick, Champion (Champions Stat Block)

The battlefield is set, the outskirts of Finsterburg transforming into a chaotic war zone. The players find themselves at the forefront, joined by a coalition of allies led by King Frederick.

**Initiative Order:**

1. **Player Characters**
2. **Allied Forces (Elven Scouts, Centaurs, Knights, Guards)**
3. **Enemy Forces (Redcaps, Darklings, Quicklings, Skeletons, Wights, Deathless Riders)**
4. **King Frederick**
*Having carved a determined path through the malevolent horde that sought to engulf Finsterburg, you stand amidst the chaos, the echoes of battle still ringing in your ears. King Frederick, his armor bearing the scars of the skirmish, calls out to you and your companions."Follow me!" he commands, his voice cutting through the clamor of combat. "We'll put an end to Gisela's reign of terror once and for all!"As you approach the imposing silhouette of Gisela's Castle of Hate, a foreboding structure that seems to defy the laws of reality, the air becomes thick with the oppressive aura of the Hexmarch. The castle rises like a dark spire against the perpetual twilight sky, its architecture a testament to the archfey's malevolent power."We face Gisela in her own domain," he declares, his gaze unwavering. "Stay close, and together, we shall confront the sorceress and put an end to the darkness she has wrought upon these lands."*
## Gisela Castle of Hate



>### Madness
> The caslte oozes Gisela all consuming maddening hate. It twists almost everything in the castle and forms in pools of black mist. Exposure to the black mist causes a player to experience one of the following 6 forms of temporary insanity. Normal rules for temporary insanity apply.
>1. **Paranoia's Embrace:**
>   - *Effect:* The character becomes convinced that their allies are plotting against them. They have disadvantage on all ability checks and saving throws that involve trusting or relying on others.
>
>2. **Rage Unbound:**
>   - *Effect:* Overwhelmed by blind fury, the character attacks the nearest creature, friend or foe, for 1 minute. The character must make a Wisdom saving throw at  the end of each of their turns to end the effect.
>
>3. **Delusions of Betrayal:**
>   - *Effect:* The character believes that everyone, including allies, is an illusion or shape-shifted enemy. They may act erratically or refuse to cooperate with others.

>4. **Hatred's Hallucinations:**
>   - *Effect:* The character sees twisted illusions fueled by Gisela's hate—horrific scenes, malevolent figures, and nightmarish landscapes. Disadvantage on Perception checks and attacks against illusions.
>
>5. **Seething Fury:**
>   - *Effect:* The character is overwhelmed by an all-encompassing rage. They gain advantage on Strength-based attacks and checks but have disadvantage on Intelligence and Wisdom-based actions.
>
>6. **Paralyzing Distrust:**
>   - *Effect:* The character becomes paralyzed with fear and suspicion. They cannot take any actions and have disadvantage on saving throws against being frightened.



**1 Encounter in the Castle Foyer**
*As you step into the foreboding foyer of Gisela's Castle of Hate, the heavy doors slam shut with a resounding thud, sealing you within. Gisela's haunting voice echoes through the air, a sinister melody urging you to seek her out if you dare. The grand space is adorned with twisted tapestries and malevolent statues, their cold gazes seeming to follow your every move. A pair of imposing staircases leads up to a central landing, and numerous doors beckon in other directions, shrouded in shadows.*

*Combat Encounter:*
The air crackles with an oppressive energy as several statues lining the foyer animate, their stony forms coming to life with a jarring, echoing sound. The statues move with a malevolent purpose, their animated armor gleaming with an unnatural sheen.

**Enemies:**
- **Animated Statues (x4):** These statues come to life with a vengeance, using the stats of animated armor. They move with surprising agility and strike with unyielding force.

**Environment:**
- **Black Mist Pool:** A large pool of inky black mist stretches across the foyer floor. Any non-construct creature that enters this area must make a Wisdom saving throw. On a failed save, the character is afflicted with Rage Unbound (as described in the temporary madness effects).

**Tactics:**
- The Animated Statues strategically position themselves to flank and surround the party, using their animated agility to strike from unexpected angles.
- Gisela's sinister voice continues to taunt and mock the players, serving as a psychological distraction.

**2 The Great Hall**

*As you enter the Great Hall, the enormity of the space unfolds before you. The room is expansive and circular, its high ceiling disappearing into shadowy heights. A dim, ambient light emanates from an unseen source, casting eerie shadows across the grandeur of the chamber.The walls are adorned with faded tapestries, each depicting scenes of nobility and joy now twisted with a malevolent touch. Tall, arched windows line the upper reaches, their stained lass cracked and depicting haunting images of sorrow and despair. The air is heavy with the weight of history and the residual echoes of countless events that have unfolded within these walls.As you stand in the Great Hall, there is an unshakable feeling of being observed. The silence is occasionally broken by soft whispers, as if the very stones themselves retain the memories of the sorrow and torment that have transpired within these hallowed walls.*

**3 The Maddening Library Tower**

*As you enter the base of the tower, you find yourself within a vast library of arcane knowledge. The air is thick with the scent of ancient parchment and the subtle undertone of foreboding. Towering shelves, lined with tomes and scrolls, surround you, creating an almost labyrinthine maze of knowledge.*
Scattered throughout the library are several books of considerable value, their bindings adorned with gold leaf and precious gems. These tomes are worth a total of 800 gold pieces for those who can discern their worth. However among the shelves, there are books filled with maddening symbols and incomprehensible diagrams. Cautionary notes left by previous explorers warn of the dangers within. Reading these tomes may inflict madness upon the reader, twisting their thoughts and perceptions.

**Maddening Effects:**

- Characters who choose to read a maddening tome must make a Wisdom saving throw. On a failed save, they suffer a temporary madness effect, as described in the temporary madness effects previously established for Gisela's Castle of Hate.

- The symbols within these tomes warp the fabric of reality, creating a disorienting and chaotic experience for those who dare to decipher their secrets.


**4 The Abandoned Servant Quarters**

*As you step into the servant quarters, a lingering sense of emptiness pervades the air. The room is devoid of life, and the silence is only broken by the occasional creaking of the floorboards beneath your feet. This once-bustling space has fallen into disuse, now a testament to the solitude that echoes through Gisela's Castle of Hate. Rows of neatly made but unoccupied beds line the walls of the various rooms. Dust has settled on the simple furniture, coating the surfaces in a fine layer of neglect. The room seems frozen in time, as if the world outside has continued to turn while this corner of the castle remains trapped in solitude.* 
Scattered around the rooms are a few personal items, remnants of the servants who once called this place home. Among them, you find small trinkets, faded portraits, and forgotten mementos, collectively worth about 300 gold pieces to those who see value in the memories they hold.

**5 Second Floor Foyer Balcony**

*As you ascend the grand staircase to the second floor, you find yourself on a balcony that overlooks the main foyer below. The air is heavy with an ominous stillness, and an unsettling presence hangs in the atmosphere. Before you, the stairway continues upward, but a foreboding sight halts your progress—a billowing bubble of black mist, dark and malevolent, blocking the path. Tendrils of the black mist extend through two of the open doors on the balcony*

 The black mist before the players forms a barrier, creating a nightmarish bubble that pulsates with eldritch energy. Any attempt to cross it triggers horrifying hallucinations and an overwhelming sense of terror. The staircase beyond is shrouded in an impenetrable fog. Tendrils of the black mist extend through two of the open doors on the balcony. The tendrils writhe with an unsettling energy, leading to unknown depths within the castle.

**Maddening Effects:**

- Characters who approach the mist must make a Wisdom DC(25) saving throw. On a failed save, they are subjected to horrifying hallucinations that induce intense fear. The visions may be tailored to each character's deepest fears and regrets.

**6 The Greenhouse**

*As you follow the tendrils of black mist, they lead you to a secluded rooftop garden above the abandoned servant quarters. The air is heavy with an unnatural stillness, and the twisted plants that surround you seem to writhe with malevolence. The tendrils guide you to a focal point—an ominous blood-red crystal pulsating with an eerie energy at the garden's center.Suddenly, the flower beds come to life as several twig blights emerge, their skeletal forms rising from the distorted soil. The Venus fly traps pivot to face you, their carnivorous mouths opening wide. A malevolent aura emanates from the crystal, and the air is thick with an ominous energy.*

**Enemies:**
- **Twig Blights (x6):** These animated, twisted branches surge from the ground, moving with a jittery and unsettling motion.
- **Venomous Fly Traps (x2):** Towering plants resembling Venus fly traps, capable of spitting hallucinogenic gas.

**Special Abilities:**
- **Hallucinogenic Gas (Venomous Fly Traps):** The Venus fly traps spit a cloud of gas in a cone, causing Hatred's Hallucinations (as described in the temporary madness effects). Characters caught in the gas must make a Wisdom saving throw to resist the effects.

Destroying the crystal causes the cloud of mist in the foyer to disparate slightly. The crystal breaks with one blow.

**7 The Dark Gallery: ***

*As you follow the dark tendril of mist, it guides you into a towering art gallery within the castle. The air grows heavy with the unsettling aura of the room. The paintings and sculptures that surround you are a nightmarish display of torment and agony, each depicting scenes of death and torture in gruesome detail. A central crystal on a pedestal gleams with an ominous light, casting an eerie glow on the disturbing artwork.

**Trap: The Maze of Torment**

*Complicating matters, you soon realize that the seemingly solid floor is, in fact, an invisible maze of tiles. Stepping off the safe path triggers a torrent of psychic energy that courses through your mind, causing immense pain.*

**Mechanics:**
- The floor is an invisible maze of tiles, and stepping off the correct path inflicts massive psychic damage (scaled for level 10 characters).
- Staring at the disturbing artwork for more than 2 rounds can induce madness, making navigation even more perilous.
- Characters who succumb to madness gain the ability to see the glowing path through the maze, providing a distorted advantage.
Destroying the crystal causes the cloud of mist in the foyer to disparate slightly. The crystal breaks with one blow.

**8 The Great Hall Grand Balcony**

*Through a door off the second-floor balcony of the foyer, you find yourself stepping onto the grand balcony that overlooks the expansive Great Hall below. The air is charged with the residual energy of the castle, and from this vantage point, you have an unparalleled view of the grandeur and the shadows that dance within the vast space.*

**9 The Echoing Study**

*You come across a small, twisted room that echoes the simplicity of a mundane mansion's study. A, t weathered desk, covered in dusty tomes and scrolls, stands in a corner. An ancient quill rests on an ink-stained parchment, frozen mid-sentence as if abandoned in haste. A single candle flickers on the desk, casting uneven shadows that dance with unseen entities. The flame appears to burn with an otherworldly vigor, resisting any attempt to snuff it out.*
As characters spend time in the Echoing Study, they begin to hear faint whispers that play on their fears and insecurities. The whispers could plant seeds of doubt or reveal hidden secrets
THe candle is a *Candle of Invocation*. THe desk contains a *Potion of Mind Reading* and a pouch of *Dust of Disappearance*.A small locked box hidden beneath a pile of old parchments contains 400 gold pieces in assorted coins and small gemstones. The lock is not particularly complex, but the box radiates a faint magical aura, hinting at additional protective measures.


**10 The Third Floor Landing and Hall**

*Ascending the grand staircase from the second floor, you reach the Third Floor Landing, a transitional space leading to the various rooms on this level. The air feels charged with an eerie energy, and the walls are adorned with faded tapestries depicting scenes of feywild landscapes now twisted by Gisela's influence.Dimly lit sconces line the walls, casting flickering shadows that seem to dance with a life of their own. The flames within the sconces burn with an otherworldly hue, contributing to the eerie atmosphere.*


**11 Gisela's Arcane Workshop: Spectral Assault**

*As you ascend to the top of the tower, you find yourself in Gisela's expansive magical workshop. The air is thick with arcane energy, and a swirling cloud of black mist hangs ominously overhead. Suddenly, ethereal forms materialize from the mist, taking on ghostly shapes and launching an assault.*

**Encounter: Spectral Assault**

*Second Person Description:*

*Before you, the magical workshop sprawls with ancient tomes, enchanted artifacts, and arcane contraptions. The atmosphere is tense with a blend of otherworldly power and a sense of Gisela's malevolence. As you take in the surroundings, a large cloud of black mist materializes above, coalescing into ghost-like forms that descend upon you with haunting speed.*

**Enemies: Spectral Shadows**

- *Spectral Shadows (x6):* These ghostly entities manifest with twisted, contorted features. Instead of their usual Life Drain ability, their touch induces a surge of psychic damage and triggers Rage Unbound—a maddening state of uncontrollable fury.

**Special Abilities:**

- **Rage Unbound:** Instead of Life Drain, the Spectral Shadows inflict psychic damage with their incorporeal touch. Victims must succeed on a Wisdom saving throw or succumb to a state of intense rage, attacking the nearest creature (friend or foe) uncontrollably for a brief duration.

**Tactics:**

- The Spectral Shadows move through objects and barriers, making them difficult to evade within the cluttered workshop.

- They strategically target characters with a mix of psychic attacks and attempts to induce Rage Unbound, exploiting the chaotic nature of their assault.

**Workshop Features:**

1. **Enchanted Workbenches:** Tables covered in arcane symbols and magical tools offer partial cover for characters seeking refuge.

2. **Gisela's Experimentation Notes:** Scattered parchments reveal Gisela's twisted experiments, providing potential insight into the sorceress's motives and methods.

3. **Floating Orbs:** Luminescent orbs suspended in the air provide dim illumination. Interacting with these orbs may trigger unpredictable magical effects.

On the work bench is one item from th following list

1. **Staff of Power:** This potent staff may have been a focal point for Gisela's magical experiments, empowering her spellcasting abilities.

2. **Cloak of Invisibility:** A magical cloak that grants invisibility, possibly tucked away in a hidden compartment.

3. **Wand of Fireballs:** A wand that conjures fiery explosions, revealing Gisela's penchant for destructive magic.

4. **Gloves of Spellcasting:** These gloves enhance the wearer's ability to cast spells, showcasing Gisela's commitment to magical mastery.

5. **Robe of Eyes:** A robe adorned with countless eyes, allowing the wearer to see in all directions—a fitting item for a sorceress with a penchant for schemes.

6. **Crystal Ball:** A magical scrying tool that might hold visions of Gisela's plans or secrets.

7. **Gem of Seeing:** A gem that grants truesight, revealing hidden truths and illusions within the workshop.

8. **Ring of Mind Shielding:** A ring that protects the wearer from mental intrusion, possibly safeguarding Gisela's secrets.

9. **Amulet of Health:** A magical amulet enhancing the wearer's constitution, potentially linked to Gisela's experiments in immortality.

1. **Pearl of Power:** A magical pearl that restores spell slots, indicating Gisela's desire for magical endurance.

11. **Helm of Teleportation:** A helm that allows instant teleportation, providing Gisela with a means of escape or swift movement.

In one of the cupboard is the following potions
1. **Potion of Mind Reading:** This potion allows the drinker to read the surface thoughts of a target for a limited duration. Gisela might have used this potion to extract information or delve into the minds of her foes.

2. **Potion of Invisibility:** A vial containing a shimmering liquid that grants the drinker invisibility for a set period. Gisela may have employed this potion for stealth or surprise attacks.

3. **Potion of Clairvoyance:** This magical potion bestows the ability to see and hear a distant location for a brief time. Gisela could have used this potion for spying or gathering intelligence from afar.

4. **Potion of Heroism:** Drinking this potion grants temporary hit points and bravery, possibly indicating Gisela's preparation for confrontations or battles where she needed enhanced resilience.

**12 The Conservatory of Haunting Melodies**

*As you step into the conservatory, the air is filled with the haunting strains of ethereal music. Transparent figures, their forms shimmering like moonlight, are engaged in a spectral performance. Ghostly musicians play instruments with a melancholic beauty that echoes the sorrows of Gisela's twisted past.*
Transparent apparitions, each with a forlorn expression, play a variety of musical instruments—cellos, violins, pianos, and more. The ghostly orchestra weaves a symphony of haunting melodies that seem to resonate with the very essence of the castle.  The air is filled with whispers that share the stories of Gisela's deteriorating mental health. Ghostly voices recount tales of her descent into madness, and the atrocities she committed with each step, each musician adding their perspective to the tragic narrative. The ghostly musicians, though trapped in their spectral existence, eagerly share secrets about the upper floors of the castle. They reveal hidden passages, warn of dangers, and hint at the true nature of Gisela's sorcery.

**13 The Fourth Floor Landing and Hallway**

*As you ascend to the fourth floor, you find yourself on a landing that opens into a mysterious hallway. The air is heavy with enchantment, and the walls are adorned with tapestries depicting feywild landscapes. An unsettling stillness pervades the atmosphere.*


**14 The Fey-Touched Guest Rooms**

*Upon entering the guest rooms, you're greeted by an ethereal ambiance that transcends the material plane. The air is imbued with the scent of exotic flowers, and each room is a haven of otherworldly comfort, designed for visitors from beyond. Elaborate murals on the walls depict mesmerizing portal scenes, hinting at the diverse realms from which Gisela's guests might hail. The portals appear to be in constant motion, as if inviting travelers to step through.Wardrobes in each room contain garments woven from feywild fabrics. These enchanted clothes adjust to fit any visitor, providing comfort while subtly attuning to the unique energies of different planes.*

The various rooms have small valuables worth about 700gp total.

**15 The Grand Bathroom**

*As you step into the turret, you find yourself in a space that defies the expectations of a traditional bathroom. This grand chamber, more opulent than practical, is a testament to Gisela's extravagant tastes and magical prowess. The domed ceiling is adorned with a celestial mural depicting the night sky of the feywild. Enchanted lights imitate the soft glow of stars, creating a serene ambiance. The centerpiece is an enormous, opalescent bath carved from a single massive gemstone. The bathwater, continuously warmed by magical means, shimmers with a faint iridescence*

**16 The Tragic Reunion**

As the players delve deeper into Gisela's Castle of Hate, they come across a chamber veiled in shadows and echoes of tormented screams. Opening the door, they discover a room where misery and tragedy intertwine—the very essence of Gisela's cruelty laid bare.

**Features:**

1. **Chains of Despair:** Elena Schwarzwald, once the love of Fredrick's life, is now bound to a bed by ethereal chains that seem to writhe with malevolent energy. The chains, remnants of Gisela's sadistic torment, glow dimly in the darkness.

2. **Incomprehensible Whispers:** Elena, her once-beautiful face now contorted in madness, utters incomprehensible whispers that echo through the room. Her eyes, filled with a haunting vacancy, briefly focus on the intruders before shifting back into the abyss of her broken mind.

3. **Torn Love Letters:** Scattered around the room are fragments of love letters and mementos from a time long past. These fragments tell a tale of a love torn asunder by the machinations of a vengeful archfey.

4. **Flickering Candlelight:** The room is illuminated by flickering candlelight, casting eerie shadows that dance across the walls, mirroring the fractured state of Elena's psyche.

**Dramatic Interaction:**

As the players approach, Elena's screams intensify, punctuating the sorrowful ambiance of the room. Fredrick, upon seeing his long-lost love in this wretched state, is overcome with grief. The players face the heart-wrenching decision of whether to leave Elena in her perpetual torment or seek a solution that may be deemed unthinkable.

**Tragic Choices:**

1. **Mercy or Cruelty:** The players must decide whether to end Elena's suffering through a merciful act or explore potential magical means to heal her shattered mind.

2. **Deeper Consequences:** Fredrick grapples with the decision, torn between wanting to spare Elena further pain and the fear of what Gisela's machinations have truly done to her. The players' advice may carry weight in the young king's anguished decision.

3. **Moral Dilemma:** This encounter forces the players to confront the grim reality of Gisela's cruelty and raises questions about the lengths one is willing to go to protect the ones they love.

**Impact on the Narrative:**

The outcome of this encounter shapes not only Fredrick's emotional journey but also influences the overall narrative. The choices made here will leave a lasting impact on the characters' relationships, the tone of the campaign, and the consequences they may face in the ongoing battle against Gisela and her twisted plots.


**17 Gisela's Chambers**

*Gisela's bedroom is a surreal amalgamation of twisted beauty and malevolent enchantments. The air in Gisela's bedroom is thick with the black mists that have solidified into fragile, glistening crystals. These crystals hang in the air like fragile decorations, reflecting a distorted beauty that mirrors Gisela's fractured mind. Adorning the walls is an intricate tapestry depicting scenes from Gisela's past, each image distorted by the maddening influence of the feywild. Characters might catch glimpses of Gisela's once-beautiful court, now transformed into a nightmarish landscape. At the center of the room stands a massive Shield Guardian—an enchanted construct crafted from dark materials and infused with the essence of the maddening mist. The guardian looms over Gisela's bed, its eyes glowing with an eerie intensity.

**Fragile Crystals**

- **Fragile Crystal Explosions:** If a character attempts to strike or throw one of the crystallized mists, it explodes upon impact, releasing a burst of maddening energy. Characters caught in the explosion must make a Wisdom saving throw or suffer the effects of a short-term madness.


**18 Vanity Chamber**

*Upon entering the Vanity Chamber, the air is thick with the fragrance of perfumed cosmetics and the sight of opulent gowns adorning the space. This room serves as a testament to Gisela's obsession with appearance and the indulgence of her feywild tastes.*

*Treasures:**

1. **Gown of the Feywild Waltz:** A particularly enchanting gown that seems to shift and dance on its own accord. When worn, the wearer gains advantage on Charisma (Performance) checks and can cast the *Dancing Lights* cantrip at will. (Value: Priceless)

2. **Feywild Blossom Jewelry Set:** A set of jewelry adorned with blossoms from the feywild. When worn together, the set grants advantage on Charisma saving throws and checks. (Value: 500 gp)

3. **Whispering Veil:** A sheer veil that seems to whisper faint, melodious secrets from the feywild. When worn, the veil grants advantage on Wisdom (Insight) checks and Charisma saving throws against charm effects. (Value: 300 gp)

4. **Feywild Glamour Pouch:** A small, intricately embroidered pouch containing magical cosmetics. When applied, these cosmetics provide advantage on Charisma-based checks for a limited time. (Value: 200 gp)


**19 Entrance to the Throne Room:**

*Standing before the imposing entrance to Gisela's Throne Room, you are met with a giant brass door adorned with intricate feywild motifs. The door harbors a mysterious lock that has 5 holes that look like they fit giant fingers rather than keys*

To open the door, characters must use the right hand of the Shield Guardian, the very construct that stands as Gisela's guardian and enforcer. Placing the hand into the lock causes the fingers to align perfectly with the finger-shaped holes, triggering the unlocking mechanism.

**20 The throne room**

*As the characters unlock the colossal brass door with the hand of the Shield Guardian, it creaks open to reveal the sprawling expanse of Gisela's Throne Room—the culmination of her dark reign over the Märchenweltgrenze. The air is thick with a malevolent aura, and the room itself seems to pulse with feywild energies. At the far end of the room, two towering thrones rest upon a raised dais. Gisela's throne is adorned with twisted vines and eerie blossoms, while an empty throne beside it seems to yearn for a rightful occupant. Ethereal orbs hover at various heights, emanating pulsating energy. These orbs seem to observe the characters' every move, reflecting Gisela's connection to the feywild and her mastery over its forces.*
*As you step into the Throne Room, a haunting melody begins to play—a melancholic tune that echoes through the vast space. The melody emanates from the vicinity of Gisela's throne, where the archfey herself sits.Her cruel eyes fixate on the intruders with a mixture of disdain and malevolent glee and she starts to monologue.*
*"Ah, look what we have here—King Flea and his merry band of miscreants. How quaint that you've managed to crawl your way into my dominion. Congratulations, Fredrick; I did not think you capable of such persistence.*

*Did you enjoy your little reunion with Elena? How heartwarming that after all these years, you still yearn for the one you cast aside like a common whore. I sought to free her of those feelings, yet here you stand, still entangled in the webs of your own selfish desires.*

*And look at the ladies in your entourage. I see your taste in women has not improved one bit, Fredrick. Perhaps you find solace in your companions since you've lost your grip on the hearts of those you've wronged.*

*But, oh, how I hate you, Fredrick. I loathe the very essence of what you did to my daughter. A million deaths, a million years in the depths of the nine hells—none of it would be punishment enough for your crimes. And when I'm through with your pitiful friends, you, my dear king, will become my eternal plaything. I will make your existence a symphony of suffering, a masterpiece of torment that will echo through the ages.*


**Mystical Mist:** The room is almost entirely filled with three dense clouds of mystical mist, each carrying the potential to induce madness. These mists periodically shift, making the battlefield an unpredictable and treacherous terrain.

**Main Objective: Use the Feybane Lantern**

*The primary goal of this encounter is not to defeat Gisela in direct combat but to use the Feybane Lantern to extract her soul. The players must survive Gisela's onslaught while successfully concentrating for three rounds to focus the magical energies of the lantern.*

**Feybane Lantern Mechanics:**

1. **Concentration Rounds:** At the start of each character's turn, the player concentrating on the Feybane Lantern must make a Constitution saving throw (DC 15) to maintain concentration. Failure resets the concentration progress.

2. **Three Rounds to Focus:** The players need three consecutive rounds of successful concentration to fully focus the Feybane Lantern. After the third successful round, the lantern is activated. Once activated, Gisela must make a Wisdom saving throw (DC 17) with disadvantage. If she fails, her soul is ensnared by the lantern, rendering her unconscious.

*As the Feybane Lantern pulses with gathered energy, you release its ethereal glow into the swirling mists of Gisela's Throne Room. In an instant, the arcane energies coalesce around Gisela, her form convulsing as the feywild shadows retreat from her essence. A fleeting but profound moment ensues, where the air resonates with the echoes of centuries-long torment. Gisela's eyes, once filled with malice, now reflect a hollow emptiness as her soul is ensnared by the lantern's enchantment. The mists recede, leaving a stillness that settles over the Throne Room, signaling the end of Gisela's reign of torment in the Märchenweltgrenze.*
A quick search of GIsela body reveals she has a staff of power on her and a crystal ball in a pedestal by her throne
## The aftermath
*The air in the Throne Room hangs heavy with the aftermath of the battle, and as the final echoes of magic fade, King Fredrick, your breaths ragged, stands amidst the remnants of the struggle. He looks around at the silent and still chamber, a mixture of exhaustion and relief etched across his face. With a deep exhale, he mutters, "It's finally over." His eyes then fall upon Gisela's lifeless body, her soul ensnared by the Feybane Lantern. A twisted anger contorts Fredrick's features, and he begins to kick the limp form with a vengeance, each impact punctuated by a cold proclamation of a crime committed against him. "For every betrayal, for every torment," he grits through clenched teeth. When his rage has run its course, he slings Gisela's soulless body over his shoulder, his expression stoic, and with determined steps, he heads towards the entrance of the Throne Room.*
Fredrick is oddly silent as he carries the limp body of Gisela down the stairs. His face is a mask of grim determination.

*As you and King Fredrick emerge from the imposing castle, the soldiers stationed outside snap to attention, saluting with a mixture of respect and wariness. The unease in their eyes is palpable as they catch a glimpse of the expression etched on Fredrick's face—an amalgamation of triumph, sorrow, and the lingering embers of vengeance.*
Fredrick continues on without a sound
*As the night surrenders to the soft glow of dawn, King Fredrick, unwavering in his resolve, strides through the misty veil that marks the boundary between Gisela's cursed domain and the rest of the Märchenweltgrenze. You and the remnants of the army have steadfastly followed his lead through the long night, the weight of the archfey's soulless body carried all the way. With a sudden and deliberate motion, Fredrick releases his grip, and Gisela's limp form lands on the ground with a thud, the sound echoing like a somber punctuation to the journey. Fredrick's eyes, a mix of weariness and grim determination, survey the landscape. "It is time to end this," he declares, his voice cutting through the morning stillness, setting the stage for the conclusion of a chapter that has held the Märchenweltgrenze in the grip of feywild machinations.*

### The execution
*In the clearing beyond the misty veil, King Fredrick stands before Gisela's soulless form, the atmosphere thick with an eerie stillness. The soldiers, having formed a solemn perimeter, watch in silence as Fredrick, his gaze hardened, grapples with the weight of what must be done. The air is charged with the gravity of the moment.*

*As the executioner's block is prepared, a tense silence blankets the clearing. Fredrick's hands tremble slightly, revealing the emotional turmoil beneath his stoic exterior. The time has come for the final act—a somber conclusion to the proxy war that has plagued the Märchenweltgrenze.*

In this pivotal moment, the players, if they choose, can intervene.It clear that Gisela must die but should it truly be Fredricks hand given his current emotional state?

*As the killing blow is delivered, a swift and definitive end to Gisela's tormented existence, a profound shift courses through the Märchenweltgrenze. The mist veil, once a boundary between realms, dissipates like morning fog, unveiling a landscape reborn. The Märchenweltgrenze, now free from the archfey's malevolence, reclaims its vitality. In the distance, Gisela's castle crumbles into dust, a symbol of a tyranny undone.*

*Fredrick, a complex mosaic of emotions etched upon his face, gazes at the transformed landscape. Satisfaction mingles with melancholy, and in a quiet moment, he speaks words of dedication to Elena. "That was for you, Elena," he murmurs, his voice carrying a mixture of closure and longing. "May we find each other in the next life." With these words, the Märchenweltgrenze begins its slow journey towards recovery, and a new chapter unfolds in the wake of Gisela's demise.*

## The return
### The parade
*As word of the triumphant victory spreads like wildfire through Silberberg, the town erupts into a joyous celebration. Citizens pour into the streets, waving banners and cheering, their faces painted with smiles of relief and jubilation. The anticipation is palpable as the townsfolk eagerly await the return of King Fredrick and his victorious army.*

*Amidst the fervor, a grand golden chariot emerges, adorned with symbols of victory and pulled through the cheering crowds. King Fredrick stands tall, his presence commanding the attention of all. The improvised parade weaves its way through the town, a procession of triumph and resilience. The air is filled with the sounds of jubilant cheers, clinking tankards, and the joyous melodies of bards recounting the tale of the Märchenweltgrenze's liberation.*

*Silberberg, once shrouded in the ominous shadow of Gisela's influence, now basks in the radiant glow of newfound freedom. The streets echo with the resounding cheers of the people, celebrating not just a military victory, but the reclamation of hope and the promise of a brighter future for the Märchenweltgrenze.*

Fredrick who had been sullen throughout the march back is in good spirts.

### Fionnuala true face
*As you enter the castle, the atmosphere is eerily calm. In the throne room, Fionnuala sits regally on the throne, her demeanor poised and congratulatory. "Congratulations on your victory," she remarks with a smile that doesn't quite reach her eyes. The air thickens with an unsettling tension as her laughter begins, a disconcerting sound that gradually morphs into a manPerhaps one is moved by mercy or seeks an alternative resolution. The weight of the decision hangs in the balance, and the narrative takes an uncertain tyrannical symphony.*

*"Oh, Gisela," Fionnuala muses, her laughter still lingering, "such a predictable pawn. Her anger made her easy to manipulate, but alas, she was always too fixated on her own vendetta against Fredrick to be much use in my grand plan. She served her purpose, but now it's time for more... interesting games." The flicker of amusement in her eyes turns predatory as she addresses you and Fredrick. "You, my dear Fredrick, and your valiant companions, shall make exquisite pawns. So much potential, so much ambition," she declares with a chilling certainty.*

*With an unsettling calmness, she addresses the gathering, her eyes glinting with amusement."Ah, dear Fredrick, you and your valiant companions have played your parts so well. A grand spectacle of victory, wouldn't you say? But let me share a tale". She leans forward, her eyes piercing through the room with an otherworldly intensity. "Gisela, in her desperation to bring ruin to Bergherz and your father, sought me out," she says with a wicked smile, "We signed a contract, I gave her the very ritual she used to tear the veil between realms. In exchange, she swore eternal fealty to me. Over the centuries, she wove more contracts, each one sealing her fate deeper into my grasp." Her lips curl into a sly smile as she revels in the unfolding drama. "And now, Fredrick, you stand victorious, unknowingly inheriting the debts of Gisela. A contract, binding you and your companions to me. Break it, and the consequences are severe. You, my dear vassals, will be turned to stone, imprisoned for eternity. The only escape? A wish. Choose wisely, for the game has only just begun.*
If the player try to use the feybane lantern read the following
*In the span of a moment, the Feybane Lantern vanishes from the your' grasp, only to reappear in Fionnuala's clutches. A sly smirk graces her features as she comments on the perceived audacity of turning her own creation against her. "How adorable", she says with venomous delight, "thinking you could wield my creation against me. The contract you signed to acquire that lantern, my dear vassals, carries consequences. Turn it against me, and you shall find yourselves transformed into owls. I won't be losing my finest chess pieces so soon." With a casual grace, Fionnuala rises from her throne, a shimmering doorway materializing before her. "Fear not, my pawns. An ambassador will soon grace your court to assist with your lack of style. Until then, cherish the bonds we've forged. Farewell." As she steps through the doorway, it dissolves into nothingness, leaving the players in the now-silent throne room, the weight of Fionnuala's words lingering in the air.*

### Fredrick breakdown

*King Fredrick's once-resolute composure crumbles like ancient ruins, and a torrent of curses and bitter lamentations escape his lips. He strides towards the throne, each step echoing with the weight of his shattered trust and the revelation of Fionnuala's manipulations. "Gods damn it all! I trusted her, believed in her twisted promises!" he pauses, struggling to find words. "She played us like marionettes on strings, and I... I fell for it, he says as he clenches his fists. "We were supposed to be the heroes, the damn saviors of Märchenweltgrenze!"  He shouts as he  slams his fist upon the throne's armrest, "Now look at us! Trapped, ensnared, and bound to her malevolent whims. . We're nothing but pawns in her wicked game, peoves on her cursed chessboard!" his demeanor changes. "By the gods, what have I done?" he says as his shoulders sag, "Märchenweltgrenze needed a hero, and I... I led us into her damned clutches. We're stuck in her game, and there's no way out."*


*"I need a bath... I feel soiled." he says he rises from his throne, "Sitting in my throne after Fionnuala graced it makes me feel so filthy." He pauses,"Send for a craftsmen to make me a new throne, burn this cursed chair. It reeks of betrayal and deceit.*