---
title: Interlude
date: "2023-12-10T22:40:32.169Z"
description: Where one arch ends and another begins
---


In the wake of the liberation of Sliberberg and the expansion of Bergherz, the players reap their well earned rewards. However their victory has brough unintended consequences. As Vasrock slips from focus, the spotlight shifts to the enigmatic Archfey Fionnuala, a figure whose motives are as intricate as the Feywild itself. Through a dance of diplomacy, enticements, and revelations, the players must navigate alliances and obligations, culminating in a contract that binds them to the Feywild's whims.*
The heavy roleplay elements of this adventure immerse players in the complexities of fey politics, testing not just their mettle in combat but their wits and interpersonal skills. Fionnuala's entrance as the primary villain marks the beginning of a new arch, where the consequences of choices made in Castle Sliberberg will echo through the players' journey
## Starting the adventure
Before starting the adventure have the players roleplay a bit. Use these prompts to guide the session and encourage your players to delve into the details.
1. **Describe Your New Abode:**
   - How would you characterize the architecture and design of your new castle/mansion/temple? Is it imposing and fortified, adorned with intricate details, or seamlessly blending with the natural surroundings?
   - What notable features or rooms does your abode have? Perhaps there's a grand hall, a private study, or a garden with mystical flora.

2. **Settling Process:**
   - How did the settling process unfold in the new kingdom? Were there any challenges or surprises along the way?
   - Did you encounter any peculiarities in the construction or magical nature of your abode that reflect the unique qualities of Märchenweltgrenze?

3. **Prominent Followers' Adjustment:**
   - How are your prominent followers adjusting to their new roles and surroundings? Have they taken on specific responsibilities within the kingdom?
   - Are there any personal or political conflicts among your followers that require your attention?

4. **Activities in the Last 2 Weeks:**
   - What activities or events have you participated in during the past two weeks? Have there been any noteworthy celebrations, gatherings, or challenges?
   - Have you explored any parts of the kingdom, encountered interesting NPCs, or delved into the local culture?


As the sun rises over the sprawling landscape of Märchenweltgrenze, its golden rays paint the sky in hues of pink and orange. The dawn heralds the beginning of a new day, casting a warm glow over the newly founded kingdom of Bergherz. The central valley, cradled along the shores of an inland sea, comes to life with the promise of possibilities and untold adventures. Read the following

*You stand at the heart of this burgeoning realm, where the air is filled with the crisp scent of mountain breezes and the subtle fragrance of blooming wildflowers. The towering peaks that surround Bergherz stand as silent sentinels, their snow-capped summits gleaming in the morning light.*

*Below, the valley unfolds with a tapestry of diverse landscapes—lush meadows, dense forests, and babbling streams converge to create a haven of natural beauty. The tranquil waters of the inland sea reflect the kaleidoscope of colors painted across the sky, mirroring the ever-changing moods of Märchenweltgrenze.*

*In the heart of the valley, the capital city of Sliberberg awakens. The hustle and bustle of a burgeoning kingdom come to life as citizens prepare for a new day. Market stalls open, the scent of freshly baked bread wafts through the air, and the echoes of laughter and conversation resonate through the streets.*

*As the rulers of this fledgling realm, you bear witness to the birth of a kingdom, shaped by the hands of destiny and the deeds of heroes. The air is charged with the promise of prosperity, unity, and the ever-present magic that permeates the Märchenweltgrenze. What adventures, challenges, and mysteries await in this land of enchantment? The destiny of Bergherz is in your hands.*

## The letter arrives

*As you go about your morning routine in the kingdom of Bergherz, enjoying the peace and prosperity you've worked to establish, the tranquility is shattered by the arrival of a breathless messenger. The thundering hooves of a fast horse herald the arrival as the rider, disheveled and panting, pulls up to your abode.*

*The messenger, clad in the livery of royal family, dismounts hastily and approaches you with urgency. Sweat beads on their forehead, and their breath is rapid from the long journey.*

*With a bow, the messenger hands you a sealed letter bearing the unmistakable insignia of Fredrick the Beast. The wax seal is still warm, and the urgency in the air is palpable. As you break the seal and unfold the parchment, you read Fredrick's words:*

**"Dearest Friends and Strongest Vassals,**

**The defeat of Vasrock has unearthed a dire complication. The threads of destiny weave a tangled web, and an unforeseen shadow threatens the very heart of Sliberberg. I implore you to make all due haste to Castle Sliberberg. Your strength, wisdom, and loyalty are needed once more. Time is of the essence.**

**With Hope and Desperation,**

**Fredrick the Beast"**

*As you finish reading the letter, the messenger anxiously awaits your response, shifting from foot to foot. The weight of Fredrick's words hangs in the air, and the destiny of Sliberberg once again rests upon your shoulders.*

You should follow this message with the following prompts. How does your character react to the urgent call from Fredrick the Beast? What thoughts and emotions stir within them as they read the letter? Considering the bonds forged during the liberation of Sliberberg, what actions do they take in response to this unforeseen complication? The kingdom of Bergherz may be flourishing, but the shadows of Sliberberg beckon, and the fate of a friend hangs in the balance.


## Arriving at Sliberberg
As the players approach Sliberberg read the following

*As you and your companions make your way towards Castle Sliberberg, the journey is filled with a sense of urgency and anticipation. The rolling hills and verdant landscapes of Märchenweltgrenze pass beneath you, a stark contrast to the shadows that loom over the once-conquered city. In the noon sun, as you crest a rise in the terrain, the imposing silhouette of Castle Sliberberg comes into view.*

*However, it's not just the castle that captures your attention. Hovering near the towers and spires are three airships, an awe-inspiring sight against the backdrop of the sky. One of them, an extravagant floating palace, takes the form of a majestic galleon. Its ornate details gleam in the sunlight, and the air around it seems to hum with otherworldly energy.*

*These airships are draped in the distinctive livery of the Archfey Fionnuala, a name familiar to you from the days of the goblin occupation. Fionnuala, known for maintaining a diplomatic presence in Sliberberg even during those dark times, now arrives with an apparent entourage. The vibrant colors of her banner flutter in the breeze, creating a stark contrast to the scars of the recent conflict.*

The big airship is the flagship of Archfey Fionnuala and her personal residence away from her castle. Prompt the players with the following. As you approach Castle Sliberberg and witness the trio of airships, what thoughts and emotions stir within your character? How does the sight of Fionnuala's floating palace resonate with the memories of the goblin occupation and her diplomatic role in Sliberberg? Consider your character's past interactions with Fionnuala and how they may feel about her involvement in the current situation. What questions or concerns does this unexpected alliance raise as you prepare to reunite with Fredrick the Beast and face the unforeseen complication?

## Meeting Fionnuala
Read the following
*As you step into the grandeur of the Castle Sliberberg's great hall, a mixture of anticipation and curiosity fills the air. The palace staff, attentive and efficient, usher you to the focal point of the room—the long, opulent dining table. Seated around it are Fredrick the Beast, regal and composed, and any players who have accepted his offer to become queen consorts.*

*However, your attention is drawn to a beautiful woman with elven features, elegantly adorned in Elizabethan costume. Her presence radiates an otherworldly grace, and she sits at the head of the table, occupying what should rightfully be King Fredrick's chair. The great hall, usually echoing with the sounds of merriment and celebration, now carries a weight of formality.*
*"Esteemed guests, I am Fionnuala, Archfey of the Feywild, and I bid you welcome to Castle Sliberberg. I extend my gratitude for your swift response to Fredrick's call. Today, we gather not just as allies but as architects of destiny. Please, take your place at this table, where plans are woven, and the fate of realms is decided."*
As you approach the table and take your seat in the presence of Fredrick the Beast and the enigmatic Fionnuala, what thoughts and emotions stir within your character? How does the sight of Fionnuala in King Fredrick's chair affect their perception of the current situation? Consider the dynamics at play, the luxurious lunch laid before you, and the undercurrents of power and diplomacy in the air. What questions or concerns does your character harbor as they navigate this unexpected and formal gathering in the heart of Sliberberg?
Fionnuala, the enigmatic Archfey, engages in small talk with the assembled guests, her elven features adorned with an inscrutable smile. As she delicately plucks morsels from the lavish lunch, she weaves a tapestry of casual conversation, all the while keenly observing the players' reactions. Her elegant words carry an air of amusement as she effortlessly navigates the intricate dance of courtly banter. Fionnuala subtly sprinkles personal details about each player into the dialogue, relishing the subtle shifts in demeanor and the intrigue that blossoms as the players attempt to decipher her intentions. Her eyes sparkle with mischief, and the air is charged with an otherworldly charm, making it clear that every word and gesture is a calculated step in a dance of both diplomacy and amusement. After watching the players squirm for a bit read the following

*As Fionnuala concludes the small talk, her smile takes on a subtle hint of wickedness. She leans forward, her eyes gleaming with an otherworldly intensity. The air in the great hall seems to thicken as she unveils a revelation that casts a shadow over the opulent surroundings.*

*"My dear guests, the intricate dance of destiny has led us to this moment. I hold a contract, inked in the ethereal threads of the Feywild, between myself and Vasrock. In exchange for rulership over the central valley and the southern Märchenweltgrenze, he swore everlasting fealty to me and pledged to raise an army in my name. With Vasrock's demise, however, the threads of that contract now entwine with your fate."*

*Fionnuala's gaze pierces through the room as she continues,"Vasrock's outstanding debt, by his demise now falls upon you, inheritors of his oaths. The first installment of this debt is simple yet profound—your oaths of everlasting fealty. You stand at the crossroads of fate, and the choice is yours. Will you fulfill the terms of this contract, or shall the consequences unfold in ways beyond your reckoning? The Feywild echoes with the weight of this choice, and the dance of destiny awaits your response."*

*As Fionnuala addresses the players and senses the tension in the air, she continues to hold court with an air of regality. Regardless of the players' reactions, guide Fionnuala's responses with the following considerations:*

1. **Magic Enforced Contract:**
   - Make it clear that the contract binding the players to Vasrock's oaths is magically enforced and carries severe consequences for any breach. Fionnuala's words should convey the inevitability and gravity of the situation.

2. **Penalties for Breaking the Contract:**
   - Describe the potential penalties associated with breaking the contract. These penalties should be extreme and underscore the magical nature of the binding. Players should understand that attempting to defy the contract could result in dire consequences.

3. **Fionnuala's Displeasure:**
   - Express Fionnuala's genuine displeasure with the turn of events. She should convey that while she finds herself bound by magical obligations, she views the players as a more acceptable alternative to Vasrock. Highlight her distaste for Vasrock's methods and emphasize that she sees potential in the player characters.

4. **Open Dialogue:**
   - Encourage the players to engage in an open dialogue with Fionnuala. Allow them to express their concerns, objections, or seek clarifications about the terms of the contract. Fionnuala can respond with a mix of diplomacy, veiled warnings, and perhaps moments of sincerity.

5. **Alternative Arrangements:**
   - While emphasizing the binding nature of the contract, Fionnuala might hint at potential ways the players could fulfill their obligations without compromising their values entirely. This could involve specific tasks, quests, or agreements that align more closely with the players' moral compass.

6. **Mysterious Intentions:**
   - Maintain an air of mystery surrounding Fionnuala's ultimate intentions. Leave room for players to speculate and question the true nature of the Feywild's influence on this situation. Fionnuala can reveal details sparingly, keeping the players intrigued and uncertain about the future.
   ## Fionnuala deals with Fredrick



   ## Fionnuala deals with the players

    Fionnuala will turn her attention towards the players and try to ensnare them if further contracts. As Fionnuala extends her offer to the player characters, guide the encounter with the following considerations:

1. **Knowledge of Backstories:**
   - Fionnuala possesses intricate knowledge of the player characters' backstories and personal goals. She will use this information to tailor her enticements, emphasizing how her assistance can directly address their individual desires and challenges.

2. **Veiled Promises:**
   - Encourage Fionnuala to make veiled promises of assistance without divulging the full extent of the potential consequences. This keeps an air of mystery and ensures the players weigh their choices carefully.

3. **Limited Wishes:**
   - Highlight Fionnuala's ability to grant a limited number of wishes per day. This serves as a powerful incentive for the players to consider her offer seriously, as the fulfillment of wishes is a rare and coveted ability.

4. **Agents in the Feywild:**
   - Describe Fionnuala's vast network of agents in the Feywild, emphasizing that her reach extends far beyond the mortal realm. This adds a layer of intrigue and suggests the players are dealing with a figure of great influence.

5. **Tailoring Offers to Goals:**
   - Fionnuala should tailor her offers to align with the personal goals and aspirations of each player character. Whether it's the resolution of a personal vendetta, the attainment of knowledge, or the fulfillment of a long-held dream, Fionnuala's enticements should address these desires.

6. **Consequences of Acceptance:**
   - Remind players that accepting Fionnuala's offers may come with unforeseen consequences. While the immediate benefits are enticing, the Feywild is known for its capricious nature, and bargains made with archfey often have repercussions that echo across time.

7. **Individual Player Interactions:**
   - Allow each player character to engage with Fionnuala individually. This provides a moment for personal roleplay, allowing players to express their characters' thoughts, reservations, and desires in response to Fionnuala's propositions.

8. **Influence on the Plot:**
   - Consider how the players' choices will influence the overall plot and the future direction of the campaign. Fionnuala's enticements may introduce new narrative threads or alter existing ones based on the players' decisions.

## Concluding the negotiations  
Read the following after Fionnuala concludes trying to tempt the players into further deals

*As Fionnuala concludes her enticements and the players contemplate their decisions, she shifts the tone of the conversation. Her demeanor transforms into that of a stern yet patient authority figure addressing young children. She begins to lay out ground rules for their newly formed relationship.*

*"Now, my dear vassals," Fionnuala's voice takes on a measured tone, akin to a parent guiding wayward children. "There are a few simple rules to abide by in this newfound alliance. First and foremost, you are not to interact with any other members of the Summer Court. It has come to my attention that you may have sought counsel from two individuals – Titania's old nursemaid and that waif of a girl apprenticed to the court pastry chef. I'll have none of that. Your allegiance is to me alone."*

*Her eyes pierce through the air as she continues,"Secondly, I must insist that you refrain from sabotaging, whether knowingly or unknowingly, any of my ongoing schemes. Cooperation is key, and I expect you to play your part without unintended interference. After all, a harmonious dance is much more pleasant than a discordant cacophony, wouldn't you agree?"*

*As Fredrick absorbs these directives, Fionnuala introduces another surprising element, "Lastly, I anticipate that you will play well with my other vassal in the region, Gisela the Archfey. Ah, the revelation surprises you, Fredrick? Of course, dear Fredrick, Gisela has been in my service for 500 years. I bestowed upon her the ritual that tore open the veil between the valley and the Feywild. It's only fitting that you collaborate, for the threads of fate have intricately woven your destinies together."*

*Fionnuala's smile, though serene, carries an undercurrent of authority. The ground rules are set, and the players find themselves navigating a complex dance of allegiances and obligations in the Feywild's intricate tapestry.*

With her ground rules firmly established, Fionnuala rises gracefully from her chair, the intricate folds of her elizabethan gown cascading around her. She bids the king and the players to accompany her to the battlements. As they ascend, the grandeur of Castle Sliberberg unfolds before them. The cool breeze carries a hint of enchantment, setting the stage for the departure.

*As Fionnuala concludes her business in Castle Sliberberg, she gracefully requests to be escorted to her palatial airship. The players witness the departure with a mix of relief and curiosity. Fionnuala, her ethereal presence illuminated by the ambient glow of the Feywild, turns to face the group.*

*"I shall be in touch with you and dear Fredrick soon. Until then, I entrust the court to your care."*

*With a flourish, she boards the airship, a floating palace adorned with intricate details. The vessel lifts off gracefully, hovering above the castle, and Fionnuala, now silhouetted against the twilight sky, bids her farewell.*

*"And fear not, Fredrick, I have dispatched a new ambassador to your court. Hopefully, they can bring a touch of elegance to this place."*

*Fredrick chuckles, "We are still in the course of redecorating, my lady."*

*With a final wave, Fionnuala disappears into the celestial expanse aboard her airship. The focus now shifts back to the player characters. As the echoes of the airship's departure fade, the grandeur of Castle Sliberberg surrounds them, presenting an opportunity for discussion and decision-making.*

**Encouragement for the Players:**
As the airship dwindles in the distance, the vast halls of Castle Sliberberg lay open before you. The lingering presence of Fionnuala's influence and the pending arrival of a new ambassador present intriguing possibilities. What will you make of this moment? The fate of Sliberberg is in your hands, and the air is thick with potential. Engage with your fellow characters, share your thoughts, and consider the paths that lie ahead. The Feywild's influence has left its mark—how will you navigate the intricacies of the future?

# The Second arch guide

In the expansive and player-driven second arc of our campaign, the stage is set for a sandbox adventure, where the narrative is shaped by the characters' choices, ambitions, and the consequences of their interactions in the world of Märchenweltgrenze. The players find themselves presented with three overarching goals, each weaving into the intricate tapestry of their journey:

### 1. **Find a Way Out of the Contract:**
   - The players are bound by a magical contract forged in the Feywild, tying them to the whims of the enigmatic Archfey Fionnuala. To break free from these mystical chains, they must embark on a quest for knowledge, seeking ancient tomes, wise sages, or forgotten rituals scattered across the realms. The journey may take them through haunted libraries, hidden sanctuaries, or encounters with beings with knowledge of ancient fey pacts.

### 2. **Build the Army for Fionnuala:**
   - Fionnuala's demand for an army requires the players to navigate the complex landscape of recruitment, diplomacy, and conquest. They may forge alliances with neighboring factions, delve into ancient ruins to uncover forgotten warforged constructs, or navigate the political intricacies of fey courts to enlist powerful allies. The building of this army could involve a series of mini-quests, each contributing to the growth and strength of their forces.

### 3. **Expand the Kingdom:**
   - To support their military endeavors and create a power base capable of challenging Fionnuala's influence, the players must expand their kingdom. This involves settling new territories, establishing trade routes, and dealing with the myriad challenges that come with governance. The expansion could lead to the exploration of uncharted lands, negotiation with rival factions, or the unearthing of ancient artifacts that hold the key to unlocking the kingdom's full potential.

### Goal 1: Finding a Way Out of the Contract

#### **A. Divine Intervention and the Wish Spell:**
   - Breaking the magical bonds of Fionnuala's contract is no small feat. The players learn that only divine intervention or the potent Wish spell can sever these mystical ties. However, these solutions come with a significant caveat – each act only breaks one character's contractual bonds.

#### **B. Researching Fey Contracts:**
   - The players delve into the ancient tomes, libraries, and mystical scholars of Märchenweltgrenze to unravel the secrets of fey contracts. This involves quests to recover lost texts, decipher cryptic glyphs, and consult with reclusive sages who hold the key to understanding the intricate nature of such pacts.

#### **C. Complicated Ties to Important NPCs:**
   - Fionnuala's influence extends beyond the player characters; she has forged contracts with significant NPCs in their lives. For instance, Talia, the newborn dragon the players have adopted, becomes a pawn in this fey game. The players discover that Fionnuala, in the guise of a fairy, approached Talia and convinced her to make a wish to become the adopted daughter of one of the players. This wish transforms Talia into a 3-month-old infant of the same race as her adoptive mother.

#### **D. The Clock is Ticking:**
   - As the players delve into the research and seek to break the contracts, the shadow of time looms overhead. Fionnuala's has a master plan and has given the players a time limit on when she comes to collect the debt.

#### **E. Divine Guidance:**
   - Seeking guidance from deities or powerful celestial entities becomes a central element of the quest. Temples, sacred groves, and ancient shrines may hold the key to connecting with beings who can offer insight, guidance, and perhaps a divine intervention to sever the fey bonds.

#### **F. Confrontations and Revelations:**
   - Along the way, the players may encounter individuals who have tangled with Fionnuala in the past, surviving or succumbing to her whims. These encounters provide valuable insights, warnings, and potential alliances, contributing to the unfolding narrative.

#### **I. Player-Driven Solutions:**
   - Encourage players to develop and explore their own solutions to the conundrum. Whether through alliances with powerful entities, forging new pacts, or uncovering ancient rituals, the journey to break the fey contracts should be as diverse and unique as the characters themselves.

Potential Adventure ideas

### 1. **The Lost Library of Sylvan Wisdom:**
   - The players learn of an ancient library hidden deep within the Feywild, known for its wealth of knowledge on fey contracts. To access this library, they must navigate through enchanted forests, overcome trials set by elusive guardians, and decipher cryptic clues to unveil the library's location.

### 2. **The Oracle of Whispers:**
   - Legends speak of an oracle with the ability to unveil hidden truths in the Feywild. The players embark on a pilgrimage to find this enigmatic seer, braving treacherous landscapes and solving riddles to gain an audience. The oracle may provide cryptic insights, guiding the players toward the means to break their contracts.

### 3. **Dreams of the Eldertree:**
   - An Eldertree in the heart of the Feywild is said to hold the collective dreams of the fey. The players enter a dreamscape, encountering symbolic representations of their contracts and allies. Navigating this surreal realm, they must confront their deepest fears and desires to unlock the wisdom hidden within the Eldertree.

### 4. **A Pact with the Patron:**
   - A powerful archfey patron offers assistance in breaking the contracts but requires the players to enter into a new pact with them. This pact may involve fulfilling a series of quests on behalf of the patron, establishing a shrine in their honor, or forging an alliance with another fey power.

### 10. **Temporal Anomalies:**
   - The players discover temporal anomalies scattered across Märchenweltgrenze. These anomalies provide glimpses into alternate timelines where their contracts were never formed. Navigating these anomalies allows the players to gather insights, clues, and perhaps even artifacts that can alter the course of their destinies.
### Goal 2: Build the Army for Fionnuala

#### **A. Kingdom Expansion:**
   - Bergherz, nestled in the mountains, faces the challenge of limited space for army recruitment. To fulfill Fionnuala's demand, the players must explore options for kingdom expansion. This involves venturing into uncharted territories, negotiating with neighboring factions for land, and overcoming the hurdles of settling in diverse environments.

#### **B. Mercenary Recruitment:**
   - The players, having liberated Sliberberg, already command the loyalty of several mercenary companies. Expanding this force requires recruiting more mercenaries from distant lands. The players embark on quests to seek out renowned mercenaries, proving their worth and convincing these formidable warriors to join their cause.

#### **C. Forging Alliances Without Troops:**
   - While traditional alliances are not an option, the players can still seek political support and resources from neighboring regions. Diplomacy becomes a key aspect of their strategy as they negotiate trade agreements, secure access to valuable resources, and build a network of political allies who indirectly contribute to their military efforts.

#### **D. Quest for Unconventional Forces:**
   - To bolster their ranks, the players delve into the lore of Märchenweltgrenze to discover unconventional and mythical forces. This could involve recruiting enchanted beings, awakening dormant constructs, or even forging pacts with ancient spirits who lend their supernatural abilities to the growing army.

#### **E. Subjugating Hostile Factions:**
   - As an alternative to alliances, the players may choose a more aggressive approach by subjugating hostile factions that resist expansion. Conquering neighboring territories requires careful planning, strategic warfare, and managing the aftermath of such conquests to maintain control over the newly acquired lands.

#### **F. Magical Augmentation:**
   - Seeking the assistance of skilled mages and enchanters, the players explore magical means to augment their army. This could involve imbuing soldiers with enhanced strength, summoning magical constructs, or tapping into ley lines to infuse the troops with arcane energies that make them more formidable on the battlefield.

#### **G. Recruitment Challenges:**
   - Building an army is not without its challenges. The players must navigate recruitment challenges, such as addressing dissent among the ranks, overcoming cultural differences between mercenaries, and managing the logistics of a diverse force. Each challenge provides an opportunity for the players to showcase their leadership skills.

#### **H. The Price of Power:**
   - As the players amass power and expand their kingdom, they must grapple with the moral and ethical consequences of their actions. Choices made in the pursuit of military strength may impact the perception of Bergherz in the eyes of neighboring realms, potentially leading to strained relationships or outright hostilities.

#### **I. Temporal Constraints:**
   - Fionnuala's deadline of eight years adds an element of urgency to the players' endeavors. They must balance the need for swift expansion with the careful management of resources and alliances, making strategic decisions that maximize the growth of their army within the given timeframe.

#### **J. Managing Resources:**
   - Expanding the kingdom requires the efficient management of resources. The players must allocate funds for recruitment, infrastructure development, and maintaining the loyalty of their growing army. Balancing these aspects becomes crucial for sustaining the military campaign.

### Adventure Ideas for Building the Army

#### 1. **The Uncharted Frontier:**
   - The players venture into uncharted territories surrounding Bergherz to explore potential lands for expansion. They face the challenges of navigating through dense forests, treacherous mountains, and unexplored wilderness. The quest involves mapping the region, dealing with local wildlife, and establishing the groundwork for future settlements.

#### 2. **The Mercenary Conclave:**
   - A gathering of renowned mercenary companies takes place in a neutral territory. The players must attend and participate in competitions, negotiations, and trials to earn the trust and loyalty of these skilled warriors. Success in these challenges ensures the recruitment of elite mercenaries for their growing army.

#### 3. **Diplomatic Maneuvers:**
   - In the absence of traditional alliances, the players engage in diplomatic missions to secure political support from neighboring regions. They attend diplomatic summits, negotiate trade agreements, and navigate the delicate dance of diplomacy to gain access to resources, funds, and indirect support for their military endeavors.

#### 4. **The Enchanted Legion:**
   - Legends speak of an ancient order of enchanted beings residing in a hidden realm. The players embark on a quest to locate this realm, overcoming magical barriers and solving puzzles to gain the allegiance of these mythical beings. The enchanted legion brings unique magical abilities to the growing army.

#### 5. **Subjugation Campaign:**
   - Faced with the resistance of rival factions, the players strategize a military campaign to subjugate hostile territories. They must plan sieges, engage in guerrilla warfare, and manage the aftermath of conquests to maintain control over the newly acquired lands. The choices made in this campaign influence the loyalty of the conquered factions.

#### 6. **Magical Augmentation Trials:**
   - Seeking the assistance of renowned mages, the players organize a series of trials to identify candidates for magical augmentation within their army. The trials involve testing candidates for resilience, adaptability to magical enhancements, and ethical considerations. Successful candidates undergo magical transformations, becoming a formidable force on the battlefield.

#### 7. **Cultural Exchange:**
   - To manage the diversity among their troops, the players organize a cultural exchange program. This involves fostering understanding and camaraderie among soldiers from different backgrounds. The players must navigate cultural differences, address potential conflicts, and promote unity within their growing army.
#### 8. **Resource Scarcity Crisis:**
   - A crisis unfolds as resources become scarce due to rapid expansion. The players must manage resource allocation, make difficult decisions on prioritizing needs, and navigate the repercussions of shortages within their kingdom. This crisis tests their leadership skills and forces them to seek creative solutions to sustain their military campaign.

#### 9. **Political Intrigues and Alliances:**
   - Political intrigue unfolds as the players navigate the delicate balance of alliances among neighboring regions. They attend political events, decipher hidden agendas, and engage in espionage to gather intelligence. The choices made in these political maneuvers influence the support or opposition they face in their quest to build Fionnuala's army.

### Goal 3: Expand the Kingdom and Form a Domain of Delight

#### **A. Fredrick's Ascension:**
   - As Fredrick the Beast approaches his transformation into an archfey, the players seize the opportunity to expand his influence. His imminent ascension grants him the power to form a Domain of Delight, a realm shaped by his ideals and charisma. The players must guide Fredrick in utilizing this power to its fullest potential.

#### **B. Settler Recruitment:**
   - The players embark on a recruitment campaign to attract settlers from neighboring regions and distant lands. They organize events, establish recruitment centers, and showcase the benefits of living in the Domain of Delight. The recent goblin invasion, coupled with Fredrick's heroic reputation, serves as a compelling motivation for settlers to join.

#### **C. Diplomatic Endeavors:**
   - Diplomacy becomes a key tool in expanding the kingdom. The players engage in negotiations with neighboring factions, forging alliances, and securing non-aggression pacts. The promise of shared prosperity within the Domain of Delight encourages diplomatic cooperation, fostering a network of friendly relations.

#### **D. Enchanted Lands Exploration:**
   - The players explore enchanted lands within Märchenweltgrenze, seeking to incorporate these mystical territories into Fredrick's Domain of Delight. These lands may contain magical resources, hidden realms, or mythical creatures that can contribute to the prosperity and enchantment of the expanding kingdom.

#### **E. Goblin Reintegration Program:**
   - In the aftermath of the goblin invasion, the players initiate a program to reintegrate goblin army into the expanding kingdom. This involves building trust, addressing past conflicts, and showcasing the benefits of peaceful coexistence. The goblins can contribute unique skills and perspectives to the growing society.

#### **F. Magical Infrastructure Development:**
   - As settlers join the kingdom, the players focus on developing magical infrastructure that aligns with the archfey nature of the Domain of Delight. This includes enchanting roads, creating ley line connections, and establishing mystical landmarks that enhance the overall magical aura of the expanding realm.

#### **G. Festivals and Celebrations:**
   - To solidify the sense of community and attract more settlers, the players organize vibrant festivals and celebrations. These events showcase the joy and enchantment within the Domain of Delight, becoming opportunities to highlight the kingdom's cultural richness and draw in new residents.


### Adventure Ideas for Expanding the Kingdom and Forming the Domain of Delight

#### 1. **The Feytown Diplomacy:**
   - The players embark on a diplomatic mission to establish relations with independent fey towns scattered throughout Märchenweltgrenze. Each town presents unique challenges, requiring the players to understand fey customs, negotiate with whimsical leaders, and solve local disputes. Successful diplomacy leads to the integration of these fey communities into the expanding Domain of Delight.

#### 2. **Claiming Uncharted Territories:**
   - Unclaimed lands with untapped magical resources beckon the players. They lead expeditions into these uncharted territories, facing challenges from mythical creatures, solving magical puzzles, and establishing outposts to claim and integrate these lands into the Domain of Delight.
#### 4. **Mystical Resource Expedition:**
   - A quest unfolds to discover and harness mystical resources from the heart of Märchenweltgrenze. The players lead an expedition, facing challenges posed by magical creatures guarding these resources. The acquired mystical essence becomes a valuable asset, enhancing the magical infrastructure and prosperity of the expanding Domain of Delight.

#### 5. **Port Eudoxia: A Tale of Two Empires:**
   - With the fall of the Empire of Port Eudoxia, the players find themselves in a geopolitical puzzle. They navigate the shifting power dynamics between the ascendant empire and neighboring regions, making strategic decisions that influence the fate of Port Eudoxia. The players may choose to establish diplomatic ties, resist imperial influence, or explore covert operations to protect the interests of the Domain of Delight.

#### 6. **Enchanted Borderlands Exploration:**
   - The players delve into the enchanted borderlands, a region teeming with magical anomalies and elusive creatures. They must navigate through shifting realities, solve magical puzzles, and make alliances with beings residing on the border between the mortal realm and the Feywild. Successfully integrating these borderlands into the expanding kingdom enhances the Domain of Delight's magical influence.

### The wildcards

Now, let's unveil the intricate roles of several wildcards in this unfolding drama. As the players delve deeper into the complex web of fey politics and alliances, the veils surrounding certain enigmatic figures begin to lift. 

**Agatha Ironraisin and Tansy Fleetfoot:**
   - The seemingly ordinary figures of Agatha Ironraisin and Tansy Fleetfoot, who aided the players in their triumph over Vasrock, are revealed to be more than just witches. They are, in fact, minor archfey, minors members of the summer court. Their mastery over the magical arts and intimate knowledge of the Feywild make them invaluable allies for the players. However, Fionnuala's watchful gaze poses a significant obstacle, as she monitors the players  every move with a hawk-like intensity. Unbeknownst to the players, Maeve, Fionnuala's eldest daughter and shrewd ambassador, infiltrates their midst, seeking to manipulate events to her mother's advantage. The challenge lies in navigating the delicate dance with these cunning archfey while keeping the ever-watchful Fionnuala at bay.

**Maeve, the Ambitious Daughter:**
   - Maeve, possessing the same devious cunning as her mother Fionnuala, stands as the ambassador sent by the Summer Court. Her ambitions extend beyond diplomatic duties, harboring a desire to seduce Fredrick and ascend to the role of queen. Her loyalty to Fionnuala is unwavering, presenting a formidable challenge for the players. However, Maeve is not a mere pawn in her mother's game; she is a woman with her own desires and ambitions. Through clever negotiation and persuasion, the players may find opportunities to sway Maeve to their cause, turning an adversary into a potential ally. The delicate balance between loyalty and personal ambition within the Summer Court adds a layer of complexity to the unfolding narrative.

**Gisela, the Fey Enchantress:**
   - Gisela, ruler of the Hexmarches to the north of Bergherz, harbors a deep-seated animosity towards Fredrick, rooted in an ancient grudge involving a broken blood oath with Edward, Fredrick's father. Her intent is clear — the destruction of Fredrick, Bergherz, and the players. Gisela's formidable magical prowess and influence over the Hexmarches make her a formidable adversary. The players must navigate the treacherous waters of this longstanding feud, exploring avenues to either reconcile with Gisela or thwart her vengeful plans. The Hexmarches, fraught with enchantments and hidden dangers, become a battleground for both political maneuvering and magical confrontations as the players contend with Gisela's relentless pursuit of retribution.


### Tips for the Dungeon Master (DM):

   - **Player-Driven Narrative:** Embrace the sandbox nature of this arc, allowing players to take the lead in pursuing their goals. Encourage them to explore the world, interact with NPCs, and shape the narrative through their decisions.
   
   - **Dynamic NPCs and Factions:** Populate the world with vibrant and dynamic NPCs and factions. These entities can serve as allies, rivals, or potential threats, adding depth and complexity to the players' interactions.

   - **Consequences of Choices:** Ensure that the players' decisions have tangible consequences. The alliances they form, territories they conquer, and deals they make should impact the world around them, creating a living, evolving narrative.

   - **Discoverable Lore:** Scatter lore and hints about breaking magical contracts throughout the campaign world. This encourages exploration and research as players seek to unravel the mysteries of their predicament.

   - **Event Triggers:** Introduce events that trigger based on the players' progress, adding a dynamic element to the sandbox. These events could include political shifts, rival factions making moves, or ancient prophecies unfolding.

   - **Personal Quests:** Weave in personal quests for each player that tie into the overarching goals. This not only adds depth to individual character stories but also enhances the overall narrative.

   - **Flexible Story Structure:** Embrace the fluidity of the narrative structure. Be prepared to adapt to the players' choices and actions, allowing the story to evolve organically based on their agency.

   - **Balance of Challenges:** Strike a balance between challenging encounters, diplomatic negotiations, and exploration. This ensures a diverse and engaging experience for the players.

*In this expansive and uncharted territory, the players are the architects of their destiny. As they navigate the complexities of the fey-infused realm, the choices they make will reverberate through the campaign, shaping the future of Märchenweltgrenze.*