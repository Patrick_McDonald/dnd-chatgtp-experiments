---
title: "fey trinkets and treasures, magic item from the Märchenweltgrenze"
date: "2023-12-08"
description: "Unique magic items inspired by the feywild and celtic folklore."
slug: on-goblinoids-of-the-feywild
categories:
  - Dungeons & Dragons
  - Feywild
  - Goblinoids
---
### Mirrors of Communion

*Wondrous Item, Rare*

The Mirrors of Communion are enchanted mirrors that enable two-way face-to-face communication between individuals who possess any mirror of the set. Crafted by skilled artificers and infused with magical energies, these mirrors serve as a means of instant and secure communication.

**Properties:**
- **Universal Communication:** Each mirror of the Mirrors of Communion is magically attuned to all other mirrors of its kind. This universal connection allows seamless communication with any individual holding another mirror of the set.

- **Two-Way Vision:** When communication is initiated, the mirrors come to life with a soft, ethereal glow. The individuals holding the mirrors can see and hear each other as if they were in the same room, fostering a sense of direct and personal interaction.

- **Distanceless Connection:** The Mirrors of Communion transcend physical distance, allowing communication regardless of how far apart the individuals are located. This makes them invaluable for maintaining contact over long distances or even between different planes of existence.

- **Secure Communication:** The communication facilitated by the Mirrors of Communion is magically secure. Attempts to eavesdrop or intercept the communication are thwarted by the enchantments on the mirrors, ensuring a private and confidential exchange.

- **Mirror Attunement:** To establish a connection, each mirror must be attuned to the individual intending to communicate. Attunement involves a simple ritual during which the user speaks their name into the mirror. Once attuned, the mirror remains connected to all other mirrors of the set.

- **Limitations:** While the Mirrors of Communion offer convenient communication, they are not omniscient. The wielders must have prior knowledge of each other's existence, and the mirrors cannot connect to individuals without their consent.

Certainly! Here are some unique magic items with a fey and fairytale theme:

### 1. **Whispering Willow Wand**
*Wand, Rare*

A slender wand made from the wood of an ancient willow tree. When waved, it emits soft whispers that only the wielder can hear. The wand can cast *Message* at will and has a special feature: once per day, it can summon the spirit of the Whispering Willow, providing guidance or secrets known only to the fey.

### 2. **Cheshire's Grinning Grimoire**
*Book, Very Rare*

A magical grimoire with a grinning Cheshire Cat on the cover. When opened, the pages reveal themselves only to those the cat deems worthy. The grimoire can cast *Detect Thoughts* and allows the user to speak telepathically with others who touch the book, as if the Cheshire Cat itself were whispering secrets.

### 3. **Moonlit Cloak of Elusion**
*Cloak, Uncommon*

This silvery cloak seems to absorb moonlight. When worn under the light of a full moon, it grants the wearer advantage on Stealth checks and allows them to cast *Invisibility* once per night. Additionally, the cloak has the power to reveal hidden truths when touched to a surface under moonlight, briefly illuminating invisible or concealed objects.

### 4. **Feyfire Lantern**
*Wondrous Item, Rare*

A lantern crafted from enchanted feywood. When lit, it doesn't emit normal light but instead projects illusions of dancing fairies and magical creatures. The lantern's light reveals hidden passages and secrets within the Feywild, acting as a guide through the enchanting realm. It can cast *Faerie Fire* once per day.

### 5. **Cinderella's Slipper**
*Wondrous Item, Legendary*

A delicate glass slipper that grants the wearer unparalleled grace and agility. When worn, the slipper allows the user to cast *Haste* on themselves once per day. If the wearer loses the slipper, it will magically return to them by the next dawn, teleporting into their possession.

### 6. **Enchanted Rose Amulet**
*Amulet, Very Rare*

An amulet featuring a preserved enchanted rose. The amulet grants advantage on Charisma-based checks when interacting with fey creatures and can cast *Dominate Person* once per day on fey beings. However, the amulet comes with the risk of the enchantment slowly fading; if the last petal of the rose falls, the amulet loses its magical properties.

### 7. **Jack's Magical Beanstalk Seeds**
*Wondrous Item, Rare*

A small pouch containing enchanted beans. When planted and watered, they rapidly grow into a massive beanstalk, reaching a magical realm in the sky. The adventurer can climb the beanstalk, and at the top, they find a hidden treasure or encounter a celestial guardian. The beans can only be used once, and the pouch regains its magical properties after a week.

### Feyweave Cloak
*Wondrous Item, Uncommon (Cloak)*

This ethereal cloak is woven from the threads of the Feywild itself. When worn, it grants advantage on Dexterity (Stealth) checks and allows the wearer to cast *Misty Step* once per day. Additionally, the cloak has the ability to briefly blend the wearer into the Feywild, granting advantage on saving throws against being charmed.

### Sylvan Blade
*Weapon (Longsword), Rare*

The Sylvan Blade is an elven longsword with a blade that shimmers with the essence of the forest. It deals an additional 1d6 slashing damage against creatures with a connection to the Feywild, such as fey creatures or those touched by fey magic. The blade can also cast *Entangle* once per day.

### Nymph's Embrace
*Armor (Studded Leather), Very Rare*

This finely crafted studded leather armor is adorned with delicate fey motifs. When worn, the Nymph's Embrace grants the wearer advantage on saving throws against spells and effects that would charm or enchant them. The armor also allows the wearer to cast *Disguise Self* at will, appearing as a beautiful fey creature.

### Faun's Melody
*Weapon (Shortbow), Rare*

This shortbow is carved from the wood of an ancient and magical tree in the Feywild. Arrows fired from Faun's Melody create a mesmerizing melody as they fly, distracting and disorienting enemies. On a critical hit, the target must make a Wisdom saving throw or be charmed for one round. The shortbow can also cast *Sleep* once per day.

### Satyr's Rhythm
*Weapon (Rapier), Very Rare*

This rapier is adorned with silver filigree and produces enchanting music with each strike. Satyr's Rhythm grants the wielder advantage on Charisma (Performance) checks and allows them to cast *Suggestion* once per day. Additionally, when the wielder successfully lands a critical hit, the rapier can create an illusory duplicate of the wielder, distracting foes and granting advantage on the next attack.

### Dryad's Whisper
*Armor (Leather), Rare*

This light leather armor is adorned with leaves and vines, offering exceptional protection while allowing for unparalleled mobility. Dryad's Whisper grants the wearer advantage on Dexterity saving throws and allows them to cast *Barkskin* once per day. The armor also has the ability to create an illusory duplicate of the wearer as a reaction, providing temporary cover.
Certainly! Drawing inspiration from Irish Fey mythology, including leprechauns and their pots of gold, here are a few magic items and weapons:

### 1. **Leprechaun's Lucky Charm**
*Wondrous Item, Rare*

This small, golden charm is shaped like a four-leaf clover and is said to be a gift from mischievous leprechauns. When worn, the Lucky Charm grants the wearer advantage on all saving throws against being charmed or frightened. Additionally, once per day, the charm can be rubbed for luck, allowing the user to reroll any one attack roll, saving throw, or ability check.

### 2. **Cloak of the Sidhe**
*Wondrous Item, Very Rare (Cloak)*

Woven from the threads of the Feywild by the Sidhe themselves, this enchanting cloak allows the wearer to step briefly into the ethereal realm. Once per day, the wearer can use a bonus action to become invisible until the start of their next turn. While invisible, they have advantage on Stealth checks.

### 3. **Dagda's Hammer**
*Weapon (Warhammer), Very Rare*

Crafted in the forges of the god Dagda, this warhammer is imbued with the strength of the earth and the power of the Feywild. When wielded, the hammer deals an additional 2d6 radiant damage on a successful hit. Once per day, the wielder can slam the hammer on the ground, causing a shockwave that knocks prone all creatures within 10 feet.

### 4. **Feyblood Dagger**
*Weapon (Dagger), Uncommon*

Forged from rare metals found only in the Feywild, this dagger is said to be infused with the essence of the Sidhe. When the dagger is used to make an attack, it deals an additional 1d4 psychic damage. Additionally, if the wielder speaks the command word, the dagger can cast *Sleep* once per day.

### 5. **Leprechaun's Pot of Illusions**
*Wondrous Item, Rare*

This seemingly ordinary pot, when filled with water, produces illusory gold coins. The illusory gold, while not real, can be used to distract or deceive others. Once per day, the pot can cast *Major Image* to create an illusory scene or object that fits within a 20-foot cube.

### 6. **Fomorian's Gaze**
*Weapon (Longbow), Rare*

This longbow is crafted from the wood of the Fomorian trees in the Feywild. Arrows fired from Fomorian's Gaze have the ability to see through illusions. Once per day, the wielder can use the longbow to cast *True Seeing* for one minute.

You're absolutely correct, and I appreciate the clarification. The Gáe Bulg is indeed often depicted as a javelin, and its most famous use in the myth involves Cú Chulainn throwing it at his friend turned rival, Láeg. In that instance, Cú Chulainn throws the Gáe Bulg with his foot, and it pierces Láeg's heart.

Let's adjust the artifact to better align with this aspect of the myth:

### Gáe Draíochta - Javelin of Unyielding Force

*Artifact (Javelin), Legendary (requires attunement)*

**Description:**
The Gáe Draíochta is an ancient javelin infused with the magic of the druids, said to be forged in the heart of the Feywild. Its shaft is made from the sacred wood of the Silver Birch, and its tip is crafted from the tooth of a mythical serpent. The javelin radiates an otherworldly energy, and when held, it seems to hum with power.

**Properties:**
- **Feywild Connection:** The Gáe Draíochta is attuned to the energies of the Feywild. When wielded, the user gains advantage on saving throws against being charmed or frightened.

- **Unyielding Force:** The javelin has the ability to strike with incredible force. On a successful hit, the wielder can choose to use a bonus action to activate the Unyielding Force ability. This ability drives the javelin deeper into the target, causing it to deal additional damage equal to the wielder's level at the start of each of the target's turns. This effect lasts until the javelin is removed (requiring a Strength check contested by the target's Strength or Dexterity).

- **Feywild Burst:** Once per day, the Gáe Draíochta can release a burst of Feywild energy. When thrown into the ground, it creates a 20-foot radius sphere of magical energy centered on the point of impact. Creatures within the sphere must make a Constitution saving throw or take radiant damage and be pushed to the edge of the sphere. The wielder is immune to the effects of this burst.

- **Fey Conduit:** The wielder of the Gáe Draíochta gains the ability to cast *Misty Step* at will, allowing for swift and mystical movement on the battlefield.

**Curse:**
The power of the Gáe Draíochta comes with a price. The wielder is bound to the javelin, and the longer they possess it, the more their connection to the Feywild deepens. Over time, the wielder may become more attuned to the capricious nature of the Feywild, and their emotions might be influenced by its unpredictable energies.

**Legend:**
It is said that the Gáe Draíochta was a gift from the Archfey of the Silver Birch to Cú Chulainn, forged to aid him in his battles against otherworldly foes. The javelin's magical properties have made it a coveted artifact, sought after by those who wish to harness the power of the Feywild.