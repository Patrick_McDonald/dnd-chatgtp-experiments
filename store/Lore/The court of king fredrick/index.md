---
title: "The Court of Fredrick the Beastman"
date: "2023-12-08"
description: "A glimpse into the court of King Fredrick the Beastman, ruler of SLiberberg, and an exploration of the key NPCs that surround him in his melancholic kingdom."
slug: the-court-of-fredrick-the-beastman
categories:
  - Dungeons & Dragons
  - Beastman
  - SLiberberg
---

# The Court of Fredrick the Beastman
![]
Welcome to the whimsical realm of Bergherz, a small but enchanting kingdom nestled on the shores of the Feysea in the heart of Märchenweltgrenze. Here, the air is infused with the magic of the Feywild, and the echoes of ancient tales resonate through the rolling hills and enchanted forests.

At the heart of Bergherz lies the court of King Fredrick the Beastman, a ruler whose appearance mirrors the enigmatic figure from Disney's *Beauty and the Beast*. However, Fredrick's kingdom is more than just a picturesque landscape; it is a realm steeped in melancholy, where the king grapples with a perpetual sense of grief and loneliness, a battle that is seldom aided an often over look by his own advisors and servants.

In this article, we delve into the intricacies of Fredrick's court, exploring the key NPCs who play pivotal roles in the administration of Bergherz. From the imposing Chancellor who strives to maintain an air of regality, to the mischievous Mistiress of the Exchequer who revels in financial pranks, each character contributes to the unique tapestry of Fredrick's kingdom.

Join us as we unravel the mysteries of Castle Sliberberg, where the walls hold secrets, the gardens whisper tales of the past, and the courtiers navigate the delicate balance between duty and the king's enduring melancholy. The Court of Fredrick the Beastman awaits, inviting you to discover the magic, both wistful and enchanting, that defines this small kingdom in the Märchenweltgrenze.

### **Chancellor Bartholomew Gearspark**
![Chancellor Bartholomew Gearspark](./chancellor.jpg)
Chancellor Bartholomew Gearspark, a middle-aged gnome with a penchant for impeccable fashion, serves as the astute and cunning overseer of King Fredrick's court. Clad in opulent garments adorned with rich fabrics and intricate embroidery, Bartholomew is the epitome of late medieval high fashion. His outfits, often in deep, regal colors, reflect both his status and his commitment to maintaining an air of aristocratic sophistication.

**Alignment:** Lawful Neutral

**Personality Trait:** A consummate snob, Chancellor Gearspark takes great pains to shield King Fredrick from the perceived unpleasantness of the masses. His demeanor exudes a sense of superiority, and he rarely lowers himself to engage with those he deems unworthy of the king's presence.

**Ideal:** Perfection in Nobility. Chancellor Gearspark believes in upholding the royal image at all costs, striving for an idealized, almost godlike portrayal of King Fredrick's majesty.

**Bond:** Loyalty Beyond Measure. The chancellor's unwavering loyalty to King Fredrick borders on adoration. He believes the king to be unparalleled in wisdom and leadership, viewing his own service as a divine duty.

**Flaw:** Obsessive Deference. Bartholomew's insistence on godlike deference toward King Fredrick has isolated the monarch from his subjects. The chancellor's overbearing demand for respect impedes the formation of genuine connections, leaving King Fredrick bereft of true companionship. Despite the loneliness it brings, Fredrick finds himself unable to replace Bartholomew due to the gnome's unparalleled competence in his role.


### **Sir Alaric Steelheart**

Sir Alaric Steelheart, a long-serving Ailfyr knight, stood as the dutiful protector of King Fredrick until the fateful defense of Castle SLiberberg. Alaric met his demise defending the castle against the forces of Vasrock, his unwavering loyalty and martial prowess on full display until the end. Unfortuantly he was defeated fighting at Fredrick side when Vasrock army invaded Bergherz

**Alignment:** Lawful Good

**Personality Trait:** A stalwart and disciplined warrior, Sir Alaric was characterized by his unwavering commitment to duty and honor. In life, he was a beacon of loyalty and dedication, always ready to lay down his life for the protection of King Fredrick.

**Ideal:** Duty Above All. Sir Alaric embodied the ideal of duty, valuing the protection of the king and the kingdom above personal gain or glory. His commitment to the Ailfyr code of honor was unwavering.

**Bond:** King Fredrick's Shield. Alaric considered himself not just a knight but a living shield for King Fredrick. His bond with the monarch ran deep, forged through years of service and loyalty.

**Flaw:** Relentless Protector. Alaric's unwavering dedication to protecting King Fredrick often led him to take risks that others might avoid. This unyielding determination ultimately cost him his life in the defense of Castle SLiberberg.

### **Mirielle Dewspark, Mistress of the Exchequer**

Meet Mirielle Dewspark, the mischievous yet exceptionally skilled Pixie Mistress of the Exchequer in King Fredrick's court. Standing just under a foot tall, Mirielle is adorned in vibrant, shimmering attire that reflects the hues of the Feywild. With her wings that glisten like morning dew, Mirielle brings a touch of whimsy to the serious matters of the kingdom.

Mirielle Dewspark's unique approach to financial management, using a horde of awakened abacuses and pens, ensures that the kingdom's finances are meticulously tracked. Despite her playful nature, Mirielle's enchanting abilities and innate understanding of magical finances make her an invaluable asset to King Fredrick's court.

**Alignment:** Chaotic Good

**Personality Trait:** Mirielle is a playful and somewhat lazy pixie, known for her light-hearted approach to her responsibilities. She revels in playing pranks, particularly at the expense of Chancellor Bartholomew Gearspark. Her laughter is infectious, and her carefree demeanor adds a unique spark to the financial dealings of the court.

**Ideal:** Joie de Vivre. Mirielle lives by the principle of "joie de vivre," finding joy in every moment and embracing the playful side of life. She believes that financial matters, while important, should not overshadow the beauty of living in the Feywild.

**Bond:** Family Enchantment. Mirielle is a devoted family woman, her tiny home nestled within the branches of a mystical tree in the Feywild. Her bond with her awakened abacuses and pens is akin to a familial connection, and she takes pride in their enchanted camaraderie.

**Flaw:** Playful Pranks. Mirielle's penchant for pranks, especially those targeting Chancellor Bartholomew Gearspark, can sometimes distract from her financial duties. While her tricks are harmless, they occasionally disrupt the serious atmosphere of the court.



### **Eamon Swiftglen Steward of the Court**

Eamon Swiftglen, the solemn Celtiru (feywild goblin) Steward of King Fredrick's court, carries the weight of tradition on his shoulders with unwavering dedication. Standing at an average height for a goblinoid, Eamon's olive-green skin and keen eyes reflect his Celtic heritage. His hair is meticulously slicked back, adding an air of precision to his appearance.

Eamon wears traditional Celtiru garments adorned with intricate patterns. Every crease, every fold, and every accessory is arranged with the precision befitting his role as steward. His commitment to the formality of his attire mirrors the discipline he brings to his duties.

**Alignment:** Lawful Neutral

**Personality Trait:** Eamon is a serious and tradition-obsessed Celtiru, maintaining an air of formality befitting his role as steward. His demeanor is reserved, and he seldom lets emotions cloud his judgment. He is known for his precision and attention to detail, both in appearance and responsibilities.

**Ideal:** Tradition and Legacy. Eamon holds the traditions of the court in the highest regard, viewing them as a legacy to be preserved and passed down through generations. His dedication to upholding the established ways of the Celtiru is unwavering.

**Bond:** The Black Book. Eamon inherited a sacred black book of court traditions from his father, the previous steward. This book serves as a guide for Eamon's decisions, and he carries it with him at all times. It contains centuries-old protocols, rituals, and the collective wisdom of Celtiru stewards.

**Flaw:** Traditionalist to a Fault. Eamon's unyielding commitment to tradition occasionally hinders adaptation to changing circumstances. He may be resistant to innovations or alterations to established court practices, even if they could enhance efficiency or better serve the kingdom.

**Romantic Pursuit:** Eamon is discreetly courting one of the maids, a fellow Celtiru named Sylva Windwhisper. Their budding romance is a source of gossip among the court, but Eamon maintains a professional exterior while carrying the flame of love in his heart.


### **Thalron Mistwalker, Eladrin Majordomo**

Lord Thalron Mistwalker, the venerable Eladrin Majordomo of Castle SLiberberg, is a figure of timeless elegance and quiet strength. His silver hair cascades gracefully around his ageless features, and his eyes, like pools of ancient wisdom, betray little emotion for an Eladrin. Despite the passage of centuries, Lord Thalron's demeanor remains soft-spoken, always polite, and exquisitely professional.

**Alignment:** Lawful Good

**Personality Trait:** Lord Thalron is the epitome of composure, displaying a stoic calm that belies the tempest within his long and storied life. Soft-spoken and reserved, he seldom shows emotion, maintaining an air of timeless elegance.

**Ideal:** Eternal Loyalty. Thalron's loyalty to King Fredrick spans centuries, a testament to his unwavering commitment to the monarch he served as a valet and armsbearer when Fredrick was but a young man. His ideal revolves around the enduring bond forged in the crucible of time.

**Bond:** Fredrick's Guardian. Lord Thalron sees himself not only as the Majordomo of Castle SLiberberg but also as the guardian of King Fredrick's legacy. His bond with the monarch is a thread woven through the tapestry of history, unbroken by the passage of time.

**Flaw:** The Weight of Centuries. While Lord Thalron's ageless existence grants him wisdom and experience, it also carries the weight of centuries. He occasionally struggles with the memories of times long past, haunted by the echoes of a bygone era.


### **Chef Marcel LeChâteau, Royal Chief**

Chef Marcel LeChâteau, the flamboyant Ailfyr culinary maestro of King Fredrick's court, is a character of extravagant flair, culinary brilliance, and insufferable arrogance. With a thin mustache that curls at the ends and a chef's hat towering above his pointed ears, Marcel embodies the epitome of Ailfyr sophistication.

**Alignment:** Neutral Evil

**Personality Trait:** Marcel is insufferably arrogant, convinced of his own culinary genius. His disdain for his assistants, whom he perceives as plebeian and dull-witted despite their evident skills, often results in heated outbursts. Marcel takes great pleasure in rubbing his favored status with King Fredrick in the faces of the rest of the kitchen staff.

**Ideal:** Culinary Perfection. Marcel's ideal is the pursuit of culinary perfection, though his definition of perfection often aligns more with his personal preferences than with the varied tastes of the court. He sees himself as an unparalleled artist in the realm of gastronomy.

**Bond:** The Royal Palate. Marcel considers King Fredrick's palate to be the canvas on which he paints his culinary masterpieces. The bond between chef and monarch is both professional and strategic, as Marcel strives to keep the king satisfied and impressed with his creations.

**Flaw:** Stubborn Ego. Marcel's greatest flaw is his unwavering belief in his own genius. This stubborn ego blinds him to the culinary talents of others, making collaboration a challenging endeavor. His outbursts and disdainful attitude have created a tumultuous kitchen environment.

### **Lady Seraphina Dewwing, Fairy Chamberlain**

Lady Seraphina Dewwing, the delicate Fairy Chamberlain of Castle SLiberberg, flutters through the royal chambers with a perpetual air of nervousness. Her ethereal wings, translucent and delicate, carry her from one task to another as she maintains the royal quarters and guards the closely-held secret of King Fredrick's melancholy. Lady Seraphina is easily flustered, and if caught off guard, she tends to fly straight up into the ceiling, hitting her head, only to gently fall back down to the floor.

**Alignment:** Lawful Good

**Personality Trait:** Lady Seraphina is a nervous wreck, constantly on edge as she attends to her duties. Her demeanor is one of perpetual surprise and anxiety, exacerbated by the weight of maintaining the secret of King Fredrick's depression. Despite her flustered nature, she is fiercely loyal to the monarch.

**Ideal:** Unseen Support. Seraphina's ideal is to be the unseen support, ensuring that the royal chambers run smoothly and that King Fredrick's struggles remain hidden from the court. Her dedication to preserving the monarch's dignity is unwavering.

**Bond:** Displacer Kitten Companion. Lady Seraphina finds solace in a mischievous displacer beast kitten she has been raising. The tiny creature provides a unique outlet for her stress, and the bond between fairy and displacer kitten is a source of comfort in the midst of her demanding responsibilities.

**Flaw:** Nervous Tendencies. Seraphina's nervousness often manifests in surprising and flustered reactions. Her tendency to fly straight into the ceiling top speed when startled has become a comical spectacle within the castle, though it also serves as a coping mechanism for the overwhelming stress she endures.

### **Lyria Blossomshade, Dryad Royal Groundskeeper**

Lyria Blossomshade, the nurturing Dryad Royal Groundskeeper, calls the heart of Castle SLiberberg's extensive gardens her home. Her essence is intertwined with the larger apple tree that stands as the centerpiece of the lush greenery. Lyria's appearance is a celebration of nature's beauty, with apple tree blossoms adorning her form, creating the illusion of a short pink dress. She always keeps at least one apple blossom delicately pinned in her "hair."

**Alignment:** Neutral Good

**Personality Trait:** Lyria embodies the tranquility of nature, radiating a calming presence within the castle gardens. She is nurturing and attentive to the needs of the flora under her care, treating each plant as a cherished friend. Lyria is passionate about her role as the groundskeeper and takes great pride in the beauty of the castle gardens.

**Ideal:** Harmony with Nature. Lyria's ideal is the harmony between the natural world and the constructed beauty of the castle gardens. She seeks to create a haven of serenity where both residents and visitors can connect with nature's wonders.

**Bond:** The Blossoming Gardens. Lyria's deepest bond is with the flourishing gardens she tends to with unwavering dedication. The plants under her care respond to her touch, blossoming in vibrant colors and creating a breathtaking tapestry of life.

**Flaw:** Protective Nature. Lyria's protective instincts sometimes lead her to be overly cautious with the castle grounds. She is wary of any potential harm befalling her beloved plants, and while her intentions are noble, her zealousness can be a source of mild frustration for those who venture into the gardens.

##  A Stroll Through Castle SLiberberg

Built as a birthday gift for Fredrick is unsuccessful attempt to cheer him, Castle Sliberberg is Fredrick personal haven nestled within the heart of the city of Sliberberg. Covering a sprawling 3 and a half acres within its stone curtain walls and towering 4 stories on top of the the ridge the city was named for the castle is an impressive sight. Built in a blocky style that is both practical and wymsical. The keep of Castle Sliberberg was the primary feature of the adventure Assult of Castle Sliberberg. Now we take a guided tour of the compound beyond the imposing white stone keep.

## The Annex
Since the beginning this building has been called simply The Annex. The Annex is a fairytale-inspired two-story stone building that mirrors the fairytale sensibilities of the rest of the castle. The exterior walls are white stone adorned with delicate carvings of floral patterns. The building has two wings that form a L shape nestled in the southeastern corner of the bailey. At the intersection of the L-shaped wings lies a two story high circular foyer with a domed roof. The dome above is painted with constellations that seem to come alive at night. The circular space is adorned with potted plants and a magical fountain at its center, where water dances in intricate patterns that mimic the movements of celestial bodies. 
The shorter west wing holds offices of some of kings advisors. Situated on the ground floor, Mirielle Dewspark's Office of the Exchequer is a bustling center of financial activity. The office is adorned with enchanted quills and abacuses, each imbued with a spark of magic. They tirelessly work away, tracking the kingdom's finances. The walls are decorated with ledgers and scrolls, creating an atmosphere of controlled chaos. Mirielle's tiny desk, adorned with golden filigree, sits prominently at the heart of the room ontop of another larger desk.Across the hall and occupying two floors, the Royal Records Office is a room dedicated to the preservation of the court's history. It houses ancient tomes, scrolls, and meticulously kept records. Shelves line the walls, reaching towards the ceiling, and a large table in the center serves as a workspace for historians and scribes. Illuminated manuscripts rest on elegant stands, creating an atmosphere that pays homage to the significance of the written word. At the very end of the adminstrative wing, Bartholomew Gearspark's circular office stands as a sign of his favor within the court. The second-floor location provides a commanding view of the castle gardens, fostering an environment of inspiration and creativity. Large windows allow natural light to illuminate the intricate gears and mechanical wonders that adorn the walls.

The longer nothern of the Annex contains luxurious guest suites, each decorated with fine tapestries, plush furnishings, and large windows that offer breathtaking views of the surrounding gardens. The suites are equipped with enchantments that ensure the comfort and well-being of the castle's esteemed guests. An arched portico enframes the first floors rooms while providing the support for a long balcony on the second floor that connects all the guest suites.

### **Stables and Carriage House**
Located in the southwestern corner of the castle along the driveway up to the keep, the stables and carriage house of Castle SLiberberg embody a refined elegance that complements the fairytale-inspired architecture of the castle. The structures, built of the same white stone as the rest of the castle, feature arched doorways and ornate carvings that celebrate the beauty of both function and form. Vines with delicate blossoms climb the walls, adding a touch of natural charm. The stable, positioned with a view of the castle gardens, is home to a select group of horses. While there are a few warhorses for the castle's garrison, the majority are fine-bred white carriage horses. These majestic creatures are cared for with meticulous attention to their well-being, their coats gleaming in the soft Feywild light. Each stall is adorned with fresh hay and enchanted brushes that groom the horses with gentle care. Attached to the stables is a carriage house. The space is home to a gilded carriage, a magnificent gift from Fredrick's subjects in a heartfelt attempt to bring joy to the perpetually melancholic king. The carriage, adorned with intricate details and plush velvet, stands as a symbol of the kingdom's enduring devotion. Sharing the space with the gilded carriage is a large iron and wood chariot that whispers tales of Fredrick's adventuring past. Since his transformaton Fredrick has been too large for any horse alive to handle, so when he wasn't forced to travel by foot he instead used chariots. Crafted for exploration and adventure, the chariot is large enough for three people and their baggage. Though Fredrick's days of roaming the Feywild came to an end two centuries ago, the chariot remains as a nostalgic reminder of a bygone era.


### Castle SLiberberg Gardens
The majority of the interior of Castle SLiberberg is taken up by the breathtaking royal gardens. At the heart of Castle SLiberberg's sprawling gardens stands an enchanted apple tree, a living testament to the bond between King Fredrick and Lyria Blossomshade. The tree was personally planted in the ruins of the old citadel of the city of SLiberberg by Fredrick during Lyria's early years as a young dryad. She and the sapling that would grow into the tree had accompanied Fredrick on some of his last adventures before the refounding. The tree has grown into a majestic centerpiece. Its branches extend gracefully, creating a natural canopy under which the air is filled with the sweet fragrance of apple blossoms. The tree features a bench swing Fredrick had installed in hopes that his long lost love Sophia would return.
The gardens, orchestrated by the whims of Lyria Blossomshade, undergo a magical transformation every few years. The arrangement of flowers, shrubs, and magical plants shifts in a harmonious dance, creating an ever-changing landscape that reflects the ebb and flow of nature's cycles. Each iteration is a new masterpiece, a testament to Lyria's creativity. What does not change are the water features. Tranquil ponds and meandering streams wind their way through the gardens, creating serene oases that invite moments of contemplation. Water lilies float on the surface of the ponds, their blossoms opening and closing in response to the magic of the Feywild. The gentle sound of water flowing adds a soothing melody to the enchanting surroundings.
