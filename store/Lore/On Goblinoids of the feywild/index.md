---
title: "On Goblinoids of the Feywild"
date: "2023-12-08"
description: "Exploring the culture and realms of goblinoids in the Feywild, shaped by the aftermath of Maglubiyet's conquest and their unique interactions with the fey."
slug: on-goblinoids-of-the-feywild
categories:
  - Dungeons & Dragons
  - Feywild
  - Goblinoids
---

## The Celtiru Goblins: 

The Celtiru goblins trace their origins to the enchanting halls of the Gloaming Court, a mystical realm within the Feywild where twilight dances with magic. Here, goblins found themselves caught in the crossfire of Maglubiyet's conquest, and those who managed to escape sought refuge in the ever-shifting landscapes of the Feywild. These goblins continue to be refeered to by the ancient name for their race the Celtiru. In the wake of Maglubiyet's conquest, Celtiru goblins experienced a cultural diaspora. Those who escaped found themselves adrift, seeking solace in various archfey domains. Their small stature and resourcefulness make them adept at integration, and Celtiru goblins can be found working in archfey cities, laboring in farms, or living in serene hamlets in the calm corners of the Feywild. Amidst their diaspora one fact is evident above all in Celtiru tribe and community. Small, tight-knit groups form the backbone of their social structure, offering safety, camaraderie, and shared purpose. Whether dwelling in archfey cities, working the fields, or residing in remote hunter-gatherer communities, Celtiru goblins find strength in unity..
 ### Celtiru communities
Celtiru settlements seamlessly integrate with the surrounding Feywild environment. Homes are often built into the landscape, utilizing the natural features of the terrain. Treehouses, burrows, and moss-covered dwellings are the norm. the heart of the settlement, a communal area serves as a gathering place for celebrations, meetings, and shared activities. Festive decorations, colorful fabrics, and bioluminescent plants contribute to the vibrant atmosphere. Leadership is typically shared among a council of elders, wise goblins who have garnered respect through their experiences. Decision-making tends to be collaborative
### Nilbogism
A unique phenomenon among the Celtiru is Nilbogism. Nilbogs, are fragments of the former Celtiru pantheon's trickster god. The unnamed trickster god sought to pull one over on Maglubiyet befores his conquest was completed and was shattered into may pieces, although many speculate that it was the trickster god who got the last laugh. When a  Nilbogs possess goblins, transforming them into wisecracking, impish rapscallions the possed goblin also is known as a nilbog or a uritlec. Nilbogs grant their hosts powers that force individuals to act in opposition to their intentions. A mere jest or trickery becomes a potent force, turning the straightforward into the convoluted and the serious into the absurd. Because of this Nilbog-possessed goblins are revered for their ability to bring merriment and joyous chaos. The community sees them as conduits of the trickster god's spirit, bringing an element of entertainment that enriches their daily lives. Also powerful archfey often seek out Nilbogs to serve as court jesters in their domains. The unpredictable nature of Nilbog antics adds a unique flavor to the fey courts, creating an atmosphere of amusement and light-heartedness.


### **The Ailfyr**

In the Feywild, the Ailfyr as the hobgoblins call themselves stand as exemplars of unwavering loyalty and dedication. Originating from the ancient courts of archfey and powerful beings, the Ailfyr are deeply ingrained with the concepts of vassalage and service. It is often considered this fact that cause Maglubiyet to be drawn to them in the first place, loyalty like that makes for perfect soldiers. In the wake of Maglubiyet's conquests, those who escaped have continued their existence as devoted servants in the Feywild, their lives defined by service to lords, ladies, and generals.

Ailfyr can be found in most fey court where they serve in diverse capacities. As soldiers, they defend their lords' realms; as administrators, they ensure the smooth functioning of domains; as scholars and mages, they delve into the mysteries of the Feywild; and as master artisans, they craft works of unparalleled beauty.

Ailfyr hobgoblins take immense pride in their work, whether it be in the forging of weapons, the administration of realms, the pursuit of knowledge, or the creation of intricate art. They view worthwhile and skillful work as a testament to their loyalty and dedication. Ailfyr are driven by an innate desire for recognition, Ailfyr hobgoblins seek praise and acclaim from both their fellows and their sworn masters. This pursuit of approval becomes a powerful motivator, propelling them to achieve greater heights in their chosen occupations.

The concept of loyalty is the cornerstone of Ailfyr existence.  Ailfyr with no lord or master actively seek out lords or ladies whom they deem worthy of their service. The process is deliberate, and they commit themselves faithfully to those they believe embody the virtues and principles befitting of their loyalty. Rarely does an Ailfyr leave their master's service willingly. Such a decision typically stems from a deemed lack of respect or a crossing of a personal moral boundary. Loyalty is both their strength and their code of honor.

### **Ailfyr Settlements**

 While the concept of an independent Ailfyr community is unheard of, scattered small settlements and Ailfyr enclaves within larger fey domains showcase the unique blend of craftsmanship and service that defines their culture. Ailfyr settlements are characterized by well-constructed wood buildings, primarily single-story longhouses. These structures serve a dual purpose as both residences and workspaces for the Ailfyr residents. Crafted with precision and care, these longhouses stand as testaments to the hobgoblins' commitment to their craft.  The facades of Ailfyr dwellings are adorned with intricately carved panels, featuring designs reminiscent of medieval Irish art. These carvings tell stories of loyalty, service, and the Ailfyr's connection to the Feywild. The artistic expressions on their homes embody the values that guide their lives. Within these longhouses, every residence doubles as a functional workspace. Ailfyr hobgoblins seamlessly integrate their work into their living spaces, blurring the lines between personal and professional life. This approach emphasizes the importance of craftsmanship in their culture.

While Ailfyr settlements exist independently, they can also be found as enclaves within larger fey domains. Ailfyr settlements adhere to a feudalistic governance structure, mirroring the hierarchical systems of service they are accustomed to. Hobgoblins follow the orders of a liege lord and their representatives, ensuring a structured and organized community life


### **Bealcú**

In the heart of the Feywild's untamed landscapes, the Bealcú, as the  bugbears call themselves, thrive as nomadic spirits, living in harmony with the enchanted forests that define their existence. Subsisting on hunting and gathering, these tribal beings weave through the dense foliage, embodying the essence of wanderlust and strength. Let's explore the nomadic life of the Bealcú, where the Feywild's forests serve as both home and inspiration for their journey.### **Bealcú Bugbears: Nomadic Spirits of the Feywild Forests**

In the heart of the Feywild's untamed landscapes, the Bealcú bugbears thrive as nomadic spirits, living in harmony with the enchanted forests that define their existence. Hunting and gathering form the core of Bealcú sustenance. The bugbears navigate the dense Feywild forests, relying on their strength, agility, and knowledge of the land to procure sustenance from the bountiful flora and fauna that grace their mystical home. Bealcú share an intimate connection with the Feywild forests, drawing inspiration from the magical energies that course through the ancient trees. 
While the Bealcú predominantly keep to the Feywild forests, rare individuals are overcome with wanderlust. Driven by an innate desire for exploration, these bugbears venture into fey domains where their strength finds purpose in various occupations that befit their capabilities.
