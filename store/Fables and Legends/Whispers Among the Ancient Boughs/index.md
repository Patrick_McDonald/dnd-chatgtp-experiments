---
title: Fables and Legends of the Märchenweltgrenze  Whispers Among the Ancient Boughs 
date: "2023-11-19T22:40:33.169Z"
description: A one shot for 5 players of level 6-9
---

### Adventure Background: 

In the heart of the Märchenweltgrenze, where the ancient trees intertwine their branches and the air is thick with magical resonance, a sinister disturbance has shattered the tranquility of the sylvan sanctuary. Dryads, the benevolent tree spirits, guardians of nature's secrets, have become the victims of a malevolent force. The harmony between the fey denizens of the Märchenweltgrenze and the human settlers teeters on the edge of chaos.As the heroes traverse the dense and enchanted forest, they are suddenly flagged down by a towering and wise treant-like fey named Oakheart. This venerable entity, a guardian of the sacred groves, beseeches the aid of the adventurers. Distress fills Oakheart's ancient eyes as the revelation is shared – someone is systematically slaughtering dryads, casting a dark shadow over the once-idyllic sanctuary.Initially, the 
Unbeknownst to the fey and settlers, the true perpetrator is an oni, a deceitful and cunning creature in the employ of Gisela, a dark force seeking to exploit the conflict between the communities for her own gain. The oni revels in sowing discord, casting a web of deception that obscures the true source of the atrocities.As Oakheart implores the heroes to uncover the reality behind the dryad slayings, a race against time ensues.




### Oakhearts grove

*As you step into the heart of Oakheart's grove, the air is alive with a vibrant energy that seems to hum with the very pulse of the Märchenweltgrenze itself. Towering trees, their gnarled branches reaching towards the sky like ancient storytellers, create a natural cathedral overhead. The foliage is dense, casting dappled sunlight upon the forest floor. Animals, their eyes gleaming with an otherworldly intelligence, regard you with a mixture of curiosity and welcome. Deer with majestic antlers, once mere forest dwellers, now exude an aura of regality. Rabbits, usually skittish, observe you with a knowing gaze, as if they share the secrets of the Märchenweltgrenze.*
Despite his size and obvious magical power Oakheart is quite friendly with the what he calls the smallfolk. He will willingly answer questions about the grove as he leads them on.

*Following Oakheart's enormous strides through the enchanted grove, you find yourself standing at the edge of a small clearing. The atmosphere changes subtly here, as if the very air holds echoes of a recent sorrow. The sunlight filters through the foliage, casting gentle beams upon the scene before you. In the center of the clearing lies the most recent victim, her once vibrant and verdant form now frozen in a tragic tableau. Her leafy hair, once a cascade of emerald hues, is now limp and dulled. The blossoms that adorned her limbs have withered, their colors drained away. The air, which once carried the sweet scent of blooming flowers, now seems tinged with an underlying note of grief.Oakheart, his branches hanging low in solemnity, gestures towards Petalshade. "This is where the tragedy unfolded," he rumbles, the resonance of his voice echoing through the clearing. "Petalshade was a guardian of these woods."*

### Investigating the scene



### Clues and Skill Checks:

#### 1. **Nature Check (DC 15):**
   - *Clue:* Examination of the surrounding flora reveals broken branches and crushed underbrush leading away from the crime scene.
   - *Outcome:* Success identifies the direction of escape; failure may lead to misinterpretation.

#### 2. **Survival Check (DC 18):**
   - *Clue:* Tracks on the ground indicate the passage of a creature much larger than a typical humanoid, with deeper imprints.
   - *Outcome:* Success reveals the distinctive gait of a creature on foot; failure might overlook or misinterpret the tracks.

#### 3. **Arcana Check (DC 20):**
   - *Clue:* Lingering traces of magic are detected in the air, suggesting the use of illusion spells to mask the true appearance.
   - *Outcome:* Success unveils the magical residue; failure may result in misidentifying the nature of the spell.

#### 4. **Investigation Check (DC 16):**
   - *Clue:* Scraps of torn fabric caught on a nearby thorn bush hint at a struggle, with fibers larger than those of a typical humanoid.
   - *Outcome:* Success reveals the presence of a larger creature; failure might overlook or misinterpret the significance.

#### 5. **Perception Check (DC 17):**
   - *Clue:* A faint but distinct scent lingers in the air – an otherworldly aroma that doesn't align with the natural surroundings or that of the locals.
   - *Outcome:* Success allows the identification of the unusual scent; failure may result in overlooking this olfactory clue.

#### 6. **Knowledge (Nature) Check (DC 16):**
   - *Clue:* Examination of the broken branches and disturbed vegetation provides insights into the possible size and weight of the creature.
   - *Outcome:* Success allows estimation of the creature's size; failure may result in an inaccurate assessment.

As the players piece together these clues through successful skill checks, a clearer picture emerges: the true killer is no mere humanoid but a larger, monstrous entity that disguised itself magically to deceive the grove's inhabitants and investigators alike. Armed with this knowledge, the players can now embark on a quest to unveil the creature's identity and prevent further harm to the delicate balance of the Märchenweltgrenze.


### Veiled Masquerade

#### Scene Details:

The village square bustles with activity as the players step into the lively marketplace. Merchants peddle their wares, children play by a bubbling fountain, and the scent of freshly baked bread wafts through the air. However, amidst this picturesque scene, a subtle undercurrent of tension lingers.

#### Oni Tactics:

1. **Invisibility Intricacies:**
   - The oni, aware of the players' pursuit, cleverly utilizes the invisibility spell. Players must rely on Perception checks to notice faint disturbances – a slight displacement of air, the subtle rustle of leaves, or the brief disruption of light.

2. **Deceptive Disguises:**
   - Transforming into different townsfolk, the oni adopts a series of personas. Players must engage in conversations, make Insight checks, and observe behavioral nuances to identify the disguised oni.

3. **Suggestion Strategies:**
   - The oni, cunningly aware of the players' attempts, uses suggestion to manipulate the villagers. Players may receive false information or encounter individuals attempting to steer them in the wrong direction.


### Possible player tatics
#### Detect magic
The simplest option is to use detect magic to search the town. The oni knows this and will try his best to stay well away from the party.

#### Interogating the townfolk


#### 1. **Perception (DC 15):**
   - *Tactic:* Players scan the crowd for subtle disturbances in the air or anomalous movements.
   - *Outcome:* Success allows players to notice the oni's presence, granting advantage on the next skill check.

#### 2. **Insight (DC 17):**
   - *Tactic:* Engaging in conversation with villagers, players attempt to discern inconsistencies or strange behavior.
   - *Outcome:* Success reveals the imposter among the villagers, providing a clue about the oni's disguised form.

#### 3. **Arcana (DC 18):**
   - *Tactic:* Players use magical knowledge to detect traces of illusion magic or disruptions caused by invisibility.
   - *Outcome:* Success unveils the magical ruse, rendering the oni momentarily visible.

#### 4. **Investigation (DC 16):**
   - *Tactic:* Searching for tangible evidence, players examine the surroundings for items out of place or discarded illusions.
   - *Outcome:* Success yields clues about the oni's movements and tactics, aiding in tracking.

#### 5. **Persuasion (DC 14):**
   - *Tactic:* Players interact with villagers to gather information, attempting to subtly uncover signs of deception.
   - *Outcome:* Success may lead to villagers unknowingly revealing the oni's influence, aiding in identification.

#### 6. **Survival (DC 16):**
   - *Tactic:* Players follow faint traces and tracks, attempting to predict the oni's next move.
   - *Outcome:* Success provides insights into the oni's escape route, guiding the pursuit.

As the players employ these tactics and succeed in their skill checks, the veil of the oni's illusions begins to unravel. The chase through the village becomes a dynamic and engaging sequence, requiring quick thinking, collaboration, and adaptability from the players to expose the malevolent force lurking within the seemingly ordinary community.


#### Key Interactions:

1. **The Flower Vendor's Tale:**
   - The oni, masquerading as the flower vendor, engages players in conversation. The real vendor, aware of the deception, discreetly signals the players, aiding them in distinguishing the imposter.

2. **Innkeeper's Ruse:**
   - Seeking refuge in the inn, the oni takes on the form of the innkeeper. Players must notice irregularities in speech patterns or knowledge about the village to unmask the creature.

3. **Marketplace Mirage Maneuver:**
   - The oni, using suggestion, convinces a group of villagers to mislead the players. Players must discern the truth from conflicting stories, realizing the orchestrated diversion.

#### Complications:

1. **Crowded Streets:**
   - Navigating through the busy village square poses its challenges. The oni exploits the crowded environment, slipping away through alleyways or merging seamlessly with the bustling crowd.

3. **Vigilant Locals:**
   - Some villagers, sensing the disruption, become wary and defensive. The players must convince them of the true threat without causing panic or hostility.


### Encounter: The Confrontation with the Cornered Oni

*As you round the final bend of the narrow village alley, your breath catches as you find yourself face to face with the elusive oni. The once bustling marketplace now echoes with the tension of an impending clash. The oni, its disguise unraveled, stands tall with a malevolent gleam in its eyes, its form shifting from villager to monstrous fiend.The cramped alley offers no escape for the cornered oni, its back against the cold stone wall. The air crackles with an imminent showdown as the oni discards its illusory human guise, revealing its true form. A greataxe materializes in its hands, a deadly instrument that promises a fierce battle ahead.The villagers, sensing the shift in atmosphere, have cleared the area, leaving you alone with the malevolent oni. The buildings loom overhead, casting shadows that seem to dance with anticipation. The Märchenweltgrenze itself seems to hold its breath, waiting to witness the outcome of this confrontation between heroes and the devious shape-shifter.*

The person turns into an **oni** and attacks the player characters. Depending  if the players are less than level 7 a **human veteran** will appear on the 4 round to aid them in the fight. Once reduced to 1/4 hitpoints the oni will try to flee by using a combination of invisibility and gaseous form.

When defeated the characters find the following on the oni body

1. **Belt Pouch:**
   - Inside the oni's belt pouch, the players discover a stash of gold coins amounting to **300 gold pieces**. These funds were likely intended to facilitate covert transactions and bribery in the course of executing Gisela's dark directive.

2. **Valuable Personal Items (Worth 150 gp):**
   - Among the oni's possessions in his satchel are a set of exquisite but practical items
     - A finely crafted silver flask adorned with subtle engravings.
     - A polished obsidian gemstone, perhaps of sentimental value.
     - A small, intricately designed spyglass with gold-plated accents.

3. **Spellbook:**
   - The oni's spellbook, meticulously maintained, contains a selection of arcane spells that align with its deceptive and illusionary capabilities. Though not inherently magical, the spells within reveal insights into the oni's magical prowess and potential tactics. Some notable spells include:
     - *Invisibility*
     - *Darkness*
     - *Suggestion*
     - *Disguise Self*
4. **Woodcutters great ax**
    See wilds beyond the witchlight for details on this magic item
5. **A letter from Gisela the sorceress**

#### Gisela's Dark Directive

*To my loyal servant,*
*In the shadows, where deceit intertwines with ambition, you stand as my emissary, a harbinger of discord in the Märchenweltgrenze. I entrust you with a task of great importance, one that shall sow the seeds of chaos and strife, ensuring that the fragile peace between the humanoid settlers and the esteemed dryads crumbles into ashes.*
*Your mission is clear: instigate conflict, fan the flames of animosity, and set ablaze the delicate balance that has thus far kept the Märchenweltgrenze in check. I demand the following actions be executed with precision and subtlety:*
*Remember, subtlety is your greatest weapon. This task requires finesse, a delicate touch that leaves no trace of Gisela's involvement. The chaos you unleash shall serve as the crucible in which the Märchenweltgrenze transforms into a realm of discord, ripe for my dominion.*
*May the shadows guide your every step.*
*Gisela*

### Conclusion:

The narrow alleys of the village now echo with the fading sounds of battle as the heroes stand victorious over the defeated oni. The malevolent force that sought to plunge the Märchenweltgrenze into chaos lies vanquished. With the oni's severed head securely in their possession, the heroes return to Oakheart's grove, a place where the forest's heart beats with ancient magic.

#### Oakheart's Gratitude:

Awaiting the heroes in the heart of the grove, Oakheart, the venerable treant-like fey, greets them with a nod of acknowledgment. His eyes, wise and ancient, express gratitude for their valor and dedication to preserving the delicate balance of the Märchenweltgrenze. As the oni's head is presented, a solemn air settles over the grove, and the dryads observe the heroes with a mixture of relief and gratitude.

#### A Rare Gift:

In a gesture of appreciation, Oakheart steps forward, a majestic aura enveloping him. With a voice like rustling leaves and the murmur of a distant brook, he addresses the heroes. "Your bravery has quelled a storm before it could consume our sanctuary. For this, I bestow upon you a rare artifact, a fragment of the forest's magic."
He presents the players with one of three potential rewards depending on the party composition (see bellow)
#### Admonishment and Reflection:

However, as Oakheart extends this magical gift, his expression takes on a somber note. Turning to his fellow dryads, he addresses them with a stern but measured tone. "Let this victory also serve as a lesson. Haste can cloud judgment, and assumptions can breed discord. The heroes before us have proven that collaboration and understanding are the keys to preserving the harmony we hold dear."

The dryads, chastened but receptive, nod in acknowledgment. The Märchenweltgrenze, while scarred by recent events, begins to exhale a sigh of relief. The balance, once teetering on the brink, is slowly being restored.

With the Amulet of Sylvan Harmony in their possession, the heroes stand as symbols of unity between fey and humanoid, guardians of the Märchenweltgrenze's delicate equilibrium. As the grove breathes with newfound tranquility, the heroes find solace in the knowledge that their deeds have woven another thread into the intricate tapestry of the ancient forest.
### Oakheart's rewards
Oakheart presents the players with one of the following
### 1. Staff of Verdant Whispers:

**Weapon (quarterstaff), rare (requires attunement by a spellcaster)**

**Description:**
This staff is crafted from the rare, resilient wood of the Märchenweltgrenze, adorned with delicate vines and leaves.

**Abilities:**
1. **Verdant Insight:**
   - You can cast the *speak with plants* and *speak with animals* spells at will, without expending a spell slot.

2. **Nature's Embrace:**
   - When you cast a spell that has a nature-based component (e.g., druidcraft, entangle), you gain a +1 bonus to spell attack rolls and spell save DC.

### 2. Amulet of Sylvan Harmony:

**Wondrous Item (amulet), rare (requires attunement)**

**Description:**
A luminous pendant adorned with a crystalline leaf that glows with an inner radiance.

**Abilities:**
1. **Sylvan Resilience:**
   - You have advantage on saving throws against effects that would harm the natural environment, such as blight or spells with the "fire" descriptor.

2. **Nature's Insight:**
   - You gain proficiency in the Nature skill. Additionally, you have advantage on Wisdom (Perception) checks related to the natural world.

### 3. Cloak of Whispering Shadows:

**Wondrous Item (cloak), rare (requires attunement)**

**Description:**
Woven from the shadowy strands of the Märchenweltgrenze, this cloak seems to meld with the shadows around it.

**Abilities:**
1. **Shadowstep:**
   - Once per short rest, as a bonus action, you can step into the shadows and teleport up to 30 feet to an unoccupied space that you can see, as long as the space is in dim light or darkness.

2. **Veil of Shadows:**
   - You can cast *minor illusion* at will, creating illusions that fit within a 5-foot cube. Additionally, you have advantage on Dexterity (Stealth) checks made in dim light or darkness.
