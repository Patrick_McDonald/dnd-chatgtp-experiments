---
title: Fables and Legends of the Märchenweltgrenze  Yuletime in Bergherz 
date: "2023-12-07T22:42:33.169Z"
description: A festive one shot for 5 players of level 6-9
---
# Background

As winter's embrace envelops the kingdom of Bergherz, the snow-covered town of Sliberberg awakens to the festive spirit of Yuletime. The town is adorned with shimmering fairy lights, festive decorations, and the scent of freshly fallen snow. The castle itself is a vision of holiday splendor, its towers and battlements outlined with twinkling lights that reflect off the pristine white landscape.The city awaits the 3 day festival of fun and merriment that  yule brings and  King Fredrick extends his gracious invitation to the players, his esteemed vassals, and the nobility of the region to partake in the festival and grand celebrations at Castle Sliberberg. Amidst the joyous festivities, a somber note taints the air. Clara Winterhaven, daughter of the Count and Countess of Winterhaven, has been acting peculiarly. Whispers circulate through the castle of her strange dreams, the peculiar gifts to mice, and the enchanting nutcracker bestowed upon her by her godfather, Master Thistledown. Unknown to all, Clara has borne witness to the enchanted nutcracker coming to life, battling the malevolent Mouse King and his horde of mice. Her dreams have become a portal to the Land of Dolls, where a war for the very essence of the season rages on.


## The adventure begins
The adventure begins in the upper ward of Sliberberg. A large crowd of men and fey alike has gathered in the the town hall square. The crowd has gathered to witness the crowning of this year's lord of Misrule, a commoner who is elevated to the honorary postion of lord of ceremonies for the city Yuletide festival, an event that signals the start of the yuletime fair. The players are right up front of the crowd. Then a fanfare sounds and the ceremony begins.Read the following.

*Amidst the vibrant glow of festive lights and the harmonious hum of joyous chatter, the town square in Sliberberg transforms into a stage of anticipation. King Fredrick, draped in regal attire, and Panthor Silverhoof, the spirited master of ceremonies, ascend a grand stage set before the town hall. A hush descends upon the gathered crowd as the two figures take center stage.*

**King Fredrick:**
*"Lords and ladies, citizens of Sliberberg, welcome to the grand culmination of our Yule Festival—the crowning of the Lord of Misrule! In the spirit of revelry and merriment, we shall embrace the chaos and laughter that ensues when a commoner ascends to this most esteemed role."*

*The king's voice resonates with warmth and regality, capturing the attention of every onlooker gathered in the square.*
**Panthor Silverhoof:**
*"Indeed, dear subjects, and what an honor it is to share this stage with our beloved monarch, King Fredrick! Without further ado, let the selection process commence. We shall choose from among you, our spirited townsfolk, a Lord of Misrule who shall guide us through the final days of our festival with their unique charm and whimsy."*
Fredrick points to a random person in the crowd. Use the following table or create your own npc.

1. **Seraphina Frostwhisper:**
   - A lively and carefree bard with silver hair and an infectious laughter. Seraphina is known for her spirited tales and love of spontaneous musical performances.

2. **Crispin Emberjoy:**
   - A mischievous gnome illusionist with a penchant for whimsical pranks. Crispin's quick wit and flair for creating illusions make him a delightful choice for the role.

3. **Isolde Merryweather:**
   - An eccentric inventor and tinkerer with a knack for crafting enchanting gadgets. Isolde's colorful attire and enthusiasm for unconventional celebrations make her a standout candidate.

4. **Thorne Bramblebreeze:**
   - A gentle and nature-loving druid known for fostering a sense of unity with the natural world. Thorne brings a touch of the Feywild to the festivities with floral crowns and lively animal companions.

5. **Quinn Silverbellows:**
   - A boisterous and good-natured blacksmith with a passion for forging festive ornaments and decorations. Quinn's infectious laughter and hearty demeanor make him a beloved figure in the community.

6. **Lilith Moonshadow:**
   - A mysterious and enchanting rogue with a penchant for shadow play and illusions. Lilith's elegant movements and enigmatic aura add an air of intrigue to the celebration.

7. **Oscar Evergleam:**
   - A jovial and gregarious halfling chef renowned for creating delectable Yule treats. Oscar's culinary skills and boundless energy promise to infuse the festival with delectable delights and laughter.

8. **Zephyr Windwhisper:**
   - A free-spirited and whimsical air genasi with the ability to manipulate breezes and create playful gusts of wind. Zephyr's ethereal presence and airy laughter embody the spirit of light-hearted chaos.
King Fredrick and Panthor Silverhoof stand at the center stage, their regal and mischievous presences blending in a harmonious da
Whoever is chosen there is palpable excitement in the air and in there eyes as they make their way to the stage. Read the following

*The air in the town square crackles with excitement as the chosen Lord of Misrule steps forward, ready to receive the symbolic crown and staff that mark the beginning of their festive reign.* 
**King Fredrick:**
*"Lords and ladies, citizens of Sliberberg, let the moment of revelry commence! Behold our chosen one, whose spirit shall guide us through the laughter-filled days ahead. With holly-crowned majesty, let us now crown our esteemed Lord of Misrule!"*
*As King Fredrick raises a wooden crown adorned with holly and evergreen, the crowd watches with bated breath, eager to witness the transition of a commoner into the whimsical embodiment of Yule merriment.*
**Panthor Silverhoof:**
*"In the spirit of joy and chaos, we place upon your head this crown of mirth and mischief. Wear it proudly, our Lord of Misrule, as a beacon of laughter and celebration!"*
*The chosen Lord of Misrule bows slightly, their eyes reflecting the anticipation of the festive responsibilities they are about to undertake.s the crown is placed upon the head of the Lord of Misrule and the staff is handed over, the crowd erupts into cheers, and the atmosphere becomes charged with the promise of joyous revelry.*

## The parade of merriment
After crowing the lord of misrule a joyous procession though the streets to the fairgrounds begins. Fredrick, the Lord of Misrule, and a lively entourage of performers and bards lead the way for the procession, ththat will wind its way to the festive fairgrounds just beyond the city wallsat will wind its way to the festive fairgrounds just beyond the city walls. Revelers don eccentric fool-inspired costumes, their garments adorned with bells that chime in harmony with the lively tunes played by a merry band of bards. As the parade sets off read the following.

**Read Aloud Description:**
*The sound of drumrolls fills the square as the parade kicks off with King Fredrick, resplendent in royal attire, leading the way. The Lord of Misrule follows closely, staff twirling in the air, and a troupe of jesters capers about, their costumes a riot of vibrant hues. A playful menagerie of fey creatures dances alongside, adding a touch of enchantment to the procession. Musicians strike up cheerful melodies, and performers twirl and tumble, casting spells of joy upon all who witness the spectacle. Bells jingle, laughter echoes, and the entire town is caught in the infectious rhythm of the Yule Parade!" The crowd, now caught up in the festive spirit, joins the procession with glee. Families and friends link arms, and children run ahead, eager to be part of the whimsical caravan. The parade weaves its way through the city streets, bringing joy to every corner of Sliberberg, as the Lord of Misrule grins mischievously, setting the tone for a celebration that promises to be remembered for years to come.*

Encourage your players to join in the parade and act like a joyous fool. Encourage them to describe how their character participates in the revelry. Do they sing? Dance? Juggle?
## The Yule Festival grounds
The Sliberberg Fairgrounds, a sprawling expanse just beyond the city walls, come alive with the vibrant energy of the Yule Festival. A tapestry of colorful tents and stalls dot the open field, each promising a unique experience for the revelers. The air is infused with the promise of the fragrant aroma of sizzling treats, the joyous melodies of minstrels, and the laughter of both young and old. When the parade reaches the fairground read the following

*The fairgrounds, sprawling and inviting, welcome the parade with open arms. Stalls adorned with festive decorations beckon, promising delights both sweet and savory. Laughter and cheers ripple through the crowd as the parade weaves its way deeper into the heart of the festivities. The revelry continues, and the fairgrounds transform into a canvas of merriment, ready to unfold the enchanting chapters of Yule celebrations yet to come. The fairgrounds, a canvas for the magic of the season, unfold before the spectators, offering a myriad of attractions and amusements. The Yule Parade, having brought its infectious spirit to this lively canvas, invites all to partake in the joyous revelry that awaits beneath the winter sky.*

Encourage the players to explore the fairground and partake the sights and attractions. Here is some suggestions for attractions.

### Sliberberg Fairgrounds Attractions

1. **Archery Tournament:**
   - **Attraction Description:** Test your precision and marksmanship at the Archery Tournament. Competitors take aim at targets with bows and arrows, vying for the title of Sliberberg's Grand Marksman.
   - **Check:** Dexterity (Archery) check.
   - **Competition Rule:** Highest total points after three rounds wins.

2. **Fey Fortune Teller Tent:**
   - **Attraction Description:** Step into the mystical tent of the Fey Fortune Teller and discover what fate has in store for you. An eccentric seer awaits to share glimpses of the future.
   - **Check:** Wisdom (Insight) check.
   - **Fortune-Telling Rule:** Success reveals a cryptic message about the character's future.

3. **Snowman Building Contest:**
   - **Attraction Description:** A designated area for friendly competition in snowman building. Participants can let their creativity shine as they fashion the most whimsical snowman.
   - **Check:** Dexterity (Sleight of Hand) check for detailed snowman features.
   - **Competition Rule:** Judges assess overall creativity, detail, and aesthetic.

4. **Yule Feast Pavilion:**
   - **Attraction Description:** A grand pavilion where festival-goers can indulge in a delectable spread of Yule delicacies. Culinary delights include roasted chestnuts, spiced cider, and holiday sweets.
   - **Check:** No check required; feast for the senses.

5. **Dancing Lights Garden:**
   - **Attraction Description:** A mesmerizing display of enchanted lights that dance and twinkle in synchronized patterns. Patrons can stroll through this radiant garden, experiencing the magic.
   - **Check:** No check required; enjoy the enchanting ambiance.

6. **Feats of Strength:**
   - **Attraction Description:** A competition of raw strength where participants can showcase their might. Test your prowess in activities like arm wrestling and tug-of-war.
   - **Check:** Strength check for each specific activity.
   - **Competition Rule:** Winners determined by the outcome of individual strength contests.

7. **Craftsmen's Bazaar:**
   - **Attraction Description:** A bustling marketplace where skilled craftsmen showcase and sell their wares. Patrons can find unique trinkets, handmade ornaments, and enchanted items.

8. **Yule Bard's Stage:**
   - **Attraction Description:** A raised stage where bards and minstrels perform festive tunes, spreading the spirit of Yule through music and song.

9. **Ice Skating Rink:**
    - **Attraction Description:** A frozen pond transformed into an enchanting ice skating rink. Festival-goers can glide across the ice and partake in the joy of winter skating.
    - **Check:** Dexterity (Acrobatics) check for impressive maneuvers.
    - **Competition Rule:** Judges assess skill and creativity in a friendly skating competition.

Read the following as the player finish exploring the yule festival.
*"As you revel in the vibrant festivities of the Sliberberg Fairgrounds, a messenger approaches, bearing an official seal and a regal invitation. The missive, emblazoned with the sigil of King Fredrick, conveys a message of honor and importance. 'Noble champions,' the messenger addresses you, 'King Fredrick sent me to find you. The time has come for you to make your way to the castle, the feast is about to begin.'"*

## The Feast of the first day of yule
*Amidst the towering stone walls adorned with festive tapestries, you find yourself in the heart of Castle Sliberberg's grand hall. A symphony of warmth and light envelops the space, as crackling hearths cast dancing shadows across long banquet tables laden with sumptuous Yuletime delicacies. Elaborate decorations of holly, evergreen, and silver baubles festoon the hall, creating an atmosphere of enchantment. The air is filled with the harmonious melodies of minstrels playing traditional Yuletime tunes, and laughter mingles with the aroma of spiced mulled wine and roasting meats. Duke Fredrick, resplendent in his regal Yuletime attire, raises a toast to the gathered nobles, invoking blessings for the season and the camaraderie shared within these walls.*
Have the players describe their character's reactions to the festive surroundings. Are they sampling the delectable treats, engaging in lively conversation, or perhaps partaking in a jovial dance? Have them take the opportunity to interact with other nobles present in the great hall. Engage in small talk, discuss the recent happenings in your respective lands, or share anecdotes of Yuletimes past.For extra engagment Have the players Share a Yuletime tradition or custom specific to your character or region. Whether it's a special toast, a unique dish, or a customary dance, embrace the opportunity to infuse the celebration with your character's personal touch.

# Introduction to the Winterhaven

*Amidst the lively Yuletime feast, King Fredrick rises from his regal seat, a goblet of mulled wine in hand. With a warm smile, he beckons you to follow as he gracefully navigates the crowded hall. Your steps echo on the polished stone floor as you approach a noble family seated at a resplendent table adorned with winter blooms and flickering candles. Duke Fredrick gestures towards the Count and Countess of Winterhaven, introducing you to the esteemed guests from the picturesque region of Winterhaven.*

**_Roleplaying the Winterhavens:_**
Count Elias Winterhaven, a tall and dignified figure, extends a genial welcome. He shares tales of Winterhaven's prosperity and the joys of celebrating Yuletime in their snowy domain. Countess Isabella, with her emerald-green eyes, exudes grace and charm. She is an eloquent conversationalist, discussing cultural events in Winterhaven and expressing her delight at being part of the Duke's celebration.

Insight Check on Lady Winterhaven:
*To discern Lady Isabella Winterhaven's hidden concerns, you may roll an Insight check. The difficulty is moderate (DC 15). A successful check reveals a subtle unease in her eyes, hinting that something troubles her beneath the surface.*

Isabella's Revelation:

*As you engage in polite conversation with the Winterhavens, you notice a momentary lapse in Lady Isabella's composure. Seizing the opportunity, you delicately inquire if everything is well. With a thoughtful gaze, she confides in you.*

**Isabella:** *My apologies if I seem preoccupied. It's Clara, our daughter. She has been acting peculiarly since receiving a toy nutcracker from her godfather, Master Thistledown as an early yule gift. Clara speaks of dreams and fantastical tales. She has also been apparently feeding feeding her dolls and sweets to the mice.*

After engaing with the winterhavens read the following

*With Lady Isabella's revelation lingering in the air, the night unfolds in a tapestry of joy and festivity. The great hall resounds with laughter, music, and the clinking of goblets. Duke Fredrick, keen on ensuring the merriment prevails, calls for more lively entertainment. Minstrels play upbeat tunes, and a group of skilled dancers takes the floor, captivating the audience with their agile movements.*

# Frantic Search at Dawn

As dawn breaks over Castle Sliberberg, a frantic energy pulses through the once tranquil halls. The players, stirring from their chambers, are met with an unexpected sight — Lady Isabella Winterhaven, her eyes wide with worry, hurrying through the castle corridors in her nightgown. The echoes of her desperate calls for Clara reverberate through the stone walls.

When the players intercept Lady Isabella, her distress is palpable. With a quivering voice, she explains the shocking turn of events. Read the following

**Isabella:** *Oh, you must help us! Clara, our dear daughter, has vanished without a trace. I awoke to find her room empty, her bed untouched. I fear the worst, and my heart is heavy with dread. Please, we need your assistance. Clara has never acted like this before, and with the strange dreams and that peculiar nutcracker... I cannot shake the feeling that her disappearance is tied to the enchantments that have woven themselves into our lives.*

The urgency in Lady Isabella's plea hangs in the air, casting a shadow over the once-celebratory castle. The players, now faced with a mystery that transcends the festive atmosphere, must decide how to proceed and unravel the enigma surrounding Clara's sudden disappearance.


# The Investigation Unfolds

*With Lady Isabella's desperate plea ringing in their ears, the players embark on an investigation to unravel the mystery behind Clara's disappearance. The castle, once filled with the echoes of merriment, now becomes a labyrinth of potential clues and hidden secrets.*

1. **Clara's Guest Suite:** The players begin their investigation in Clara's guest suite, where they find a room untouched since the night before. The Nutcracker, a focal point on the dollhouse, emanates a faint magical aura. 
   
    - *Perception Check (DC 12):* Observing the Nutcracker closely, players notice that its eyes gleam with a subtle magical luminescence, indicating a connection to the fey.

    - *Arcana Check (DC 18):* Characters well-versed in magical lore recognize faint traces of Feywild magic on the dollhouse. This insight suggests that the portal may lead to Märchenweltgrenze, a realm where fey enchantments hold sway.

    - *Stealth Check (DC 20):* Characters with proficiency in Stealth may discover that one of the dollhouse panels is slightly ajar. Crazy as it might seem. It seems Clara may have crawled inside, leaving it slightly open in her haste.

2. **Interviewing Castle Staff:** Interacting with the castle staff yields scattered information. Some recall hearing faint sounds during the night, others mention whispers of dreams and enchantment. The staff emphasizes Clara's affinity for the Nutcracker and her recent peculiar behavior. Players can use Insight or Persuasion checks (DC 15) to coax additional details, learning that no one saw Clara leave her room that night.

3. **Tracing Master Thistledown:** Heading to the west ward of Sliberberg, the players find Master Thistledown's workshop. The gnome, is not present. Instead, a sign hangs from his door reads in flowing script, "Out for a Spell. Back when the need is most dire." When Master Thistledown returns carrying a stack of bundles, he seems coy when questioned. With a sly smile, he suggests the players give Clara's room a thorough search after dark, hinting at secrets yet to be unveiled.



# Clara's Room at Nightfall_

As the moon graces the sky with its silvery glow, the players find themselves standing outside Clara's guest suite. The once-festive castle halls now echo with an eerie stillness, interrupted only by the occasional distant laughter from the dwindling celebrations. The room lies before them, silent and foreboding. Those with the keenest senses among the party notice a faint, ethereal glow emanating from the dollhouse tucked in the corner.

**_Encounter Rules:_**

*As the players cautiously approach the dollhouse, the glow becomes more pronounced. The room seems to shimmer with a magical aura, and the dollhouse takes on an otherworldly radiance. Characters with a passive perception higher than 15 notice the glow intensifying, drawing their attention.*

*Players may choose to open the dollhouse, revealing a small, shimmering portal within. The portal, connected to Märchenweltgrenze, beckons with a mesmerizing allure. The characters can decide whether to enter the portal and pursue Clara or take other actions before stepping into the fey-infused realm.*

**_Player Actions:_**
1. **Investigate Further (Investigation Check, DC 15):** Characters may choose to investigate the room for additional clues before entering the portal. A successful check unveils a diary on Clara's bedside table, hinting at her fascination with dreams and fairy tales. This newfound information may provide insight into her mindset.


3. **Enter the Portal:** If the players choose to enter the portal, they find themselves transported to the enchanting Land of Dolls. Here, they navigate through fey-infused landscapes, encountering whimsical creatures and uncovering the mysteries woven into Märchenweltgrenze.

**_Consequences:_**
- Failing to notice the glow or neglecting to investigate may result in missed opportunities or challenges within Märchenweltgrenze.
- Depending on their actions, the players may uncover more about Clara's connection to the fey realm and the consequences of her journey.

# The land of dolls
When the players enter the land of dolls read the following
*You find yourselves standing in a land bathed in the soft glow of twilight. The sky above is a canvas of pastel hues, adorned with stars that seem to dance with a playful light. The ground beneath your feet is a carpet of vibrant flowers that release a sweet, intoxicating fragrance with every step. Trees, adorned with glowing blossoms, stand like ancient sentinels, their branches forming intricate archways.*

**_Lost in Whimsy: Seeking Direction in Märchenweltgrenze_**

*As the players step into the enchanting realm of Märchenweltgrenze, the landscape stretches endlessly before them, a kaleidoscope of fantastical sights. The air is thick with magic, making navigation challenging. The initial sense of disorientation sets in, and the players find themselves surrounded by a dreamscape that defies conventional logic.*


# Lost in Whimsy

Amidst the swirling colors and shimmering trees, the players must devise a plan to find their bearings and discern a direction. The air is filled with the gentle hum of magic, and whispers of unseen creatures beckon them to explore. Here are potential actions the players may take to navigate this dreamlike landscape:

1. **Observation (Perception Check, DC 15):** Characters may observe the surrounding environment, seeking any distinguishing features or patterns in the landscape. A successful check reveals subtle shifts in the colors of the sky or the alignment of peculiarly shaped trees that might serve as landmarks.

2. **Harmony with Nature (Nature Check, DC 18):** Characters may attune themselves to the natural elements of Märchenweltgrenze. By connecting with the magical essence of the land, they can discern the subtle cues that guide them. Success provides a sense of the general cardinal directions.

3. **Whispered Guidance (Wisdom Saving Throw, DC 14):** The players may close their eyes and attune their senses to the whispers of the magical creatures around them. A successful saving throw allows them to pick up on ethereal guidance, leading them in a specific direction.

4. **Inventive Navigation (Intelligence Check, DC 16):** Characters may use their intelligence to devise a method of tracking their movement, perhaps by leaving markers or interpreting the magical flora's behavior. Success provides a makeshift compass to guide them.

**_Revelation: The Distant Castle Towers_**

As the players strive to make sense of their surroundings, a sudden break in the landscape reveals the distant silhouette of castle towers on the horizon. The enchanting spires rise above the whimsical landscape, serving as a beacon in the dreamlike expanse.

# Vermin Ambush:

*As yoy traverse the magical landscape, a seemingly ordinary road unfolds before you. The air crackles with a sudden tension, and without warning, the foliage on either side rustles as a horde of giant mice emerges. These oversized rodents, with gleaming eyes and razor-sharp teeth, approach with aggressive intent. Their chittering sounds unnaturally aggressive, and their collective gaze fixes on you as they threaten to devour their unexpected intruders.*


##  Giant Mice Ambush

The players find themselves facing a swarm of giant mice. The combatants vary in size, some as large as a dog, and others towering over the adventurers. The chittering horde converges, hungry for a confrontation.

**_Enemy Composition:_**
1. **Giant Mice (x6):** Medium-sized creatures with enhanced strength and agility. They attack with bites and claws, aiming to overwhelm the players through sheer numbers.
   
2. **Giant Mouse Alpha (x1):** The largest and most aggressive of the mice, serving as the leader of the swarm. It boasts increased hit points and delivers more potent attacks.

**_Terrain:_**
The battle takes place on the enchanted road, surrounded by towering, glowing trees and whimsical flora. The players may leverage the magical environment for strategic advantages.

**_Combat Objectives:_**
- **Survival:** The primary objective is to survive the onslaught of the giant mice and eliminate the threat they pose.
  
- **Alpha Defeat:** Defeating the Giant Mouse Alpha weakens the morale of the swarm, providing a potential opportunity for negotiation or intimidation.

**_Scaling the Encounter:_**
- **Level 6-8:** Reduce the number of giant mice to 4 and lower the Giant Mouse Alpha's hit points.
  
- **Level 9-10:** Keep the original composition.
  
- **Level 11-12:** Increase the number of giant mice to 8 and enhance the Giant Mouse Alpha's abilities.

**_Enemy Tactics:_**
- **Swarming Attacks:** The giant mice attempt to overwhelm individual players through sheer numbers.
  
- **Alpha's Roar:** The Giant Mouse Alpha emits a terrifying roar, potentially causing fear among the players.

**_Conclusion:_**
As the players fend off the aggressive giant mice, the enchanting landscape serves as a backdrop to this unexpected skirmish. Once victorious, they may choose to examine the aftermath, uncovering potential clues or items that shed light on the fey-infused challenges awaiting them in Märchenweltgrenze.*

## Siege of the Enchanted Castle:

*As you approach the towering castle on the horizon, the air echoes with the clatter of tiny weapons and the chittering of an approaching horde. The castle, seemingly out of a fairy tale, is under siege by an army ogiant mice. The tiny defenders valiantly fight against an overwhelming force of giant mice, led by a formidable Nutcracker. The fate of the castle hangs in the balance as you find themselves thrust into the midst of this fantastical conflict.*


The players join the fray, arriving just in time to aid the beleaguered toy defenders. The enchanting battlefield is marked by the clash of miniature armies, the castle walls serving as the last line of defense against the relentless onslaught.

**_Enemy Composition:_**
1. **Toy Pikemen (x8):** Small, animated toy soldiers armed with tiny spears. They move with surprising speed and precision, attacking in coordinated formations.

2. **Toy Men at Arms (x4):** Larger toy soldiers wielding miniature swords and shields. They provide a sturdier front line and have increased hit points.

3. **Toy Crossbowmen (x4):** Animated toy figures armed with tiny crossbows. They attack from a distance, targeting players with precision.

4. **Giant Mouse Horde (x12):** A relentless swarm of giant mice, adding chaos to the battlefield.

5. **Nutcracker Commander:** The Nutcracker, adorned in regal attire, leads the toy soldiers. He boasts enhanced combat abilities and serves as the tactical mind behind the enchanted army.

**_Terrain:_**
The battle unfolds in the courtyard of the enchanted castle, surrounded by shimmering towers and fairy-tale architecture. Players can navigate the battlefield strategically, utilizing cover and vantage points.

**_Combat Objectives:_**
- **Toy Army Defense:** The primary objective is to defend the enchanted toy soldiers and assist in repelling the giant mouse horde.

- **Nutcracker Commander Defeat:** Defeating the Nutcracker Commander weakens the morale of the toy army and disrupts the coordination of the attackers.

**_Scaling the Encounter:_**
- **Level 6-8:** Reduce the number of toy soldiers by half and lower the hit points of the Nutcracker Commander.
  
- **Level 9-10:** Keep the original composition.
  
- **Level 11-12:** Increase the number of toy soldiers and enhance the Nutcracker Commander's abilities.

**_Enemy Tactics:_**
- **Formation Attacks:** The toy soldiers coordinate attacks, utilizing formation tactics to gain an advantage.

- **Crossbow Suppression:** Toy Crossbowmen focus on providing cover fire to suppress the giant mouse horde and create openings for the players.

- **Nutcracker's Leadership:** The Nutcracker Commander strategically directs the toy soldiers, providing buffs and bonuses to their attacks.

# The throne room
When the players enter the castle read the following
*As you step into the regal confines of the castle's throne room, your eyes are drawn to a sight straight from a storybook. Clara, dressed in attire befitting a princess, stands at the throne, her gaze a mix of determination and trepidation. The room, adorned with sparkling tapestries and ethereal glow, bears witness to both enchantment and uncertainty.*

*In a voice that echoes with both innocence and bravery, Clara begins her tale. "I saw the Nutcracker come to life," she explains, her eyes reflecting the fantastical events she witnessed. "He led my siblings' toys into battle against the mice and their dreaded leader, the 7-headed Mouse King."*

*"I threw my shoe at the Mouse King when he nearly captured the Nutcracker," she continues, a glint of defiance in her eyes. "He demanded my dolls and sweets in exchange for the Nutcracker's life, and I agreed. But he wanted more."*

*Suddenly, the grand doors creak open, revealing the Nutcracker himself, regal and resolute, flanked by a squad of animated toy Men at Arms. "It's not looking good," the Nutcracker announces, his voice tinged with urgency. "The mice are forcing their way through the defenses, and we're in dire need of aid."*

## The 7-Headed Mouse King's Onslaught_**

**_The 7-Headed Mouse King's Onslaught_**

*As Clara concludes her tale in the throne room, the air thickens with tension. A malevolent presence permeates the grand chamber, heralding the arrival of the formidable 7-Headed Mouse King. The doors, previously guarded, creak open under the relentless pressure of the horde outside. The monstrous rodent, crowned with seven menacing heads, forces its way into the room, determined to confront those who dared defy its demands.*

---

**_Combat Encounter: Confrontation with the 7-Headed Mouse King_**

*The players find themselves facing the towering and nightmarish form of the 7-Headed Mouse King. Each of its heads hisses and snarls with malicious intent, and its massive, gnarled body seems almost too large for the confines of the throne room. The Nutcracker, Clara, and the toy defenders prepare for the impending battle against this fey-infused adversary.*

**_Enemy Composition:_**
1. **7-Headed Mouse King:** A colossal, multi-headed rodent with distinct abilities for each head. Each head possesses unique attacks and traits, making the Mouse King a formidable adversary.

2. **Swarm of Giant Mice (x4):** The Mouse King commands additional giant mice, adding chaos to the battlefield.

3. **Toy Defender Reinforcements:** Clara's toy defenders join the fray, offering support to the players. These include Toy Pikemen, Toy Men at Arms, and Toy Crossbowmen.

**_Terrain:_**
The throne room serves as the battleground, adorned with regal tapestries and ornate pillars. The confined space challenges the players to navigate strategically.

**_Combat Objectives:_**
- **Defeat the Heads:** The primary objective is to defeat each of the 7 heads of the Mouse King. Each head must be targeted individually to weaken the creature.

- **Protect Clara:** Clara's safety is paramount. If she falls in battle, the toy defenders' morale may waver, making the encounter more challenging.

**_Scaling the Encounter:_**
- **Level 6-8:** Reduce the number of heads on the Mouse King to 5 and lower its hit points.
  
- **Level 9-10:** Keep the original composition.
  
- **Level 11-12:** Increase the number of heads on the Mouse King to 9 and enhance its abilities.

**_Enemy Tactics:_**
- **Multi-Headed Assault:** The Mouse King attacks with multiple heads, each head having a unique attack pattern.
  
- **Summoning Swarms:** The Mouse King summons additional giant mice to overwhelm the players.
  
- **Head-Specific Traits:** Each head possesses a specific trait, such as breath attacks, venomous bites, or magical abilities.


*As the doors burst open, the air is filled with the overwhelming scent of fur and malice. The 7-Headed Mouse King, crowned in grotesque majesty, forces its way into the throne room. The Nutcracker assumes a battle-ready stance and commands, "Clara, run! Get away as far as you can." However, Clara, gripping her makeshift sword, defiantly responds, "No! You cannot leave. I love you." The toy defenders, animated with fey energy, prepare for the ultimate confrontation.*

*The Mouse King's seven heads fixate on the intruders, and a chorus of hisses fills the air. The battle for Märchenweltgrenze reaches its zenith as the players brace themselves to face this nightmarish adversary, knowing that the fate of the enchanted castle hinges on the outcome of this fey-infused clash.*

The players find themselves facing the towering and nightmarish form of the 7-Headed Mouse King. Each of its heads hisses and snarls with malicious intent, and its massive, gnarled body seems almost too large for the confines of the throne room. The Nutcracker, Clara, and the toy defenders prepare for the impending battle against this fey-infused adversary.

**_Enemy Composition:_**
1. **7-Headed Mouse King:** A colossal, multi-headed rodent with distinct abilities for each head. Each head possesses unique attacks and traits, making the Mouse King a formidable adversary.

2. **Swarm of Giant Mice (x4):** The Mouse King commands additional giant mice, adding chaos to the battlefield.

3. **Toy Defender Reinforcements:** Clara's toy defenders join the fray, offering support to the players. These include Toy Pikemen, Toy Men at Arms, and Toy Crossbowmen.

**_Terrain:_**
The throne room serves as the battleground, adorned with regal tapestries and ornate pillars. The confined space challenges the players to navigate strategically.

**_Combat Objectives:_**
- **Defeat the Heads:** The primary objective is to defeat each of the 7 heads of the Mouse King. Each head must be targeted individually to weaken the creature.

- **Protect Clara:** Clara's safety is paramount. If she falls in battle, the toy defenders' morale may waver, making the encounter more challenging.

**_Scaling the Encounter:_**
- **Level 6-8:** Reduce the number of heads on the Mouse King to 5 and lower its hit points.
  
- **Level 9-10:** Keep the original composition.
  
- **Level 11-12:** Increase the number of heads on the Mouse King to 9 and enhance its abilities.

**_Enemy Tactics:_**
- **Multi-Headed Assault:** The Mouse King attacks with multiple heads, each head having a unique attack pattern.
  
- **Summoning Swarms:** The Mouse King summons additional giant mice to overwhelm the players.
  
- **Head-Specific Traits:** Each head possesses a specific trait, such as breath attacks, venomous bites, or magical abilities.


## A Bittersweet Farewell

*As the echoes of battle subside, the throne room is left in a poignant silence. The 7-Headed Mouse King lies defeated, and the toy defenders slowly return to their inanimate states. Amidst the aftermath, the Nutcracker, once valiant and regal, now lays mortally wounded.*

*Clara, tears streaming down her face, rushes to the Nutcracker's side. His wooden form, once full of life, now shows the scars of battle. Weakly, the Nutcracker turns to Clara, his voice strained but filled with a desperate hope. "Did you mean it, Clara? When you said you loved me?"*

*Clara, overcome with sorrow, nods through her tears. "Yes, with all my heart." The Nutcracker's wooden face softens into a tender smile as he fades from existence. Clara, left kneeling beside the vanishing form, clutches at the remnants of a love that transcended the boundaries of reality.*


## A Tearful Journey Home

*You gather Clara into your arms, her weeping echoing in the now-silent throne room. The weight of the Nutcracker's sacrifice lingers in the air as you carry Clara back through the enchanted castle. The once-jubilant tapestries and sparkling pillars now feel like silent witnesses to a tale of love and loss.*

*As you approach the portal that brought you into Land of dolls, Clara's weeping becomes a mournful melody. The enchanted gateway looms before you, promising a return to the mundane world. Once through the portal, Clara's fairytale princess costume transforms back into the attire of a young noblewoman, her tiara the only remnant of the fantastical journey.*
# Back to reality
In Clara's room, the air is still and time seems to have stood still. The players, as if caught in a moment frozen in time, start to perceive the subtle signs that something is amiss. The flickering candle flames cast an unwavering glow, the time on the clock has barely changed even though they were gone for hours and the enchanted landscape outside Clara's window remains unchanged.
Clara, confirms the discrepancy. *"I stayed in the Land of Dolls for 18 days," she whispers*

As the revelation settles in, distant sounds begin to reach their ears—a joyous commotion emanating from the great hall. Laughter, music, and the clinking of goblets paint a vivid picture of a celebration in full swing. The players, standing in Clara's room, feel a subtle pull toward the heart of the castle, where the feasting and merriment are unfolding. It becomes clear to the players that they need to bring Clara to the great hall thaat is where her parents are most likely to be. Read the following when they enter the great hall

*The doors swing open, revealing a scene of jubilation. Nobles and courtiers revel in the yuletide festivities, their voices rising in joyous harmony. The aroma of roasted meats and spiced mulled wine permeates the air, creating an atmosphere of warmth and camaraderie. Clara's parents, Count and Countess Winterhaven, stand near the royal dais, engaged in conversation with other guests. As Clara steps into the great hall, her parents turn toward her, their expressions a blend of relief and concern. The room falls momentarily silent as Clara's eyes meet her parents', a shared understanding passing between them. The Countess rushes forward, enveloping Clara in a tender embrace. "My dear, you're back!" she exclaims, her voice a mix of joy and worry. The Count, a stoic figure, joins the embrace, and for a moment, the family is reunited in the midst of the festive revelry.*

The beautiful moment is interrupted by the slightly frail but jolly laugh. The players find that Master Thistledown is attending the feast. If asked the Winterhavens invited him to the castle to help find their daughter. He now clears his throat as if to speak. Read the following.

*Master Thistledown eyes twinkle with an impish delight as he prepares to make a surprising introduction."Ah, my dear friends!" Master Thistledown exclaims, his voice carrying a playful cadence. " Allow me to introduce you to another of my godchildren, who, alas, was unfortuantly late in arriving at the festivities." With a flourish, Master Thistledown signals to a young man standing nearby, his features strikingly resembling a human version of the Nutcracker from the Land of Dolls. The resemblance is uncanny. "This, my dear friends," Master Thistledown announces with a theatrical bow, "is young Percival, a charming lad and the latest addition to my godchildren's list. As Percival steps forward, Clara's eyes widen in surprise and joy. The young man leans in and whispers something in Clara's ear, causing her face to light up with a radiant smile. The two exchange a few more words, and then, hand in hand, they stroll away from the bustling great hall.*
If asked about anything that had happened over the last two days simply smiles and gives cryptic answers that hint that Percival was suffering from a curse. He also gives you a **bag of toy soldiers** as a gift

# Appendexs
## Encounters

### Encounter with Panthor Silverhoof at the Yule Festival

As players meander through the bustling Yule Festival in Sliberberg's market ward, the air is filled with laughter, music, and the enchanting aroma of winter treats. Amidst the festivities, a vibrant satyr named Panthor Silverhoof, adorned in festive attire and carrying a holly-covered staff, approaches the players with a warm and infectious grin.

**Panthor Silverhoof:**
Panthor's fur, a rich blend of earthy browns and greens, seems to shimmer in the festival lights. His mischievous eyes twinkle with merriment, and the jingling bells adorning his clothing add a playful melody to his every step.

**Interaction:**
As Panthor recognizes the players, he gracefully sidles up to them, staff in hand, and executes a flourishing bow.

*"Ah, lords and ladyships! What a joy it is to find you amidst the revelry! Panthor Silverhoof, at your service and the master of ceremonies for this splendid Yule Festival. The mirth is contagious, is it not? I couldn't help but seek your esteemed company in such jubilant times."*

Panthor takes a moment to glance around at the lively festival, a joyful glint in his eyes.

*"And might I say, you all have the air of individuals who appreciate the finer arts. Have you considered gracing the hallowed halls of the Hillside Amphitheater with your presence? A tale or two told on our stage would undoubtedly add to the richness of our cultural tapestry. Picture it—a performance that echoes through the ages, immortalized in the hearts of Sliberberg's denizens."*

**Possible Player Responses:**
1. **Expressing Interest:**
   - *Enthusiastically agree to consider attending the Hillside Amphitheater and inquire about the upcoming performances.*

2. **Diplomatic Response:**
   - *Politely acknowledge Panthor's invitation, expressing the need to check their schedule but expressing genuine interest.*

3. **Curiosity:**
   - *Express curiosity about the current shows at the Hillside Amphitheater and inquire about the types of performances featured.*

4. **Playful Banter:**
   - *Engage in playful banter with Panthor, expressing delight at the festival's ambiance and subtly deflecting the invitation.*

5. **Decline Gracefully:**
   - *Politely decline the invitation, citing other festival activities or prior engagements.*

Regardless of their response, Panthor remains in high spirits, sharing anecdotes about the festival, praising the players' noble stature, and extending warm wishes for a joyous Yule celebration. As the encounter concludes, Panthor dances away into the lively crowd, leaving a trail of laughter and goodwill in his wake.

### Encounter with Whimsy Glade at the Yule Festival

Amidst the lively Yule Festival in Sliberberg, the players encounter a whimsical and unexpected entity named Whimsy Glade. As they navigate the bustling market ward, a seemingly ordinary neighborhood pops into existence, complete with cozy houses, blooming gardens, and the delightful sounds of laughter and merriment.

**Whimsy Glade:**
Appearing like a quaint and charming suburban neighborhood, Whimsy Glade exudes an air of animated enthusiasm. Signs, windows, and even mailboxes come to life with vibrant text, each expressing the joyful personality of the sentient village. Whimsy Glade uses its various elements to communicate, creating an engaging and lively presence.

**Interaction:**
The players find themselves in the heart of Whimsy Glade, where the houses seem to giggle and the lampposts sway playfully. Text on signs and windows sparkles with an effervescent energy as Whimsy Glade addresses the adventurers.

*"Oh my stars! Welcome to Whimsy Glade, darlings! Isn't this just the most faaaaaabulous little village you ever did see? Teehee! I'm positively quaking with excitement to be here in the midst of your charming festivities!"*

Whimsy Glade's exuberance is infectious, and the neighborhood itself seems to join in the revelry, with animated flowers nodding in agreement and friendly picket fences dancing.

**Possible Player Responses:**
1. **Embrace the Whimsy:**
   - *Engage with Whimsy Glade's playful energy, expressing delight at the magical village's appearance and inquiring about its experiences in Sliberberg.*

2. **Curiosity:**
   - *Express curiosity about Whimsy Glade's origin, abilities, and the reason for its visit to the Yule Festival.*

3. **Polite Interaction:**
   - *Politely acknowledge Whimsy Glade's presence, asking about its unique abilities and any interesting happenings within the magical village.*

4. **Play Along:**
   - *Play along with the whimsical nature of Whimsy Glade, adopting a lighthearted tone and sharing in its animated enthusiasm.*

5. **Concerned Inquiry:**
   - *Express concern about the potential impact of Whimsy Glade's presence on the festival and the reactions of the townsfolk.*

Whimsy Glade responds to the players with giddy delight, sharing stories of its adventures, the quirky residents of the village, and its sheer joy at being part of the Yule Festival. As the encounter unfolds, players may find themselves entangled in the whimsical charm of this sentient neighborhood, leaving them with a unique and enchanting memory of their time at the festival.

## Creatures
**Nutcracker**

*Medium Construct (Fey), Neutral Good*

**Armor Class** 17 (Natural Armor)  
**Hit Points** 80 (10d8 + 40)  
**Speed** 30 ft.

**STR** 18 (+4)  
**DEX** 12 (+1)  
**CON** 18 (+4)  
**INT** 10 (0)  
**WIS** 12 (+1)  
**CHA** 10 (0)

**Skills** Perception +3  
**Damage Immunities** Poison  
**Condition Immunities** Poisoned, Charmed, Frightened  
**Senses** Darkvision 60 ft., Passive Perception 13  
**Languages** Common, Sylvan (understands but can't speak)

**Challenge** 4 (1,100 XP)

**Magic Resistance.** The Nutcracker has advantage on saving throws against spells and other magical effects.

**Actions**

**Multiattack.** The Nutcracker makes two attacks: one with its Bite and one with its Sword.

**Bite.** *Melee Weapon Attack:* +6 to hit, reach 5 ft., one target. *Hit:* 10 (1d10 + 4) piercing damage.

**Sword.** *Melee Weapon Attack:* +6 to hit, reach 5 ft., one target. *Hit:* 12 (2d6 + 4) slashing damage.

**Enchanted Defense (Recharge 5-6).** The Nutcracker gains a +2 bonus to AC until the start of its next turn.

**Legendary Actions**

The Nutcracker can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time and only at the end of another creature's turn. The Nutcracker regains spent legendary actions at the start of its turn.

**Bite.** The Nutcracker makes one Bite attack.

**Crack the Nut (Costs 2 Actions).** The Nutcracker swings its sword with a mighty blow, making a Sword attack.

**Enchanted Leap (Costs 3 Actions).** The Nutcracker magically leaps up to 30 feet to an unoccupied space it can see.

*Note: Adjust the challenge rating based on your party's level and the desired difficulty of the encounter.*

### Toy Pikeman

*Medium Construct (Fey), Neutral*

**Armor Class** 15 (Natural Armor)  
**Hit Points** 16 (3d8 + 3)  
**Speed** 30 ft.

**STR** 12 (+1)  
**DEX** 10 (0)  
**CON** 12 (+1)  
**INT** 1 (-5)  
**WIS** 10 (0)  
**CHA** 1 (-5)

**Skills** Perception +2  
**Damage Immunities** Poison  
**Condition Immunities** Poisoned, Charmed, Frightened  
**Senses** Darkvision 60 ft., Passive Perception 12  
**Languages** --

**Challenge** 1/2 (100 XP)

**Actions**

**Spear.** *Melee Weapon Attack:* +3 to hit, reach 5 ft., one target. *Hit:* 5 (1d8 + 1) piercing damage.

---

### Toy Men at Arms

*Medium Construct (Fey), Neutral*

**Armor Class** 16 (Natural Armor)  
**Hit Points** 22 (4d8 + 4)  
**Speed** 30 ft.

**STR** 14 (+2)  
**DEX** 12 (+1)  
**CON** 12 (+1)  
**INT** 1 (-5)  
**WIS** 10 (0)  
**CHA** 1 (-5)

**Skills** Perception +2  
**Damage Immunities** Poison  
**Condition Immunities** Poisoned, Charmed, Frightened  
**Senses** Darkvision 60 ft., Passive Perception 12  
**Languages** --

**Challenge** 1 (200 XP)

**Actions**

**Longsword.** *Melee Weapon Attack:* +4 to hit, reach 5 ft., one target. *Hit:* 7 (1d8 + 2) slashing damage.

---

### Toy Crossbowman

*Medium Construct (Fey), Neutral*

**Armor Class** 14 (Natural Armor)  
**Hit Points** 13 (3d8)  
**Speed** 30 ft.

**STR** 10 (0)  
**DEX** 14 (+2)  
**CON** 10 (0)  
**INT** 1 (-5)  
**WIS** 10 (0)  
**CHA** 1 (-5)

**Skills** Perception +2  
**Damage Immunities** Poison  
**Condition Immunities** Poisoned, Charmed, Frightened  
**Senses** Darkvision 60 ft., Passive Perception 12  
**Languages** --

**Challenge** 1/4 (50 XP)

**Actions**

**Crossbow.** *Ranged Weapon Attack:* +4 to hit, range 80 ft./320 ft., one target. *Hit:* 5 (1d8 + 2) piercing damage.

### Swarm of Giant Mice

*Small Beast, Unaligned*

**Armor Class** 10  
**Hit Points** 36 (8d6 + 8)  
**Speed** 30 ft.

**STR** 9 (-1)  
**DEX** 12 (+1)  
**CON** 12 (+1)  
**INT** 2 (-4)  
**WIS** 10 (0)  
**CHA** 2 (-4)

**Senses** Darkvision 30 ft., Passive Perception 10  
**Languages** --

**Challenge** 1/2 (100 XP)

**Traits**

**Pack Tactics.** The swarm has advantage on an attack roll against a creature if at least one of the swarm's allies is within 5 feet of the creature and the ally isn't incapacitated.

**Swarm.** The swarm can occupy another creature's space and vice versa, and the swarm can move through any opening large enough for a Tiny mouse. The swarm can't regain hit points or gain temporary hit points.

**Actions**

**Bites.** *Melee Weapon Attack:* +3 to hit, reach 0 ft., one target in the swarm's space. *Hit:* 10 (4d4) piercing damage, or 5 (2d4) piercing damage if the swarm has half of its hit points or fewer.

---

### Giant Mouse

*Tiny Beast, Unaligned*

**Armor Class** 12  
**Hit Points** 5 (2d4)  
**Speed** 30 ft.

**STR** 2 (-4)  
**DEX** 14 (+2)  
**CON** 11 (0)  
**INT** 2 (-4)  
**WIS** 12 (+1)  
**CHA** 2 (-4)

**Senses** Darkvision 30 ft., Passive Perception 11  
**Languages** --

**Challenge** 1/8 (25 XP)

**Actions**

**Bite.** *Melee Weapon Attack:* +0 to hit, reach 5 ft., one target. *Hit:* 1 piercing damage.

---

### Giant Mouse Alpha

*Tiny Beast, Unaligned*

**Armor Class** 13  
**Hit Points** 11 (4d4 + 4)  
**Speed** 30 ft.

**STR** 4 (-3)  
**DEX** 16 (+3)  
**CON** 12 (+1)  
**INT** 3 (-4)  
**WIS** 12 (+1)  
**CHA** 3 (-4)

**Senses** Darkvision 30 ft., Passive Perception 11  
**Languages** --

**Challenge** 1/4 (50 XP)

**Pack Tactics.** The mouse has advantage on an attack roll against a creature if at least one of the mouse's allies is within 5 feet of the creature and the ally isn't incapacitated.

**Actions**

**Bite.** *Melee Weapon Attack:* +0 to hit, reach 5 ft., one target. *Hit:* 3 (1d4 + 1) piercing damage.

### 7-Headed Mouse King

*Medium Monstrosity (Fey), Chaotic Evil*

**Armor Class** 16 (Natural Armor)  
**Hit Points** 120 (16d8 + 48)  
**Speed** 30 ft.

**STR** 18 (+4)  
**DEX** 14 (+2)  
**CON** 16 (+3)  
**INT** 8 (-1)  
**WIS** 10 (0)  
**CHA** 12 (+1)

**Saving Throws** DEX +5  
**Senses** Darkvision 60 ft., Passive Perception 10  
**Languages** Common, Sylvan  
**Challenge** 7 (2,900 XP)

**Multiattack.** The Mouse King makes three attacks: one with its Bite and two with its Claws.

**Bite.** *Melee Weapon Attack:* +7 to hit, reach 5 ft., one target. *Hit:* 15 (2d8 + 4) piercing damage.

**Claw.** *Melee Weapon Attack:* +7 to hit, reach 5 ft., one target. *Hit:* 12 (2d6 + 4) slashing damage.

**Swarm of Mice (Recharge 5-6).** The Mouse King releases a swarm of giant mice in a 20-foot radius centered on itself. Each creature in that area must make a DC 15 Dexterity saving throw, taking 28 (8d6) piercing damage on a failed save, or half as much damage on a successful one.

**Legendary Actions**

The Mouse King can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time and only at the end of another creature's turn. The Mouse King regains spent legendary actions at the start of its turn.

**Bite.** The Mouse King makes one Bite attack.

**Claw (Costs 2 Actions).** The Mouse King makes one Claw attack.

**Summon Mice (Costs 3 Actions).** The Mouse King summons a swarm of giant mice. The swarm appears in an unoccupied space within 30 feet of the Mouse King.

## Magic items

### Bag of Toy Soldiers

*Wondrous Item (bag), Rare (requires attunement)*

This whimsical bag is adorned with colorful embroidery and depicts scenes of toy soldiers marching in formation. When you attune to the Bag of Toy Soldiers, you gain the ability to summon living toy soldiers from within the bag.

**Summon Toy Soldiers.** As an action, you can reach into the bag and summon a squad of toy soldiers. The squad consists of four toy soldiers, each transforming into a Medium-sized living construct for 1 hour. The living constructs are friendly to you and your allies and follow your verbal commands.

## Npcs