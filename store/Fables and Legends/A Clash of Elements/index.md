---
title: Fables and Legends of the Märchenweltgrenze  A Clash of Elements 
date: "2023-11-16T22:40:33.169Z"
description: For 5 players of level 6-9
---

# Background

# Starting the adventure


*As you traverse through the winding Hochtal Notch, the air gradually becomes crisper, and the scent of pine fills your senses. Emerging from the mountainous pass, you're greeted by a breathtaking sight—the Bergseeufer Valley. A sparkling mountain lake stretches out before you, its pristine waters mirroring the snow-capped peaks that encircle the valley. The tranquility of the scene is marred by the recent aftermath of a fierce battle, evident in the scorch marks and frozen patches that scar the landscape.*

It takes another 3 hours traveling though the valley to reach the village. As they travel they pass through groves chared to cinders and massive drifts of slowly melting ice

# welcome to the Village of Bergseeufer:

*Nestled along the serene shores of the sparkling mountain lake, the village of Bergseeufer exudes a quaint charm that belies the recent tumultuous events that have unfolded in its vicinity. The scent of freshly baked bread mingles with the crisp mountain air as you approach the heart of the settlement. The sound of a babbling brook that feeds into the lake adds a soothing melody to the surroundings.*

**1. Marketplace Square:**
The central hub of Bergseeufer, the Marketplace Square, is a lively space where villagers gather to trade goods and share stories. Colorful stalls adorned with vibrant fabrics and seasonal produce line the square. 

**2. Greta Bakery:**
Situated near the Marketplace Square, Greta Bakery is a charming establishment owned by Greta Wolkenschein. The delightful aroma of magically infused pastries and bread wafts through the air, drawing locals and visitors alike.

**3. Bergseeufer Tavern:**
Overlooking the lake, the Bergseeufer Tavern is a cozy retreat where locals gather to share tales and unwind. The establishment boasts a collection of dragon-themed decor, reflecting the village's unique circumstance. The recent battles between the dragons have left patrons with tales of awe and fear, creating a surreal backdrop for the village's social center.

**4. Lakeside Docks:**
Stretching out along the lake's edge, the Lakeside Docks serve as a hub for fishing and trade. Colorful boats bob gently on the water, and fishermen mend their nets in preparation for the day's catch. The recent dragon battles have disrupted the usually tranquil fishing routine, casting an air of uncertainty over the once-thriving lakeside activities.

**5. Silvermoon Apothecary:**
A quaint apothecary, the Silvermoon Apothecary, is run by a wise herbalist named Elara. Known for her knowledge of both mundane and magical remedies, Elara provides healing salves and potions to the villagers. The apothecary has become a focal point for those affected by the aftermath of the dragon battles, seeking both physical and emotional solace.

# THe dragon duel

*The air crackles with tension as you enter the village of Bergseeufer. The echoes of your footsteps are drowned out by a sudden rush of wind, and you instinctively look skyward. Above the sparkling mountain lake, the azure sky transforms into a battleground as the silver and red dragons,clash once again.Their massive forms dance in the air, scales gleaming with an otherworldly brilliance as they circle each other. The villagers, well-acquainted with the routine, rush for cover, their expressions a mixture of fear and resignation. The dragons' roars reverberate through the valley, shaking the very foundations of Bergseeufer.Flashes of silver and bursts of flame illuminate the sky, casting an ethereal glow over the village. The lake below ripples with the shockwaves of their aerial acrobatics, creating a mesmerizing display of elemental prowess. As the dragons unleash their breath weapons, a frozen mist and searing heat clash, creating a brief, ephemeral fog that envelops the lakeside.It becomes clear that the dragons' territorial dispute is far from resolved, and the village of Bergseeufer once again finds itself caught in the crossfire of this titanic struggle. The villagers, looking to the sky with a mix of awe and trepidation, turn to you, the newly arrived heroes, seeking a solution to the chaos that unfolds above.*

# What the villagers know

1. **Marketplace Vendor (Gretchen, a Fruit Vendor):**
   - "Aye, the dragons, they did! Came swoopin' in like a tempest, they did. Scared poor ol' Schatzi half to death! Been here for decades, I have, and never seen nothin' like it. Every time they start their scrap, our fruits and veggies end up smashed or frozen solid. Can't make a decent apple pie without worryin' about dragon scuffles!"

2. **Local Fisherman (Johan, by the Lakeside Docks):**
   - "You askin' 'bout them dragons, eh? Been fishin' these waters for as long as I can remember. Waters used to be calm, but now it's like fishin' in a war zone. Dragons fightin' overhead, it scares the fish away. It's a tough time for us fishermen. Can't provide for the village like we used to."

3. **Tavern Patron (Klaus, a Regular at Bergseeufer Tavern):**
   - "Dragons? Oh, don't get me started! Bergseeufer used to be the quietest place in the mountains. Now? It's like living next to a volcano and an iceberg at the same time. The Bergseeufer Tavern used to be a place for merry gatherings, but now it's more like a bunker when those dragons start their roarin'."

4. **Elen Wolkenschein (at Elen's Enchanted Bakery):**
   - "Well, if it ain't the brave souls lookin' to tackle the dragon dilemma. Dragons showed up around the same time, causing all sorts of ruckus. My bakery is me pride and joy, but these dragon battles are affecting business, not to mention the safety of me patrons. Something's gotta be done, and I reckon you're the ones to do it."

5. **Village Elder (Hildegarde, Near the Marketplace Fountain):**
   - "The dragons, aye. They've disrupted our peaceful lives. Showed up near-simultaneous, they did. We've always known about their lairs, the silver to the north and the red to the south. Lately, it's been intolerable. We've sent missives, we've prayed, but we need a more decisive solution. The people of Bergseeufer can't take much more of this chaos."

# An unexpected guide
*As the sun begins to cast long shadows over the village of Bergseeufer, you find yourselves surrounded by the gentle hum of activity in the Marketplace Square. The conversations with the townsfolk have revealed the impact of the dragon conflict on their lives. It's in this moment that a young woman, appearing as an 18-year-old country girl with silver-blond hair, approaches you. She carries a basket filled with freshly picked alpine flowers. "Um, hi there," she begins somewhat timidly, her laughter carrying the melodic quality of mountain streams. "I'm Elen Wolkenschein. I couldn't help but overhear you talking to the villagers about the dragons. You seem like capable folks, and, well, I know the silver dragon personally. Her name is Elena, and she's actually quite nice." Elen gestures to the basket of alpine flowers, offering them with a warm smile. "I was just out picking these, and I thought maybe I could help. If you're interested, I can lead you to Elena's lair in the mountains. She might be willing to talk, and who knows, maybe we can find a way to settle this dragon dispute without more chaos."*
# Up the mountain

Elen leads out of the village along a rough foot track to the north.
## Worg Ambush in the Mountains:
Halfway through their journey players who succed on a dc 15 passive perception check notice a faint rusling and growls from the underbrush of the pine forest Elen is leading them through. Otherwise they will continue on before the encounter starts and the worgs will get a suprise round.

*As Elen guides you into the towering pines that blanket the mountains above Bergseeufer, the air takes on a crisp chill. The scent of pine needles surrounds you, creating an idyllic backdrop for the journey. Suddenly, a swift rustling echoes through the forest, accompanied by low, menacing growls that pierce the tranquility.Emerging from the underbrush, a pack of worgs, their fur a mix of mottled grays and browns, bursts forth with predatory intent. Elen, caught off guard, lets out a terrified scream and instinctively seeks refuge behind your group as the worgs close in.*

**Worg Pack:**
- The pack consists of six worgs, their yellow eyes gleaming with hunger and intelligence.
- The leader, distinguishable by its larger size and a scarred muzzle, takes a central position, coordinating the pack's movements.
- The worgs, sleek and agile, move with predatory grace as they circle, sizing up their prey.


## The lair of Elena
It takes anotherhour to reach the lair. Elen leads them up a small rise and there see the lair. Read the following
*As you approach the lair of Elena, the silver dragon, a sense of awe overtakes you. The abandoned wizard's tower rises majestically against the mountainous backdrop, its spires reaching towards the sky. Surrounding the tower are intricate ice sculptures, each a frozen tribute to heroes from history and legends. Their forms are preserved in a state of eternal glory, a testament to the magical prowess of the silver dragon who calls this place home. The tower is encased in walls made of solid mist, an ethereal barrier that seems both impenetrable and enchanting. The misty walls give the lair an otherworldly ambiance, with glimpses of the tower's silhouette visible through the shifting vapor. A gate, adorned with symbols of ancient arcane runes, stands as the entrance to this mythical domain.*

## The truth
As the players enter the outer wall of the lair the players who succeed on a passive perception check realize that Elen has stopped just outside the gate. Read the following
*As you step into the enigmatic lair, a hushed atmosphere envelops you, and the realization dawns that Elen has halted just beyond the gate. Turning to face you, she wears a look of sincerity tinged with a hint of sadness. Apologetically, she mutters a soft apology and twists a delicate ring on her finger. With a subtle shimmer, Elen undergoes a breathtaking transformation, her form shifting seamlessly into that of a young silver dragon.The young dragon gracefully moves forward, navigating the ruins of the wizard's tower's base with a majestic gait. Her scales gleam with a silvery luminescence, casting an ethereal glow upon the surroundings. As she settles upon a cushion made of mist, she confides, "There was no other way to communicate with you. The townsfolk, they are good people, but I harbor doubts about their tolerance for my presence in the town and heaven forbid that brute Rothgar finds yet another excuse to terrorize Bergseeufer."Seating herself upon the cushion, Elen's gaze fixates upon you with a mix of determination and vulnerability. "I seek your aid in ridding Bergseeufer of Rothgar's menacing presence," she implores, her voice resonating with a blend of dragon wisdom and the earnest plea of a creature caught between two worlds.*

Elena then goes on to explain her plan. She and Rothgar are equally matched although she openly admits that Rothgar is the better fighter. However he suspects that if he confronted with stronger opponent or stronger opposition he will cut his losses and run. She will ask the players to provide the little bit of extra force to tip the scales in her favor. The players might question her intentions at which she scoffs truely shocked at the allegations. She geniunly loves the village of Bergseeufer. She tells the players about all the times she went down to the town in her guise of ELen and all the fond memories of interacting with the town folk.


# Confronting Rothgar
The next day Elena in her guise of Elen leads the player characters to Rothgars lair. It is a 6 hour trek over increasingly rough terrian. The landscape become progressively more volcanic as they get closer. Elena will pause beside a large rock outside the cave entrance to ROthgar's lair. Read the following

*As you and Elana, disguised as Elen, stand before the imposing entrance to Rothgar's lair, a hushed tension hangs in the air. The cavern's entrance looms ahead, its foreboding archway adorned with ominous carvings warning of the fiery power that resides within. Elana, her guise concealing her true draconic nature, turns to you, her eyes reflecting a mix of determination and concern."Are you ready for this?" she inquires, her voice steady but carrying an undercurrent of anticipation. As you nod in affirmation, she deactivates a magical ring adorning her finger. The illusion that veils her draconic form dissolves, revealing the majestic silver dragon beneath.With a graceful ascent, Elana takes to the skies, her silver scales gleaming in the sunlight. She hovers above the entrance to Rothgar's lair and lets out a resounding roar that echoes through the mountainous terrain. The cavern seems to respond with a low, rumbling growl."Rothgar!" she commands, her voice echoing with authority. "Come out and face us!" Elana's roar dissipates into the mountainous echoes, a thunderous response emerges from the depths of Rothgar's lair. The air seems to shimmer with heat as the colossal form of the red dragon emerges into the open. Rothgar's wings unfurl, sending gusts of hot wind through the cavern, and he takes to the skies with a deafening bellow. "Bah! Elena, the silver pup, dares to challenge me?" Rothgar's voice booms, filled with arrogance and contempt. His words carry an air of entitlement, as if the very act of challenging him is beneath his consideration. As Rothgar descends from the heights, landing before Elana and the player characters, he puffs himself up to his full, imposing stature. His scales glow with an intense red hue, and he flexes his wings with a flourish. The cavern seems to groan under the weight of his presence, and the heat emanating from him is palpable. "Elana, you've always been soft, thinking diplomacy will save you," Rothgar sneers, his eyes narrowing with disdain as he glances between Elana and the figure beside her. "Who's this? Another weakling here to grovel before my might?"*

## Dragon Diplomacy

The players find themselves just outside Rothgar's lair, facing the daunting task of convincing the arrogant red dragon that they pose a significant threat. Success in this diplomatic encounter hinges on their ability to present a convincing argument and showcase their capabilities. Here are possible skill checks and approaches the players can take:

1. **Persuasion Check (DC 15):**
   - The primary check to convince Rothgar that the players are a formidable force. A successful roll demonstrates their ability to articulate their case persuasively. They can highlight their achievements, alliances, and past victories to bolster their credibility.

2. **Intimidation Check (DC 18):**
   - Players can attempt to intimidate Rothgar by showcasing their combat prowess, using aggressive body language, and emphasizing their readiness for a fight. Success in this check may make Rothgar think twice about underestimating them.

3. **Deception Check (DC 20):**
   - If the players have any tricks up their sleeves or wish to exaggerate their capabilities, a successful deception check might convince Rothgar that they possess secret weapons or formidable allies yet to be revealed.

4. **Insight Check (DC 15):**
   - A successful insight check allows the players to gauge Rothgar's reactions and tailor their argument based on his emotional state. Understanding his pride, fears, or insecurities can be key to manipulating the encounter in their favor.

5. **Knowledge (Arcana) Check (DC 18):**
   - A demonstration of knowledge about dragons, especially red dragons like Rothgar, can impress him. Players can share insights into dragon weaknesses, preferred tactics, or historical defeats of red dragons to emphasize their understanding.

6. **Performance Check (DC 15):**
   - A charismatic performance, whether through a captivating speech, an impressive display of skills, or a showcase of magical abilities, can enhance the players' overall appeal and make them appear more formidable.

7. **History Check (DC 17):**
   - Players can reference past events, battles, or legendary tales where individuals of their capabilities turned the tide against powerful foes. Demonstrating a rich history of success will support their claim.

8. **Diplomacy Teamwork (DC 20):**
   - If the players coordinate their efforts and use a combination of skills, such as one character making a persuasive argument while another performs a captivating display, they can gain advantage on their primary skill checks.


**Success**
>Rothgar, his fiery gaze momentarily softening with contemplation, takes a moment to process your words. The weight of your arguments appears to have left an impression on the proud red dragon. A flicker of thoughtful consideration passes across his draconic features as he contemplates the possibilities. After a measured pause, Rothgar's tone carries a challenge. "Words alone won't sway me. If you seek my acknowledgment, you must prove your strength. There lurks a formidable monster in the craggy recesses of the nearby mountains, a creature whose might matches its rugged surroundings. Should you best this beast, I will give due consideration to Elana's proposal. However, should you fail, you shall leave this place, your quest unfulfilled."
>The red dragon's demand hangs in the air, the weight of the impending trial emphasizing the gravity of the situation. 
**Failure**
Rothgar attacks see Battle of the Dragons


## Rothgars trial of strength
Depending on level of the party run one of the three following encounters
**1. Encounter: The Hydra's Lair**

As the players venture into the craggy mountains near Rothgar's lair, they discover the damp and cavernous lair of a fearsome Hydra. The air is thick with the stench of decay as the creature, aware of intruders, awakens from its slumber. The battle unfolds amidst the echoing roars and the hiss of the Hydra's regenerating heads.

- **Hydra (CR 8):** The primary adversary, the Hydra, possesses multiple heads, each snapping and biting independently. Players must contend with its regenerative abilities and find a way to prevent the growth of new heads.

- **Terrain Challenges:** The cavern's uneven ground and the presence of pools of water create dynamic terrain. Players may use the environment to their advantage, but they must also avoid getting caught in the Hydra's clutches.

**2. Encounter: Frost Giant Ambush**

Further into the mountains, the players find themselves ambushed by a cunning Frost Giant and a pack of hunting hounds. The bitter cold wind howls as the giant looms over them, accompanied by the snarls of the relentless hounds.

- **Frost Giant (CR 8):** The imposing Frost Giant, armed with a massive greataxe, challenges the players with powerful melee attacks. His attacks are enhanced by the biting cold, causing additional frost damage.

- **Hunting Hounds (CR 1/2):** A pack of agile and ferocious hounds accompanies the giant. While individually less threatening, their coordinated attacks and ability to harry players can be a considerable challenge.

- **Snowstorm Dynamics:** Periodic snowstorms affect visibility, providing the giant and hounds with opportunities to ambush and flank. Players must navigate the changing weather conditions to gain the upper hand.

**3. Encounter: Chasing the Chimera**

As the players press forward, they find themselves in a narrow mountain pass where a Chimera lurks. The creature takes to the air, its lion head roaring, as it unleashes a barrage of attacks. The chase is on as the players navigate the treacherous terrain to confront this mythical beast.

- **Chimera (CR 6):** The Chimera's multiple heads present diverse threats, with a fiery breath weapon, a poisonous bite, and slashing claws. Its ability to fly adds an extra layer of complexity to the encounter.

- **Treacherous Terrain:** The narrow mountain pass poses challenges as players must navigate uneven ground and avoid falling into ravines. The Chimera takes advantage of its aerial superiority, making the terrain a crucial factor in the battle.

*Rothgar, perched on his lofty vantage point, watches with a mix of astonishment and begrudging respect as the players emerge victorious from the trials he set before them. The Hydra lies defeated, the Frost Giant and his hounds dispatched, and the Chimera's roars echo no more. Elana, reveling in her triumph, glances up at the dragon with a smug expression."Well, Rothgar," she declares confidently, "as you can plainly see, my chosen allies have proven themselves formidable. Without a doubt, our next battle would tip in my favor. The winds of change blow, and it seems your reign of terror in this valley has come to an end." Rothgar, caught off guard by the efficacy of the players, concedes with a begrudging nod. Elana seizes the moment, her tone now magnanimous, "My offer remains, Rothgar. Leave this valley swiftly, and you may take with you as much of your horde as you can carry. Your presence will trouble these lands no more." With a reluctant acknowledgment, Rothgar agrees to the terms. The red dragon, once the undisputed master of the mountains, now finds himself compelled to relinquish his claim.*

# Battle of the Dragons:

As Rothgar's arrogance and pride get the better of him, the peaceful resolution slips away, and the mountains become the arena for a fierce clash between dragons. The battle pits Rothgar, the fiery red dragon, against Elana, the gleaming silver dragon, and the player characters.

**Stage 1: Clash of Titans**

The confrontation begins with a clash of titanic forces. Rothgar unleashes his fiery breath, while Elana counters with her frosty breath weapon. The players must navigate the tumultuous battlefield, avoiding searing flames and biting cold while launching attacks of their own.

- **Rothgar (Young Red Dragon):** As a formidable spellcaster and melee combatant, Rothgar engages both in aerial assaults and on the ground. His fiery breath, tail attacks, and claw strikes make him a relentless adversary.

- **Elana (Young Silver Dragon):** Elana, possessing a mix of ice-based attacks and support spells, takes to the skies to combat Rothgar. Her frost breath, wing attacks, and ability to cast protective spells create a dynamic and challenging aerial encounter.

**Stage 2: Aerial Ballet of Destruction**

The dragons take their battle to the skies, weaving and diving in a dazzling display of draconic agility. Players must use ranged attacks, spells, and strategic positioning to contribute to the airborne skirmish while avoiding devastating aerial assaults from both dragons.

- **Environmental Hazards:** The high-altitude battle introduces environmental hazards, such as gusts of wind and unpredictable air currents. Players must contend with these factors while maintaining their assault on the dragons.

**Stage 3: Desperation and Last Stand**

As the battle rages on, both dragons begin to show signs of fatigue. Rothgar, unwilling to yield, enters a berserker-like state, intensifying his attacks. Elana, determined to protect the valley, draws upon her remaining strength to unleash a final, powerful breath attack.

- **Players' Critical Roles:** The players' actions in this stage can significantly impact the outcome. Strategic use of spells, abilities, and teamwork is crucial to turning the tide of the battle in favor of Elana and the players.

**Conclusion: The Fate of Bergseeufer Valley**

The outcome of the battle determines the fate of Bergseeufer Valley. If Rothgar falls, Elana thanks the players for their assistance, and the valley is saved from the red dragon's tyranny. However, if Elana and the players are defeated, Rothgar, emboldened by his victory, solidifies his rule, and the valley descends into darkness.

This climactic encounter will test the players' strength, strategy, and ability to work together in the face of a powerful adversary. The consequences of their actions will echo throughout the valley and shape the course of their quest to rebuild Bergherz.



# Conclusion
Read the one that applies
**Adventure Outro - Rothgar Scared Off:**

*As Rothgar, the once-tyrannical red dragon, is sent fleeing from Bergseeufer Valley, Elana, the young silver dragon, descends upon the town. In the town square, she stands regal, her scales shimmering in the sunlight. A hushed silence falls over the gathered townsfolk as she addresses them."Fear not, good people of Bergseeufer Valley," Elana declares, her voice carrying the weight of assurance. "Rothgar is no longer a threat to your homes or this valley. I apologize for any unintended consequences my actions may have caused during our struggle, and I thank you for your resilience. "With genuine sincerity, Elana beseeches the townsfolk to consider the offer brought by the valiant player characters. "I have decided to lend my support to the kingdom of Bergherz," she announces, "believing that as part of this realm, your town will prosper and thrive. Let us forge a new beginning together, free from the shadow of Rothgar's tyranny." The townsfolk, their fears alleviated, look to the future with newfound hope, and the adventurers find themselves at the heart of a community on the brink of a bright and prosperous chapter.*

---

**Adventure Outro - Rothgar Defeated in Battle:**
*Following the intense battle against Rothgar, Elana, the victorious yet wounded silver dragon, descends upon the town. In the midst of the town square, she addresses the gathered townsfolk, her presence a symbol of triumph and liberation. "Fear not, resilient people of Bergseeufer Valley," Elana declares, her voice resonating with a newfound sense of confidence despite her injuries. "Rothgar's menace has been vanquished, and your homes are safe. I apologize for any collateral damage my actions may have caused during our struggle, and I extend my gratitude for your courage." With a heartfelt appeal, Elana urges the townsfolk to consider the offer presented by the courageous player characters. "I have decided to pledge my support to the kingdom of Bergherz," she announces, "and I believe that as part of this realm, your town will flourish. Let us build a new future together, free from the shadow of Rothgar's tyranny." The townsfolk, now free from the specter of the red dragon, look towards a future filled with promise, and the adventurers find themselves standing as heroes in the heart of a town ready to embrace the opportunities ahead. Elana's presence, though marked by injuries, exudes a confidence that resonates with the resilience of Bergseeufer Valley.*