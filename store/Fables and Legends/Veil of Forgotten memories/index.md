---
title: Fables and Legends of the Märchenweltgrenze  Veil of Forgotten memories
date: "2023-11-18T22:40:33.169Z"
description: A one shot for 5 players of level 6-9
---


# Adventure background

In the heart of the Märchenweltgrenze, the picturesque village of Silberhain and the nearby Fey settlement are gripped by a mysterious plague that steals away memories. This insidious affliction has cast a dark shadow over the once-thriving community, where settlers and fey lived in harmony, celebrating their shared history and building a future together.

The Memory Weavers, a clandestine group of dark fey, have infiltrated the enchanted waters of the Silver Spring, a source of life for Silberhain. Through a malevolent ritual, the Memory Weavers have cursed the well that draws from this spring, using it as a conduit to steal memories from both settlers and fey alike.

As a result, Silberhain is now a village on the verge of discord. The once-cooperative relationship between settlers and fey is strained as blame is cast, and mistrust festers in the wake of forgotten dreams. The Memory Weavers' curse has also spread to the nearby Fey settlement, threatening the delicate balance that has allowed these communities to coexist for generations.

# Adventure Begins:

>As you enter the village of Silberhain, a sense of enchantment surrounds you. Silver-leaved trees glisten in the soft sunlight, casting dappled shadows on the cobblestone streets. The air is filled with the sweet scent of wildflowers, and the sounds of laughter and conversation echo through the Market Square. However, beneath this idyllic exterior, a palpable tension lingers. Settlers and fey exchange guarded glances, their once-friendly interactions now tinged with suspicion. The normally vibrant colors of Lumenschein, the Fey settlement on the outskirts, seem dulled as though a veil has been cast over the entire village.

*Notes for the DM:*

Silberhain is a village in flux. The once-thriving Market Square, usually bustling with activity, is now a scene of confusion and worry. Settlers and fey avoid eye contact, and conversations are hushed. The normally pristine Silver Spring, a central feature of the village, now emits a faint, eerie glow. Buildings that once stood as testaments to unity now showcase signs of neglect, with fey canopies and settlers' cottages showing visible wear and tear.

As the adventurers arrive, the atmosphere is tense, and the village elders, both settler and fey, anxiously await their assistance in resolving the memory loss plaguing their people. The Council Hall, where leaders convene to make decisions, stands as a symbol of the fractured unity, with heated discussions audible from outside.

Amidst this disquiet, the grove of Remembrance at the outskirts seems untouched, its crystal obelisk standing tall, reflecting memories of happier times. The adventurers are met with a mix of hopeful gazes and skeptical glares, as the villagers, both settler and fey, wonder if these outsiders can truly lift the veil of forgotten dreams.


# The Man in Disarray

*As you make your way through the Market Square, the normally bustling heart of Silberhain, you notice a commotion near the town well. A disheveled man, his eyes filled with confusion and fear, stands in the center of a small crowd. His clothes are torn, and he wields an improvised club, swiping it wildly at anyone who comes too close. The man's voice rises above the ambient murmur of the square as he loudly declares, "Where am I? What have you done to me? Let me go!" His gaze darts around, and he points accusingly at various villagers, accusing them of being his supposed captors.*

**DM Notes:**

The man is a victim of the Memory Weavers' curse, experiencing a severe form of memory loss. He genuinely believes he has been kidnapped and brought to Silberhain against his will. The crowd around him is a mix of concerned onlookers and frustrated individuals trying to calm him down.

1. **Approaching the Situation:**
   - The players can choose to approach the man directly, attempt to calm him, or address the onlookers to prevent further escalation.

2. **Insight Check:**
   - A successful Insight check reveals that the man's confusion is genuine, and his memory loss is a result of the curse. This information may guide the players in dealing with him more empathetically.

3. **De-escalation Options:**
   - Players can use Persuasion or Deception to convince the man that they are not his captors and are here to help.
   - A character proficient in Medicine can attempt a check to administer a calming potion, potentially alleviating the man's aggressive behavior.

4. **Confrontation Consequences:**
   - If the situation escalates, the town guards may intervene, adding an additional layer of tension to the already strained atmosphere in Silberhain.
   - A violent resolution may result in negative reactions from both settlers and fey, further complicating the players' diplomatic efforts.


As the commotion in the Market Square continues, a portly man in his late fifties steps forward, attempting to maintain a semblance of authority. Mayor Ernst Hohenberg, the local figure of authority, appears flustered and overwhelmed. His thinning gray hair is disheveled, and his usually well-kept attire shows signs of wear.

**Dialogue:**

*"Ah, thank the stars you've arrived! I'm Mayor Hohenberg, and we're in dire need of your help. The memory loss plaguing our village has thrown everything into chaos. People are scared, tensions are high, and I fear the unity we once had is slipping away. I've heard whispers of the fey being blamed, and that's the last thing we need. Please, you must help us get to the bottom of this. I don't know what's causing the memory loss, but we can't afford for the blame to fall on Lumenschein, our neighboring Fey settlement. We need answers, and we need them quickly. Can you help us, kind travelers?"*

**DM Notes:**

1. **Mayor Hohenberg's Dilemma:**
   - Mayor Hohenberg is genuinely concerned for the well-being of Silberhain and Lumenschein.
   - He has no knowledge of the Memory Weavers or the true cause of the memory loss.
   - The mayor is desperate for a swift resolution to quell the rising tensions in the village.

2. **Blame on the Fey:**
   - Mayor Hohenberg reveals that some villagers are starting to blame the fey for the memory loss, suspecting them of dark magic.
   - This misinformation is exacerbating the mistrust between settlers and fey.

3. **Request for Diplomacy:**
   - The mayor beseeches the players to investigate the cause of the memory loss and clear Lumenschein of any suspicion.
   - He emphasizes the importance of maintaining the delicate balance between settlers and fey in Silberhain.

4. **Vague Knowledge:**
   - Mayor Hohenberg knows very little about the Fey settlement and has limited understanding of the magical aspects of the Märchenweltgrenze.
   - He leaves the details of the investigation in the hands of the adventurers.


# Investigating the town

**Investigation in Silberhain: Unraveling the Threads**

As the players embark on their investigation through Silberhain, they encounter a series of clues that gradually lead them to the cursed well, the epicenter of the memory loss affliction.

1. **Perception Check (DC 12):**
   - While navigating the Market Square, players notice a distraught woman holding a faded family portrait. She mentions losing the memory of her husband and points toward the town well, hinting that something may be amiss.

2. **Insight Check (DC 15):**
   - Engaging with concerned villagers reveals that the memory loss is not limited to settlers; fey in Lumenschein are also affected. The suspicion of fey involvement begins to seem misplaced.

3. **Investigation Check (DC 14):**
   - The players observe a group of children playing a game involving colored ribbons. One child mentions that the colors used to be more vibrant, hinting at the loss of vivid memories.

4. **Nature Check (DC 16) or Arcana Check (DC 18):**
   - A knowledgeable fey resident, a sprite named Elara, approaches the players in Lumenschein. She hints at an unnatural disturbance in the magical energies around the town well and suggests investigating its source.

5. **Stealth Check (DC 13):**
   - Observant players may notice a hooded figure, seemingly watching from a distance. If they attempt to approach, the figure vanishes into the shadows, leaving only a faint trail of enchanted threads.

6. **Survival Check (DC 15):**
   - Tracking the enchanted threads with a successful Survival check leads the players through Lumenschein and back into Silberhain. The threads eventually converge near the town well.

7. **Perception Check (DC 16):**
   - Near the town well, players notice that the air shimmers with faint magical energy. A closer inspection reveals a subtle distortion, as if reality itself is being tampered with.

8. **Detect Magic Spell (No Check Required):**
   - If a character uses Detect Magic near the well, they sense an aura of enchantment emanating from the water.

**Conclusion:**
The culmination of these clues and skill checks points the players toward the cursed well in Silberhain. It becomes evident that the source of the memory loss is tied to the well, and further investigation in this area will likely yield more information about the nature of the curse and its potential origins.


# THe well

**Discovering the Source: Enigmatic Artefact**

As the players investigate the cursed well in Silberhain, they discover a hidden compartment at the base of the well, concealed by enchantments. Inside, they find an enigmatic artifact emanating dark magic. The artifact is a small, intricately carved obsidian sphere with mysterious runes etched into its surface.

**Player Interaction:**

1. **Arcana Check (DC 20):**
   - A character with proficiency in Arcana might recognize the runes as ancient and associated with forbidden enchantments. However, the purpose of the artifact remains unclear.

2. **Investigation Check (DC 18):**
   - Searching for any markings or clues on the artifact reveals faint traces of a symbol that resembles a twisted tree, a potential connection to the Märchenweltgrenze's mystical nature.

3. **Detect Magic Spell (No Check Required):**
   - Casting Detect Magic identifies the artifact as strongly infused with enchantment magic, but its specific properties remain elusive.

4. **Nature Check (DC 22):**
   - A character proficient in Nature senses an unnatural resonance with the Märchenweltgrenze's magical essence, suggesting a potential link between the artifact and the dark fey.

5. **Speak with Dead Spell (No Check Required):**
   - If the players attempt to speak with a deceased individual affected by the memory loss while holding the artifact, the spell yields distorted and unintelligible responses, indicating interference from the artifact.

**DM Note:**

The artifact serves as a mysterious focal point for the dark magic affecting Silberhain. Despite the players' efforts, the true nature and purpose of the artifact remain shrouded in mystery. The enchantments on the artifact resist magical scrutiny and attempts to discern its origin or purpose.

As the players unveil the mysterious artifact, a gasp ripples through the gathered crowd in the Market Square. Whispers and murmurs echo as onlookers exchange nervous glances. The atmosphere grows tense, and a subtle undercurrent of suspicion permeates the air.

**Mayor Hohenberg Steps Forward:**

Mayor Ernst Hohenberg steps forward, his portly figure now exuding an air of urgency. He clears his throat, addressing the uneasy crowd.

*"Citizens of Silberhain, please, let us not jump to conclusions. Our friends, the fey of Lumenschein, have been part of this community for generations. Blaming them will only serve Gisela's agenda of division. The heroes before us are here to aid us, not to sow discord. We must work together to uncover the truth and restore unity to our village. Now, I beseech you, do not let fear guide your judgment."*

Despite Mayor Hohenberg's attempt to quell the rising tension, some in the crowd continue to exchange suspicious glances, and hushed conversations persist.

**The Mayor's Plea:**

*"Dear adventurers, I implore you to go to Lumenschein. Speak with their leaders, assure them that we seek cooperation, not blame. We cannot afford to let fear tear apart what generations have built. Go, seek the faeries, and bring back the understanding that will save Silberhain from further strife. We place our hopes in your hands."*

**DM Note:**

The players now face a critical decision: to heed Mayor Hohenberg's plea and seek diplomatic resolution with Lumenschein, or pursue a different course of action. The delicate balance between settlers and fey in Silberhain hangs in the balance, and the heroes' choices will shape the future of the Märchenweltgrenze communities.


# Lumenschein


**Lumenschein: The Fey Enclave**

*Description (Second Person):*

*As you venture deeper into the Märchenweltgrenze, the vibrant hues of Silberhain gradually give way to an ethereal realm known as Lumenschein. The air here is filled with the soft glow of bioluminescent flora, casting a gentle radiance over the tree canopy. Treehouses, suspended among the branches, create a tapestry of interconnected dwellings, each adorned with sparkling fairy lights. Luminescent butterflies flutter in the air, leaving trails of ephemeral color. The air is alive with the melodic sounds of fey laughter and the soothing hum of mystical energy. Lumenschein is a testament to the harmonious coexistence of settlers and fey, where nature and magic intertwine in breathtaking beauty.*

**Pandemonium in Lumenschein: Unraveling Tranquility**

*Description (Second Person):*

*As you approach the heart of Lumenschein, the enchanting atmosphere is shattered by a cacophony of dissonance. Fey residents, once carefree and playful, are now in a state of pandemonium. Luminescent butterflies flutter erratically, and the fairy lights dim as if unable to withstand the weight of worry. Fey individuals move hurriedly, exchanging worried glances and animated discussions. A sense of despair hangs in the air, a stark contrast to the usual serenity of Lumenschein. The grove, once a haven of tranquility, now echoes with the distress of a community grappling with the same memory loss afflicting Silberhain.*

---

**Encounter: Unraveling the Fey Enigma**

*As the players explore Lumenschein, seeking answers to the shared affliction, they come across a secluded grove. The once-clear waters of the Silver Brook, which flows through Lumenschein, now shimmer with a faint, eerie glow. An ancient fey guardian named Elowen, usually a beacon of wisdom, is frantically attempting to purify the water using a magical crystal.*

**Elowen's Plea:**

*"Oh, kind travelers, Lumenschein is in dire straits! Our beloved Silver Brook, the lifeblood of our settlement, has been tainted. Dark magic has seeped into its waters, stealing away the memories of our people. I've tried to cleanse it, but the enchantment is too powerful. Please, help us unravel this fey enigma and restore Lumenschein to its former harmony."*

**DM Note:**

1. **Investigation Check (DC 15):**
   - Observing the Silver Brook reveals traces of enchanted threads similar to those found in Silberhain. The threads lead to a hidden alcove within the grove.

2. **Nature Check (DC 18):**
   - A character proficient in Nature recognizes the twisted tree symbol on a nearby stone altar, hinting at a connection to the Märchenweltgrenze's mystical energies.

3. **Detect Magic Spell (No Check Required):**
   - Casting Detect Magic on the water reveals a potent enchantment. However, the spell struggles to penetrate the depths of the fey magic intertwined with the curse.

4. **Arcana Check (DC 20):**
   - Examining the altar yields insights into fey rituals, suggesting that the curse is not a mere coincidence but a deliberate act against Lumenschein.
# Following the tallsman

1. **History Check (DC 15):**

2. **Arcana Check (DC 18):**
   - An Arcana check unveils faint magical threads extending from both artifacts, interweaving in the magical tapestry of the Märchenweltgrenze. The threads converge in a specific direction, indicating a potential location.


**The Converging Threads:**

As the players follow the clues and leads, they find themselves in a dense, mystical part of the Märchenweltgrenze. The air is thick with enchantment, and the foliage seems to shift and rearrange itself, creating an otherworldly maze. Faint whispers fill the air, and the magical threads guide the players to the entrance of an ancient grove.

# THe grove of memories

*"As you step into the forests's heart, following the magical threads that converge toward a hidden grove, the air becomes charged with an otherworldly energy. The dense foliage gives way to an ancient gateway adorned with a twisted tree symbol—the entrance to the elusive Grove of Memories.As you pass through the threshold, the world around you shimmers, and the grove unfolds like a surreal dreamscape. The air is thick with enchantment, and the very ground beneath your feet seems to whisper forgotten tales. Luminescent butterflies dance through the air, leaving ephemeral trails that hint at the mystical journey awaiting you.The path ahead forks into illusion-laden pathways, each appearing as real as the next. Shadows play tricks on the periphery of your vision, and the soft hum of fey magic echoes through the grove. This is the lair of the Memory Weavers, elusive beings who manipulate the very fabric of memory.The twisted trees seem to watch your every move, and the grove itself appears alive with the memories of those who have wandered into its depths. As you venture further, wisps of silvery mist drift through the air, carrying with them the echoes of conversations and laughter long forgotten."*



**Encounter: Reflections of Deceit**

*As you progress deeper into the Grove of Memories, the magical aura intensifies, and the air seems to shimmer with hidden enchantments. Suddenly, your surroundings blur, and a disorienting sensation grips you. When clarity returns, the familiar faces of your companions wear expressions of confusion and uncertainty.*

**Scene Setting:**

In the clearing, your group finds itself surrounded by spectral echoes of your own reflections. Each member of the party now faces an identical duplicate—an uncanny doppelganger that moves with unsettling precision. The grove itself seems to have woven illusions, challenging your perception of reality.

**Challenge: Identifying Friend from Foe**

1. **Insight Checks:**
   - Players can make Insight checks to discern the subtle nuances that differentiate their true allies from the impostors. The DC varies based on the quality of the doppelgangers' mimicry.

2. **Shared Memories:**
   - Each player must recall a specific shared memory or event that only the real members of the party experienced. The impostors will struggle to replicate these memories accurately.

3. **Wisdom Saving Throws:**
   - Characters periodically make Wisdom Saving Throws to resist the lingering enchantment that clouds their recent memories. A successful save grants clarity, while a failure adds to the confusion.

**Roleplaying Element:**

The doppelgangers, once identified, attempt to sow discord and confusion by mimicking the true characters' personalities and quirks. They may engage in misleading conversations, attempting to manipulate emotions and trust.

**DM Note:**

This encounter not only tests the players' ability to trust their instincts and each other but also introduces a psychological element as they grapple with the uncertainty of their own memories. The Grove of Memories weaves a complex web of illusions, challenging the very essence of the characters' identities. The resolution leads to a thrilling confrontation with the deceptive dopplegangers, adding a layer of intrigue and mystery to the grove's challenges.
**Imposter**
- **Armor Class:** 12 (Natural Armor)
- **Hit Points:** 52 (8d8 + 16)
- **Speed:** 30 ft.

**Abilities:**
- **Strength:** 12 (+1)
- **Dexterity:** 14 (+2)
- **Constitution:** 14 (+2)
- **Intelligence:** 11 (+0)
- **Wisdom:** 12 (+1)
- **Charisma:** 14 (+2)

**Skills:**
- **Deception:** +4
- **Insight:** +3
- **Stealth:** +4

**Senses:**
- Darkvision 60 ft., Passive Perception 11

**Languages:**
- Common

**Shapechanger:**
- The impostor can use its action to polymorph into a Small or Medium humanoid it has seen or back into its true form. Its statistics, other than its size, are the same in each form. Any equipment it is wearing or carrying isn't transformed. It reverts to its true form if it dies.

**Surprise Attack:**
- If the impostor surprises a creature and hits it with an attack during the first round of combat, the target takes an extra 10 (3d6) damage from the attack.

**Multiattack:**
- The impostor makes two melee attacks.

**Claw:**
- Melee Weapon Attack: +3 to hit, reach 5 ft., one target.
- Hit: 5 (1d6 + 2) slashing damage.

**False Appearance:**
- While the impostor remains motionless and isn't incapacitated, it is indistinguishable from a normal creature.

**Alignment:**
- Unaligned

**CR (Challenge Rating):**
- 2 (450 XP)

**Memory Puzzle: The Echoing Contradiction**

As you progress through the grove, the path leads you to a mysterious chamber adorned with arcane symbols. In the center of the chamber stands a pedestal, upon which rests a series of illuminated buttons. The air hums with magical energy, and an inscription on the pedestal reads, "Reflect on what is forgotten to proceed."

**Puzzle Description:**
The puzzle consists of a grid of buttons, each glowing with a soft radiance. When pressed, the buttons emit musical tones, creating an enchanting melody that reverberates through the chamber. The goal is to illuminate the buttons in the correct sequence to unlock the passage forward.

**Memory Alteration Twist:**
Upon entering the chamber, a subtle enchantment takes hold. Each character experiences a momentary lapse in memory, rendering them unable to recall the solution to the puzzle. However, they sense that the answer lies in remembering the opposite of what they have forgotten.

**Objective:**
The players must press the buttons in the exact opposite sequence of the solution that they have forgotten. If they press a button they've forgotten, it triggers a magical reset, and they must try again.

**Example Solution:**
If the forgotten solution is "Top-Right, Bottom-Left, Top-Left," the correct sequence becomes "Bottom-Left, Top-Right, Bottom-Right."

**Hints:**
1. **Wisdom Checks (DC 15):**
   - Players can make Wisdom checks to see if they can recall any fleeting memories or clues about the original solution.

2. **Reflective Glyphs:**
   - Inspecting the surrounding walls reveals subtle reflective glyphs that hint at the reversed nature of the puzzle.

3. **Trial and Error:**
   - Characters can experiment with pressing buttons and observing the musical tones to deduce the correct sequence through trial and error.

**Consequence of Failure:**
If the players fail to solve the puzzle, a magical barrier briefly manifests, causing the chamber to reset. The players hear faint echoes of laughter, a mischievous reminder of the grove's illusions.

**DM Note:**
This puzzle challenges the players' ability to think in opposites, adding a twist of memory alteration to enhance the mystical atmosphere of the Grove of Memories. It engages their problem-solving skills while emphasizing the theme of contradictions within the enchanting realm.

**Encounter: Veil of Forgotten Shadows**

As you progress through the Grove of Memories, the air thickens with an eerie stillness. Suddenly, shadows coalesce into a mysterious figure—a being draped in shadows and shrouded in a cloak that seems to absorb all light. The air around it vibrates with an unsettling energy.

**The Forgotten Shade:**
Before you stands the Forgotten Shade, a creature that exists in the realm between memory and oblivion. Its form is obscured, and its presence is an elusive whisper that dances at the edge of consciousness.

**Combat Mechanics:**

1. **Memory Erosion Touch:**
   - The Forgotten Shade makes a melee touch attack against a target. On a successful hit, the target must make a Wisdom saving throw (DC 14) or forget the presence of the Forgotten Shade. The target will be unable to recall any details about the creature, making it invisible and intangible to them until the end of their next turn.

2. **Veil of Shadows:**
   - The Forgotten Shade can cloak itself in an impenetrable veil of shadows as a bonus action. While veiled, it gains advantage on Dexterity saving throws and remains hidden from those who have forgotten its presence.

3. **Shrouded Strikes:**
   - The Forgotten Shade's attacks deal psychic damage, representing the erosion of memories and the disorientation caused by its elusive nature.

**Tactics:**
The Forgotten Shade employs hit-and-run tactics, fading in and out of existence. It targets individuals with its Memory Erosion Touch, causing them to forget its presence and rendering itself temporarily immune to their attacks.

**Counterplay:**

1. **Wisdom Saving Throws:**
   - Players can make Wisdom saving throws at the start of their turn to resist the memory erosion effect and maintain awareness of the Forgotten Shade.

2. **Magical Detection:**
   - Spells or abilities that reveal invisible or ethereal creatures can temporarily pierce the veil of shadows.

3. **Shared Knowledge:**
   - Players who successfully resist the memory erosion effect can share information about the Forgotten Shade with their allies, mitigating the impact of its attacks.

**Consequence of Forgetfulness:**
Characters who forget about the Forgotten Shade may inadvertently target their allies or waste actions searching for an unseen foe. The psychological impact of forgetting adds an extra layer of challenge to the encounter.

Certainly! Below are three stat blocks for the Forgotten Shade, each tailored to different challenge levels. Adjust the specific hit points, damage, and other numerical values based on the desired difficulty for your party.

### Forgotten Shade - Challenge Rating 5
**Armor Class:** 15 (natural armor)  
**Hit Points:** 80 (10d8 + 30)  
**Speed:** 30 ft.

**Abilities:**
- **Strength:** 12 (+1)
- **Dexterity:** 18 (+4)
- **Constitution:** 16 (+3)
- **Intelligence:** 12 (+1)
- **Wisdom:** 14 (+2)
- **Charisma:** 10 (+0)

**Skills:**
- Stealth +6

**Traits:**
- **Memory Erosion Touch:**
   - Melee Spell Attack: +6 to hit, reach 5 ft., one target.
   - Hit: 15 (3d8 + 2) psychic damage.
   - The target must succeed on a DC 14 Wisdom saving throw or forget the presence of the Forgotten Shade until the end of their next turn.

- **Veil of Shadows:**
   - As a bonus action, the Forgotten Shade can cloak itself in shadows, gaining advantage on Dexterity saving throws until the start of its next turn.

### Forgotten Shade - Challenge Rating 7
**Armor Class:** 16 (natural armor)  
**Hit Points:** 110 (13d8 + 52)  
**Speed:** 30 ft.

**Abilities:**
- **Strength:** 14 (+2)
- **Dexterity:** 20 (+5)
- **Constitution:** 18 (+4)
- **Intelligence:** 14 (+2)
- **Wisdom:** 16 (+3)
- **Charisma:** 12 (+1)

**Skills:**
- Stealth +7

**Traits:**
- **Memory Erosion Touch:**
   - Melee Spell Attack: +7 to hit, reach 5 ft., one target.
   - Hit: 21 (4d8 + 3) psychic damage.
   - The target must succeed on a DC 15 Wisdom saving throw or forget the presence of the Forgotten Shade until the end of their next turn.

- **Veil of Shadows:**
   - As a bonus action, the Forgotten Shade can cloak itself in shadows, gaining advantage on Dexterity saving throws until the start of its next turn.

### Forgotten Shade - Challenge Rating 9
**Armor Class:** 17 (natural armor)  
**Hit Points:** 150 (15d8 + 75)  
**Speed:** 30 ft.

**Abilities:**
- **Strength:** 16 (+3)
- **Dexterity:** 22 (+6)
- **Constitution:** 20 (+5)
- **Intelligence:** 16 (+3)
- **Wisdom:** 18 (+4)
- **Charisma:** 14 (+2)

**Skills:**
- Stealth +8

**Traits:**
- **Memory Erosion Touch:**
   - Melee Spell Attack: +8 to hit, reach 5 ft., one target.
   - Hit: 28 (5d8 + 4) psychic damage.
   - The target must succeed on a DC 16 Wisdom saving throw or forget the presence of the Forgotten Shade until the end of their next turn.

- **Veil of Shadows:**
   - As a bonus action, the Forgotten Shade can cloak itself in shadows, gaining advantage on Dexterity saving throws until the start of its next turn.


### Encounter: Arcane Amnesiacs

*As you delve deeper into the Grove of Memories, you encounter a group of mysterious figures cloaked in veils of shifting enchantment. These are the Arcane Amnesiacs, beings that feed on the very fabric of magical memories. Their presence distorts the weave of arcane energy, causing uncertainty and forgetfulness among spellcasters.*

**Arcane Amnesiac Traits:**
- **Armor Class:** 14 (mage armor)  
- **Hit Points:** 60 (8d8 + 24)  
- **Speed:** 30 ft.

**Abilities:**
- **Strength:** 12 (+1)
- **Dexterity:** 14 (+2)
- **Constitution:** 16 (+3)
- **Intelligence:** 14 (+2)
- **Wisdom:** 12 (+1)
- **Charisma:** 14 (+2)

**Skills:**
- Arcana +4, Stealth +4

**Traits:**
1. **Memory Drain:**
   - As a bonus action, an Arcane Amnesiac within 5 feet of a spellcaster can attempt to drain their magical memory. The spellcaster must succeed on a DC 14 Intelligence saving throw or forget one memorized spell of the Arcane Amnesiac's choice. This effect lasts until the end of the spellcaster's next long rest.

2. **Spellbound Veil:**
   - The Arcane Amnesiacs are constantly shrouded in a magical aura that makes them resistant to the effects of spells. They have advantage on saving throws against spells.

3. **Spellshatter Pulse:**
   - Once per round, as a reaction to a spell being cast within 30 feet, an Arcane Amnesiac can release a disruptive pulse. Spellcasters within 10 feet of the Arcane Amnesiac must succeed on a DC 14 Intelligence saving throw or forget one memorized spell of the Arcane Amnesiac's choice. This effect lasts until the end of the spellcaster's next long rest.

**Tactics:**
The Arcane Amnesiacs prioritize targeting spellcasters to disrupt their magical abilities. They strategically move around the battlefield, attempting to drain spells and trigger Spellshatter Pulses.

**Counterplay:**
1. **Dispel Magic:**
   - Dispel Magic can temporarily negate the Spellbound Veil, making the Arcane Amnesiacs vulnerable to magical effects.

2. **Counter-Drain:**
   - Spellcasters can attempt to drain the memories of the Arcane Amnesiacs using similar mechanics, creating a high-stakes dynamic.

3. **Antimagic Field:**
   - An Antimagic Field can neutralize the Arcane Amnesiacs' ability to drain and disrupt spells.

**Consequence of Memory Drain:**
Spellcasters who succumb to Memory Drain may find themselves unable to access a crucial spell, potentially altering the course of the encounter.

When all the Arcane Amnesiac are defeated they drop a **deck of illusions**


### Encounter: The Memory Weavers' Atelier

*As you venture deeper into the heart of the Grove of Memories, you enter a chamber bathed in soft, iridescent light. In the center of the room, intricate webs of radiant threads stretch across the walls, forming an otherworldly tapestry. At the heart of this breathtaking display stand the Memory Weavers—a group of spider-like humanoid figures with eight arms, each delicately weaving memories into the fabric of their art.*

**Memory Weaver Traits:**
- **Armor Class:** 15 (natural armor)
- **Hit Points:** 90 (12d8 + 36)
- **Speed:** 30 ft., climb 30 ft.

**Abilities:**
- **Strength:** 14 (+2)
- **Dexterity:** 16 (+3)
- **Constitution:** 16 (+3)
- **Intelligence:** 12 (+1)
- **Wisdom:** 18 (+4)
- **Charisma:** 14 (+2)

**Skills:**
- Perception +7, Insight +7

**Traits:**
1. **Artisan Weaving:**
   - The Memory Weavers can use their action to weave memories into their tapestries or to unravel memories from the existing artwork. This process is not harmful by itself.

2. **Naive Friendliness:**
   - The Memory Weavers greet the players with genuine friendliness and curiosity. They are open to conversation and artistic collaboration.

**Interaction:**
Upon entering the chamber, the Memory Weavers pause their work, turning their attention toward the intruders with wide, multifaceted eyes. With a graceful and slightly awkward demeanor, they approach, their delicate forms exuding an aura of creativity and innocence.

**Possible Approaches:**
1. **Collaboration:**
   - If the players express a desire to collaborate and share positive memories, the Memory Weavers eagerly join in, creating a tapestry that reflects the beauty of shared experiences.

2. **Revelation of Harm:**
   - If the players provide evidence of the harm caused by the memories woven into the tapestries, the Memory Weavers express genuine surprise and remorse. They are willing to rectify the situation by removing problematic memories.

**Consequence of Collaboration:**
A successful collaboration with the Memory Weavers results in the creation of a unique tapestry that reflects the players' shared memories, imbued with the magical essence of the Grove of Memories. This tapestry can serve as a powerful token or boon.

**Consequence of Revelation:**
If the players reveal the unintended consequences of the Memory Weavers' work, the creatures willingly assist in unraveling memories from the tapestries. However, this process requires delicate handling to avoid causing harm to the Memory Weavers or disrupting the magical balance of the grove.

**DM Note:**
This encounter offers a non-combat resolution, emphasizing diplomacy, empathy, and the power of artistic collaboration. The Memory Weavers add a touch of innocence to the Grove of Memories, highlighting the unintended consequences of their artistic endeavors. The players have the opportunity to influence the outcome based on their approach and choices during the encounter.


### Conclusion: Threads of Reckoning

As the players collaborate with the Memory Weavers, the enchanting chamber of radiant tapestries comes alive with a new energy. The weavers, initially unaware of the unintended consequences of their artistry, express heavy hearts as the players present evidence of the turmoil their work has wrought.

With a deep sense of responsibility, the Memory Weavers gather around the problematic tapestries. Delicately, they begin to unravel the threads that hold the memories of Silberhain and Lumenschein. As the last strands are carefully undone, a palpable shift in the atmosphere occurs. The once-vibrant memories return to the grove, leaving behind a sense of renewal and closure.

In the wake of this collaboration, the Memory Weavers express gratitude to the players. Their naive friendliness transforms into a profound appreciation for the interconnectedness of memories and the impact of their creations on the world. With newfound insight, they promise to approach their art with greater care and consideration.

As the players exit the chamber, they find themselves back in the familiar surroundings of Silberhain. The vibrant life of the village has returned, and the echoes of laughter and bustling activity fill the air. Lumenschein, too, begins to stir with life as the memories of its fairy inhabitants return like a gentle breeze.


### Rewards and Gratitude

As the players stand in the rejuvenated villages of Silberhain and Lumenschein, the leaders express their deepest gratitude for the pivotal role the adventurers played in restoring harmony to the Märchenweltgrenze. Mayor Hohenberg, with a genuine smile, steps forward, presenting a pouch adorned with the emblem of Silberhain.

**Mayor Hohenberg's Reward:**
- **Pouch of Gratitude:** A finely crafted pouch containing 100 gold pieces. The coins gleam with a distinct luster, a symbol of the village's prosperity and appreciation.

Elowen, the fairy leader of Lumenschein, floats gracefully toward the players, her wings aglow with a soft radiance. In a voice that echoes with the magic of the Feywild, she extends a delicate hand, presenting a shimmering magic item.

**Elowen's Gift:**
- **Glimmerstone Amulet:** This enchanting amulet is adorned with a small, radiant gemstone that captures the essence of Lumenschein's magical aura. When worn, the amulet grants the wearer advantage on Charisma (Persuasion) checks and emits a gentle light that can brighten a 10-foot radius.

As the leaders express their appreciation, the villagers join in a heartfelt cheer, echoing their relief and joy. The players, now adorned with tokens of gratitude, feel the warmth of the Märchenweltgrenze's appreciation for their heroic efforts.