---
title: Fables and Legends of the Märchenweltgrenze  Whispers of the Tides 
date: "2023-11-16T22:40:33.169Z"
description: A one shot for 5 players of level 6-9
---

# Background

**Adventure Background: Shadows Over Seestadt**

Welcome to "Shadows Over Seestadt," an exciting Dungeons & Dragons adventure that unfolds in the coastal town of Seestadt, a community teetering on the brink of discord. This module is designed for a party of 4 to 5 player characters ranging from 6th to 9th level, ready to face challenges both magical and mundane as they navigate a world caught between the aftermath of goblin occupation and the lingering influence of a sinister sea witch.

**Synopsis:**

Seestadt, a picturesque coastal town, becomes the epicenter of a malevolent force's machinations. The sea witch Morganna, has been manipulating the townsfolk granting some luck while forcing the ones she helps to commit crimes against the others, all in service to her own dark machinations. The heroes must navigate the complex relationships and tensions within Seestadt, quell the riots spurred by Morganna's influence, and convince the citizens to stand united against the encroaching darkness.

# Starting the adventure

The player characters arrive in the town via a riverboat heading down river from Sliberberg. Read the following when they step off the barge
*You arrive at the coastal town of Seestadt, where the salty sea breeze carries a sense of unease through the narrow cobblestone streets. The once vibrant community now stands on the precipice of discord. The town hugs the shoreline of the cove, its buildings a mix of sturdy stone structures and quaint wooden houses, their facades telling tales of a long and storied history. Seestadt is surrounded by towering cliffs that frame the sparkling sea, a picturesque backdrop now marred by unsettling signs of arson. Wisps of smoke linger in the air, and charred remnants bear witness to recent fires that have scarred the heart of the town. Tensions run high among the townsfolk, casting a shadow over the normally peaceful community, as whispers of sabotage and mistrust echo through the alleys.*
As the players make their way through the street describe how the towns folk are acting. The give suspicious glances at each other and the players and hurry about when making their way through towns> The players will witness a verbal altercation or two while navigating the streets.
# The brawl

*As you approach the town hall, the normally quiet streets of Seestadt erupt into chaos. A boisterous brawl unfolds before you, a maelstrom of flying fists and heated accusations. Fishermen, faces contorted in anger, exchange blows and raucous shouts, each claiming the other as the source of the recent misfortunes plaguing the town. Amidst the mayhem, an aged sea elf adorned in an old captain's uniform valiantly attempts to restore order. Shouting above the tumult, he pleads for calm and endeavors to wedge himself between the feuding parties. Unfortunately, his noble efforts are met with a sucker punch from an unseen assailant, leaving the aged sea elf reeling and the brawl escalating further.*



*The sea elf brushes off the dust from his sea captain's uniform, rises to his feet with a grateful nod toward the heroes. His weathered face bears the marks of countless storms, and his keen sea-green eyes reflect a mixture of gratitude and concern. "Thank you, brave ones," Thalorin says with a solemn nod. "The winds of discontent have been stirring in Seestadt, and it seems they've reached a tempest's fury. I'm Thalorin Stormtide, once the captain of the finest ship to sail these waters. Now, I find myself trying to keep the town from tearing itself apart." He motions for the heroes to join him at a slightly more sheltered corner of the square, away from the lingering tension."In recent weeks, misfortunes have befallen us, and the blame has been cast far and wide. Arson, theft, and mysterious accidents have left us in a state of disarray. The fishermen, once bound by the common love for the sea, have turned on each other, each pointing fingers and baring fists." Thalorin sighs, a heavy burden evident in his voice. "Rumors of sabotage and betrayal have poisoned the air. The merfolk, who have been our kin for centuries, now find themselves accused of malevolent deeds. We're on the brink of unraveling, and I fear for the heart of Seestadt." He looks to the heroes with a hint of hope in his eyes. "If there's any chance to quell this storm and uncover the true source of our troubles, I believe you might be the ones to do it. Will you lend your aid to Seestadt and help us weather this turmoil?"*
# Meeting the mayor

*As you make your way to the town hall, the echoes of the recent brawl still lingering in the air, you encounter a young man strolling along the cobbled streets. He's arm in arm with a pretty young lass, their laughter cutting through the tense atmosphere like a ray of sunshine. Their carefree demeanor offers a stark contrast to the turmoil that currently grips Seestadt.>Upon reaching the town hall, a diligent secretary guides you through its corridors, leading you into a room where an older man awaits. To your surprise, the resemblance between the young couple outside and the older gentleman is unmistakable. The man seated before you, adorned in the formal attire befitting a mayor, looks up from his paperwork with a warm smile. The mayor gestures for you to take a seat, and the room exudes an air of affluence, adorned with maritime-themed decor. "I've heard you've offered your assistance to Seestadt in these troubled times. We're grateful for any help we can get. Please, share with us what you've learned, and let us see if we can untangle the web of misfortune that's woven itself around our beloved town."*
**Mayor Reinhard von Stein - Roleplaying Guide:**

*Appearance:* Mayor Reinhard von Stein is a man in his late fifties, with salt-and-pepper hair and a neatly trimmed beard that adds an air of sophistication to his appearance. His sharp, intelligent eyes betray years of experience, and his formal attire befits his position as the leader of Seestadt.

*Personality:*
1. **Dedicated to Seestadt:** Mayor Reinhard is deeply committed to the well-being of Seestadt. His dedication is evident in the lines etched on his face, forged through years of navigating the challenges that come with leading a coastal town.

2. **Pragmatic Leader:** Reinhard is a pragmatic leader, focusing on immediate concerns and the pressing needs of his community. He may seem reserved, with a tendency to prioritize local matters over grand alliances or distant kingdoms.

3. **Fatherly Concern:** As a father to Adrian, Reinhard is protective and deeply concerned about the well-being of his son. Any threat to the stability of Seestadt weighs heavily on his mind, given the impact it could have on the younger generation.

*Interaction:*
1. **Welcoming Yet Preoccupied:** Mayor von Stein welcomes the heroes warmly, recognizing the potential aid they bring. However, it's clear that his thoughts are preoccupied with the town's recent troubles and the need to maintain order.

2. **Local Focus:** Reinhard may express gratitude for external assistance but might convey a reluctance to engage in broader geopolitical matters. His primary concern is the immediate stability of Seestadt and its relationship with the merfolk.

3. **Meticulous Listener:** Reinhard listens carefully to the heroes' insights and discoveries, appreciating any information that could shed light on the town's current predicament. He values practical solutions and tangible results.

*Goals and Motivations:*
1. **Stabilize Seestadt:** The mayor's primary goal is to restore peace and stability to Seestadt. He sees external assistance as a means to an end, a way to quell the growing unrest and protect the town's future.

2. **Protecting His Son:** Reinhard's fatherly instincts drive him to ensure the safety of his son, Adrian. Any solution that guarantees the well-being of Seestadt and its residents, especially the younger generation, resonates strongly with him.

3. **Maintain Independence:** While grateful for aid, Reinhard may express a desire to maintain Seestadt's independence, skeptical of entanglements that could divert resources from immediate concerns.

*Dialogue Examples:*
1. **Concern for the Town:** "Seestadt has weathered many storms, both at sea and within our community. Your assistance is appreciated, but my foremost concern is the well-being of our people."

2. **Fatherly Worries:** "Adrian is the future of Seestadt. I can't help but worry about what these recent troubles might mean for him and the generations to come."

3. **Pragmatic Realism:** "Forgive me if I seem hesitant to entertain thoughts of distant kingdoms. Our focus must be on the troubles at our doorstep. Practical solutions, here and now, are what Seestadt needs."

# Thalorin's Troubled Revelation:

*As you step out of the mayor's office, Thalorin Stormtide intercepts you by the door to the town hall. The lines on his face seem to have deepened since your earlier encounter, and a sense of urgency lingers in his sea-green eyes. "Trouble's brewing, and it seems it's reached a fever pitch," Thalorin murmurs, glancing around to ensure no eavesdroppers linger nearby. "I could tell from the mayor's expression that things did not go well in there. People are on edge, and the town's unraveling faster than I feared." He takes a deep breath, his voice laced with frustration. "About a month back, folks here started experiencing strokes of incredible luck. Fortunes turned overnight, but it wasn't long before that luck took a sinister turn for others. Equipment failing, buildings catching fire, ships sinking, and people being attacked. The blame game began, and now we're teetering on the edge of chaos." Thalorin, the last vestige of reason in the tumultuous town, continues, "I've tried to reason with them, but they've stopped listening. I've become a mere echo in the cacophony of accusations." He pauses, his expression weary yet determined. "I've seen two potential leads. One of the fishermen, the one with the unnaturally good luck, was spotted tossing an oil lantern into a boat a few nights back. And then there's this mysterious woman who caught Adrian von Stein's eye. She arrived just before the accusations against the merfolk began, and I saw her swimming near one of the vanished fishing boats. It's as if the troubles are tied to her arrival." Thalorin looks to you, a plea in his eyes. "We need answers, and we need them fast. The town is on the verge of tearing itself apart."*
# Following Thalorin Stormtide leads

## Investigating Otto the Fisherman:
   Otto is a fisherman who has had an amazing string of good luck recently. Most notably he has pulling large hauls of fish, so large his boat has nearly capsized on more than one occasion. His luck is due to a charm given to him by Morganna. Her price for the charm, he was to sabotage his rivals any several times a week.
1. **Questioning Townsfolk:**
   - **Insight Check (DC 12):** The players inquire about Otto's recent streak of good luck. The townsfolk share that he's been pulling in massive catches, an unusual feat even for the most skilled fishermen. They mention that he often visits the Snapping Line Inn after work.

2. **Tailing Otto:**
   - **Stealth Check (DC 15):** The players decide to discreetly follow Otto after his visit to the Snapping Line Inn. As they tail him, they witness Otto maliciously cutting the nets and fishing lines of a rival fisherman under the cover of darkness. A successful Stealth Check ensures they remain undetected.

   - **Perception Check (DC 12):** The players, while tailing Otto, notice him assaulting another fisherman in one of the town's alleys. A successful Perception Check reveals that Otto's actions seem deliberate and fueled by more than just rivalry.

3. **Searching Otto's Home:**
   - **Investigation Check (DC 15):** The players decide to search Otto's home for any clues. However, the house appears ordinary, with no evidence pointing to his recent acts. Otto seems to have been careful in keeping his personal space free of incriminating evidence.

4. **Searching Otto's Boat:**
   - **Investigation Check (DC 18):** Upon searching Otto's boat, the players find an unsettling charm hidden among the fishing gear. It's crafted from fish bones, coral, and shells intricately woven together. A successful Investigation Check reveals an enchantment, suggesting a connection to supernatural forces.

**Consequences:**

1. **Successful Investigation:**
   - If the players succeed in multiple checks, they gather enough evidence to confront Otto. They may choose to apprehend him or use the information to pressure him into revealing the source of his newfound luck.

2. **Partial Success:**
   - If the players only succeed in some checks, they might have an incomplete understanding of Otto's actions. This could lead to a confrontation with Otto without fully understanding the magical charm's significance.

3. **Failure:**
   - Failing the majority of checks may result in the players having insufficient evidence. They might find themselves in a difficult situation, either wrongly accusing Otto or missing a crucial piece of information.

**Otto's Revelations:**
   - Upon confrontation, Otto might confess to making a deal with a mysterious sea entity. The charm, gifted by this entity, has granted him incredible fishing luck, but at a dark cost. The entity demanded he sabotage others to maintain the balance. Otto, now remorseful, seeks redemption but fears the repercussions of breaking the pact.
## Investigating Adrian's Mysterious Lover:
   Adrian recently appeared in town and nobody knows who she is. In truth she is the mermaid Nerida. Morganna gave her a charm that turns her into a human in exchange for sabotaging the boats and ships in the port

1. **Questioning Townsfolk:**
   - **Insight Check (DC 14):** The players inquire about Adrian's mysterious lover. The townsfolk share that she is a recent arrival, and nobody knows anything about her except Adrian. No one seems to know where she lives.

2. **Tailing Adrian's Lover:**
   - **Stealth Check (DC 16):** The players discreetly follow Adrian's lover to gather more information. They witness her diving into the sea at a secluded spot late in the day and not reemerging until the next morning. A successful Stealth Check ensures they remain undetected.

   - **Perception Check (DC 14):** Observing her actions, the players notice the mysterious lover's peculiar behavior, raising questions about her connection to the sea.

3. **Direct Interrogation:**
   - **Intimidation Check (DC 18):** The players attempt a direct interrogation, pressuring her with threats. If successful, she might crack under the pressure and reveal some information about her secretive activities.

4. **Confronting with the Ocean Dive:**
   - **Persuasion Check (DC 16):** Instead of intimidation, the players confront her with the discovery of her late-night ocean dives. If successful, she might choose to reveal her true nature and the reasons behind her actions.

**Consequences:**

1. **Successful Investigation:**
   - If the players succeed in multiple checks, they gather enough evidence to confront Adrian's lover. Depending on their approach, she may confess to her true identity and the dark pact she made with a sea hag.

2. **Partial Success:**
   - If the players only succeed in some checks, they might have an incomplete understanding, leading to a confrontation without all the necessary information.

3. **Failure:**
   - Failing the majority of checks may result in the players having insufficient evidence. They might need to continue investigating or risk making decisions based on incomplete information.

**Adrian's Lover's Revelations:**
   - When confronted with the evidence, Adrian's lover may break down and confess. She reveals that she is, in fact, a mermaid who made a pact with a powerful sea hag to gain the ability to transform into a human. Her task was to sabotage the boats as part of the dark bargain.

   - She explains the complexities of her relationship with Adrian, the struggle to keep her true identity hidden, and the guilt she bears for the misfortunes brought upon Seestadt. Now exposed, she faces a choice between breaking the pact or finding a way to fulfill her obligations without causing harm to the town. The players are left to decide how to handle this revelation and its potential impact on the fate of Seestadt.

# Morganna lair
It takes an hour by rowboat to reach Morganna Lair. The party is battered by rough surf the whole way leaving the party wet and with one level of exhaustion. When they reach the lair read the following
>Guided by Nerida in her mermaid form, you follow the foreboding rocky coast to the cave entrance. The air is thick with the scent of salt and the distant roar of the pounding surf becomes a constant backdrop to the impending confrontation.

1. **Nature Check (DC 14):**
   - A successful Nature check allows you to discern the peculiar green glow's origin—a combination of phosphorescent algae and mystical energies intertwining to create an eerie luminescence around the cave.

2. **Perception Check (DC 15):**
   - As you approach, keen perception reveals the rhythmic silhouette of Sahuagin guards patrolling near the cave entrance. Their aquatic forms blend seamlessly with the shadows, a testament to their skill in navigating both land and sea.

3. **Stealth Check (DC 16):**
   - To avoid alerting the Sahuagin to your presence, you may attempt a Stealth check. Success enables you to advance silently, staying one step ahead of the guards as you reach the cave entrance.

4. **Nerida's Warning:**
   - Before parting ways, Nerida emphasizes the danger that lies within the cave. She recounts her encounter with Morganna and cautions about the Sahuagin guards. Her parting words echo with concern for your safety.

>As you stand before the cave entrance, the ominous green glow intensifies, casting an otherworldly pallor over the surroundings. The guardians await within, their watchful eyes piercing the darkness. The choice of how to approach—the blade unsheathed or a more subtle approach—lies with you. The secrets of Morganna and the resolution to Seestadt's troubles await in the depths beyond the cave's threshold.

**Encounter: Sahuagin Ambush**

**Setup:**
- The cave is dimly lit with the ominous green glow intensifying as you proceed deeper. The Sahuagin guards lie in wait, their webbed hands gripping tridents, eyes trained on the entrance. The guards are under orders to not let anyone enter the cave without Morganna permission.

**Sahuagin Guards:**
- **Level 6-7:** (x3) Sahuagin (HP: 45 each)
- **Level 8-9:** (x4) Sahuagin (HP: 60 each)

**Environment:**
- The cave's uneven terrain provides partial cover in some areas, and the narrow passages restrict movement.

**Initiative:**
- Roll for initiative as the party enters the cave, determining the order of actions.

**Tactics:**
- The Sahuagin guards are strategically positioned to take advantage of their underwater combat abilities. They prioritize attacking from the water when possible, using their superior swimming speed to navigate the cave's shallow pools.

**Flexible Adjustments:**
- Adjust the number of Sahuagin and their hit points based on the party's average level. Add more Sahuagin for higher-level parties or reduce their numbers for lower-level parties.
  
**Special Actions:**
1. **Net Attack:** Sahuagin attempt to entangle a player using a weighted net. The targeted player must succeed on a Dexterity saving throw (DC 12) to avoid being restrained.

2. **Shark's Teeth:** On a critical hit, the Sahuagin inflicts additional damage as they bite down with the ferocity of a shark. Roll an additional damage die for the bite attack.

**Dynamic Environment:**
- The cave's green glow is a result of bioluminescent algae on the walls. Players can interact with these algae to create areas of dim light, potentially giving them advantages in combat.

**Victory Conditions:**
- The Sahuagin guards aim to repel intruders. Victory is achieved if the players defeat the guards, forcing the remaining Sahuagin to flee or surrender.

**Failure Conditions:**
- If the party is overwhelmed and forced to retreat, they may face consequences such as increased Sahuagin patrols or triggering an alert within the cave.


## Morganna's Grotto:

>As you enter Morganna's grotto, the dim light of phosphorescent algae reveals a surreal and arcane atmosphere. The air is heavy with the scent of the sea, mixed with the pungent aroma of exotic herbs and alchemical concoctions. The grotto is a combination of natural formations and carefully crafted spaces, reflecting the sea witch's dual nature as a master of both the mystical and the elemental.

**Description:**

1. **Arcane Workspace:**
   - A large, worn oak table dominates the center of the grotto. Parchments, quills, and ancient tomes scatter the surface, detailing dark rituals, alchemical formulas, and unsettling prophecies.

2. **Living Quarters:**
   - Draped fabrics in deep blues and greens create a makeshift bedroom area. A large clamshell functions as a bed, adorned with intricate seaweed patterns. Shelves along the walls display jars of strange ingredients and sea curiosities.

3. **Alchemical Apparatus:**
   - A bubbling cauldron hangs above a magical flame, its contents emitting an otherworldly glow. Shelves are lined with vials containing potent elixirs and strange substances, revealing Morganna's expertise in dark arts.

4. **Mystical Symbols:**
   - Arcane symbols and runes, drawn in phosphorescent paint, adorn the walls. These markings seem to resonate with an otherworldly power, hinting at the sea witch's connection to ancient and forbidden magic.

**Investigation Checks:**

1. **Arcane Workspace:**
   - **Arcana Check (DC 15):** Examining the parchments and tomes reveals details of Morganna's dark rituals and her plan for the town's destruction.

2. **Living Quarters:**
   - **Investigation Check (DC 14):** Searching through the shelves and personal items yields a hidden compartment containing a small vial of mermaid's tears, a potent reagent for dark magic.

3. **Alchemical Apparatus:**
   - **Nature Check (DC 16):** Investigating the alchemical substances uncovers a vial of enchanted sea foam, a common magic item that can create illusions of aquatic creatures when applied.

4. **Mystical Symbols:**
   - **Religion Check (DC 18):** Deciphering the symbols reveals an invocation to a sea deity, hinting at Morganna's connection to a malevolent force that has guided her actions.

**Treasure:**

1. **Seafoam Amulet:**
   - A common magic item - An amulet imbued with the essence of the sea. When worn, it grants advantage on saving throws against water-based effects.

2. **Mermaid's Tear Elixir:**
   - A common magic item - When consumed, this elixir grants the ability to breathe underwater for one hour.

**Clues to Morganna's Plan:**
   - In a hidden compartment within the arcane workspace, the players find a map detailing strategic points in Seestadt marked with symbols resembling flames and swords. Scattered notes reveal Morganna's intention to manipulate existing tensions in the town, escalating them to the point of chaos and destruction.

**Revelation:**
   - The gathered clues point to Morganna's endgame: the town's self-destruction. The players learn that she has returned to Seestadt to witness the devastating culmination of her dark machinations, leaving the fate of the town hanging in the balance. The urgency to return to Seestadt and thwart Morganna's plan weighs heavily on the party.


# Return to Seestadt
When the players reach the town read the following
>As you re-enter town, a scene of utter chaos unfolds before you. The air is thick with the acrid scent of smoke as billowing plumes rise into the sky. The once tranquil streets are now battlegrounds, consumed by a full-scale riot that echoes with the fervor of anger and despair. Flames dance uncontrollably, hungrily devouring structures and casting an ominous glow upon the frenzied tumult.
>
>Amidst the pandemonium, your eyes catch sight of a lone figure. Clad in tattered garments, the silhouette stands atop the harbor master's office, surveying the havoc below. The figure's presence, an eerie juxtaposition against the backdrop of burning buildings, hints at a deeper connection to the unfolding turmoil. The rioters, a swirling mass of discontent, seem oblivious to the enigmatic observer, their attention consumed by the frenzy of destruction and chaos.
A player can make dc 15 perception check to see that the figure has a grey octopus like skin.

## Confronting Morganna
>As you close in on Morganna, the sea hag, the air becomes charged with a malevolent energy. Her wild, seaweed-like hair whips around her grotesque features, and her eerie, luminescent eyes fixate on you with a sinister gleam. She cackles, a sound that sends shivers down your spine.
>"Foolish mortals, you have stumbled into my web of chaos, but you are too late to halt the tides of fate that I have set into motion. This quaint town, so blinded by their desires, has become my puppetry stage, dancing to the tune of their own demise.Wishes granted, bargains struck, and now the time of reckoning is at hand. The fabric of this community unravels as they turn on one another, fueled by their own greed and desperation. Oh, how delightful it is to watch the world crumble under the weight of its own desires!The merfolk, once allies, are now implicated in the web of deceit, and the townsfolk tear at each other's throats. A splendid tragedy, don't you think? All orchestrated by my hand, with a dash of chaos and a sprinkle of despair.You sought to thwart my design, but your feeble efforts were in vain. The final act is already underway, and you are but spectators to the unraveling of their destiny. Embrace the chaos, for in the end, it shall consume everything!"




The final confrontation unfolds on the sloping wooden roof of the harbormaster's office, where the sea witch Morganna stands with an otherworldly malevolence. The battleground is treacherous, with sections of the roof damaged or missing, creating uneven footing.




Certainly! Below are stat blocks for Morganna at different power levels, tailored for a boss encounter with 4 to 5 player characters between levels 6 and 9.

### Morganna the Sea Witch - Rank 6-7

**Hit Points:** 120  
**Armor Class:** 15 (Mage Armor)  
**Speed:** 30 ft., Swim 40 ft.

**Abilities:**
- **Spellcasting:** Morganna is a 6th-level spellcaster. Her spellcasting ability is Intelligence (spell save DC 14, spell attack bonus +6).
  
   - Cantrips (at will): Eldritch Blast, Mage Hand, Prestidigitation
   - 1st-level spells (4 slots): Charm Person, Mage Armor, Ray of Sickness
   - 2nd-level spells (3 slots): Crown of Madness, Misty Step
   - 3rd-level spells (2 slots): Water Walk, Lightning Bolt

- **Ocean's Wrath:** Once per short rest, Morganna can summon a surge of ocean energy. All water-based spells she casts within the next minute deal an additional 2d6 damage.

- **Enchanted Tentacles:** Morganna commands animated tentacles to protect her. When a melee attack targets her, the tentacles impose disadvantage on the attack roll.

### Morganna the Sea Witch - Rank 8-9

**Hit Points:** 150  
**Armor Class:** 16 (Mage Armor)  
**Speed:** 30 ft., Swim 40 ft.

**Abilities:**
- **Spellcasting:** Morganna is an 8th-level spellcaster. Her spellcasting ability is Intelligence (spell save DC 15, spell attack bonus +7).

   - Cantrips (at will): Eldritch Blast, Mage Hand, Prestidigitation
   - 1st-level spells (4 slots): Charm Person, Mage Armor, Ray of Sickness
   - 2nd-level spells (3 slots): Crown of Madness, Misty Step
   - 3rd-level spells (3 slots): Water Walk, Lightning Bolt, Counterspell
   - 4th-level spells (2 slots): Control Water, Dimension Door

- **Ocean's Wrath:** Once per short rest, Morganna can summon a surge of ocean energy. All water-based spells she casts within the next minute deal an additional 2d8 damage.

- **Enchanted Tentacles:** Morganna commands animated tentacles to protect her. When a melee attack targets her, the tentacles impose disadvantage on the attack roll.

### Morganna - Additional Features (Both Ranks)

- **Legendary Resistance (3/Day):** If Morganna fails a saving throw, she can choose to succeed instead.

- **Lair Actions:** On initiative count 20 (losing initiative ties), Morganna takes a lair action to cause one of the following effects:

   1. **Summon Water Elementals:** Morganna summons 2d4 water elementals that appear in unoccupied spaces she can see within 60 feet. These elementals are friendly to Morganna and act on her turn.

   2. **Rising Tide:** Morganna raises the water level in a 30-foot radius around her. The area becomes difficult terrain, and creatures must succeed on a Strength saving throw (DC 14) or be knocked prone.

   3. **Sea's Embrace:** Morganna targets one creature she can see within 60 feet. The target must succeed on a Wisdom saving throw (DC 15) or be charmed for 1 minute.

**Note:** Adjust the number and power level of Morganna's spells and abilities based on your party's composition and desired level of challenge. Tailor the encounter to provide an engaging and memorable experience for your players.

**Battle Map Description:**
- The roof is approximately 30 feet long and 20 feet wide, with a gradual slope toward the edge.
- The roof has various obstacles, such as chimneys, crates, and weather-beaten shingles, providing partial cover.
- The ocean's rhythmic waves crash against the building's foundation below.

**Combat Notes:**
- Morganna moves fluidly across the roof, using her shapechanging abilities to confound attackers.
- Waves of Despair is a powerful area-of-effect attack that can turn the tide of battle. Heroes should spread out to minimize its impact.
- Morganna strategically employs the roof's obstacles for cover and line of sight advantage.
- The fight intensifies as Morganna summons water elementals to join the fray, adding an additional layer of challenge for the heroes.

As the battle reaches its climax, the heroes must navigate the precarious rooftop, contend with Morganna's unpredictable magic, and thwart her dark designs to save Seestadt from descending into chaos.

# Dealing with the aftermath

## Quelling the Riot

As Morganna's influence wanes with her defeat, the players find themselves amidst a riot-stricken town. Groups of commoners, driven to desperation and anger, lash out in chaos. The challenge now is to restore order without resorting to violence.

**Setup:**
- The town square is filled with rioters, their faces contorted with frustration and despair. Buildings smolder, and the echoes of shouts and breaking glass fill the air.

**Commoners:**
- **Level 6-7:**
  - Rioters (x4) (HP: 16 each)

- **Level 8-9:**
  - Rioters (x5) (HP: 24 each)

**Environment:**
- The town square provides opportunities for creative intervention. Barrels of water, improvised barricades, and abandoned objects are scattered around.

**Initiative:**
- Roll for initiative to determine the order in which players can take actions during the encounter.

**Objective:**
- The players must find non-lethal ways to calm the rioters and restore order. This can include persuasion, intimidation, or creative use of spells and abilities.

**Special Actions:**
1. **Rioter Distractions:** Rioters might be distracted by certain events, such as the sound of a building collapsing or a sudden appearance of town guards trying to contain the chaos.

2. **Improvised Barricades:** Players can strategically use nearby objects to create barricades, hindering the rioters' movement and creating natural barriers.

3. **Water Barrels:** Splashing rioters with water from barrels may temporarily break their aggression and allow for more effective communication.

**Dynamic Environment:**
- The players can use the environment to their advantage. Successful Intelligence or Wisdom checks (DC 12) can reveal opportunities to manipulate the surroundings.

**Victory Conditions:**
- The encounter ends when the players successfully quell the riot without killing any rioters. This might involve calming the crowd, addressing their grievances, or organizing a collaborative effort to rebuild.

**Failure Conditions:**
- If the players resort to lethal force or fail to de-escalate the situation within a set number of rounds, the riot intensifies, potentially causing more damage and casualties.

**Rewards:**
- Successful resolution of the riot earns the players the gratitude of the townsfolk and a potential ally in future endeavors. Additionally, they may receive information or clues about lingering issues that need attention in the aftermath of Morganna's influence.


## Addressing the Townsfolk

As the chaos of the riot subsides, the mayor, with a weary yet determined expression, manages to gather the townsfolk in the town hall for a crucial assembly. The player characters now face the challenging task of convincing the shaken citizens of Seestadt about the truth of Morganna's malevolent influence.

**Setup:**
- The town hall is filled with a diverse crowd of townsfolk, their faces reflecting a mix of fear, uncertainty, and skepticism. The mayor stands at a makeshift podium, waiting for the player characters to address the assembly.

**Objective:**
- Convince the townsfolk of Seestadt about the reality of Morganna's dark influence and the necessity to stand united for the town's recovery.

**Skill Check Opportunities:**

1. **Persuasion (DC 15):**
   - Convince the crowd that Morganna was manipulating them, exploiting their fears and desires for her own sinister ends. Success boosts the crowd's morale.

2. **Insight (DC 14):**
   - Read the emotions of the crowd and address specific concerns. A successful check reveals pockets of skepticism or fear that can be individually addressed.

3. **Performance (DC 12):**
   - Use charisma and oratory skills to captivate the crowd. A successful check inspires hope and trust among the townsfolk.

4. **Nerida's Gift Display:**
   - If Nerida presents the gift she received from Morganna and shares her personal experience, it grants advantage on the persuasion check.

5. **Otto's Testimony:**
   - If Otto testifies to his own experiences with Morganna and the consequences of her gifts, it provides advantage on insight checks and persuasion checks related to the gifts.

6. **Knowledge (Arcana/History) (DC 16):**
   - Share insights into Morganna's dark magic, explaining the dangers she posed and the potential consequences if her influence is not fully understood.

**Dynamic Responses:**

- **Success:**
  - A successful overall persuasion check (the majority of individual checks pass) results in the crowd gradually calming down, acknowledging the reality of Morganna's deceit. The townsfolk become more willing to work together for the town's restoration.

- **Partial Success:**
  - Even with some successful checks, there might be lingering skepticism or pockets of unrest. The situation becomes more fragile, and additional efforts may be needed to fully unite the crowd.

- **Failure:**
  - If the persuasion checks fail overall, the crowd remains skeptical or even resistant to the characters' message. This could lead to prolonged distrust and difficulties in rebuilding the town.

**Consequences:**

- **United Town:** If successful, the townfolk rally together, grateful for the heroes' intervention, and are more cooperative during the recovery process.

- **Divided Town:** If unsuccessful, the townsfolk remain divided, making the recovery more challenging. There may be lingering tensions and obstacles to overcome.

If the players are at risk of losing read the following

>As Nerida steps forward on the town hall steps, a hush falls over the crowd. She clutches the charm given to her by Morganna, a seemingly innocuous trinket that concealed her mermaid nature. With a deep breath, she breaks the charm, and a shimmering transformation ensues. Nerida's legs meld together into a sparkling fin, her skin takes on a pearlescent hue, and her hair billows like seaweed.
>However, the joyous gasps from the crowd are short-lived as Nerida's expression shifts from triumph to distress. Her breathing quickens, and her movements become labored. Adrian, realizing the toll this transformation takes on her, rushes to her side. As Nerida collapses, gasping for breath, Adrian catches her in his arms. Gasping Nerida manages to say, "Darling please help." 
>Adrian, cradling Nerida gently, looks up at the gathered townsfolk with a mixture of concern and frustration. "You let yourselves be manipulated by a sea witch, one who wanted to destroy our town for her own amusement," he chides the crowd. "This is the consequence of our greed and jelousy. We must learn from this and rebuild together, ensuring that no outsider can sow discord among us again."
>Adrian carries Nerida toward the nearest body of water, urging the crowd to make way. The townsfolk, now confronted with the consequences of their actions, look on with a mix of remorse and understanding. As Adrian carefully lowers Nerida into the water, a sense of unity begins to emerge from the shared realization that they were all victims of Morganna's schemes.
>Nerida, once submerged, takes in a deep breath as her mermaid form adjusts to the water. Adrian, determined to stand by her side, addresses the crowd one final time. "Let us rebuild our town, mending the wounds caused by our own shortcomings. Together, we can ensure that Seestadt stands stronger than ever."

# Conclusion
*As the echoes of the heroes' words resonate within the town hall, a transformative energy sweeps through the gathered citizens of Seestadt. The tide of skepticism begins to recede, replaced by a glimmer of hope and unity. The townsfolk, inspired by the heroes' conviction and dedication, cast aside the shadows of mistrust and choose to join King Fredrick's alliance willingly.*

*In the days that follow, the once-fragmented community of Seestadt becomes a symbol of resilience and rebirth. The heroes' efforts, coupled with the support of King Fredrick and the capital, weave a tapestry of rebuilding that stretches from the narrow cobblestone streets to the bustling harbor.*

*A riverboat laden with chests of coins and resources arrives from the capital, a tangible expression of the kingdom's commitment to Seestadt's revival. The townsfolk, now united in purpose, work tirelessly alongside the heroes to reconstruct their homes and breathe life back into the heart of the coastal town.*

*The Thicketrealm, once shrouded in mystery and danger, becomes a source of newfound prosperity. The heroes' journey, marked by diplomacy, courage, and understanding, turns Seestadt into a beacon of hope for the kingdom. As the first rays of dawn pierce through the lingering shadows, Seestadt emerges from the darkness, stronger and more resilient than ever before.*

*The kingdom of Bergherz, though scarred by goblin occupation and the influence of a sea witch, rises from the ashes with Seestadt at its core. The heroes' names become etched in the annals of history, celebrated for turning the tide against despair and guiding Seestadt into a future bathed in the warm light of renewed hope.*

*And so, the adventurers, having forged alliances and restored a kingdom, stand at the harbor's edge, watching as Seestadt sails towards a brighter horizon. The riverboat, laden with the capital's generosity, sets a course for renewal, carrying the promise of a prosperous future for Seestadt and the kingdom they helped save.*