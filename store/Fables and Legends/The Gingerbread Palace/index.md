---
title: Fables and Legends of the Märchenweltgrenze  The Gingerbread Palace 
date: "2023-11-18T22:42:33.169Z"
description: A one shot for 5 players of level 6-9
---



**Adventure Background: Sweet Whispers of Märchenweltgrenze**

A series of mysterious disappearances has cast a pall over Zuckerdorf, leaving the villagers in a state of fearful uncertainty. The affliction strikes without warning, claiming the adolescents of the village as its prey. Children and teens, drawn by an irresistible allure, venture into the woods and never return. The village Alderman, distraught by the vanishing of his own children, has sought aid in the heart of Zuckerdorf's enchantment. Unbeknownst to the villagers, a trio of cunning witches has taken residence within the Märchenweltgrenze. Masters of illusion and enchantment, these witches have crafted a bewitching gingerbread palace deep in the heart of the forest. The sweet scent of their confectionary creation acts as a siren's call, luring unsuspecting adolescents into its sugary grasp.nce enticed into the enchanted palace, the captivated youths become prisoners, ensnared by the witches' dark magic. The once-vibrant village Alderman, now desperate and tormented by the disappearance of his children, pleads for assistance from anyone brave enough to confront the malevolent force within the Forbidden woods.


**Adventure begins**


*As you enter the village square, normally abuzz with the lively chatter of locals and the vibrant colors of market stalls, you sense an unspoken tension. Faces that were once illuminated with warmth are now etched with worry, and the laughter that once echoed through the air has been replaced by hushed conversations laden with fear.The woods, usually a source of mystical allure, now casts a shadow over Zuckerdorf. Every villager you pass seems to cast wary glances toward the edge of the woods, as if expecting the trees themselves to unveil the secrets behind the recent tragedies.*

**The encounter in the market square**

*As you linger in the village square, observing the unease that hangs in the air, a disheveled woman rushes towards you. Her eyes are wide with desperation, and her voice trembles as she addresses you."Please! You there, travelers! Have you seen my children? Two of them, a boy and a girl. They went into the Märchenweltgrenze and haven't returned. I fear something terrible has happened to them!" Before you can respond, a man, distinguished by a well-tailored but now slightly disheveled suit, follows closely behind the distraught woman. He gently places a reassuring hand on her shoulder, attempting to calm her frantic pleas. "Dear, please. We mustn't alarm these travelers. They might be able to help," he says, his tone a mix of concern and urgency. The man then turns his attention to you, his eyes searching for a glimmer of hope. "Adventurers, I am Alderman Klaus, and this is my wife, Helga. Our children have gone missing, lured into the Märchenweltgrenze. We've searched everywhere, but there's no sign of them. I beg of you, please help us. Whatever has taken hold of our village is sinister, and we fear for the safety of our youth. Will you aid us in finding our children and putting an end to this dread that has befallen Zuckerdorf?"*

**The woods**
*As you venture deeper into the Märchenweltgrenze, the dense canopy of ancient trees casts long shadows, and an eerie silence settles over the forest. The air is thick with enchantment, and the subtle rustle of leaves seems to carry a haunting melody.sAmidst the twisted branches and mystical foliage, a tiny figure flits into view. A pixie, her luminous wings shimmering in the dappled sunlight, hovers before you. Her eyes widen in both relief and urgency as she addresses the group."Travelers! Oh, please, you must listen!" The pixie's voice is a delicate chorus of chimes, carrying both urgency and a hint of sorrow. "Turn back! None who pass beyond this point return. The woods hold secrets darker than the shadows, and peril awaits those who dare venture further."She darts anxiously through the air, her tiny form surrounded by a faint glow. "I tried to warn them, the little boy and girl who entered, but they wouldn't heed my words. They continued on, drawn by a sweet enchantment that blinds them to the dangers ahead. Please, you must turn back before it's too late!" The pixie's plea hangs in the air, her luminous eyes pleading for understanding. The Märchenweltgrenze, once an enchanting tapestry of mystical wonders, now reveals a foreboding side as the pixie's warning resonates through the ancient trees. The choice to press forward or heed the pixie's words now rests in the hands of the adventurers.*

**The castle**

*As you proceed through the Märchenweltgrenze, guided by the enchanting aroma of sweet confections, the towering trees part to reveal an unexpected sight—a majestic castle made entirely of gingerbread and adorned with spun sugar decorations. The air is infused with the scent of warm spices, inviting yet strangely unsettling.The castle, though remarkable in its confectionary grandeur, seems eerily silent. As you cautiously approach the entrance, there is no sign of the witches that purportedly inhabit this enchanted domain. The grand doors, seemingly crafted from giant gingerbread cookies, creak open with an eerie lack of resistance, revealing the interior of the gingerbread castle.Inside, the air is thick with enchantment, and the architecture defies the laws of nature. Walls made of gingerbread bricks, adorned with candied windows and frosting details, stretch upward in fantastical spirals. The floor beneath your feet feels soft, like a carpet of gingerbread crumbs.The main hall unfolds before you, featuring a grand staircase adorned with marzipan banisters and lollipop handrails. Crystalized sugar chandeliers hang from the ceiling, casting a soft, warm glow that dances upon the sugar-coated walls. Yet, the castle's eerily silent halls echo with a haunting absence, leaving the true nature of this enchanted domain shrouded in mystery.*
>### Stickyness
>In the enchanted gingerbread castle, there are puddles of carmel and honey that are infused with a magical stickiness that lingers,
>
>- **Appearance:** Patches of the floor and furniture, seemingly made of caramel or honey, glisten with an otherworldly stickiness. These areas are strategically placed, making navigation through the castle a delicate dance.
>
>- **Effect on Movement:** When a character enters a sticky floor area, their movement speed is halved, and they must succeed on a Dexterity saving throw (DC determined by the DM) to avoid getting partially stuck.
>
>- **Partial Sticking:** On a failed save, the character's movement speed remains halved until they take an action to free themselves. This can be done with a successful Strength (Athletics) check or a Dexterity (Acrobatics) check.
>
>- **Persistent Stickiness:** Characters who free themselves from the sticky floor carry the residue on their shoes for a duration determined by the DM (e.g., 1 minute, 10 minutes, or until removed). During this time, they leave sticky footprints wherever they go.

### Sweet Ambush in the Foyer

*As you explore the grand foyer of the gingerbread castle, the air becomes tense with an unsettling stillness. Suddenly, the towering doors you entered through slam shut with an echoing thud. The once-decorative elements of the room—giant candied pillars, marzipan statues, and frosting-covered tapestries—begin to shift and stir. Before you can react, dessert-like creatures come to life, and a peculiar honey-like puddle at the center of the foyer beckons your attention.*

#### Dessert Minions:

Emerging from the sugary surroundings, animated desserts take on sinister forms, ready to confront the intruders. A **gelatnous cube** and 3 **gingerbrutes** manifest from the scenery


#### Sticky Honey Puddle:

In the midst of the chaos, a honey-like puddle at the center of the foyer seems to call out to the players. Those who venture into the sticky honey may find themselves facing an additional challenge.

- **Appearance:** The honey puddle appears deceptively benign, its golden surface reflecting the ambient light in a mesmerizing manner.

- **Effect on Movement:** Characters who enter the honey puddle must succeed on a Dexterity saving throw (DC determined by the DM) or experience the sticky mechanic explained above.

- **Distraction:** The animated desserts are drawn to the sweet scent of the honey, potentially providing an opportunity for players to strategically use the environment to their advantage.

### The Sticky Feast

*As you step into the grand dining room off the foyer, you find yourself surrounded by an opulent scene. A grand table, seemingly crafted from delectable caramel, dominates the room. Chairs, adorned with spun sugar cushions, beckon invitingly. However, before you can fully take in the sweet surroundings, a horde of Gingerbread men attack*
As the players enter 4 to 8 **Gingerbrutes** attack from the kitchen

#### Sticky Feast Terrain:

- **Caramel Table and Chairs:**
  - *Appearance:* The grand dining table and chairs appear to be made entirely of caramel. The surface glistens with a tempting sheen.
  - *Effect on Movement:* Characters moving through or around the table and chairs must succeed on a Dexterity saving throw (DC determined by the DM) or experience the sticky mechanic explained above.
  - *Tactical Element:* The sticky caramel terrain can be both an advantage and a hindrance, providing cover and opportunities for creative combat maneuvers.


###  Witches' Kitchen

*As you step into the seemingly ordinary castle kitchen, the aroma of sugary delights fills the air. Rows of pots brimming with golden honey line the shelves, and the room is dominated by a large brick oven in the center. Your attention is drawn to the corner of the kitchen, where a cage made of candy canes holds a frightened, plump boy. He looks up with wide eyes as you enter, a mixture of hope and fear in his gaze.*

#### Enemies:

**Malevolent Witch:**
- *Description:* A wicked figure clad in dark robes, with a twisted staff adorned with crystallized sugar. The witch's eyes gleam with malevolence, and her presence is a harbinger of dark magic.
- *Abilities:* Enchanting Hex (imposing detrimental effects on targets), Confectionary Manipulation (manipulating sweets to create obstacles), Dark Sorcery (unleashing powerful magical attacks).

**Captive Child:**
- *Description:* A frightened little boy held captive in a cage made of candy canes. His innocence contrasts sharply with the malevolent atmosphere of the kitchen.
The witch has a dagger worth 30gp and a pouch filled with common spell components. There is also a *Heward's Handy spice pouch* on one of the shelves
#### Terrain Features:

- **Brick Oven:**
  - *Description:* The brick oven dominates one side of the kitchen, emanating heat and dark enchantment. It seems to be a focal point of the witch's malevolent plans.

- **Candy Cane Cage:**
  - *Description:* A cage made of intertwined candy canes holds the captive little boy. Breaking the cage may release him, but it could also draw the attention and wrath of the Malevolent Witch.

- **Pots of Honey:**
  - *Description:* Several pots filled with honey are scattered around the kitchen. Characters on either side can choose to throw honey on the floor, creating sticky puddles and affecting movement.


### The Gingerbread Library

As the players explore deeper into the gingerbread castle, they stumble upon a magical and whimsical room—the Gingerbread Library. The air is infused with the scent of sweet knowledge, and the shelves are lined with books made of gingerbread and candy wrappers. The room is both a feast for the senses and a treasure trove of enchanted lore.


### Rooms: The Witches' Bedrooms

As the players ascend to the second floor of the gingerbread castle, they discover the personal chambers of the witch sisters. Despite the enchanting confectionary decor, there is an underlying macabre atmosphere in each room, revealing hints of the witches' darker nature.

#### **1. Room of Whispers:**
- *Description:* The air in this room is thick with an otherworldly stillness. Gingerbread walls adorned with licorice vines provide a dark backdrop. A four-poster bed, draped in spider silk and licorice, dominates the room. Marzipan figurines line the shelves, frozen in poses of agony and despair. The centerpiece is a mirror made of spun sugar, reflecting shadows that seem to whisper secrets.

  - **Macabre Element:** The licorice vines on the walls occasionally writhe and whisper forbidden incantations, revealing the dark rituals practiced by the witch who inhabits this room.
  - Silver-threaded Spider Silk Drapes: Exquisite spider silk drapes, intricately woven with silver threads, adding an air of luxury to the room. They are worth 200gp
  

#### **2. Room of Shadows:**
- *Description:* This room is shrouded in an eerie dimness. The furniture, including a bed with a canopy of black licorice, seems to absorb light. The walls are covered in gingerbread cookies shaped like distorted faces, their expressions frozen in silent screams. A cauldron of dark chocolate bubbles in the corner, emitting an unsettling aroma.

  - **Macabre Element:** The gingerbread faces on the walls occasionally shift, contorting into grotesque expressions. These faces are enchanted to capture the fleeting moments of fear and anguish experienced by those who have crossed the witch's path.
  - The wardrobe contains a *Hat of Disguise*
  - A small silver and obsidian hand mirror can be found that is worth 200gp

#### **3. Room of Whirlwinds:**
- *Description:* The atmosphere in this room is charged with energy. Candy cane whirlwinds dance around a floating bed made of cotton candy clouds. A wardrobe made of enchanted licorice twists and contorts. Shelves hold jars of colorful potions and pickled gingerbread creatures. The air is filled with the faint hum of magical currents.

  - **Macabre Element:** The enchanted licorice wardrobe occasionally conjures spectral figures that manifest briefly before dissipating. These apparitions are remnants of the witch's past, embodying moments of darkness and tragedy.
  - On the vanity there is an *Elixir of Health* and a jewlery box containing  Small, crystallized trinkets that radiate faint magical energy. These can be sold to collectors for 200gp.
### Encounter: The Undercroft Guardians

As the players descend into the undercroft of the gingerbread castle, they encounter the Sweettooth Horrors, formidable guardians appointed to protect the secrets hidden below. The air is thick with the scent of molten candy, and sticky honey drips down from above, creating momentary obstacles on the floor.

#### **Setting:**
- **Undercroft Entrance:**
  - *Description:* The undercroft entrance is guarded by two imposing Sweettooth Horrors. The walls are made of dark chocolate, and overhead, a network of enchanted honey drains releases sticky honey intermittently.

- **Sticky Honey Puddles:**
  - *Description:* The honey drains create puddles on the floor. These sticky puddles hinder movement temporarily, creating a dynamic and challenging environment.

#### **Enemies:**
- **Sweettooth Horrors:**
  - *Description:* Towering, humanoid creatures made of molten candy and enchanted sweets. Their eyes gleam with malice, and their mouths drip with gooey confections.
  - *Abilities:* Candy Crush (powerful melee attack), Sticky Grasp (restraining targets with sticky tendrils), Sugar Frenzy (temporary burst of speed and increased damage).

#### **Dynamic Elements:**
1. **Honey Drips:**
   - *Effect:* Honey periodically drips from the enchanted drains, creating sticky puddles on the floor. Characters moving through these areas must make Dexterity saving throws to avoid becoming temporarily stuck.



### Gingerbread Jail Cells

As the players descend into the depths of the gingerbread castle, they stumble upon a heart-wrenching scene—an underground dungeon filled with gingerbread jail cells. Each cell is constructed with candy cane bars, and within them, children huddle, their eyes wide with fear. The sweet aroma of gingerbread mingles with the sorrowful atmosphere, creating a scene that demands compassion and empathy.

#### Setting:

- **Gingerbread Jail Cells:**
  - *Description:* Rows of small cells, each enclosed with candy cane bars. The children inside are a mix of races, all dressed in tattered clothing, their expressions a blend of hope and despair.

- **Dim Light:**
  - *Description:* The dungeon is dimly lit by enchanted lanterns, casting soft hues of warm light that dance against the gingerbread walls.

- **Whispers of Freedom:**
  - *Effect:* As the players approach, they hear faint whispers of gratitude and hope from the imprisoned children. The air is thick with a longing for freedom and safety.

### the Molten Candy Hallway

As the players traverse the gingerbread castle, they enter a seemingly ordinary hallway. The walls are adorned with candy canes, and the air is filled with the sweet scent of confections. However, their journey takes an unexpected turn as both doors at the end of the hallway slam shut, and the floor beneath them begins to retract, revealing a pool of gooey molten hard candy below.

#### Description:

- **Gingerbread Walls:**
  - *Description:* The walls are decorated with candy canes and vibrant gumdrops, creating a visually enticing but deceptive setting.

- **Molten Candy Pool:**
  - *Description:* The floor retracts to unveil a pool of molten hard candy below. The candy is viscous and emits a warm glow, hinting at its dangerously high temperature.

- **Locked Doors:**
  - *Description:* The doors at both ends of the hallway are securely locked. Attempts to force them open prove futile.

#### Puzzle Elements:

1. **Candy Pressure Plates:**
   - *Description:* The players notice that the floor has pressure plates resembling colorful candies. Stepping on these plates triggers the retraction of the floor. Players must find a way to navigate the hallway without activating the plates.

2. **Candy Cane Handrails:**
   - *Description:* Candy cane handrails line the sides of the hallway. Observant players may notice that certain sections of the handrails can be pressed or twisted to reveal hidden switches.

#### Challenge:

1. **Movement Timing:**
   - *Challenge:* Players must carefully time their movements to avoid stepping on the candy pressure plates as they navigate the retracting floor.

2. **Hidden Switches:**
   - *Challenge:* Finding and activating the hidden switches on the candy cane handrails can temporarily halt the floor retraction, giving the players a moment of respite.

#### Consequences:

1. **Falling into the Molten Candy:**
   - *Effect:* Characters who fall into the molten candy suffer fire damage and become stuck, requiring a Strength (Athletics) check to pull themselves free.

2. **Locked Doors:**
   - *Effect:* If players attempt to force open the locked doors, they trigger additional pressure plates, accelerating the floor retraction.

#### Resolution:

1. **Timing and Teamwork:**
   - *Solution:* Players must work together to time their movements, avoiding the candy pressure plates. The discovery and activation of the hidden switches on the candy cane handrails provide crucial pauses in the floor retraction.

2. **Creative Solutions:**
   - *Solution:* Players may also employ creative solutions, such as using spells, tools, or specific abilities to interact with the environment and overcome the challenge.

### Boss Encounter: The Witch Sisters' Throne Room

In the grand throne room of the gingerbread castle, the players come face to face with the formidable Witch Sisters. Seated on candycane thrones that hover above a shallow sea of molten candy, the sisters await the intruders. Floating candy wafers and starburst mints act as precarious platforms amidst the sea, creating a dynamic battlefield for the final confrontation.

#### **Setting:**
- **Throne Room:**
  - *Description:* The throne room is adorned with gingerbread walls and candy decorations. Hovering in the center are the candycane thrones of the Witch Sisters. Molten candy surrounds the thrones, and floating candy wafers and starburst mints act as unstable platforms.

- **Molten Candy Sea:**
  - *Description:* The molten candy sea inflicts burning damage on any character who enters it. Additionally, characters submerged in the sea become sticky, imposing penalties on their movement and actions.

#### **Enemies:**
- **Witch Sister Duo:**
  - *Description:* The Witch Sisters are formidable spellcasters with dark powers. Each sister has unique abilities, and they coordinate attacks to challenge the players.

#### **Dynamic Elements:**
1. **Floating Platforms:**
   - *Effect:* The candy wafers and starburst mints float on the molten candy sea. Players must strategically move across these platforms to engage the Witch Sisters and avoid the burning damage of the molten candy.

#### **Consequences:**
1. **Burning Damage:**
   - *Effect:* Characters who enter the molten candy sea suffer burning damage per round. This ongoing damage adds urgency to the players' decisions.

2. **Sticky Debuff:**
   - *Effect:* Characters submerged in the molten candy become sticky.


### Witch Sister of Evocation

**Witch Sister - Evocation Specialist (CR 8)**

*Medium humanoid (human), chaotic evil*

**Armor Class** 15 (mage armor)  
**Hit Points** 90 (12d8 + 36)  
**Speed** 30 ft.

**STR** 10 (+0)  
**DEX** 14 (+2)  
**CON** 16 (+3)  
**INT** 18 (+4)  
**WIS** 12 (+1)  
**CHA** 14 (+2)

**Saving Throws** Int +7, Wis +4  
**Skills** Arcana +7, Insight +4  
**Senses** Darkvision 60 ft., Passive Perception 11  
**Languages** Common, Abyssal

**Spellcasting:** The Witch Sister is a 9th-level spellcaster. Her spellcasting ability is Intelligence (spell save DC 15, +7 to hit with spell attacks). She has the following wizard spells prepared:

- *Cantrips (at will):* Fire Bolt, Prestidigitation, Mage Hand
- *1st level (4 slots):* Magic Missile, Shield, Burning Hands
- *2nd level (3 slots):* Scorching Ray, Mirror Image
- *3rd level (3 slots):* Fireball, Counterspell
- *4th level (3 slots):* Fire Shield, Dimension Door
- *5th level (1 slot):* Immolation

**Actions**

***Fire Ray.*** Ranged Spell Attack: +7 to hit, range 120 ft., one target. Hit: 21 (6d6) fire damage.

***Flame Wave (Recharge 5-6).*** The Witch Sister unleashes a wave of intense flames in a 15-foot cone. Each creature in that area must make a DC 15 Dexterity saving throw, taking 27 (6d8) fire damage on a failed save, or half as much damage on a successful one.

### Witch Sister of Enchantment

**Witch Sister - Enchantment Specialist (CR 8)**

*Medium humanoid (human), chaotic evil*

**Armor Class** 12  
**Hit Points** 75 (10d8 + 30)  
**Speed** 30 ft.

**STR** 10 (+0)  
**DEX** 12 (+1)  
**CON** 16 (+3)  
**INT** 18 (+4)  
**WIS** 14 (+2)  
**CHA** 14 (+2)

**Saving Throws** Int +7, Wis +5  
**Skills** Arcana +7, Deception +5  
**Senses** Darkvision 60 ft., Passive Perception 12  
**Languages** Common, Infernal

**Spellcasting:** The Witch Sister is a 9th-level spellcaster. Her spellcasting ability is Intelligence (spell save DC 15, +7 to hit with spell attacks). She has the following wizard spells prepared:

- *Cantrips (at will):* Mage Hand, Friends, Minor Illusion
- *1st level (4 slots):* Charm Person, Sleep, Tasha's Hideous Laughter
- *2nd level (3 slots):* Hold Person, Suggestion
- *3rd level (3 slots):* Hypnotic Pattern, Major Image
- *4th level (3 slots):* Charm Monster, Dimension Door
- *5th level (1 slot):* Modify Memory

**Actions**

***Charm Ray.*** Ranged Spell Attack: +7 to hit, range 60 ft., one humanoid target. Hit: The target must succeed on a DC 15 Wisdom saving throw or be charmed for 1 minute. The charmed creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.

***Enchanting Gaze (Recharge 5-6).*** The Witch Sister targets one creature she can see within 30 feet. If the target can see the Witch Sister, it must succeed on a DC 15 Wisdom saving throw or be charmed for 1 minute. The charmed creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.

## Boss scaling guide

### Witch Sister of Evocation

#### CR 6

- **Hit Points:** 65 (8d8 + 24)
- **Spellcasting DC:** 14
- **Fire Ray Damage:** 16 (4d6)

#### CR 7

- **Hit Points:** 75 (10d8 + 30)
- **Spellcasting DC:** 15
- **Fire Ray Damage:** 18 (5d6)

#### CR 8

- **Hit Points:** 85 (12d8 + 36)
- **Spellcasting DC:** 15
- **Fire Ray Damage:** 20 (6d6)

#### CR 9

- **Hit Points:** 95 (14d8 + 42)
- **Spellcasting DC:** 16
- **Fire Ray Damage:** 22 (7d6)

### Witch Sister of Enchantment

#### CR 6

- **Hit Points:** 55 (7d8 + 21)
- **Spellcasting DC:** 14
- **Charm Ray DC:** 14

#### CR 7

- **Hit Points:** 65 (9d8 + 27)
- **Spellcasting DC:** 15
- **Charm Ray DC:** 15

#### CR 8

- **Hit Points:** 75 (11d8 + 33)
- **Spellcasting DC:** 15
- **Charm Ray DC:** 15

#### CR 9

- **Hit Points:** 85 (13d8 + 39)
- **Spellcasting DC:** 16
- **Charm Ray DC:** 16

### Conclusion:

As the final echoes of magical battle subside in the grand throne room of the gingerbread castle, the Witch Sisters defeated, the air begins to shift. The once vibrant confectionary walls lose their luster, and cracks spiderweb across the candycane thrones. The molten candy sea below starts to churn with an otherworldly energy.

The players, having rescued the kidnapped children from the enchanted dungeons, now witness the inevitable fate of the gingerbread castle. With each step they take, leading the children out to safety, the structural integrity of the castle weakens. Fragments of gingerbread and shards of candy fall, creating a melodic rain of sugary debris.

The very magic that sustained the whimsical structure begins to unravel. The castle groans and protests as if acknowledging its impending dissolution. The once enchanting aroma of sweets now carries a hint of sorrow, the echoes of the Witch Sisters' defeat intermingling with the scent of burning sugar.

In the waning moments, the players find themselves at the castle's threshold, the rescued children by their side. The castle, a symbol of both peril and wonder, reaches its final act. It crumbles into a cascade of powdered sugar, leaving no trace of its existence but the sweet memories of a magical encounter.

As the players and children watch, the gust of a departing magical breeze carries away the last remnants of gingerbread dust. The once-threatening castle is no more, and the Märchenweltgrenze, freed from the sinister influence, returns to a sense of calm.

The rescued children, now safe and sound, look up at the adventurers with a mix of gratitude and awe. The Märchenweltgrenze, though forever changed, is on its way to healing, and the memories of the adventure will linger as a testament to the bravery of those who ventured into the sweet and perilous unknown. The Märchenweltgrenze, once again a haven for both fey and settlers, welcomes the dawn of a new era, thanks to the courage and cunning of the adventurers who dared to face the Witch Sisters in their crumbling sugary domain.


The alderman, whose own children were among the rescued, steps forward, his eyes reflecting a mixture of gratitude and heartfelt appreciation. With a solemn nod, he extends a finely crafted purse filled with clinking gold coins, the weight of their reward echoing the value placed on the safe return of the kidnapped children.

*"Brave adventurers," the alderman begins, his voice resonating with sincerity, "we owe you more than words can express. You've not only saved our children but also brought an end to the dark enchantment that plagued our town. Please accept this token of our gratitude, a humble reward for your valor."*

He presents the player characrters with a purse containing 200gp for the players efforts.